#ifndef _TENSORS_COALESCED_CUH_
#define _TENSORS_COALESCED_CUH_

#include <cuda.h>
#include <cuda_runtime.h>

namespace desmo {
namespace cucoalesced {

#define BLOCK_SIZE 256

// Macros for accessing coalesced memory
#define VERTEX_METRIC(x, idx) x[idx]
#define COALESCED_VERTEX_ADJACENT(x, vidx, aidx, vsize) \
    x[(1 + aidx)*vsize + vidx]
#define COALESCED_VERTEX(x, vidx) x[vidx]

void cu_computeVertexMetrics(long size, int divisoes,
                             long4 *const vertexMetrics, int gpu = 1);

void cu_computeTensors(long size, const char2 *const vertexesValences,
                       float3 *const coalescedVertexes,
                       float3 *const coalescedVertexesNormals,
                       const long4 *const vertexesMetrics,
                       float3 *const pdir1s, float3 *const pdir2s,
                       float4 *const metricTensors,
                       float4 *const contravMetricTensors,
                       float4 *const curvTensors, float2 *const ks,
                       int gpu = 1);

} // namespace cucoalesced
} // namespace desmo

#endif // _TENSORS_COALESCED_CUH_
