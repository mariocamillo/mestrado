#ifndef _CUMESH_H_
#define _CUMESH_H_

#include <string>
#include <exception>
#include <cuda_runtime.h>
#include "mesh.h"

namespace desmo {

class CuMesh
{
public:
    /**
      * Types of pre-defined meshes available for loading
      */
    enum MESH_TYPE
    {
        SADDLE = 0,      //!< {u, v, u^2 - v^2}
        COSINUS,         //!< {u, v, Cos[2u]*Cos[2u]}
        SINUS,           //!< {u, v, Sin[2u]*Cos[2v]}
        GAUSS,
        // Jump the 4th. Just to maintain consistency with deforma.cpp
        GUTTER = 5,      //!< {u, v, u^2}
        GUTTER2,         //!< {u, v, v^2}
        CYLINDER,
        STRETCHED_PLANE,
        SHRINKING_PLANE,
        SHEARED_SADDLE,  //!< {u + (t*v/2), v, t*(u^2 - v^2)}
    };

    class CuMeshException : public std::exception {
        public:
            CuMeshException(const char *what) : m_what(what) {}
            virtual const char *what() const throw() { return m_what; }
        private:
            const char *m_what;
    };

    /// Default constructor. All pointers to NULL and empty variables.
    CuMesh();

    /// Destructor. Free all memory.
    virtual ~CuMesh();

    /**
     * Init from a halfmesh.
     * @param [in] mesh Pointer to a halfmesh structure (already filled).
     *                  Internal halfmesh structure is Point3D. See deforma.h
     *
     * @return The elapsed time, in micro-seconds.
     */
    virtual double init(Mesh *const mesh);

    /**
     * Init from a .obj file.
     * @param [in] objPath A string containing the path to the .obj
     *                     with the mesh data.
     *
     * @return The elapsed time, in micro-seconds.
     */
    virtual double init(const std::string &objPath) { return 0.; }
    
    /**
     * Init from a pre-defined mesh type. Width, height and valence are being
     * ignored for now (fixed in divisoes x divisoes with valence of 8). For
     * divisoes, see Deforma.h
     * @param [in] type    The type of the mesh to be generated.
     * @param [in] width   Width of the mesh to be generated.
     * @param [in] height  Height of the mesh to be generated.
     * @param [in] valence The valence of the mesh.
     *
     * @return The elapsed time, in micro-seconds.
     */
    virtual double init(MESH_TYPE type, int width, int height, int valence) { return 0.; }

    /**
     * Allocate GPU resources and copy the data to GPU. init MUST be called
     * before this method
     */
    virtual double initGPU();
    
    /**
     * Compute the mesh normals (for faces and vertexes).
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeNormals(bool gpu);

    /**
     * Returns the total allocated memory for the class.
     *
     * @return The total memory allocated, in bytes
     */
    unsigned long long getTotalMemory() const;

    // Getters
    float4 const * getVertexes() const { return mVertexes; }
    float4 const * getVertexesGPU() const { return mVertexesGPU; }
    float4 const * getVertexesNormals() const { return mVertexesNormals; }
    float4 const * getVertexesNormalsGPU() const { return mVertexesNormalsGPU; }
    float4 const * getFacesNormals() const { return mFacesNormals; }
    float4 const * getFacesNormalsGPU() const { return mFacesNormalsGPU; }
    long2 const * getAdjacency() const { return mAdjacency; }
    long2 const * getAdjacencyGPU() const { return mAdjacencyGPU; }
    long int getTotalVertexes() const { return mTotalVertexes; }
    long int getTotalFaces() const { return mTotalFaces; }
    int getVertexValence() const { return mVertexValence; }
    int getFaceValence() const { return mFaceValence; }

protected:
    /**
     * Load an obj file to a halfmesh object.
     * @param [in] objPath A string containing the path to the .obj
     *                     with the mesh data.
     * @return A pointer to the halfmesh object filled with the obj mesh data.
     */
    Mesh *loadObj(std::string &objPath) {}

    /**
     * Extract the needed informations from the halfmesh object.
     * @param [in] halfmesh Pointer to the halfmesh object.
     */
    virtual void extractInfoFromHalfMesh(Mesh *const halfmesh);

    /**
     * Fill a halfmesh object with the data for a given mesh type.
     * @param [in,out] halfmesh Pointer to the halfmesh object.
     */
    virtual void loadHalfMeshByType(Mesh *const halfmesh, MESH_TYPE type,
                                    int width, int height) {}

    /// Mesh's vertexes position in CPU memory
    float4 *mVertexes;

    /// Mesh's vertexes position in GPU memory
    float4 *mVertexesGPU;
    
    /// Normals to each vertex in CPU memory
    float4 *mVertexesNormals;

    /// Normals to each vertex in GPU memory
    float4 *mVertexesNormalsGPU;
    
    /// Normals to each face in CPU memory
    float4 *mFacesNormals;

    /// Normals to each face in GPU memory
    float4 *mFacesNormalsGPU;

    /// Vertexes to each face
    long int *mFaces;
    
    /// Vertexes to each face in GPU memory
    long int *mFacesGPU;

    /// Neighbours to each vertex (x is vertex, y is face)
    long2 *mAdjacency;
    
    /// Neighbours to each vertex in GPU memory (x is vertex, y is face)
    long2 *mAdjacencyGPU;

    /// Total number of vertexes in the mesh
    long int mTotalVertexes;

    /// Total number of faces in the mesh
    long int mTotalFaces;

    /// Vertex valence (number of neighbours for each vertex) of the mesh
    int mVertexValence;

    /// Face valence (number of vertexes for each face) of the mesh
    int mFaceValence;
};

} //namespace desmo
#endif // _CUMESH_H_
