#ifndef _CUMESHCCL_H_
#define _CUMESHCCL_H_

#include <string>
#include <exception>
#include <cuda_runtime.h>
#include "cumesh.hpp"

namespace desmo {

class CuMeshCCL : public CuMesh
{
public:
    /// Default constructor. All pointers to NULL and empty variables.
    CuMeshCCL(char layout) : CuMesh(), m_layout(layout) {}

    /// Destructor. Free all memory.
    virtual ~CuMeshCCL() {}

protected:
    /**
     * Extract the needed informations from the halfmesh object.
     * @param [in] halfmesh Pointer to the halfmesh object.
     */
    void extractInfoFromHalfMesh(Mesh *const halfmesh);

    char m_layout;
};

} //namespace desmo
#endif // _CUMESHCCL_H_
