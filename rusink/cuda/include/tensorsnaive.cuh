#ifndef _TENSORS_NAIVE_CUH_
#define _TENSORS_NAIVE_CUH_

#include <cuda.h>
#include <cuda_runtime.h>

namespace desmo {
namespace cunaive {

#define BLOCK_SIZE 192

void cu_computeVertexMetrics(long size, int divisoes,
                             long2 *const vertexMetrics, int gpu = 1);

void cu_computeTensors(long size, int valence, const float4 *const vertexes,
                       const float4 *const vertexesNormals,
                       const long2 *const adjacency,
                       const long2 *const vertexesMetrics,
                       float3 *const pdir1s, float3 *const pdir2s,
                       float4 *const metricTensors,
                       float4 *const contravMetricTensors,
                       float4 *const curvTensors, float2 *const ks,
                       int gpu = 1);

} // namespace cunaive
} // namespace desmo

#endif // _TENSORS_NAIVE_CUH_
