#include <exception>
#include <vector>
#include <cuda_runtime.h>
#include <CudaHelper/helper_cuda.h>
#include "mesh.h"

namespace desmo {
namespace utils {

#define FREE_GPU_RESOURCE(x, f)        \
    if (x != NULL) {                   \
        checkCudaErrors(f(x));         \
        x = NULL;                      \
    }

extern int WARP_SIZE;

class NullMeshException : public std::exception {
public:
    NullMeshException() {}
    virtual const char *what() const throw() { return "Mesh pointer is NULL!"; }
};

inline long int getMaxVertexValence(const vector<VertexID> &vertexes,
                               Mesh *const halfmesh)
{
    size_t max = 0;
    for (vector<VertexID>::const_iterator it = vertexes.begin();
         it != vertexes.end(); ++it)
    {
        vector<VertexID> neighbours;
        halfmesh->getVVertices(*it, &neighbours);
        if (neighbours.size() > max) max = neighbours.size();
    }

    return max;
}

inline long int getMaxFaceValence(const vector<FaceID> &faces, Mesh *const halfmesh)
{
    size_t max = 0;
    for (vector<FaceID>::const_iterator it = faces.begin();
         it != faces.end(); ++it)
    {
        vector<VertexID> faceVertexes;
        halfmesh->getFVertices(*it, &faceVertexes);
        if (faceVertexes.size() > max) max = faceVertexes.size();
    }

    return max;
}

inline void copyPoint3D2Float4(const Point3D &src, float4 &dst)
{
    dst.x = static_cast<float>(src.x);
    dst.y = static_cast<float>(src.y);
    dst.z = static_cast<float>(src.z);
}

inline long int findElemIdx(const float4 *const elems, long int size,
                            long int id)
{
    for (long int i = 0; i < size; ++i)
    {
        if (elems[i].w == (float)id) return i;
    }

    return -1;
}

} // namespace utils
} // namespace desmo
