#ifndef _MESH_CUH_
#define _MESH_CUH_

#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 256

void cu_computeFaceNormals(long size,
                           int valence,
                           const long *const faces,
                           const float4 *const vertexes,
                           float4 *const facesNormals,
                           int gpu = 1);

void cu_computeVertexNormals(long size,
                             int valence,
                             const long2 *const adjacency,
                             const float4 *const facesNormals,
                             float4 *const vertexesNormals,
                             int gpu = 1);

#endif // _MESH_CUH_