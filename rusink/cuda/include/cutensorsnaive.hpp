#ifndef _CU_TENSORS_NAIVE_H_
#define _CU_TENSORS_NAIVE_H_

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cumesh.hpp"
#include "cutensors.hpp"

namespace desmo {

class CuTensorsNaive : public CuTensors
{
public:
    /// Constructor
    CuTensorsNaive();

    /// Destructor
    virtual ~CuTensorsNaive();

    /**
     * Allocate GPU resources and copy the data to GPU. init MUST be called
     * before this method
     */
    double initGPU();

    /**
     * Compute the vertex metrics for the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeVertexMetric(bool gpu);

    /**
     * Compute the curvature and metric tensors for each vertex in the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeTensors(bool gpu);

    /**
     * Returns the total allocated memory for the class.
     *
     * @return The total memory allocated, in bytes
     */
    unsigned long long getTotalMemory() const;

    // Getters
    long2 const * getVertexesMetrics() const { return mVertexesMetrics; }
    long2 const * getVertexesMetricsGPU() const { return mVertexesMetricsGPU; }

protected:
    /**
     * Initialize the object.
     */
    void init(CuMesh *mesh);

    /**
     * Release resources
     */
    void freeResources();
    
    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (CPU memory)
    long2 *mVertexesMetrics;

    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (GPU memory)
    long2 *mVertexesMetricsGPU;
};

} // namespace desmo
#endif // _CU_TENSORS_NAIVE_H_