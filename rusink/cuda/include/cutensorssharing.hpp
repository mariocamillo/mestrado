#ifndef _CU_TENSORS_SHARING_H_
#define _CU_TENSORS_SHARING_H_

#include <vector>
#include <set>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cumesh.hpp"
#include "cutensors.hpp"

namespace desmo {

class CuTensorsSharing : public CuTensors
{
public:

    class WrongPosException : public std::exception
    {
        public:
            WrongPosException() : std::exception() {}
            const char *what() const throw() { return "Unexpected shared position";}
    };

    /// Constructor
    CuTensorsSharing();

    /// Destructor
    virtual ~CuTensorsSharing();

    /**
     * Allocate GPU resources and copy the data to GPU. init MUST be called
     * before this method
     */
    virtual double initGPU();

    /**
     * Compute the vertex metrics for the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeVertexMetric(bool gpu);

    /**
     * Compute the curvature and metric tensors for each vertex in the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeTensors(bool gpu);

    /**
     * Returns the total allocated memory for the class.
     *
     * @return The total memory allocated, in bytes
     */
    unsigned long long getTotalMemory() const;

    size_t getSharedMemUsage() const { return mSharedMemSize; }

private:
    struct DataElem
    {
        long id;
        long sharedPos;

        DataElem() : id(0), sharedPos(0) {}

        DataElem(long _id, long pos) : id(_id), sharedPos(pos) {}

        bool operator==(const DataElem &other) const { return this->id == other.id; }
        bool operator<(const DataElem &other) const { return this->id < other.id; }
    };
    typedef std::set<DataElem> ElementsSet;
    /**
     * Initialize the object.
     */
    void init(CuMesh *mesh);

    /**
     * Release resources
     */
    void freeResources();

    /// Number of thread blocks that will be issued.
    long mNumBlocks;

    /// The maximum size of shared memory to be used.
    size_t mSharedMemSize;

    /// Size of duplicate data for each thread block.
    std::vector<long> mBlockSizes;

    /// Size of duplicate data for each thread block (GPU memory).
    long *mBlockSizesGPU;

    /// Position in the duplicate data array for each block.
    std::vector<long> mBlockPositions;

    /// Position in the duplicate data array for each block (GPU memory).
    long *mBlockPositionsGPU;

    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (CPU memory)
    long4 *mVertexesMetrics;

    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (GPU memory)
    long4 *mVertexesMetricsGPU;
    
    /// Position of the vertexes needed by each vertex (itself and its neighbours) (CPU memory)
    std::vector<long> mVertexPositions;
    
    /// Position of the vertexes needed by each vertex (itself and its neighbours) (GPU memory)
    long *mVertexPositionsGPU;

    /// Adjacent vertexes in a sharing way (CPU memory)
    std::vector<float3> mSharedVertexes;

    /// Adjacent vertexes in a sharing way (GPU memory)
    float3 *mSharedVertexesGPU;
    
    /// Adjacent vertexes in a sharing way (CPU memory)
    std::vector<float3> mSharedVertexesNormals;

    /// Adjacent vertexes in a sharing way (GPU memory)
    float3 *mSharedVertexesNormalsGPU;
};

} // namespace desmo
#endif // _CU_TENSORS_SHARING_H_
