#ifndef _CU_TENSORS_H_
#define _CU_TENSORS_H_

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cumesh.hpp"

namespace desmo {

class CuTensors
{
public:
    /// Constructor
    CuTensors();

    /// Destructor
    virtual ~CuTensors();

    /**
     * Allocate GPU resources and copy the data to GPU. init MUST be called
     * before this method
     */
    virtual double initGPU();

    /**
     * Compute the vertex metrics for the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    virtual double computeVertexMetric(bool gpu) = 0;

    /**
     * Compute the curvature and metric tensors for each vertex in the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    virtual double computeTensors(bool gpu) = 0;

    /**
     * Returns the total allocated memory for the class.
     *
     * @return The total memory allocated, in bytes
     */
    virtual unsigned long long getTotalMemory() const;
    
    virtual size_t getSharedMemUsage() const { return 0; }

    // Getters
    CuMesh * getMesh() {return mMesh; }
    float4 const * getMetricTensors() const { return mMetricTensors; }
    float4 const * getCurvTensors() const { return mCurvTensors; }
    float3 const * getPDir1s() const { return mPDir1s; }
    float3 const * getPDir2s() const { return mPDir2s; }
    float2 const * getKs() const { return mKs; }
    float4 const * getMetricTensorsGPU() const { return mMetricTensorsGPU; }
    float4 const * getCurvTensorsGPU() const { return mCurvTensorsGPU; }
    float3 const * getPDir1sGPU() const { return mPDir1sGPU; }
    float3 const * getPDir2sGPU() const { return mPDir2sGPU; }
    float2 const * getKsGPU() const { return mKsGPU; }

    // Setters
    double setMesh(CuMesh *mesh);

protected:
    /**
     * Initialize the object.
     */
    virtual void init(CuMesh *mesh);

    /**
     * Release resources
     */
    void freeResources();

    /// The surface mesh
    CuMesh *mMesh;
    
    /// The metric tensor for each vertex in the mesh (CPU memory)
    float4 *mMetricTensors;

    /// The metric tensor for each vertex in the mesh (GPU memory)
    float4 *mMetricTensorsGPU;
    
    /// The contra-variant metric tensor for each vertex in the mesh (CPU memory)
    float4 *mContravMetricTensors;

    /// The contra-variant metric tensor for each vertex in the mesh (GPU memory)
    float4 *mContravMetricTensorsGPU;
    
    /// The curvature tensor for each vertex in the mesh (CPU memory)
    float4 *mCurvTensors;

    /// The curvature tensor for each vertex in the mesh (GPU memory)
    float4 *mCurvTensorsGPU;
    
    /// The principal directions 1 for each vertex in the mesh (CPU memory)
    float3 *mPDir1s;
    
    /// The principal directions 1 for each vertex in the mesh (GPU memory)
    float3 *mPDir1sGPU;
    
    /// The principal directions 2 for each vertex in the mesh (CPU memory)
    float3 *mPDir2s;

    /// The principal directions 2 for each vertex in the mesh (GPU memory)
    float3 *mPDir2sGPU;
    
    /// Gaussian and Median curvatures (CPU memory)
    float2 *mKs;

    /// Gaussian and Median curvatures (GPU memory)
    float2 *mKsGPU;
};

} // namespace desmo
#endif // _CU_TENSORS_H_
