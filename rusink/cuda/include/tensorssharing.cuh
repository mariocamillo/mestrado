#ifndef _TENSORS_SHARING_CUH_
#define _TENSORS_SHARING_CUH_

#include <cuda.h>
#include <cuda_runtime.h>

namespace desmo {
namespace cusharing {

extern int BLOCK_SIZE;

// Macros for accessing sharing memory
// TODO: make macro to access face adjacent (no plus one)
#define VERTEX_METRIC(x, idx) x[idx]
#define SHARING_VERTEX_ADJACENT(x, vidx, aidx, vsize) \
    x[(1 + aidx)*vsize + vidx]
#define SHARING_VERTEX(x, vidx) x[vidx]

void cu_computeVertexMetrics(long size, int divisoes,
                             long4 *const vertexMetrics, int gpu = 1);

void cu_computeTensors(long size, const size_t sharedMemSize, int valence,
                       const long *const blockSizes,
                       const long *const blockPositions,
                       const long *const vertexPositions,
                       float3 *const sharedVertexes,
                       float3 *const sharedVertexesNormals,
                       const long4 *const vertexesMetrics,
                       float3 *const pdir1s, float3 *const pdir2s,
                       float4 *const metricTensors,
                       float4 *const contravMetricTensors,
                       float4 *const curvTensors, float2 *const ks,
                       int numBlocks, int gpu = 1);

} // namespace cusharing
} // namespace desmo

#endif // _TENSORS_SHARING_CUH_
