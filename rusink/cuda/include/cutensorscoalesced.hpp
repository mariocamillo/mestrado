#ifndef _CU_TENSORS_COALESCED_H_
#define _CU_TENSORS_COALESCED_H_

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cumesh.hpp"
#include "cutensors.hpp"

namespace desmo {

class CuTensorsCoalesced : public CuTensors
{
public:
    /// Constructor
    CuTensorsCoalesced();

    /// Destructor
    virtual ~CuTensorsCoalesced();

    /**
     * Allocate GPU resources and copy the data to GPU. init MUST be called
     * before this method
     */
    virtual double initGPU();

    /**
     * Compute the vertex metrics for the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeVertexMetric(bool gpu);

    /**
     * Compute the curvature and metric tensors for each vertex in the mesh.
     *
     * @param [in] gpu true for computation made on GPU, false otherwise.
     *
     * @return The elapsed time, in micro-seconds.
     */
    double computeTensors(bool gpu);

    /**
     * Returns the total allocated memory for the class.
     *
     * @return The total memory allocated, in bytes
     */
    unsigned long long getTotalMemory() const;

private:
    /**
     * Initialize the object.
     */
    void init(CuMesh *mesh);

    /**
     * Release resources
     */
    void freeResources();
    
    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (CPU memory)
    long4 *mVertexesMetrics;

    /// Adjacency list for next and previous vertexes in X and Y directions.
    /// It is actually a helper data to estimate bases (GPU memory)
    long4 *mVertexesMetricsGPU;
    
    /// Per vertex valence (CPU memory)
    char2 *mVertexesValences;
    
    /// Per vertex valence (GPU memory)
    char2 *mVertexesValencesGPU;

    /// Adjacent vertexes in a coalesced way (CPU memory)
    float3 *mCoalescedVertexes;

    /// Adjacent vertexes in a coalesced way (GPU memory)
    float3 *mCoalescedVertexesGPU;
    
    /// Adjacent vertexes' normals in a coalesced way (CPU memory)
    float3 *mCoalescedVertexesNormals;

    /// Adjacent vertexes' normals in a coalesced way (GPU memory)
    float3 *mCoalescedVertexesNormalsGPU;
};

} // namespace desmo
#endif // _CU_TENSORS_COALESCED_H_
