#include "cutensorsnaive.hpp"
#include "Timer.h"
#include "utils.h"
#include "cuutils.hpp"
#include "tensorsnaive.cuh"
#include "deforma.h" // apenas para ter o divisoes

using namespace desmo::utils;

namespace desmo {

CuTensorsNaive::CuTensorsNaive() : CuTensors()
{
    mVertexesMetrics = NULL;
    mVertexesMetricsGPU = NULL;
}

CuTensorsNaive::~CuTensorsNaive()
{
    freeResources();
}

void CuTensorsNaive::freeResources()
{
    // Free parent resources
    CuTensors::freeResources();

    // Free CPU resources
    FREE_CPU_RESOURCE(mVertexesMetrics, delete []);

    // Free GPU resources
    FREE_GPU_RESOURCE(mVertexesMetricsGPU, cudaFree);
}

void CuTensorsNaive::init(CuMesh *mesh)
{
    // Free old resources
    freeResources();

    // Call parent init
    CuTensors::init(mesh);

    // Allocate CPU resources
    long numVertexes = mMesh->getTotalVertexes();
    mVertexesMetrics = new long2[numVertexes*2];
}

double CuTensorsNaive::initGPU()
{
    START_FUNCTION_TIMER();

    // Call parent GPU init
    CuTensors::initGPU();

    // Allocate GPU resources
    long numVertexes = mMesh->getTotalVertexes();
    checkCudaErrors(cudaMalloc(&mVertexesMetricsGPU,
                               numVertexes*2*sizeof(long2)));

    RETURN_FUNCTION_TIMER();
}

double CuTensorsNaive::computeVertexMetric(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    if (gpu)
    {
        cunaive::cu_computeVertexMetrics(mMesh->getTotalVertexes(), divisoes,
                                         mVertexesMetricsGPU, 1);
    }
    else
    {
        cunaive::cu_computeVertexMetrics(mMesh->getTotalVertexes(), divisoes,
                                         mVertexesMetrics, 0);
    }

    RETURN_FUNCTION_TIMER();
}

double CuTensorsNaive::computeTensors(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    if (gpu)
    {
        cunaive::cu_computeTensors(mMesh->getTotalVertexes(),
                                   mMesh->getVertexValence(),
                                   mMesh->getVertexesGPU(),
                                   mMesh->getVertexesNormalsGPU(),
                                   mMesh->getAdjacencyGPU(),
                                   mVertexesMetricsGPU, mPDir1sGPU, mPDir2sGPU,
                                   mMetricTensorsGPU, mContravMetricTensorsGPU,
                                   mCurvTensorsGPU, mKsGPU, 1);
    }
    else
    {
        cunaive::cu_computeTensors(mMesh->getTotalVertexes(),
                                   mMesh->getVertexValence(),
                                   mMesh->getVertexes(),
                                   mMesh->getVertexesNormals(),
                                   mMesh->getAdjacency(),
                                   mVertexesMetrics, mPDir1s, mPDir2s,
                                   mMetricTensors,
                                   mContravMetricTensors, mCurvTensors, mKs,
                                   0);
    }

    RETURN_FUNCTION_TIMER();
}

unsigned long long CuTensorsNaive::getTotalMemory() const
{
    if (mMesh == NULL)
        return 0;
    
    long numVertexes = mMesh->getTotalVertexes();
    unsigned long long total = CuTensors::getTotalMemory();
    total += numVertexes*2*sizeof(long2);      // vertexes metrics

    return total;
}

};
