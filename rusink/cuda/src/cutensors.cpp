#include "cutensors.hpp"
#include "Timer.h"
#include "utils.h"
#include "cuutils.hpp"
#include "deforma.h" // apenas para ter o divisoes

using namespace desmo::utils;

namespace desmo {

CuTensors::CuTensors()
{
    mMesh = NULL;
    mMetricTensors = NULL;
    mMetricTensorsGPU = NULL;
    mContravMetricTensors = NULL;
    mContravMetricTensorsGPU = NULL;
    mCurvTensors = NULL;
    mCurvTensorsGPU = NULL;
    mPDir1s = NULL;
    mPDir1sGPU = NULL;
    mPDir2s = NULL;
    mPDir2sGPU = NULL;
    mKs = NULL;
    mKsGPU = NULL;
}

CuTensors::~CuTensors()
{
    freeResources();
}

void CuTensors::freeResources()
{
    // Free CPU resources
    FREE_CPU_RESOURCE(mMesh, delete);
    FREE_CPU_RESOURCE(mMetricTensors, delete []);
    FREE_CPU_RESOURCE(mContravMetricTensors, delete []);
    FREE_CPU_RESOURCE(mCurvTensors, delete []);
    FREE_CPU_RESOURCE(mPDir1s, delete []);
    FREE_CPU_RESOURCE(mPDir2s, delete []);
    FREE_CPU_RESOURCE(mKs, delete []);

    // Free GPU resources
    FREE_CPU_RESOURCE(mMetricTensorsGPU, cudaFree);
    FREE_CPU_RESOURCE(mContravMetricTensorsGPU, cudaFree);
    FREE_CPU_RESOURCE(mCurvTensorsGPU, cudaFree);
    FREE_CPU_RESOURCE(mPDir1sGPU, cudaFree);
    FREE_CPU_RESOURCE(mPDir2sGPU, cudaFree);
    FREE_CPU_RESOURCE(mKsGPU, cudaFree);
}

double CuTensors::setMesh(CuMesh *mesh)
{
    START_FUNCTION_TIMER();
    init(mesh);
    RETURN_FUNCTION_TIMER();
}

void CuTensors::init(CuMesh *mesh)
{
    // Free old resources
    freeResources();

    mMesh = mesh;
    if (mMesh == NULL)
        throw NullMeshException();

    // Allocate new resources

    // CPU resources
    long numVertexes = mMesh->getTotalVertexes();
    mMetricTensors = new float4[numVertexes];
    mContravMetricTensors = new float4[numVertexes];
    mCurvTensors = new float4[numVertexes];
    mPDir1s = new float3[numVertexes];
    mPDir2s = new float3[numVertexes];
    mKs = new float2[numVertexes];
}

double CuTensors::initGPU()
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    // GPU resources
    long numVertexes = mMesh->getTotalVertexes();
    checkCudaErrors(cudaMalloc(&mMetricTensorsGPU,
                               numVertexes*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mContravMetricTensorsGPU,
                               numVertexes*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mCurvTensorsGPU, numVertexes*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mPDir1sGPU, numVertexes*sizeof(float3)));
    checkCudaErrors(cudaMalloc(&mPDir2sGPU, numVertexes*sizeof(float3)));
    checkCudaErrors(cudaMalloc(&mKsGPU, numVertexes*sizeof(float2)));

    RETURN_FUNCTION_TIMER();
}

unsigned long long CuTensors::getTotalMemory() const
{
    if (mMesh == NULL)
        return 0;

    long numVertexes = mMesh->getTotalVertexes();
    unsigned long long total = 0;
    total += 3*numVertexes*sizeof(float4); // metric, contrav metric, curv
    total += 2*numVertexes*sizeof(float3); // pdir1, pdir2
    total += numVertexes*sizeof(float2);   // Ks

    return total;
}

}
