#include "cumesh.hpp"
#include <vector>
#include "deforma.h"
#include "utils.h"
#include "Timer.h"
#include "cuutils.hpp"
#include "mesh.cuh"

namespace desmo {

using namespace utils;

CuMesh::CuMesh()
{
    mAdjacency = NULL;
    mAdjacencyGPU = NULL;
    mVertexesNormals = NULL;
    mFacesNormals = NULL;
    mFacesNormalsGPU = NULL;
    mVertexesNormalsGPU = NULL;
    mFaces = NULL;
    mFacesGPU = NULL;
    mVertexes = NULL;
    mVertexesGPU = NULL;
    mVertexValence = 0;
    mFaceValence = 0;
    mTotalVertexes = 0;
    mTotalFaces = 0;
}

CuMesh::~CuMesh()
{
    // Free CPU memory
    FREE_CPU_RESOURCE(mAdjacency, delete []);
    FREE_CPU_RESOURCE(mFacesNormals, delete []);
    FREE_CPU_RESOURCE(mVertexesNormals, delete []);
    FREE_CPU_RESOURCE(mFaces, delete []);
    FREE_CPU_RESOURCE(mVertexes, delete []);

    // Free GPU memory
    FREE_GPU_RESOURCE(mAdjacencyGPU, cudaFree);
    FREE_GPU_RESOURCE(mFacesNormalsGPU, cudaFree);
    FREE_GPU_RESOURCE(mVertexesNormalsGPU, cudaFree);
    FREE_GPU_RESOURCE(mFacesGPU, cudaFree);
    FREE_GPU_RESOURCE(mVertexesGPU, cudaFree);
}

double CuMesh::init(Mesh *const halfmesh)
{
    if (halfmesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    // CPU resources
    extractInfoFromHalfMesh(halfmesh);

    RETURN_FUNCTION_TIMER();
}

double CuMesh::initGPU()
{
    if (mTotalVertexes == 0 || mTotalFaces == 0)
        throw CuMeshException("Not correctly initialized. Call init() first!");

    START_FUNCTION_TIMER();

    // Allocate GPU resources
    checkCudaErrors(cudaMalloc(&mVertexesGPU, mTotalVertexes*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mVertexesNormalsGPU,
                               mTotalVertexes*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mFacesNormalsGPU, mTotalFaces*sizeof(float4)));
    checkCudaErrors(cudaMalloc(&mAdjacencyGPU,
                               mTotalVertexes*mVertexValence*sizeof(long2)));
    checkCudaErrors(cudaMalloc(&mFacesGPU,
                               mTotalFaces*mFaceValence*sizeof(long)));

    // Copy to GPU memory
    checkCudaErrors(cudaMemcpy(mVertexesGPU, mVertexes,
                               mTotalVertexes*sizeof(float4),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mVertexesNormalsGPU, mVertexesNormals,
                               mTotalVertexes*sizeof(float4),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mFacesNormalsGPU, mFacesNormals,
                               mTotalFaces*sizeof(float4),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mAdjacencyGPU, mAdjacency,
                               mTotalVertexes*mVertexValence*sizeof(long2),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mFacesGPU, mFaces,
                               mTotalFaces*mFaceValence*sizeof(long),
                               cudaMemcpyHostToDevice));

    RETURN_FUNCTION_TIMER();
}

void CuMesh::extractInfoFromHalfMesh(Mesh *const halfmesh)
{
    START_FUNCTION_TIMER();

    vector<VertexID> vertexes;
    halfmesh->getAllVertices(&vertexes);
    vector<FaceID> faces;
    halfmesh->getAllFaces(&faces);

    // Allocate CPU resources
    mVertexValence = getMaxVertexValence(vertexes, halfmesh);
    mFaceValence = getMaxFaceValence(faces, halfmesh);
    mTotalVertexes = (long)vertexes.size();
    mTotalFaces = (long)faces.size();
    mVertexes = new float4[mTotalVertexes];
    mVertexesNormals = new float4[mTotalVertexes];
    mFacesNormals = new float4[mTotalFaces];
    mAdjacency = new long2[mTotalVertexes*mVertexValence];
    mFaces = new long int[mTotalFaces*mFaceValence];

    // Extract vertexes info
    int count = 0;
    for (vector<VertexID>::iterator vertex = vertexes.begin();
         vertex != vertexes.end(); ++vertex, ++count)
    {
        //cout << "mVertexes[" << count << "]=" << *vertex << endl;
        Point3D *vertexP3D = (Point3D *)halfmesh->getVGeomPtr(*vertex);
        copyPoint3D2Float4(*vertexP3D, mVertexes[count]);
        mVertexes[count].w = *vertex; // vertex ID for neighbourh extraction
    }

    // Extract face info
    int fcount = 0;
    for (vector<FaceID>::iterator face = faces.begin();
         face != faces.end(); ++face, ++fcount)
    {
        //cout << "mFaces[" << fcount << "]=" << *face << endl;
        mFacesNormals[fcount] = float4();
        mFacesNormals[fcount].w = *face; // face ID for neighbourh extraction

        // Get vertexes adjacent to the face
        vector<VertexID> vertexes;
        halfmesh->getFVertices(*face, &vertexes);
        int vcount = 0;
        for (vector<VertexID>::iterator vertex = vertexes.begin();
             vertex != vertexes.end(); ++vertex, ++vcount)
        {
            long int idx = findElemIdx(mVertexes, mTotalVertexes, *vertex);
            if (idx == -1) throw CuMeshException("Couldn't find vertex idx");
            else mFaces[vcount*mTotalFaces + fcount] = idx;
        }
    }
    
    // Extract vertex adjancency info
    int vcount = 0;
    for (vector<VertexID>::iterator vertex = vertexes.begin();
         vertex != vertexes.end(); ++vertex, ++vcount)
    {
        // Get adjacent vertexes and faces
        vector<VertexID> neighbours;
        halfmesh->getVVertices(*vertex, &neighbours);
        vector<FaceID> faces;
        halfmesh->getVFaces(*vertex, &faces);

        // Fill the adjacency list
        for (int acount = 0; acount< mVertexValence; ++acount)
        {
            long int adjIdx = acount*mTotalVertexes + vcount;
            // The vertex has the acount-th neighbour
            if (acount < neighbours.size())
            {
                long int idx = findElemIdx(mVertexes, mTotalVertexes,
                                           neighbours[acount]);
                if (idx == -1) throw CuMeshException("Couldn't find neighbour idx");
                else mAdjacency[adjIdx].x = idx;
            }
            else
            {
                mAdjacency[adjIdx].x = -1;
            }

            // The vertex has the acount-th adjacent face
            if (acount < faces.size())
            {
                long int idx = findElemIdx(mFacesNormals, mTotalFaces,
                                           faces[acount]);
                if (idx == -1) throw CuMeshException("Couldn't find find idx");
                else mAdjacency[adjIdx].y = idx;
            }
            else
            {
                mAdjacency[adjIdx].y = -1;
            }
        }
    }
    timer.stop();
    //cout << "extractInfo: " << timer.getElapsedTimeInMicroSec() << "us" << endl;
}

double CuMesh::computeNormals(bool gpu)
{
    START_FUNCTION_TIMER();

    if (gpu)
    {
        cu_computeFaceNormals(mTotalFaces, mFaceValence, mFacesGPU,
                              mVertexesGPU, mFacesNormalsGPU, 1);
        cu_computeVertexNormals(mTotalVertexes, mVertexValence,
                                mAdjacencyGPU, mFacesNormalsGPU,
                                mVertexesNormalsGPU, 1);
    }
    else
    {
        cu_computeFaceNormals(mTotalFaces, mFaceValence, mFaces, mVertexes,
                              mFacesNormals, 0);
        cu_computeVertexNormals(mTotalVertexes, mVertexValence, mAdjacency,
                                mFacesNormals, mVertexesNormals, 0);
    }

    RETURN_FUNCTION_TIMER();
}

unsigned long long CuMesh::getTotalMemory() const
{
    unsigned long long total = 0;
    total += 2*mTotalVertexes*sizeof(float4);             // vertexes, normals
    total += mTotalFaces*sizeof(float4);                  // faces
    total += mTotalVertexes*mVertexValence*sizeof(long2); // vertex valence
    total += mTotalFaces*mFaceValence*sizeof(long);       // face valence

    return total;
}

} //namespace desmo
