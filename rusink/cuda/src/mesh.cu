#include "mesh.cuh"
#include <iostream>
#include <CudaHelper/helper_math.h>
#include "utils.h"

// BEGIN - face normal estimation ---------------------------------------------
#if 1
/**
 * Calculates the normal of a given face.
 */
__device__ __host__
    void _computeTriangularFaceNormal(long idx, long size,
                                      const long *const faces,
                                      const float4 *const vertexes,
                                      float4 *const facesNormals)
{
    const int valence = 3;

    float3 pt[3];
    for (int i = 0; i < valence; ++i) 
    {
        pt[i] = make_float3(vertexes[faces[i*size + idx]]);
    }
    pt[1] -= pt[0];
    pt[2] -= pt[0];
    pt[0] = cross(pt[1], pt[2]);
    normalize(pt[0]);
    facesNormals[idx] = make_float4(pt[0]);
}

//TODO: receive a function pointer here, instead of valence
__global__
    void computeFaceNormal(long size, int valence, const long *const faces,
                           const float4 *const vertexes,
                           float4 *const facesNormals)
{
    long int faceIdx = blockDim.x*blockIdx.x + threadIdx.x;
    //TODO:call the function from the parameter
    if (faceIdx < size) // only run if we are inside the boundaries
        _computeTriangularFaceNormal(faceIdx, size, faces, vertexes, facesNormals);
}

void cu_computeFaceNormals(long size,
                           int valence,
                           const long *const faces,
                           const float4 *const vertexes,
                           float4 *const facesNormals,
                           int gpu)
{
    if (gpu)
    {
        int numBlocks = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        //TODO:pass a function pointer according to valence
        computeFaceNormal<<<numBlocks, BLOCK_SIZE>>>(size, valence, faces,
                                                     vertexes, facesNormals);
        cudaThreadSynchronize();
        LOG_LINE(std::cout, "FaceNormals: " 
                 << cudaGetErrorString(cudaGetLastError()));
    }
    else
    {
        for (long int i = 0; i < size; ++i)
        {
            //TODO:pass a function pointer according to valence
            _computeTriangularFaceNormal(i, size, faces, vertexes, facesNormals);
        }
    }
}
#endif
// END - face normal estimation -----------------------------------------------

// BEGIN - vertex normal estimation -------------------------------------------
#if 1
/**
 * Calculates the normal of a given vertex (avarage of adjacent faces).
 */
__device__ __host__ void _computeVertexNormal(long idx, long size,
                                              int valence,
                                              const long2 *const adjacency,
                                              const float4 *const facesNormals,
                                              float4 *const vertexesNormals)
{
    float4 normal = make_float4(0);
    for (int i = 0; i < valence; ++i) 
    {
        long faceIdx = adjacency[i*size + idx].y;
        if (faceIdx != -1)
            normal += facesNormals[faceIdx];
    }
    //TODO: isso parece errado. Nao precisaria dividir pelo numero de faces
    //      antes de normalizar?
    normalize(normal);
    vertexesNormals[idx] = normal;
}

__global__
    void computeVertexNormal(long size, int valence,
                             const long2 *const adjacency,
                             const float4 *const facesNormals,
                             float4 *const vertexesNormals)
{
    long int vertexIdx = blockDim.x*blockIdx.x + threadIdx.x;
    if (vertexIdx < size) // only run if we are inside the boundaries
        _computeVertexNormal(vertexIdx, size, valence, adjacency, facesNormals,
                             vertexesNormals);
}

void cu_computeVertexNormals(long size,
                             int valence,
                             const long2 *const adjacency,
                             const float4 *const facesNormals,
                             float4 *const vertexesNormals,
                             int gpu)
{
    if (gpu)
    {
        int numBlocks = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        computeVertexNormal<<<numBlocks, BLOCK_SIZE>>>(size, valence,
                                                       adjacency,
                                                       facesNormals,
                                                       vertexesNormals);
        cudaThreadSynchronize();
        LOG_LINE(std::cout, "VertexNormals: " 
                 << cudaGetErrorString(cudaGetLastError()));
    }
    else
    {
        for (long int i = 0; i < size; ++i)
        {
            _computeVertexNormal(i, size, valence, adjacency, facesNormals,
                                 vertexesNormals);
        }
    }
}
#endif
// END - vertex normal estimation ---------------------------------------------
