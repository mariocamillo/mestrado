#include "deforma.h"
#include <iostream>
#include <cuda.h>
#include <CudaHelper/helper_cuda.h>
#include "cumesh.hpp"
#include "cutensorsnaive.hpp"
#include "cutensorscoalesced.hpp"
#include "cutensorssharing.hpp"
#include "cuutils.hpp"
#include "utils.h"
#ifdef WIN32
#ifdef _DEBUG
//#include <vld.h>
#endif
#endif

using namespace std;
using namespace desmo;
using namespace desmo::utils;

#define FOR_EACH_ELAPSED(X)                                                \
    X(ELAPSED_LOAD, "Mesh load time")                                      \
    X(ELAPSED_LOAD_GPU, "CPU to GPU mesh load time")                       \
    X(ELAPSED_NORMALS, "Normal estimation time")                           \
    X(ELAPSED_METRIC_BORDERS, "Vertex metric and border computation time") \
    X(ELAPSED_TENSORS, "Metric and curvature tensors estimation time")     \
    X(ELAPSED_PHI_PSI_THETA, "Phi, psi and theta estimation time")         \
    X(ELAPSED_N_M, "N, M and internal energy estimation time")             \
    X(ELAPSED_TOTAL_COMP, "Total elapsed time for computation")            \
    X(ELAPSED_TOTAL, "Total elapsed time")

#define DEF_ENUM(name, msg) name,
#define DEF_NAME_ARRAY(name, msg) #name,
#define DEF_MSG_ARRAY(name, msg) msg,

enum ELAPSED_IDX {
    FOR_EACH_ELAPSED(DEF_ENUM)
    ELAPSED_END
};

string ELAPSED_NAMES[] = {FOR_EACH_ELAPSED(DEF_NAME_ARRAY) "ELAPSED_END"};
string ELAPSED_MSGS[] = {FOR_EACH_ELAPSED(DEF_MSG_ARRAY) "End"};

const string CUR_STATE_OPT = "--cur-state-only";
bool curStateOnly = false;
bool CALC_LOAD_TIMES = false;
bool SHARING_ONLY = true;

int TIME_AVG_ITER_NUM;
int desmo::utils::WARP_SIZE;
char layout;

// Sum two elapsed times array. Result is in the dst
void sumElapseds(const double *const src, double *const dst)
{
    for (int i = 0; i < ELAPSED_END; ++i)
    {
        dst[i] += src[i];
    }
}

// Divide elapsed times array by a double value
void divElapsed(double *const elapsed, double div)
{
    for (int i = 0; i < ELAPSED_END; ++i)
    {
        elapsed[i] = elapsed[i]/div;
    }
}

// Calculate total elapsed time
void calcElapsedTotal(double *const elapsed)
{
    elapsed[ELAPSED_TOTAL] = 0;
    for (int i = 0; i < ELAPSED_END; ++i)
    {
        if (i != ELAPSED_TOTAL && i != ELAPSED_TOTAL_COMP)
        {
            elapsed[ELAPSED_TOTAL] += elapsed[i];
            if (i != ELAPSED_LOAD && i != ELAPSED_LOAD_GPU)
                elapsed[ELAPSED_TOTAL_COMP] += elapsed[i];
        }
    }
}

// Print elapsed times
void printElapsed(const double *const elapsed, const char *const msg)
{
    cout << msg << endl;
    for (int i = 0; i < ELAPSED_END; ++i)
    {
        cout << ELAPSED_MSGS[i] << ":" << elapsed[i] << "us" << endl;
    }
    cout << "-----------------------------------------------------" << endl;
}

// Print memory usage
void printMemUsage(CuTensors *const ct0, CuTensors *const ct1,
                   const char *const msg)
{
    cout << msg << endl;
    unsigned long long mem;
    double memMB;
    mem = ct0->getMesh()->getTotalMemory() + ct0->getTotalMemory();
    if (!curStateOnly)
    {
        mem += ct1->getMesh()->getTotalMemory() + ct1->getTotalMemory();
    }
    memMB = double(mem)/(1024.*1024.);
    cout << memMB << "MB" << endl;
    cout << "Shared mem usage: " << ct0->getSharedMemUsage() << endl;
    cout << "-----------------------------------------------------" << endl;
}

// deforma.cpp globals
extern double MESH_AREA;
extern double MASS_VERTEX;
extern double diff_base[2];

void calculate_tensors_cpu(Mesh **m0, Mesh **m1,
                           double *const elapsed)
{
    Mesh m2;
    MESH_AREA = (umax-umin) * (vmax-vmin);

    MASS_VERTEX = MASS / pow(double(divisoes+1),2);

    diff_base[0] = (umax-umin) / divisoes;
    diff_base[1] = (vmax-vmin) / divisoes;

    // Load the meshes
    
    if (*m0 == nullptr)
    {
        *m0 = new Mesh();
        *m1 = new Mesh();
        elapsed[ELAPSED_LOAD] = debug_load(*m0, *m1, &m2, (int)desmo::CuMesh::SADDLE, !curStateOnly);
        elapsed[ELAPSED_LOAD_GPU] = 0;
    }

    if (!SHARING_ONLY)
    {
        // Make m0 the initial state mesh
        elapsed[ELAPSED_LOAD] += debug_update(*m0,0,(int)desmo::CuMesh::SADDLE);
        elapsed[ELAPSED_NORMALS] = compute_normals(*m0);
        elapsed[ELAPSED_METRIC_BORDERS] = compute_vertex_metric_and_borders(*m0);
        elapsed[ELAPSED_TENSORS] = compute_tensors(*m0);
        elapsed[ELAPSED_PHI_PSI_THETA] = compute_phi_psi_theta(*m0);

        if (!curStateOnly)
        {
            // Now calculate tensors for current state
            elapsed[ELAPSED_NORMALS] += compute_normals(*m1);
            elapsed[ELAPSED_METRIC_BORDERS] += compute_vertex_metric_and_borders(*m1);
            elapsed[ELAPSED_TENSORS] += compute_tensors(*m1);
            elapsed[ELAPSED_PHI_PSI_THETA] += compute_phi_psi_theta(*m1);
            elapsed[ELAPSED_N_M] = compute_tensors_n_m(*m1,*m0);
            debug_print_values(*m1);
            m2.removeMesh();
        }
    }
}

void calculate_tensors_gpu(Mesh *const m0, Mesh *const m1,
                           CuTensors *const ct0, CuTensors *const ct1,
                           double *const elapsed, bool useGPU)
{
    CuMesh *cm0 = ct0->getMesh();
    CuMesh *cm1 = ct1->getMesh();

    // Load the meshes
    if (ct0->getMesh() == NULL)
    {
        cm0 = new CuMesh();
        elapsed[ELAPSED_LOAD] = cm0->init(m0);
        elapsed[ELAPSED_LOAD] +=  ct0->setMesh(cm0);
        elapsed[ELAPSED_LOAD_GPU] = cm0->initGPU() + ct0->initGPU();
        elapsed[ELAPSED_LOAD_GPU] = 0;
    }

    // Make m0 the initial state mesh
    elapsed[ELAPSED_NORMALS] = cm0->computeNormals(useGPU);
    elapsed[ELAPSED_METRIC_BORDERS] = ct0->computeVertexMetric(useGPU);
    elapsed[ELAPSED_TENSORS] = ct0->computeTensors(useGPU);
    //elapsed[ELAPSED_PHI_PSI_THETA] = compute_phi_psi_theta(m0);

    if (!curStateOnly)
    {
        if (cm1 == NULL)
        {
            cm1 = new CuMesh();
            elapsed[ELAPSED_LOAD] += cm1->init(m1);
            elapsed[ELAPSED_LOAD] += ct1->setMesh(cm1);
            elapsed[ELAPSED_LOAD_GPU] += cm1->initGPU() + ct1->initGPU();
        }

        // Now calculate tensors for current state
        elapsed[ELAPSED_NORMALS] += cm1->computeNormals(useGPU);
        elapsed[ELAPSED_METRIC_BORDERS] += ct1->computeVertexMetric(useGPU);
        elapsed[ELAPSED_TENSORS] += ct1->computeTensors(useGPU);
        //elapsed[ELAPSED_PHI_PSI_THETA] += compute_phi_psi_theta(m1);
        //elapsed[ELAPSED_N_M] = compute_tensors_n_m(m1,m0);
    }
    if (!useGPU)
    {
        elapsed[ELAPSED_LOAD_GPU] = 0;
    }
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        cerr << "Usage: " << argv[0] << "<num iterations> <mesh size> ["
             << CUR_STATE_OPT << "]" << endl;
        exit(1);
    }
    else
    {
        TIME_AVG_ITER_NUM = atoi(argv[1]);
        divisoes = atoi(argv[2]);
    }

    if (argc > 3 && CUR_STATE_OPT.compare(argv[3]) == 0)
    {
        curStateOnly = true;
    }
    else
    {
        cerr << "Usage: " << argv[0] << "<num iterations> <mesh size> ["
             << CUR_STATE_OPT << "]" << endl;
        exit(1);
    }

    double tCPUM[ELAPSED_END] = {0};
    double tCPU[ELAPSED_END] = {0};
    double tGPU[ELAPSED_END] = {0};
    double tGPUCoalesced[ELAPSED_END] = {0};
    double tGPUSharing[ELAPSED_END] = {0};

    // Initialize GPU
    cudaSetDevice(0);

    // Get GPU properties
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    WARP_SIZE = prop.warpSize;
    cout << "Max per block shared memory is: " << prop.sharedMemPerBlock << " bytes" << endl;

    cout << "Executing estimations " << TIME_AVG_ITER_NUM << " time(s)" << endl;
    cout << "Mesh size is: " << divisoes << "x" << divisoes << endl;
    Mesh *m0CPU, *m1CPU;
    m0CPU = m1CPU = nullptr;
    CuTensorsNaive *ctn0 = new CuTensorsNaive();
    CuTensorsNaive *ctn1 = new CuTensorsNaive();
    CuTensorsCoalesced *ctc0 = new CuTensorsCoalesced();
    CuTensorsCoalesced *ctc1 = new CuTensorsCoalesced();
    CuTensorsSharing *cts0 = new CuTensorsSharing();
    CuTensorsSharing *cts1 = new CuTensorsSharing();
    for (int i = 0; i <= TIME_AVG_ITER_NUM; ++i)
    {
        cout << ".";
        double tmpCPU[ELAPSED_END] = {0};
        double tmpCPUM[ELAPSED_END] = {0};
        double tmpGPU[ELAPSED_END] = {0};
        double tmpGPUCoalesced[ELAPSED_END] = {0};
        double tmpGPUSharing[ELAPSED_END] = {0};

        calculate_tensors_cpu(&m0CPU, &m1CPU, tmpCPUM);
        if (!SHARING_ONLY)
        {
            calculate_tensors_gpu(m0CPU, m1CPU, ctn0, ctn1, tmpCPU, false);
            calculate_tensors_gpu(m0CPU, m1CPU, ctn0, ctn1, tmpGPU, true);
            calculate_tensors_gpu(m0CPU, m1CPU, ctc0, ctc1, tmpGPUCoalesced,
                                  true);
        }
        calculate_tensors_gpu(m0CPU, m1CPU, cts0, cts1, tmpGPUSharing,
                              true);

        // Elapsed load for GPU should add CPU as it uses CPU meshes to
        // extract info
        tmpCPU[ELAPSED_LOAD] += tmpCPUM[ELAPSED_LOAD];
        tmpGPU[ELAPSED_LOAD] += tmpCPUM[ELAPSED_LOAD];
        tmpGPUCoalesced[ELAPSED_LOAD] += tmpCPUM[ELAPSED_LOAD];
        tmpGPUSharing[ELAPSED_LOAD] += tmpCPUM[ELAPSED_LOAD];

        // Accumulate elapsed times. Discard first for warm-up
        if (i > 0)
        {
            if (!SHARING_ONLY)
            {
                sumElapseds(tmpCPUM, tCPUM);
                sumElapseds(tmpCPU, tCPU);
                sumElapseds(tmpGPU, tGPU);
                sumElapseds(tmpGPUCoalesced, tGPUCoalesced);
            }
            sumElapseds(tmpGPUSharing, tGPUSharing);
        }
        // Display memory usage in first run (it's constant)
        else
        {
            if (!SHARING_ONLY)
            {
                printMemUsage(ctn0, ctn1, "GPU memory usage:");
                printMemUsage(ctc0, ctc1, "GPU coalesced mem usage:");
            }
            printMemUsage(cts0, cts1, "GPU sharing alg mem usage:");
        }

        // Delete meshes if we're calculating load times
        if (CALC_LOAD_TIMES)
        {
            delete m0CPU;
            delete m1CPU;
            m0CPU = nullptr;
            m1CPU = nullptr;
            delete ctn0;
            delete ctn1;
            delete ctc0;
            delete ctc1;
            delete cts0;
            delete cts1;
            CuTensorsNaive *ctn0 = new CuTensorsNaive();
            CuTensorsNaive *ctn1 = new CuTensorsNaive();
            CuTensorsCoalesced *ctc0 = new CuTensorsCoalesced();
            CuTensorsCoalesced *ctc1 = new CuTensorsCoalesced();
            CuTensorsSharing *cts0 = new CuTensorsSharing();
            CuTensorsSharing *cts1 = new CuTensorsSharing();
        }
    }
    cout << endl;
    
    // Free memory
    delete m0CPU;
    delete m1CPU;
    delete ctn0;
    delete ctn1;
    delete ctc0;
    delete ctc1;
    delete cts0;
    delete cts1;

    if (TIME_AVG_ITER_NUM > 0)
    {
        if (!SHARING_ONLY)
        {
            divElapsed(tCPUM, TIME_AVG_ITER_NUM);
            divElapsed(tCPU, TIME_AVG_ITER_NUM);
            divElapsed(tGPU, TIME_AVG_ITER_NUM);
            divElapsed(tGPUCoalesced, TIME_AVG_ITER_NUM);
            calcElapsedTotal(tCPUM);
            calcElapsedTotal(tCPU);
            calcElapsedTotal(tGPU);
            calcElapsedTotal(tGPUCoalesced);
            
            printElapsed(tCPUM, "CPU average times (Mathias):");
            printElapsed(tCPU, "CPU average times:");
            printElapsed(tGPU, "GPU average times:");
            printElapsed(tGPUCoalesced, "GPU Coalesced average times:");
        }
        divElapsed(tGPUSharing, TIME_AVG_ITER_NUM);
        calcElapsedTotal(tGPUSharing);
        printElapsed(tGPUSharing, "GPU Sharing alg average times:");
    }

    cudaDeviceReset();

    return 0;
}
