#include "cutensorscoalesced.hpp"
#include <CudaHelper/helper_math.h>
#include "Timer.h"
#include "utils.h"
#include "cuutils.hpp"
#include "tensorscoalesced.cuh"
#include "deforma.h" // apenas para ter o divisoes

using namespace desmo::utils;

namespace desmo {

CuTensorsCoalesced::CuTensorsCoalesced() : CuTensors()
{
    mVertexesMetrics = NULL;
    mVertexesValences = NULL;
    mCoalescedVertexes = NULL;
    mCoalescedVertexesNormals = NULL;
    mVertexesMetricsGPU = NULL;
    mVertexesValencesGPU = NULL;
    mCoalescedVertexesGPU = NULL;
    mCoalescedVertexesNormalsGPU = NULL;
}

CuTensorsCoalesced::~CuTensorsCoalesced()
{
    freeResources();
}

void CuTensorsCoalesced::freeResources()
{
    // Free parent resources
    CuTensors::freeResources();

    // Free CPU resources
    FREE_CPU_RESOURCE(mVertexesMetrics, delete []);
    FREE_CPU_RESOURCE(mVertexesValences, delete []);
    FREE_CPU_RESOURCE(mCoalescedVertexes, delete []);
    FREE_CPU_RESOURCE(mCoalescedVertexesNormals, delete []);

    // Free GPU resources
    FREE_GPU_RESOURCE(mVertexesMetricsGPU, cudaFree);
    FREE_GPU_RESOURCE(mVertexesValencesGPU, cudaFree);
    FREE_GPU_RESOURCE(mCoalescedVertexesGPU, cudaFree);
    FREE_GPU_RESOURCE(mCoalescedVertexesNormalsGPU, cudaFree);
}

void CuTensorsCoalesced::init(CuMesh *mesh)
{
    freeResources();

    // Call parent init
    CuTensors::init(mesh);

    // Allocate CPU resources
    long numVertexes = mMesh->getTotalVertexes();
    int vertexValence = mMesh->getVertexValence();
    long coalescedSize = numVertexes*(vertexValence + 1);
    mVertexesMetrics = new long4[numVertexes];
    mVertexesValences = new char2[numVertexes];
    mCoalescedVertexes = new float3[coalescedSize];
    mCoalescedVertexesNormals = new float3[coalescedSize];

    // Get data from mesh (make redundant)
    const float4 *vertexes = mMesh->getVertexes();
    const float4 *vertexesNormals = mMesh->getVertexesNormals();
    const long2 *adjacency = mMesh->getAdjacency();
    for(long i = 0; i < numVertexes; ++i)
    {
        COALESCED_VERTEX(mCoalescedVertexes, i) = make_float3(vertexes[i]);
        COALESCED_VERTEX(mCoalescedVertexesNormals, i) = 
                                               make_float3(vertexesNormals[i]);

        // Copy adjacent vertexes and faces info to coalesced arrays
        char2 valence = {0};
        for(int j = 0; j < vertexValence; ++j)
        {
            int nidx = adjacency[j*numVertexes + i].x;

            // adjacent vertex
            if (nidx != -1)
            {

                COALESCED_VERTEX_ADJACENT(mCoalescedVertexes,
                                          i, j, numVertexes) =
                                                   make_float3(vertexes[nidx]);
                ++(valence.x);
            }
        }
        mVertexesValences[i] = valence;
    }
}

double CuTensorsCoalesced::initGPU()
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    // Call parent GPU init
    CuTensors::initGPU();

    // Allocate GPU resources
    long numVertexes = mMesh->getTotalVertexes();
    int vertexValence = mMesh->getVertexValence();
    long coalescedSize = numVertexes*(vertexValence + 1);
    checkCudaErrors(cudaMalloc(&mVertexesMetricsGPU,
                               numVertexes*sizeof(long4)));
    checkCudaErrors(cudaMalloc(&mVertexesValencesGPU,
                               numVertexes*sizeof(char2)));
    checkCudaErrors(cudaMalloc(&mCoalescedVertexesGPU,
                               coalescedSize*sizeof(float3)));
    checkCudaErrors(cudaMalloc(&mCoalescedVertexesNormalsGPU,
                               coalescedSize*sizeof(float3)));

    // Copy data to GPU
    checkCudaErrors(cudaMemcpy(mVertexesValencesGPU, mVertexesValences,
                               numVertexes*sizeof(char2),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mCoalescedVertexesGPU, mCoalescedVertexes,
                               coalescedSize*sizeof(float3),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mCoalescedVertexesNormalsGPU,
                               mCoalescedVertexesNormals,
                               coalescedSize*sizeof(float3),
                               cudaMemcpyHostToDevice));

    RETURN_FUNCTION_TIMER();
}

double CuTensorsCoalesced::computeVertexMetric(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    if (gpu)
    {
        cucoalesced::cu_computeVertexMetrics(mMesh->getTotalVertexes(),
                                             divisoes, mVertexesMetricsGPU, 1);
    }
    else
    {
        cucoalesced::cu_computeVertexMetrics(mMesh->getTotalVertexes(),
                                             divisoes, mVertexesMetrics, 0);
    }

    RETURN_FUNCTION_TIMER();
}

double CuTensorsCoalesced::computeTensors(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    if (gpu)
    {
        cucoalesced::cu_computeTensors(mMesh->getTotalVertexes(),
                                       mVertexesValencesGPU,
                                       mCoalescedVertexesGPU,
                                       mCoalescedVertexesNormalsGPU,
                                       mVertexesMetricsGPU, mPDir1sGPU,
                                       mPDir2sGPU, mMetricTensorsGPU,
                                       mContravMetricTensorsGPU,
                                       mCurvTensorsGPU,
                                       mKsGPU, 1);
    }
    else
    {
        cucoalesced::cu_computeTensors(mMesh->getTotalVertexes(),
                                       mVertexesValences, mCoalescedVertexes,
                                       mCoalescedVertexesNormals,
                                       mVertexesMetrics, mPDir1s, mPDir2s,
                                       mMetricTensors, mContravMetricTensors,
                                       mCurvTensors, mKs, 0);
    }

    RETURN_FUNCTION_TIMER();
}

unsigned long long CuTensorsCoalesced::getTotalMemory() const
{
    if (mMesh == NULL)
        return 0;
    
    long numVertexes = mMesh->getTotalVertexes();
    int vertexValence = mMesh->getVertexValence();
    long coalescedSize = numVertexes*(vertexValence + 1);
    unsigned long long total = CuTensors::getTotalMemory();
    total += numVertexes*sizeof(long4);      // vertexes metrics
    total += 2*coalescedSize*sizeof(float3); // coalesced vertex, normal
    total += numVertexes*sizeof(char2);      // valence

    return total;
}

};
