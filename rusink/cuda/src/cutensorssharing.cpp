#include "cutensorssharing.hpp"
#include <CudaHelper/helper_math.h>
#include "Timer.h"
#include "utils.h"
#include "cuutils.hpp"
#include "tensorssharing.cuh"
#include "deforma.h" // apenas para ter o divisoes

using namespace desmo::utils;

namespace desmo {

CuTensorsSharing::CuTensorsSharing() : CuTensors()
{
    mSharedMemSize = 0;
    mVertexesMetrics = NULL;
    mBlockSizesGPU = NULL;
    mBlockPositionsGPU = NULL;
    mVertexPositionsGPU = NULL;
    mVertexesMetricsGPU = NULL;
    mSharedVertexesGPU = NULL;
    mSharedVertexesNormalsGPU = NULL;
}

CuTensorsSharing::~CuTensorsSharing()
{
    freeResources();
}

void CuTensorsSharing::freeResources()
{
    // Free parent resources
    CuTensors::freeResources();

    // Free CPU resources
    FREE_CPU_RESOURCE(mVertexesMetrics, delete []);

    // Free GPU resources
    FREE_GPU_RESOURCE(mBlockSizesGPU, cudaFree);
    FREE_GPU_RESOURCE(mBlockPositionsGPU, cudaFree);
    FREE_GPU_RESOURCE(mVertexPositionsGPU, cudaFree);
    FREE_GPU_RESOURCE(mVertexesMetricsGPU, cudaFree);
    FREE_GPU_RESOURCE(mSharedVertexesGPU, cudaFree);
    FREE_GPU_RESOURCE(mSharedVertexesNormalsGPU, cudaFree);
}

void CuTensorsSharing::init(CuMesh *mesh)
{
    freeResources();

    // Call parent init
    CuTensors::init(mesh);

    // Calculate how many thread blocks we have
    long numVertexes = mMesh->getTotalVertexes();
    mNumBlocks = (numVertexes + cusharing::BLOCK_SIZE)/cusharing::BLOCK_SIZE;

    // Allocate CPU resources
    // TODO: make face and vertex sharing size different
    int vertexValence = mMesh->getVertexValence();
    mVertexesMetrics = new long4[numVertexes];
    mVertexPositions.reserve(numVertexes*(vertexValence + 1));
	mVertexPositions.insert(mVertexPositions.begin(), numVertexes*(vertexValence + 1), -1);
    mBlockSizes.reserve(mNumBlocks);
    mBlockPositions.reserve(mNumBlocks);

    // Get data from mesh (make redundant)
    const float4 *vertexes = mMesh->getVertexes();
    const float4 *vertexesNormals = mMesh->getVertexesNormals();
    const long2 *adjacency = mMesh->getAdjacency();

    mSharedMemSize = 0;
    long sharedVertexesSize = 0;
    long sharedFacesSize = 0;
    ElementsSet *blockVertexes = new ElementsSet[mNumBlocks];
    for(long i = 0; i < mNumBlocks; ++i)
    {
        long tcount;
        for (tcount = 0; tcount < cusharing::BLOCK_SIZE; ++tcount)
        {
            long vertexIdx = i*cusharing::BLOCK_SIZE + tcount;
            if (vertexIdx >= numVertexes)
            {
                break;
            }

            DataElem elem(vertexIdx, blockVertexes[i].size());
            ElementsSet::iterator it = blockVertexes[i].insert(elem).first;
            mVertexPositions[vertexIdx] = it->sharedPos;

            for (int k = 0; k < vertexValence; ++k)
            {
                long nidx = adjacency[k*numVertexes + vertexIdx].x;

                if (nidx != -1)
                {
                    DataElem elem(nidx, blockVertexes[i].size());
                    ElementsSet::iterator it = blockVertexes[i].insert(elem).first;
                    mVertexPositions[k*numVertexes + vertexIdx] = it->sharedPos;
                }
                else
                {
                    mVertexPositions.push_back(-1);
                }
            }
        }

        size_t blockMem;
        blockMem = blockVertexes[i].size()*2*sizeof(float3); // vertices and normals
        if (blockMem > mSharedMemSize)
        {
            mSharedMemSize = blockMem;
        }

        sharedVertexesSize += blockVertexes[i].size();
    }

    // Prepare vectors to receive data
    mSharedVertexes.reserve(sharedVertexesSize);
    mSharedVertexes.insert(mSharedVertexes.begin(), sharedVertexesSize, make_float3(0));
    mSharedVertexesNormals.reserve(sharedVertexesSize);
    mSharedVertexesNormals.insert(mSharedVertexesNormals.begin(), sharedVertexesSize,
                                  make_float3(0));
    long curPos = 0;

    // This is the data actually copied to shared memory
    for (long i = 0; i < mNumBlocks; ++i)
    {
        mBlockSizes.push_back(blockVertexes[i].size());
        mBlockPositions.push_back(curPos);
        curPos += blockVertexes[i].size();

        for (ElementsSet::iterator it = blockVertexes[i].begin(); it != blockVertexes[i].end(); ++it)
        {
            mSharedVertexes[it->sharedPos] = make_float3(vertexes[it->id]);
            mSharedVertexesNormals[it->sharedPos] = make_float3(vertexesNormals[it->id]);
        }
    }

    // Calculate vertices for metric tensor calculation
    cusharing::cu_computeVertexMetrics(mMesh->getTotalVertexes(),
                                       divisoes, mVertexesMetrics, 0);
    // Now update them to point to the correct shared memory array position
    for (long i = 0; i < numVertexes; ++i)
    {
        long blockIdx = i/cusharing::BLOCK_SIZE;
        ElementsSet::iterator it;
        it = blockVertexes[blockIdx].find(DataElem(mVertexesMetrics[i].x, 0));
        if (it != blockVertexes[blockIdx].end()) mVertexesMetrics[i].x = it->sharedPos;
        else mVertexesMetrics[i].x = -1;
        it = blockVertexes[blockIdx].find(DataElem(mVertexesMetrics[i].y, 0));
        if (it != blockVertexes[blockIdx].end()) mVertexesMetrics[i].y = it->sharedPos;
        else mVertexesMetrics[i].y = -1;
        it = blockVertexes[blockIdx].find(DataElem(mVertexesMetrics[i].z, 0));
        if (it != blockVertexes[blockIdx].end()) mVertexesMetrics[i].z = it->sharedPos;
        else mVertexesMetrics[i].z = -1;
        it = blockVertexes[blockIdx].find(DataElem(mVertexesMetrics[i].w, 0));
        if (it != blockVertexes[blockIdx].end()) mVertexesMetrics[i].w = it->sharedPos;
        else mVertexesMetrics[i].w = -1;
    }

    /*cout << "Num blocks: " << mNumBlocks << endl; 
    for (int i = 0; i < mNumBlocks; ++i)
        cout << "Block[" << i << "]: " << mBlockPositions[i] << "|" << mBlockSizes[i] << endl;

    cout << "Num vertexes: " << numVertexes << endl; 
    cout << "Vertexes size: " << mSharedVertexes.size() << endl; 
    cout << "Positions size: " << mVertexPositions.size() << endl; */
    delete [] blockVertexes;
}

double CuTensorsSharing::initGPU()
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    // Call parent GPU init
    CuTensors::initGPU();

    // Allocate GPU resources
    long numVertexes = mMesh->getTotalVertexes();
    checkCudaErrors(cudaMalloc(&mVertexesMetricsGPU,
                               numVertexes*sizeof(long4)));
    checkCudaErrors(cudaMalloc(&mBlockSizesGPU,
                               mBlockSizes.size()*sizeof(long)));
    checkCudaErrors(cudaMalloc(&mBlockPositionsGPU,
                               mBlockPositions.size()*sizeof(long)));
    checkCudaErrors(cudaMalloc(&mVertexPositionsGPU,
                               mVertexPositions.size()*sizeof(long)));
    checkCudaErrors(cudaMalloc(&mSharedVertexesGPU,
                               mSharedVertexes.size()*sizeof(float3)));
    checkCudaErrors(cudaMalloc(&mSharedVertexesNormalsGPU,
                               mSharedVertexesNormals.size()*sizeof(float3)));

    // Copy data to GPU
    checkCudaErrors(cudaMemcpy(mVertexesMetricsGPU, mVertexesMetrics,
                               mMesh->getTotalVertexes()*sizeof(long4),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mBlockSizesGPU, &mBlockSizes[0],
                               mBlockSizes.size()*sizeof(long),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mBlockPositionsGPU, &mBlockPositions[0],
                               mBlockPositions.size()*sizeof(long),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mVertexPositionsGPU, &mVertexPositions[0],
                               mVertexPositions.size()*sizeof(long),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mSharedVertexesGPU, &mSharedVertexes[0],
                               mSharedVertexes.size()*sizeof(float3),
                               cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(mSharedVertexesNormalsGPU, &mSharedVertexesNormals[0],
                               mSharedVertexesNormals.size()*sizeof(float3),
                               cudaMemcpyHostToDevice));

    RETURN_FUNCTION_TIMER();
}

double CuTensorsSharing::computeVertexMetric(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

/*    if (gpu)
    {
        cusharing::cu_computeVertexMetrics(mMesh->getTotalVertexes(),
                                           divisoes, mVertexesMetricsGPU, 1);
    }
    else
    {
        cusharing::cu_computeVertexMetrics(mMesh->getTotalVertexes(),
                                           divisoes, mVertexesMetrics, 0);
    }*/

    RETURN_FUNCTION_TIMER();
}

double CuTensorsSharing::computeTensors(bool gpu)
{
    if (mMesh == NULL)
        throw NullMeshException();

    START_FUNCTION_TIMER();

    if (gpu)
    {
        cusharing::cu_computeTensors(mMesh->getTotalVertexes(), mSharedMemSize,
                                     mMesh->getVertexValence(),
                                     mBlockSizesGPU, mBlockPositionsGPU,
                                     mVertexPositionsGPU,
                                     mSharedVertexesGPU,
                                     mSharedVertexesNormalsGPU,
                                     mVertexesMetricsGPU, mPDir1sGPU,
                                     mPDir2sGPU, mMetricTensorsGPU,
                                     mContravMetricTensorsGPU,
                                     mCurvTensorsGPU,
                                     mKsGPU, mNumBlocks, 1);
    }
    else
    {
        cusharing::cu_computeTensors(mMesh->getTotalVertexes(), mSharedMemSize,
                                     mMesh->getVertexValence(),
                                     &mBlockSizes[0], &mBlockPositions[0],
                                     &mVertexPositions[0],
                                     &mSharedVertexes[0],
                                     &mSharedVertexesNormals[0],
                                     mVertexesMetrics, mPDir1s,
                                     mPDir2s, mMetricTensors,
                                     mContravMetricTensors,
                                     mCurvTensors,
                                     mKs, mNumBlocks, 0);
    }

    RETURN_FUNCTION_TIMER();
}

unsigned long long CuTensorsSharing::getTotalMemory() const
{
    if (mMesh == NULL)
        return 0;
    
    long numVertexes = mMesh->getTotalVertexes();
    unsigned long long total = CuTensors::getTotalMemory();
    total += numVertexes*sizeof(long4);      // vertexes metrics
    total += (mSharedVertexes.size() + mSharedVertexesNormals.size())*sizeof(float3); // shared vertex, normal
    total += mVertexPositions.size()*sizeof(long); // positions
    total += (mBlockSizes.size() + mBlockPositions.size())*sizeof(long);

    return total;
}

};
