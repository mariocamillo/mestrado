#include "tensorssharing.cuh"
#include <iostream>
#include <CudaHelper/helper_math.h>
#include <blmatrix3d.hpp>
#include "utils.h"

#define M_PI 3.14159265358979323846f

namespace desmo {
namespace cusharing {

int BLOCK_SIZE = 256;

// BEGIN - vertex metric estimation -------------------------------------------
#if 1

// TODO: Make generic on width and height
__device__ __host__
    void _computeVertexMetric(long idx,
                              int divisoes,
                              long4 *const vertexMetrics)
{
    int col = idx % (divisoes+1);
    int line = (int) floor((float)idx / (divisoes +1 ));
    long4 vertexMetric = make_long4(-1, -1, -1, -1);

    if (line<divisoes)
    {
        vertexMetric.x = idx + divisoes + 1;
        VERTEX_METRIC(vertexMetrics, vertexMetric.x).z = idx;
    }

    if ( col<divisoes) 
    {
        vertexMetric.y = idx + 1;
        VERTEX_METRIC(vertexMetrics, vertexMetric.y).w = idx;
    }
    VERTEX_METRIC(vertexMetrics, idx) = vertexMetric;
}

__global__
    void computeVertexMetric(long size, int divisoes,
                             long4 *const vertexMetrics)
{
    long int vertexIdx = blockDim.x*blockIdx.x + threadIdx.x;
    if (vertexIdx < size)
        _computeVertexMetric(vertexIdx, divisoes, vertexMetrics);
}

void cu_computeVertexMetrics(long size,
                             int divisoes,
                             long4 *const vertexMetrics,
                             int gpu)
{
    if (gpu)
    {
        int numBlocks = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        computeVertexMetric<<<numBlocks, BLOCK_SIZE>>>(size, divisoes,
                                                       vertexMetrics);
        cudaThreadSynchronize();
        LOG_LINE(std::cout, "VertexMetrics: " 
                 << cudaGetErrorString(cudaGetLastError()));
    }
    else
    {
        for (long int i = 0; i < size; ++i)
        {
            _computeVertexMetric(i, divisoes, vertexMetrics);
        }
    }
}
#endif
// END - vertex metric estimation ---------------------------------------------

// BEGIN - Quaternion functions -----------------------------------------------
#if 1
// theory source: http://3dgep.com/?p=1815#Rotations
typedef struct _quat
{
    float3 v;
    float  s;
} quaternion;

__device__ __host__ inline
    quaternion multQuaternion(const quaternion &q1, const quaternion &q2)
{
    quaternion ret = {q1.s*q2.v + q2.s*q1.v + cross(q1.v, q2.v),
                      q1.s*q2.s - dot(q1.v, q2.v)};
    return ret;
}

__device__ __host__ inline
    quaternion invQuaternion(const quaternion &q1)
{
    quaternion ret = {0-(q1.v), q1.s};
    return ret;
}
#endif
// END - Quaternion functions -------------------------------------------------

// BEGIN - base_a estimation --------------------------------------------------
#if 1
__device__ __host__
    void rotateVector(float3 &base_a, const float3 &new_n)
{
    float angRot;
    float3 axisRot;

    axisRot = normalize(cross(base_a, new_n));
    angRot = acos(dot(normalize(base_a), new_n));
    
    if (angRot<M_PI)
    {
        angRot += (float) (- M_PI/2);
    }
    else
    {
        angRot = (float)  (-1 * ( 2 * M_PI - angRot) + M_PI/2);
    }

    if (fabs(angRot)>0.01)
    {
        quaternion quatRot = {axisRot*sin(angRot/2), cos(angRot/2)};
        quaternion pure = {base_a, 0};
        base_a = multQuaternion(multQuaternion(quatRot, pure),
                                invQuaternion(quatRot)).v;
    }
}

__device__ __host__ inline
    float3 estimateBase_a(const long next, const long prev,
                          const float3 &vertex,
                          const float3 &vertexNormal,
                          const float3 *const vertexes)
{
    float3 base = make_float3(0);
    if (next != -1) {
        // Preference: forward finite difference
        float3 tmp = vertexes[next];
        base = tmp - vertex;
    } else if (prev != -1) {
        // backward finite difference
        float3 tmp = vertexes[prev];
        base = vertex - tmp;
    }
    rotateVector(base, vertexNormal);

    return base;
}

__device__ __host__
    void estimateBases_a(const long4 &vertexMetric,
                         const float3 &vertex,
                         const float3 &vertexNormal,
                         const float3 *const &vertexes,
                         float3 *const base)
{
    base[0] = estimateBase_a(vertexMetric.x, vertexMetric.z, vertex,
                             vertexNormal, vertexes);
    base[1] = estimateBase_a(vertexMetric.y, vertexMetric.w, vertex,
                             vertexNormal, vertexes);
    base[2] = vertexNormal;
}
#endif
// END - base_a estimation ----------------------------------------------------

// BEGIN - rusinkiewicz implementation ----------------------------------------
#if 1
template<typename T>
__device__ __host__ inline
    void swap(T &a, T &b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

// BEGIN - Linear equation solver ---------------------------------------------
#if 1
// Szymon Rusinkiewicz
// Princeton University
// OpenCL translation by Mario S Camillo, Campinas University

// Perform LDL^T decomposition of a symmetric positive definite matrix.
// Like Cholesky, but no square roots.  Overwrites lower triangle of matrix.
__device__ __host__ inline
    bool ldltdc(float A[3][3], float rdiag[3])
{
    float v[3-1];
    for (int i = 0; i < 3; ++i)
    {
        for (int k = 0; k < i; ++k)
            v[k] = A[i][k]*rdiag[k];
        for (int j = i; j < 3; ++j)
        {
            float sum = A[i][j];
            for (int k = 0; k < i; ++k)
                sum -= v[k] * A[j][k];
            if (i == j)
            {
                if (sum <= 0.0f)
                    return false;
                rdiag[i] = 1.0f/sum;
            }
            else
            {
                A[j][i] = sum;
            }
        }
    }

    return true;
}

// Solve Ax=B after ldltdc
__device__ __host__ inline
    void ldltsl(float A[3][3], float rdiag[3], float B[3], float x[3])
{
    int i;
    for (i = 0; i < 3; ++i)
    {
        float sum = B[i];
        for (int k = 0; k < i; ++k)
            sum -= A[i][k]*x[k];
        x[i] = sum*rdiag[i];
    }
    for (i = 3 - 1; i >= 0; --i)
    {
        float sum = 0;
        for (int k = i + 1; k < 3; ++k)
            sum += A[k][i]*x[k];
        x[i] -= sum*rdiag[i];
    }
}
#endif
// END - Linear equation solver -----------------------------------------------

__device__ __host__
void proj_tens(const float3 &old_u, const float3 &old_v, const float3 &old_n, 
    float old_a11, float old_a12, float old_a21, float old_a22,
    const float3 &new_u, const float3 &new_v, const float3 &new_n, 
    float &new_a11, float &new_a12, float &new_a21, float &new_a22)
{
    blMatrix3d<float> old_base_J;
    blMatrix3d<float> new_base_J;
    blMatrix3d<float> Jacobi;

    // (linha, coluna)
    old_base_J.M[0][0] = old_u.x;
    old_base_J.M[1][0] = old_u.y;
    old_base_J.M[2][0] = old_u.z;
    old_base_J.M[0][1] = old_v.x;
    old_base_J.M[1][1] = old_v.y;
    old_base_J.M[2][1] = old_v.z;
    old_base_J.M[0][2] = old_n.x;
    old_base_J.M[1][2] = old_n.y;
    old_base_J.M[2][2] = old_n.z;


    new_base_J.M[0][0] = new_u.x;
    new_base_J.M[1][0] = new_u.y;
    new_base_J.M[2][0] = new_u.z;
    new_base_J.M[0][1] = new_v.x;
    new_base_J.M[1][1] = new_v.y;
    new_base_J.M[2][1] = new_v.z;
    new_base_J.M[0][2] = new_n.x;
    new_base_J.M[1][2] = new_n.y;
    new_base_J.M[2][2] = new_n.z;

    Jacobi = inv(old_base_J) * new_base_J ;

    new_a11 = old_a11 * Jacobi.M[0][0] * Jacobi.M[0][0] + old_a12 * Jacobi.M[0][0] * Jacobi.M[1][0] +  old_a21 * Jacobi.M[1][0] * Jacobi.M[0][0] + old_a22 * Jacobi.M[1][0] * Jacobi.M[1][0];
    new_a12 = old_a11 * Jacobi.M[0][0] * Jacobi.M[0][1] + old_a12 * Jacobi.M[0][0] * Jacobi.M[1][1] +  old_a21 * Jacobi.M[1][0] * Jacobi.M[0][1] + old_a22 * Jacobi.M[1][0] * Jacobi.M[1][1];
    new_a21 = old_a11 * Jacobi.M[0][1] * Jacobi.M[0][0] + old_a12 * Jacobi.M[0][1] * Jacobi.M[1][0] +  old_a21 * Jacobi.M[1][1] * Jacobi.M[0][0] + old_a22 * Jacobi.M[1][1] * Jacobi.M[1][0];
    new_a22 = old_a11 * Jacobi.M[0][1] * Jacobi.M[0][1] + old_a12 * Jacobi.M[0][1] * Jacobi.M[1][1] +  old_a21 * Jacobi.M[1][1] * Jacobi.M[0][1] + old_a22 * Jacobi.M[1][1] * Jacobi.M[1][1];
}

// Rotate a coordinate system to be perpendicular to the given normal
// Szymon Rusinkiewicz
// Princeton University
__device__ __host__
void rot_coord_sys(const float3 &old_u, const float3 &old_v,
    const float3 &new_norm,
    float3 &new_u, float3 &new_v)
{

    /*
    new_u = old_u;
    new_v = old_v;
    float3 old_norm = old_u CROSS old_v;
    float ndot = old_norm DOT new_norm;
    if (unlikely(ndot <= -1.0f)) {
    new_u = -new_u;
    new_v = -new_v;
    return;
    } 

    // Normal components of <new_u> and <new_v> with respect to <new_norm>
    float3 perp_old = new_norm - ndot * old_norm;

    // Normal component of <new_norm> with respect to <old_norm>
    // new_u = new_u - (new_norm) * (new_u DOT (new_norm))
    //       = new_u - (new_norm) * (new_u DOT (ndot * old_norm + perp_old))
    //       = new_u - (new_norm) * (new_u DOT perp_old)
    // Instead =, using <dperp> to ensure that <new_u> and <new_v>
    // are unitary

    float3 dperp = 1.0f / (1 + ndot) * (old_norm + new_norm);

    new_u -= dperp * (new_u DOT perp_old);
    new_v -= dperp * (new_v DOT perp_old);
    */
    float3 old_norm = cross(old_u, old_v);
    normalize(old_norm);
    float ndot = dot(old_norm, new_norm);

    if (ndot <= -1.0f) 
    {
        new_u = 0-old_u;
        new_v = 0-old_v;
        return;
    } 
    else 
        if (fabs(ndot-1.0) < 1e-6) 
        {
            new_u = old_u;
            new_v = old_v;
            return;
        }

        float3 rot = cross(new_norm, old_norm);

        normalize(rot);  // direction float3tor must be unitary (Ting - 27/01/2013)

        float r00, r01, r02, r10, r11, r12, r20, r21, r22;
        float s = sqrt(1 - ndot*ndot);

        // Bug: 3 expressions are incorrect (Ting - 27/01/2012)
        // r00 = ndot + (1-ndot)*rot.x*rot.x;
        r00 = rot.x*rot.x + (1-rot.x*rot.x)*ndot;
        r01 = (1-ndot)*rot.x*rot.y - s*rot.z;
        r02 = (1-ndot)*rot.x*rot.z + s*rot.y;
        r10 = (1-ndot)*rot.x*rot.y + s*rot.z;
        // r11 = ndot + (1-ndot)*rot.y*rot.y;
        r11 = rot.y*rot.y+(1-rot.y*rot.y)*ndot;
        r12 = (1-ndot)*rot.y*rot.z-s*rot.x;
        r20 = (1-ndot)*rot.x*rot.z-s*rot.y;
        r21 = (1-ndot)*rot.y*rot.z+s*rot.x;
        // r22 = ndot + (1-ndot)*rot.z*rot.z;
        r22 = rot.z*rot.z+(1-rot.z*rot.z)*ndot;

        new_u.x = r00*old_u.x + r01*old_u.y + r02*old_u.z;
        new_u.y = r10*old_u.x + r11*old_u.y + r12*old_u.z;
        new_u.z = r20*old_u.x + r21*old_u.y + r22*old_u.z;
        new_v.x = r00*old_v.x + r01*old_v.y + r02*old_v.z;
        new_v.y = r10*old_v.x + r11*old_v.y + r12*old_v.z;
        new_v.z = r20*old_v.x + r21*old_v.y + r22*old_v.z;

        /*float3 tmp;
        tmp.x = r00*new_norm.x + r01*new_norm.y + r02*new_norm.z;
        tmp.y = r10*new_norm.x + r11*new_norm.y + r12*new_norm.z;
        tmp.z = r20*new_norm.x + r21*new_norm.y + r22*new_norm.z;*/

        float t = dot(new_norm, old_norm);

        //LOG_LINE(cout, "ndot = " << ndot << "; s = " << s << "; new_norm = " << new_norm << ", " << old_norm << ", t = " << t);
}

// Given a curvature tensor, find principal directions and curvatures
// Makes sure that pdir1 and pdir2 are perpendicular to normal
// Szymon Rusinkiewicz
// Princeton University
__device__ __host__
void diagonalize_curv(const float3 &old_u, const float3 &old_v,
    float ku, float kuv, float kv,
    const float3 &new_norm,
    float3 &pdir1, float3 &pdir2, float &k1, float &k2)
{
    float3 r_old_u, r_old_v;
    rot_coord_sys(old_u, old_v, new_norm, r_old_u, r_old_v);

    float c = 1, s = 0, tt = 0;
    if (kuv != 0.0f) 
    {
        // Jacobi rotation to diagonalize
        float h = 0.5f * (kv - ku) / kuv;
        tt = (h < 0.0f) ?
            1.0f / (h - sqrt(1.0f + h*h)) :
        1.0f / (h + sqrt(1.0f + h*h));
        c = 1.0f / sqrt(1.0f + tt*tt);
        s = tt * c;
    }

    k1 = ku - tt * kuv;
    k2 = kv + tt * kuv;

    if (fabs(k1) >= fabs(k2)) 
    {
        pdir1 = c*r_old_u - s*r_old_v;
    } 
    else 
    {
        swap(k1, k2);
        pdir1 = s*r_old_u + c*r_old_v;
    }
    pdir2 = cross(new_norm, pdir1);
}

__device__ __host__
void estimate_tensors (const int valence,
                       const float3 *V, const float3 *nv,
                       float3 &base_a1, float3 &base_a2, float3 &base_n,
                       float3 &pdir1, float3 &pdir2,
                       float &return_a11, float &return_a12, float &return_a22,
                       float &return_b11, float &return_b12, float &return_b22,
                       float &k1, float &k2)
{
    float3 t,b,n,ee;

    n = nv[0];
    t = V[1] - V[0];
    ee = t;
    normalize(t);
    b = cross(n, t);

    float m[3] = {0, 0, 0};
    float w[3][3] = { {0,0,0}, {0,0,0}, {0,0,0} };

    float u = dot(ee, t);
    float v = dot(ee, b);

    for (int j = 0; j < valence; j++) 
    {
        float3 e1 = V[(j+1)%valence+1] - V[0];
        float u1 = dot(e1, t);
        float v1 = dot(e1, b);
        w[0][0] += u*u;
        w[0][1] += u*v;
        w[2][2] += v*v;
        float3 dn = nv[j+1] - nv[0];
        float dnu = dot(dn, t);
        float dnv = dot(dn, b);
        m[0] += dnu*u;
        m[1] += dnu*v + dnv*u;
        m[2] += dnv*v;
        u = u1; v = v1;
    }

    w[1][1] = w[0][0] + w[2][2];
    w[1][2] = w[0][1];

    float diag[3];
    float b11, b12, b22;

    if (ldltdc(w, diag)) 
    { 
        ldltsl(w, diag, m, m);
        b11 = m[0], b12 = m[1], b22 = m[2];
    } 
    else 
    {
        // TODO: how this case should be handled? (Ting)
    }


    diagonalize_curv(t, b, b11, b12, b22,
        n, pdir1, pdir2, k1, k2);

    normalize(pdir1);
    normalize(pdir2);

    // TODO: Verificar inversao de sinais em k1 em k1
    k1 *= -1;
    k2 *= -1;

    return_a11 = dot(base_a1, base_a1);
    return_a12 = dot(base_a1, base_a2);
    return_a22 = dot(base_a2, base_a2);

    proj_tens(pdir1, pdir2, cross(pdir1, pdir2), k1, 0, 0, k2, base_a1,
              base_a2, base_n, return_b11, return_b12, return_b12, return_b22);

    return;
}
#endif
// END - rusinkiewicz implementation ------------------------------------------

// BEGIN - tensors estimation -------------------------------------------------
#if 1
__device__ __host__
    void _computeVertexTensors(long2 &idx, long size, int valence,
                               float3 *const s_vertexes,
                               float3 *const s_vertexesNormals,
                               const long *const vPos,
                               const long4 *const vertexesMetrics,
                               float3 *const pdir1s, float3 *const pdir2s,
                               float4 *const metricTensors,
                               float4 *const contravMetricTensors,
                               float4 *const curvTensors, float2 *const ks)
{
    float3 v[9];
    float3 nv[9];
    v[0] = s_vertexes[vPos[idx.y]];
    nv[0] = s_vertexesNormals[vPos[idx.y]];

    // Compute neighbour vertexes normal and update their positions to half way
    // into the edge.
    // TODO: This could be done when coalescing the memory.
    for ( int i = 1; i <= valence; ++i)
    {
		long posIdx = (i-1)*size + idx.y;
        // neighbour position is half way into the edge
        if (vPos[posIdx] == -1)
        {
            break;
        }
        else
        {
            float3 adj = s_vertexes[vPos[posIdx]];
            v[i] = v[0] + (adj - v[0])*0.5;
			nv[i] = s_vertexesNormals[vPos[posIdx]];
        }
    }
    
    float3 a;
    {
        float3 base_a[3];
        {
            long4 vertexMetric = vertexesMetrics[idx.y];
            estimateBases_a(vertexMetric, v[0], nv[0],
                            s_vertexes, base_a);
        }

        {
            float3 pdir1, pdir2, b;
            float2 k;
            estimate_tensors (valence, v, nv, base_a[0],
                              base_a[1], base_a[2], pdir1, pdir2, a.x, a.y,
                              a.z, b.x, b.y, b.z, k.x, k.y);

            metricTensors[idx.y] = make_float4(a.x, a.y, a.y, a.z);
            curvTensors[idx.y] = make_float4(b.x, b.y, b.y, b.z);
            pdir1s[idx.y] = pdir1;
            pdir2s[idx.y] = pdir2;
            ks[idx.y] = k;
        }
    }

    // Calcula tensor metrico contravariante
    float area_det_a = (a.x*a.z) - (a.y*a.y);
    float4 c_a;

    if (fabs(area_det_a) > 1e-6) 
    {
        c_a = make_float4(a.z/area_det_a, a.y/area_det_a, a.y/area_det_a,
                          a.x/area_det_a);
    } 
    else 
    {
        c_a = make_float4(0);
    }
    contravMetricTensors[idx.y] = c_a;
}

__device__ __host__
void copySharedMemory(long size, int valence, long blockStart, long tpb /*threads per block*/,
                      long threadIndex, long blockSize, long blockPosition,
                      float3 *sharedVertexes, float3 *sharedVertexesNormals,
                      float3 *shared, float3 **s_vertexes,
                      float3 **s_vertexesNormals)
{
    long validThreadsInBlock = ((blockStart+tpb<size) ? tpb : size-blockStart);
    *s_vertexes = (float3 *)shared;
    *s_vertexesNormals = *s_vertexes + blockSize;

	// Try a coalesced memory copy
	for (int pos = threadIndex; pos < blockSize; pos += validThreadsInBlock)
	{
		(*s_vertexes)[pos] = sharedVertexes[blockPosition + pos];
		(*s_vertexesNormals)[pos] = sharedVertexesNormals[blockPosition + pos];
	}
}

__global__
void computeVertexTensors(long size, int valence,
                       const long *const blockSizes,
                       const long *const blockPositions,
					   const long *const vertexPositions,
                       float3 *const sharedVertexes,
                       float3 *const sharedVertexesNormals,
                       const long4 *const vertexesMetrics,
                       float3 *const pdir1s, float3 *const pdir2s,
                       float4 *const metricTensors,
                       float4 *const contravMetricTensors,
                       float4 *const curvTensors, float2 *const ks)
{
    long2 vertexIdx;
    vertexIdx.x = threadIdx.x;
    vertexIdx.y = blockDim.x*blockIdx.x + threadIdx.x;

    // Get shared memory array positions
    extern __shared__ float3 shared[];
    float3 *s_vertexes;
    float3 *s_vertexesNormals;

    // Every thread in the block copy some of this block's data to shared memory
    if (vertexIdx.y < size)
    {
        copySharedMemory(size, valence, (blockDim.x*blockIdx.x), blockDim.x, threadIdx.x,
                         blockSizes[blockIdx.x], blockPositions[blockIdx.x],
                         sharedVertexes, sharedVertexesNormals, shared,
                         &s_vertexes, &s_vertexesNormals);
    }

    // Wait copy to be over
    __syncthreads();

    if (vertexIdx.y < size)
        _computeVertexTensors(vertexIdx, size, valence,
                              s_vertexes, s_vertexesNormals, vertexPositions,
                              vertexesMetrics, pdir1s, pdir2s, metricTensors,
                              contravMetricTensors, curvTensors, ks);
}

void cu_computeTensors(long size, const size_t sharedMemSize, int valence,
                       const long *const blockSizes,
                       const long *const blockPositions,
                       const long *const vertexPositions,
                       float3 *const sharedVertexes,
                       float3 *const sharedVertexesNormals,
                       const long4 *const vertexesMetrics,
                       float3 *const pdir1s, float3 *const pdir2s,
                       float4 *const metricTensors,
                       float4 *const contravMetricTensors,
                       float4 *const curvTensors, float2 *const ks,
                       int numBlocks, int gpu)
{
    if (gpu)
    {
        computeVertexTensors<<<numBlocks, BLOCK_SIZE, sharedMemSize>>>(size, valence, blockSizes,
                                                                       blockPositions,
                                                                       vertexPositions,
                                                                       sharedVertexes,
                                                                       sharedVertexesNormals,
                                                                       vertexesMetrics, pdir1s,
                                                                       pdir2s, metricTensors,
                                                                       contravMetricTensors,
                                                                       curvTensors, ks);
        cudaThreadSynchronize();
        LOG_LINE(std::cout,"Tensors: " 
                 << cudaGetErrorString(cudaGetLastError()));
    }
    else
    {
        for (int block = 0; block < numBlocks; ++block)
        {
            // Simulate thread block shared memory
            // Get shared memory array positions
            char *aux = new char[sharedMemSize];
            float3 *s_vertexes;
            float3 *s_vertexesNormals;

            for (long thread = 0; thread < BLOCK_SIZE; ++thread)
            {
                long2 vertexIdx = {thread, block*BLOCK_SIZE+thread};
                if (vertexIdx.y >= size) break;

                copySharedMemory(size, valence, block*BLOCK_SIZE, BLOCK_SIZE, thread,
                                 blockSizes[block], blockPositions[block],
                                 sharedVertexes, sharedVertexesNormals,
                                 (float3 *)aux, &s_vertexes, &s_vertexesNormals);
            }

            for (long thread = 0; thread < BLOCK_SIZE; ++thread)
            {
                long2 vertexIdx = {thread, block*BLOCK_SIZE+thread};
                if (vertexIdx.y >= size) break;
            
                _computeVertexTensors(vertexIdx, size, valence, s_vertexes, s_vertexesNormals,
                                      vertexPositions, vertexesMetrics, pdir1s, pdir2s, metricTensors,
                                      contravMetricTensors, curvTensors, ks);
            }

            delete [] aux;
        }
    }
}

#endif
// END - tensors estimation ---------------------------------------------------

} // namespace cusharing
} // namespace desmo
