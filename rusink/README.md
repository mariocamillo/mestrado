
This folder contains 2 different applications that implement Rusinkiewicz algorithm for curvature tensor estimation.
The folder `cuda` has the implementation in CUDA using shared memory to mitigate the number of accesses to the main memory when traversing a vertex 1-ring neighborhood.
Folder `opengl+glsl` has two different implementations in OpenGL.
The first is a translation of Griffin et al.'s algorithm in GLSL while the second is an implementation using tessellation shaders.

## How to build

The process to build both applications is quite similar.
First make sure you have the required pre-requisites installed:

* OpenGL
* CUDA

And, in Linux only:

* glfw3
* X11
* glew
* metis

With the pre-requisites satisfied, run CMake to generate Visual Studio/make files.

```bash
$ cd cuda
```
or
```bash
$ cd opengl+glsl
```
then
```bash
$ mkdir build && cd build
$ cmake ../
```
This will generate a **.sln** file in Windows which can be use with Visual Studio, or a **Makefile** in linux.
Building from one of those the following binaries should be available: `desmo` or `desmo_glsl`

## How to run

### CUDA
The binary `desmo` takes two positional arguments: the number of iterations; and the size of the mesh.
So, running like below
```
$ ./desmo 10 100
```
Would run the computations for a 100x100 saddle mesh 10 times.

### OpenGL+GLSL

The binary `desmo_glsl` takes three positional arguments: the number of iterations; the number of meshes; and the size of the mesh.
So, running like below
```
$ ./desmo 10 2 100
```
Would run the computations for 2 100x100 saddle meshes 10 times.