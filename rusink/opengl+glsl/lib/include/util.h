/*

	Copyright 2011 Etay Meiri

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _UTIL_H
#define _UTIL_H


#include <GL/glew.h>
#ifdef WIN32
#include <Windows.h> // avoid warning of APIENTRY redefinition in glfw
#else
#include <unistd.h>
#endif
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <map>
#include <string>
#include <sstream>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>

#define ZERO_MEM(a) memset(a, 0, sizeof(a))

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

#ifdef WIN32
#define SNPRINTF _snprintf_s
#define RANDOM rand
#define SRANDOM srand((unsigned)time(NULL))
#else
#define SNPRINTF snprintf
#define RANDOM random
#define SRANDOM srandom(getpid())
#endif

#define INVALID_OGL_VALUE 0xFFFFFFFF

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }

#define GLExitIfError()                                                          \
{                                                                               \
    GLenum Error = glGetError();                                                \
                                                                                \
    if (Error != GL_NO_ERROR) {                                                 \
        printf("OpenGL error in %s:%d: 0x%x\n", __FILE__, __LINE__, Error);     \
        exit(0);                                                                \
    }                                                                           \
}

#define GLCheckError() (glGetError() == GL_NO_ERROR)

#define clean_pointer_vector(vec) \
    while (!vec.empty())          \
    {                             \
        delete *(vec.begin());    \
        vec.erase(vec.begin());   \
    }

/**
 * Class to map a half-mesh vertex ID to a VBO vertex index
 */
class IDIdxMap
{
public:
    IDIdxMap() : mLastIdx(0) {}
    ~IDIdxMap() {}

    /**
     * Gets the VBO vertex index given the half-mesh vertex id
     */
    GLulong getIdx(long id)
    {
        GLulong idx;
        std::map<long, GLulong>::const_iterator it = mMap.find(id);
        if (it != mMap.end())
        {
            idx = it->second;
        }
        else
        {
            idx = mLastIdx++;
            mMap[id] = idx;
        }

        return idx;
    }
private:
    std::map<long, GLulong> mMap;
    GLulong mLastIdx;
};

/**
 * Simple helper function to print a 4 elements vector
 *
 * @param [in] v The vector to be written
 * @return String with the printed vector
 */
inline std::string printVec(const glm::vec4 &v)
{
    std::stringstream ss;
    ss << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
    return ss.str();
}

/// Structure to store mesh information ready to be used by OpenGL
template<class VertexInfoType>
struct MeshInfo {
    MeshInfo() : vertices(NULL), verticesByteLen(0), indices(NULL), indicesByteLen(0)
    {
    }

    ~MeshInfo()
    {
        delete [] vertices;
        delete [] indices;
    }

    VertexInfoType *vertices;
    GLsizei verticesByteLen;
    GLsizei numVertices;
    GLuint *indices;
    GLsizei indicesByteLen;
    GLsizei numIndices;
};

/// Structure to represent a triangle
struct Tri {
  int a,b,c;
};

/**
 * Abstracts a Model-View-Projection UBO
 */
class MVPUBO {
public:
    /**
     * Constructor. Creates the ubo and the matrices mv and p
     */
    MVPUBO() 
    {
        glGenBuffers(1, &m_ubo);
        mv(glm::mat4(1.0f));
        p(glm::mat4(1.0f));
    }

    /**
     * Constructor. Creates the ubo and copy the matrices mv and p
     *
     * @param [in] _mv The Model-View matrix
     * @param [in] _p The Projection matrix
     */
    MVPUBO(const glm::mat4 &_mv, const glm::mat4 &_p)
    {
        glGenBuffers(1, &m_ubo);
        mv(_mv);
        p(_p);
        update();
    }

    ~MVPUBO()
    {
        glDeleteBuffers(1, &m_ubo);
    }

    /// getter
    const glm::mat4 &mv() const
    {
        return m_matrices[0];
    }

    /// getter
    const glm::mat4 &p() const
    {
        return m_matrices[1];
    }

    /// setter
    void mv(const glm::mat4 &matrix) { setMatrix(0, matrix); }

    /// setter
    void p(const glm::mat4 &matrix) { setMatrix(1, matrix); }

    /// getter
    GLuint ubo() const
    {
        return m_ubo;
    }

    /**
     * Updates the UBO value in the GPU
     */
    void update()
    {
        if (m_needsUpdate)
        {
            glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
            glBufferData(GL_UNIFORM_BUFFER, 32*sizeof(float), glm::value_ptr(m_matrices[0]),
                         GL_DYNAMIC_DRAW);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
            m_needsUpdate = false;
        }
    }

private:
    /**
     * Matrix update code to keep track of when an UBO data update is needed
     */
    void setMatrix(int idx, const glm::mat4 &matrix)
    {
        if (m_matrices[idx] == matrix) return;

        m_matrices[idx] = matrix;
        m_needsUpdate = true;
    }

    GLuint m_ubo;
    glm::mat4 m_matrices[2]; // MV and P
    bool m_needsUpdate;
};

/**
 * Structure to manage the OpenGL buffers needed by the application
 */
struct GLBuffers
{
    /// Constructor. Generate OpenGL buffers
    GLBuffers()
    {
        vbosSize = 1;
        glGenBuffers(vbosSize, vbos);
        glGenBuffers(1, &iboTriangles);
    }
    ~GLBuffers()
    {
        cleanup();
        for (GLuint i = 0; i < vbosSize; ++i) vbos[i] = 0;
        iboTriangles = 0;
    }

    /// delete the buffers
    void cleanup()
    {
        glDeleteBuffers(vbosSize, vbos);
        glDeleteBuffers(1, &iboTriangles);
    }

    GLuint vbos[1];
    GLuint vbosSize;
    GLuint iboTriangles;
};

#endif // _UTIL_H
