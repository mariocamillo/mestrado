#include <map>
#include <string>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "shaderLoader.h"

/**
 * Abstracts a Render-to-Texture draw pass in the GPU
 */
class RTTPass {
public:
    /**
     * Construct a RTTPass object that will render to a texture of size
     * width x height.
     *
     * @param width  Width of the output texture.
     * @param height Height of the output texture.
     * @param numOutTexs Number of output textures in this pass.
     */
    RTTPass(GLsizei width, GLsizei height, size_t numOutTexs);
    ~RTTPass();

    /**
     * Creates a shader program using the shader files passed as parameters.
     *
     * If any shader is passed as an empty string the shader program is created
     * without that shader step. Underneath it uses \sa ShaderLoader
     *
     * @param vs  Vertex shader path.
     * @param tcs Tesselation Control shader path.
     * @param tes Tesselation Evaluation shader path.
     * @param gs Geometry shader path.
     * @param fs Fragment shader path.
     *
     * @return Successfulness of the shader program creation.
     */
    bool SetupShaders(const std::string &vs, const std::string &tcs,
                      const std::string &tes, const std::string &gs,
                      const std::string &fs);

    /**
     * Set the OpenGL rendering options for this pass
     */
    bool SetupRenderOptions(bool cull, bool depth, bool stencil, bool wireframe,
                            bool alpha, bool blend,
                            const std::vector<GLenum> &blendState);

    /**
     * Creates the output textures
     *
     * @return True if successfull, false otherwise
     */
    bool SetupOutputs();

    /**
     * Renders the given vbo and ibo to texture with the shaders and options setup.
     *
     * @param vbo The vertex buffer object to be rendered.
     * @param ibo The index buffer object to be rendered.
     * @param mvp_ubo UBO for the MVP matrix.
     * @param numElems Number of elements to be drawn in this call.
     * @param useBarrier Use glBarrier during render call
     *
     * @return Boolean indicating if the render was successful.
     */
    bool Render(GLuint vbo, GLuint ibo, GLuint mvp_ubo, GLuint numElems,
                bool useBarrier);

    /**
     * Reads the output texture back from the GPU.
     *
     * Allocates enough memory to read the texture back and read it using OpenGL.
     * The ownership of the allocated memory is passed to the caller.
     *
     * @return Float4 array with the texture data (row major).
     */
    void ReadTexBack(std::vector<glm::vec4 *> &outTexs);

    ShaderLoader &ShaderProgram() { return m_shaderProg; }

    const std::vector<GLuint> &OutTexs() { return m_outTexs; }

    void addUniform1i(GLint location, GLint value)
    {
        m_uniform1is[location] = value;
    }

    void addUniform2i(GLint location, glm::ivec2 value)
    {
        m_uniform2is[location] = value;
    }

private:
    GLsizei m_width;
    GLsizei m_height;
    GLuint m_fbo;
    GLuint m_mvp_location;
    std::vector<GLuint> m_outTexs;
    ShaderLoader m_shaderProg;
    bool m_cull;
    bool m_depth;
    bool m_stencil;
    bool m_wireframe;
    bool m_alpha;
    bool m_blend;
    std::vector<GLenum> m_blendState;
    std::map<GLint, GLint> m_uniform1is;
    std::map<GLint, glm::ivec2> m_uniform2is;
};