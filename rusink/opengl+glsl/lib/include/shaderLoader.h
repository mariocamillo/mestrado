#ifndef _SHADER_LOADER_H_
#define _SHADER_LOADER_H_

#include <list>
#include <string>
#include <GL/glew.h>

/**
 * Abstracts a shader loading process, from reading the text file to linking
 * and using the program
 */
class ShaderLoader {
public:
    ShaderLoader() :
        m_shaderObjList(),
        m_shaderProgram(0),
        m_Linked(false)
    {
    }

    ~ShaderLoader();

    /**
     * Initilize the loader
     *
     * @return True if initialized, false otherwise
     */
    bool init();

    /**
     * Adds a shader to the shader program
     *
     * @param [in] shaderType The type of the shader
     * @param [in] path String with the path of the shader source code file
     * @return True if the shader was added, false otherwise
     */
    bool addShader(const GLenum shaderType, const std::string &path);

    /**
     * Links the shader program
     *
     * @return True if successfully linked, false otherwise
     */
    bool link();

    /**
     * If the shader program is ready, use it for rendering
     *
     * @return True if using this shader program, false otherwise
     */
    bool use()
    {
        if (!m_Linked)
        {
            return false;
        }

        glUseProgram(m_shaderProgram);
        return true;
    }

    /**
     * Uses the defaul OpenGL shaders for fixed pipeline
     *
     * This affects all shaderLoader objects... be careful when using
     */
    static void useDefaultShaders()
    {
        glUseProgram(0);
    }

    /**
     * Get the location of a uniform variable in the shader program
     *
     * @param [in] uniformName C-string with the uniform variable's name
     */
    GLint getUniformLocation(const GLchar *uniformName)
    {
        return glGetUniformLocation(m_shaderProgram, uniformName);
    }
    
    /**
     * Get the index of a uniform block in the shader program
     *
     * @param [in] blockName C-string with the uniform block's name
     */
    GLint getUniformBlockIndex(const GLchar *blockName)
    {
        return glGetUniformBlockIndex(m_shaderProgram, blockName);
    }

private:
    /**
     * Loads all the shader source code as a single string
     *
     * @param [in] path String with the path to the source code file
     * @return String with the shader code
     */
    std::string loadShaderCode(const std::string &path);

    /**
     * Clears all the shader objects in this class
     */
    void clearShaderObjList();

    typedef std::list<GLuint> ShaderObjList;
    ShaderObjList m_shaderObjList;
    GLuint m_shaderProgram;
    bool m_Linked;
};

#endif // _SHADER_LOADER_H_
