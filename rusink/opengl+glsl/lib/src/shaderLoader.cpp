#include "shaderLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "util.h"

using namespace std;

ShaderLoader::~ShaderLoader()
{
    // Delete the intermediate shader objects that have been added to the program
    // The list will only contain something if shaders were compiled but the object itself
    // was destroyed prior to linking.
    clearShaderObjList();

    if (m_shaderProgram != 0)
    {
        glDeleteProgram(m_shaderProgram);
        m_shaderProgram = 0;
    }
}

bool ShaderLoader::init()
{
    m_shaderProgram = glCreateProgram();

    if (m_shaderProgram == 0) {
        cerr << "Error creating shader program" << endl;
        return false;
    }

    return true;
}

void ShaderLoader::clearShaderObjList()
{
	for (ShaderObjList::iterator it = m_shaderObjList.begin() ; it != m_shaderObjList.end() ; it++)
    {
        glDeleteShader(*it);
    }

	m_shaderObjList.clear();
}

bool ShaderLoader::link()
{
    GLint Success = 0;
    GLchar errorLog[1024] = { 0 };

    glLinkProgram(m_shaderProgram);

	GLExitIfError();

    glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &Success);
	if (Success == 0) {
		glGetProgramInfoLog(m_shaderProgram, sizeof(errorLog), NULL, errorLog);
		cerr << "Error linking shader program: " << errorLog << endl;
        return false;
	}

    glValidateProgram(m_shaderProgram);
    glGetProgramiv(m_shaderProgram, GL_VALIDATE_STATUS, &Success);
    if (!Success) {
        glGetProgramInfoLog(m_shaderProgram, sizeof(errorLog), NULL, errorLog);
        cerr << "Invalid shader program: " << errorLog << endl;
        return false;
    }

    // Delete the intermediate shader objects that have been added to the program
    for (ShaderObjList::iterator it = m_shaderObjList.begin() ; it != m_shaderObjList.end() ; it++)
    {
        glDeleteShader(*it);
    }

    m_shaderObjList.clear();

	m_Linked = true;
    return true;
}

string ShaderLoader::loadShaderCode(const string &path)
{
	// TODO: try, catch -> throw ?
	ifstream shaderFile;
	shaderFile.open(path.c_str(), ios::in | ios::binary);
	stringstream content;
	content << shaderFile.rdbuf();
	return content.str();
}

bool ShaderLoader::addShader(const GLenum shaderType, const string &path)
{
	string shaderText = loadShaderCode(path);
	// TODO: check if shader was correctly loaded

    GLuint shaderObj = glCreateShader(shaderType);
    if (shaderObj == 0) {
        cerr << "Error creating shader type " << shaderType << endl;
        return false;
    }

    // Save the shader object - will be deleted in the destructor
    m_shaderObjList.push_back(shaderObj);

    const GLchar* shaderTexts[1];
    shaderTexts[0] = shaderText.c_str();
    GLint shaderLengths[1];
    shaderLengths[0]= shaderText.size();
    glShaderSource(shaderObj, 1, shaderTexts, shaderLengths);

    glCompileShader(shaderObj);

    GLint success;
    glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLchar infoLog[1024];
        glGetShaderInfoLog(shaderObj, 1024, NULL, infoLog);
        cerr << "Error compiling " << path << " : " << infoLog << endl;
        return false;
    }

    glAttachShader(m_shaderProgram, shaderObj);

    return GLCheckError();
}
