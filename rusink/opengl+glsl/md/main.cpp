#include <iostream>
#include "cpumd.hpp"

using namespace std;

int main(int argc, char **argv)
{
    CPUMD t1(2.f, 1.f, 1.25f, 200, 1234, 5.f);
    CPUMD t2(2.f, 1.f, 1.25f, 200, 5678, 5.f);
    if (t1.CheckCorrectness(t1.Molecules(), 0.0001f) &&
        !t1.CheckCorrectness(t2.Molecules(), 0.0001f))
    {
        cout << "Test PASSED" << endl;
    }
    else
    {
        cout << "Test FAILED" << endl;
    }

    return 0;
}
