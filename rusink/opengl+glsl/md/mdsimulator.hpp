#ifndef _MD_SIMULATOR_H_
#define _MD_SIMULATOR_H_

#include <cstdlib>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>

#define MD_MAX_NEIGHBORS 12

/**
 * Abstract class to represent a Molecular Dynamics (Lennard-Jones forces)
 */
class MDSimulator
{
public:
    /**
     * Initializes the molecules for the simulation. Initial forces and
     * velocities are zeroed.
     *
     * @param [in] squaredCutDist Squared cut distance for neighborhood of a
     *                            molecule
     * @param [in] epsilon Lennard-Jones constant for strength
     * @param [in] sigma Lennard-Jones constant for length
     * @param [in] numMolecules number of molecules to be generated
     * @param [in] seed The seed for the random number generator
     * @param [in] worldEdge Edge of the world where the molecules will be
     *                       generated. Applied to all 3 dimensions
     */
    MDSimulator(const float squaredCutDist, const float epsilon,
                const float sigma, const size_t numMolecules, const int seed,
                const float worldEdge) :
        mSquaredCutDist(squaredCutDist),
        mEpsilon(epsilon),
        mSigma(sigma)
    {
        srand(seed);
        for (size_t i = 0; i < numMolecules; ++i)
        {
            glm::vec4 molecule;
            molecule.x = static_cast<float>(rand()*worldEdge/RAND_MAX);
            molecule.y = static_cast<float>(rand()*worldEdge/RAND_MAX);
            molecule.z = static_cast<float>(rand()*worldEdge/RAND_MAX);
            molecule.w = i;
            mMolecules.push_back(molecule);
        }
    }

    /**
     * Default destructor. Does nothing
     */
    virtual ~MDSimulator() {}

    /**
     * Check the correctness of the calculations
     *
     * @param gt Array with the ground truth positions for the molecules
     * @param epsilon Error margin for the positions
     *
     * @return True if all molecules within the error margin, false otherwise
     */
    virtual bool CheckCorrectness(const std::vector<glm::vec4> &gt,
                                  const float epsilon)
    {
        if (gt.size() != mMolecules.size())
        {
            return false;
        }

        for (size_t i = 0; i < mMolecules.size(); ++i)
        {
            if (glm::any(glm::epsilonNotEqual(gt[i], mMolecules[i], epsilon)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Update the neighborhood of each molecule based on the squared cut
     * distance
     */
    virtual double UpdateNeighbors() = 0;

    /**
     * Update the position of the molecules using Lennard-Jones forces equation
     *
     * @param dt The time step since last update
     */
    virtual void UpdatePositions(const float dt) = 0;

    const std::vector<glm::vec4> &Molecules() const { return mMolecules; }

protected:
    float mSquaredCutDist; ///< Squared distance of neighborhood
    float mEpsilon; ///< Lennard-Jones constant for strength
    float mSigma; ///< Lennard-Jones constant for length
    std::vector<glm::vec4> mMolecules; ///< Array of molecules
};

#endif //_MD_SIMULATOR_H_
