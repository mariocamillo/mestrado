#ifndef _CPU_MD_H_
#define _CPU_MD_H_

#include "mdsimulator.hpp"

class CPUMD : public MDSimulator
{
public:
    CPUMD(const float squaredCutDist, const float epsilon,
          const float sigma, const size_t numMolecules, const int seed,
          const float worldEdge) :
        MDSimulator(squaredCutDist, epsilon, sigma, numMolecules, seed,
                    worldEdge)
    {
        mNeighbors.resize(numMolecules, std::vector<size_t>());
        mForces.resize(numMolecules, glm::vec3(0));
        mMoleculesOld.resize(numMolecules, glm::vec3(0));
        for (size_t i = 0; i < numMolecules; ++i)
        {
            mMoleculesOld[i] = glm::vec3(mMolecules[i]);
            mNeighbors[i].reserve(MD_MAX_NEIGHBORS);
        }
    }


    virtual double UpdateNeighbors()
    {
        for (size_t i = 0; i < mMolecules.size(); ++i)
        {
            mNeighbors[i].clear();
            for (size_t j = 0; j < mMolecules.size(); ++j)
            {
                if (j == i)
                {
                    continue;
                }
                float dx = mMolecules[i].x - mMolecules[j].x;
                float dy = mMolecules[i].y - mMolecules[j].y;
                float dz = mMolecules[i].z - mMolecules[j].z;
                float squaredDist = dx*dx + dy*dy + dz*dz;
                if (squaredDist <= mSquaredCutDist)
                {
                    mNeighbors[i].push_back(j);
                }

                if (mNeighbors[i].size() >= MD_MAX_NEIGHBORS)
                {
                    break;
                }
            }
        }
    }

    virtual void UpdatePositions(const float dt)
    {
        // Calculate force using Lennard-Jones
        const float sigma6 = mSigma*mSigma*mSigma*mSigma*mSigma*mSigma;
        const float lj2 = 24*mEpsilon*sigma6;
        const float lj1 = 2*lj2*sigma6;
        for (size_t i = 0; i < mMolecules.size(); ++i)
        {
            mForces[i] = glm::vec3(0);
            for (size_t j = 0; j < mNeighbors[i].size(); ++j)
            {
                size_t n = mNeighbors[i][j];
                float dx = mMolecules[i].x - mMolecules[n].x;
                float dy = mMolecules[i].y - mMolecules[n].y;
                float dz = mMolecules[i].z - mMolecules[n].z;
                float r2inv = 1.f/(dx*dx + dy*dy + dz*dz);
                float r6inv = r2inv*r2inv*r2inv;
                float lj = r2inv*r6inv*(lj1*r6inv - lj2);
                mForces[i].x += dx*lj;
                mForces[i].y += dy*lj;
                mForces[i].z += dz*lj;
            }
        }

        // Update position using simple Verlet integration
        for (size_t i = 0; i < mMolecules.size(); ++i)
        {
            glm::vec3 old = mMoleculesOld[i];
            glm::vec3 cur = glm::vec3(mMolecules[i]);
            mMolecules[i] = glm::vec4(2.f*cur - old + mForces[i]*dt,
                                      mMolecules[i].w);
            mMoleculesOld[i] = cur;
        }
    }


private:
    std::vector<std::vector<size_t> > mNeighbors;
    std::vector<glm::vec3> mForces;
    std::vector<glm::vec3> mMoleculesOld;
};

#endif // _CPU_MD_H_
