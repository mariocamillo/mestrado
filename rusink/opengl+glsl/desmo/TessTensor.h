#ifndef _TESS_TENSOR_H_
#define _TESS_TENSOR_H_

#include <string>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "util.h"
#include "mesh.h"
#include "Timer.h"
#include "TensorEstimator.h"
#include "shaderLoader.h"

// curvature, cv-metric, metric, pdirk1, pdirk2
#define NUM_TEX_OUTPUTS 5

/**
 * Implements a metric and curvature tensors estimation algorithm based on
 * Rusinkiewicz' method using tessellation shaders to access the 1-ring
 * neighborhood of the vertices
 */
class TessTensor : public TensorEstimator {
public:
    TessTensor();
    ~TessTensor();

    /**
     * Initialize the necessary buffer objects indices, creates the output
     * textures for the shaders and the IBO from the half-mesh
     *
     * @param [in] hmesh The half-mesh to have its tensors estimated
     * @param [in] vertexMap Maps vertex half-mesh ID to VBO index
     * @param [in] mvp_ubo UBO that points to the shaders' Model-View-Projection
     *                     transformation matrices
     * @param [in] vbo The OpenGL VBO index for the mesh
     * @param [in] ibo The OpenGL IBO index for the mesh. For
     *                 interface compatibility
     * @return The elapsed time of the function
     */
    double Setup(Mesh &hmesh, IDIdxMap &vertexMap, GLuint mvp_ubo, GLuint vbo,
                 GLuint ibo)
    {
        START_FUNCTION_TIMER();

        m_vbo = vbo;
        m_mvp_ubo = mvp_ubo;
        iboFromHalfMesh(hmesh, vertexMap);
        setOutputObjects();

        RETURN_FUNCTION_TIMER();
    }
    
    /**
     * Use tessellation shader to calculate the curvature tensors of
     * the previously set up mesh
     *
     * @param [in] useBarrier Use an OpenGL barrier to prevent starting new
     *                        calculations while the last one hasn't finished
     * @return The elapsed time of the function
     */
    double calculateTensors(bool useBarrier);

    /**
     * Reads the calculated tensors back from the GPU
     *
     * @return The elapsed time of the function
     */
    double readTensorsBack();

    /**
     * Compares the calculated tensors with the expected results for the
     * half-mesh
     *
     * @param [in] hmesh The half-mesh. This data structure contains the
     *                   tensors ground truth
     * @param [in] vertexMap Map half-mesh vertex ID to VBO vertex index
     * @param [out] minError The minumum encountered error
     * @param [out] maxError The maximum encountered error
     * @param [out] avgError The average encountered error
     * @return Elapsed time of the function
     */
    double compareResults(Mesh &hmesh, IDIdxMap &vertexMap, double &minError,
                          double &maxError, double &avgError);

    // XXX: Do nothing for now
    virtual double debugDraw(int width, int height) { return 0.0f; }

    /**
     * Returns if the class is initialized and ready for use
     * 
     * @return True if initialized, false otherwise
     */
    bool isInit() { return m_init; }

    /**
     * Get the ith shader program used by the class
     *
     * @param [in] i The index of the shader program to be retrieved
     *
     * @return The ShaderLoader object on the specified index
     */
    ShaderLoader &getShaderProgram() { return m_shaderProgram; }

private:
    static GLuint getMaxNeighbours(const vector<VertexID> &vertexes, Mesh &halfmesh);

    /**
     * Creates the output textures used by the shaders
     */
    void setOutputObjects();

    /**
     * Creates an IBO for the tessellation shaders from a half-mesh
     *
     * @param [in] hmesh The half-mesh
     * @param [in] vertexMap Half-mesh vertex ID to VBO vertex index map
     */
    void iboFromHalfMesh(Mesh &hmesh, IDIdxMap &vertexMap);

    /**
     * Initializes the class
     *
     * @param [in] vertex String with the path to the vertex shader
     * @param [in] tessCtrl String with the path to the tessellation control shader
     * @param [in] tessEval String with the path to the tessellation evaluation shader
     * @param [in] geom String with the path to the geometry shader
     * @param [in] frag String with the path to the fragment shader
     * @return True if successfully initialized, false otherwise
     */
    bool init(const std::string &vertex, const std::string &tessCtrl, const std::string tessEval,
              const std::string &geom, const std::string &frag);

    /**
     * Read an SSBO back from the GPU. Used for debugging purposes
     *
     * @param [in] ssbo The OpenGL SSBO index
     * @param [in] bindingPoint The SSBO binding point
     * @param [out] dest Array to receive the SSBO data
     * @param [in] byteLen Amount of bytes to read from the SSBO
     * @return True if read successfully, false otherwise
     */
    bool readSSBO(GLuint ssbo, GLuint bindingPoint, void *dest, GLsizei byteLen);

    /**
     * Read texture back from the GPU
     *
     * @param tex The OpenGL texture index
     * @param dest Array to receive the texture data
     * @return True if read successfully, false otherwise
     */
    bool readTex(GLuint tex, void *dest);

    /**
     * Calculates the error between two 4 elements vectors
     *
     * @param [in] a First vector
     * @param [in] b Second vector
     * @param [in] outStr String to be written together with the erro results
     * @param [out] minError updatable min error
     * @param [out] maxError updatable max error
     * @param [out] avgError updatable average error
     */
    void calculateVecError(const glm::vec4 &a, const glm::vec4 &b, const std::string &outStr,
                           double &minError, double &maxError, double &avgError);

    bool m_init;
    GLuint m_vbo;
    GLuint m_fbo;
    GLuint m_renderTexs[NUM_TEX_OUTPUTS];
    GLuint m_ibo;
    GLuint m_mvp_ubo;
    GLint m_mvp_location;
    GLuint m_numElements;
    GLuint m_numVertices;
    GLuint m_maxNeighbours;
    glm::ivec2 m_texSize;
    glm::vec4 *m_tensors[NUM_TEX_OUTPUTS];
    ShaderLoader m_shaderProgram;

    // XXX: debug buffers
    GLuint m_vertex_ssbo, m_tessctrl_ssbo, m_tesseval_ssbo;
};

#endif // _TESS_TENSOR_H_
