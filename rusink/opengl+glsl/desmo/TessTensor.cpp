#include "TessTensor.h"
#include <cmath>
#include <glm/ext.hpp>
#include "deforma.h"

#define ADD_SHADER(type, path) \
    (!path.empty() ? m_shaderProgram.addShader(type, path) : true)

static inline int int_round(double a)
{
    return static_cast<int>((a - floor(a) >= 0.5F ) ? ceil(a) : floor(a));
}

static inline glm::ivec2 square_approximation(int a)
{
    double root = std::sqrt(a);
    return glm::ivec2(int_round(root), ceil(root));
}

using namespace glm;

TessTensor::TessTensor() :
    m_vbo(0),
    m_ibo(0),
    m_fbo(0),
    m_mvp_ubo(0),
    m_mvp_location(GL_INVALID_VALUE),
    m_numElements(0),
    m_numVertices(0),
    m_maxNeighbours(0)
{
    for (int i = 0; i < NUM_TEX_OUTPUTS; ++i)
    {
        m_tensors[i] = NULL;
    }
    m_init = init("./ttc_vertex.glsl", "./ttc_tess_control.glsl", "./ttc_tess_eval.glsl", "",
                  "./ttc_frag.glsl");
}

TessTensor::~TessTensor()
{
    for (int i = 0; i < NUM_TEX_OUTPUTS; ++i)
    {
        delete [] m_tensors[i];
    }
}

bool TessTensor::init(const std::string &vertex, const std::string &tessCtrl,
                      const std::string tessEval, const std::string &geom, const std::string &frag)
{
    if (m_shaderProgram.init() &&
        ADD_SHADER(GL_VERTEX_SHADER, vertex) &&
        ADD_SHADER(GL_TESS_CONTROL_SHADER, tessCtrl) &&
        ADD_SHADER(GL_TESS_EVALUATION_SHADER, tessEval) &&
        ADD_SHADER(GL_GEOMETRY_SHADER, geom) &&
        ADD_SHADER(GL_FRAGMENT_SHADER, frag) &&
        m_shaderProgram.link())
    {
        m_mvp_location = m_shaderProgram.getUniformBlockIndex("MVP");
        return true;
    }

    return false;
}


double TessTensor::calculateTensors(bool useBarrier)
{
    START_FUNCTION_TIMER();

    m_shaderProgram.use();

    // setup openGL
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_ALPHA_TEST);
    glViewport(0, 0, m_texSize.x, m_texSize.y); // setup viewport (equal to textures size)
    // make a single patch have the vertex, the bases and the neighbours
    glPatchParameteri(GL_PATCH_VERTICES, m_maxNeighbours + 5);

    // Wait all writes to shader storage to finish
    if (useBarrier) {
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT |
                        GL_TEXTURE_FETCH_BARRIER_BIT |
                        GL_TEXTURE_UPDATE_BARRIER_BIT);
    }

    glUniform2i(m_shaderProgram.getUniformLocation("size"), m_texSize.x, m_texSize.y);
    glUniform2f(m_shaderProgram.getUniformLocation("vertexStep"), (umax - umin)/divisoes,
                (vmax-vmin)/divisoes);
   
    // Bind buffers
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBindBufferBase(GL_UNIFORM_BUFFER, m_mvp_location, m_mvp_ubo);

    // Make textures active
    for (int i = 0; i < NUM_TEX_OUTPUTS; ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, m_renderTexs[i]);
    }

    // XXX: debug buffers
    /*glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, m_vertex_ssbo);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, m_tessctrl_ssbo);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, m_tesseval_ssbo);*/

    // no need to pass index array 'cause ibo is bound already
    glDrawElements(GL_PATCHES, m_numElements, GL_UNSIGNED_INT, 0);
    RETURN_FUNCTION_TIMER();
}

static void printDebugBuffer(glm::vec4 *buffer, GLuint len, std::string bufferName)
{
    for (GLuint i = 0; i < len; ++i)
    {
        if (buffer[i].x != -1)
            cout << "Vertex #" << i << " " << bufferName << ": " << printVec(buffer[i]) << endl;
    }
}


double TessTensor::readTensorsBack()
{
    START_FUNCTION_TIMER();

    // Wait all writes to shader storage and texture to finish
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_TEXTURE_UPDATE_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT);

    for (int i = 0; i < NUM_TEX_OUTPUTS; ++i)
    {
        if (m_tensors[i] == NULL) {
            m_tensors[i] = new glm::vec4[m_texSize.x*m_texSize.y];
        }
        memset(m_tensors[i], 0, m_texSize.x*m_texSize.y*sizeof(glm::vec4));
        readTex(m_renderTexs[i], m_tensors[i]);
    }

    // XXX: debug vectors
    /*glm::vec4 *debug = new glm::vec4[m_numVertices];
    readSSBO(m_vertex_ssbo, 4, debug, m_numVertices*sizeof(glm::vec4));
    printDebugBuffer(debug, m_numVertices, "vertexDebug");
    readSSBO(m_tessctrl_ssbo, 5, debug, m_numVertices*sizeof(glm::vec4));
    printDebugBuffer(debug, m_numVertices, "ctrlDebug");
    readSSBO(m_tesseval_ssbo, 5, debug, m_numVertices*sizeof(glm::vec4));
    printDebugBuffer(debug, m_numVertices, "evalDebug");
    delete [] debug;*/
    RETURN_FUNCTION_TIMER();
}

double TessTensor::compareResults(Mesh &hmesh, IDIdxMap &vertexMap, double &minError, double &maxError,
                                 double &avgError)
{
    START_FUNCTION_TIMER();

    vector<VertexID> vertices;
    double minAErr, maxAErr, avgAErr, minBErr, maxBErr, avgBErr;

    minAErr = minBErr = 99999;
    maxAErr = maxBErr = 0.;
    avgAErr = avgBErr = 0.;
    hmesh.getAllVertices(&vertices);
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        GLulong vertexIdx = vertexMap.getIdx(vertices[i]);
        Point3DGeom *vertex = reinterpret_cast<Point3DGeom *>(hmesh.getVGeomPtr(vertices[i]));
        glm::vec4 theoryTensor, vecErr;

        {
            theoryTensor = glm::vec4(vertex->a[0][0], vertex->a[0][1], vertex->a[1][0], vertex->a[1][1]);
            stringstream ss;
            ss.clear();
            ss << "vertex#" << vertexIdx << " metric tensor: ";
            calculateVecError(m_tensors[2][vertexIdx], theoryTensor, ss.str(),
                              minAErr, maxAErr, avgAErr);
        }

        {
            theoryTensor = glm::vec4(vertex->b[0][0], vertex->b[0][1], vertex->b[1][0], vertex->b[1][1]);
            stringstream ss;
            ss.clear();
            ss << "vertex#" << vertexIdx << " curvature tensor: ";
            calculateVecError(m_tensors[0][vertexIdx], theoryTensor, ss.str(),
                              minBErr, maxBErr, avgBErr);
        }

        //cout << "vertex#" << vertexIdx << "pdir1: " << printVec(m_tensors[vertexIdx].pdir_k1) << endl;
    }

    avgAErr /= vertices.size();
    avgBErr /= vertices.size();
    
    cout << "metric tensors: min error " << minAErr << ", max error " << maxAErr << ", avgErr "
         << avgAErr << endl;
    cout << "curvature tensors: min error " << minBErr << ", max error " << maxBErr << ", avgErr "
         << avgBErr << endl;

    RETURN_FUNCTION_TIMER();
}

/*************************************** private functions ***************************************/
GLuint TessTensor::getMaxNeighbours(const vector<VertexID> &vertexes, Mesh &halfmesh)
{
    GLuint max = 0;
    for (vector<VertexID>::const_iterator it = vertexes.begin();
         it != vertexes.end(); ++it)
    {
        vector<VertexID> neighbours;
        halfmesh.getVFaces(*it, &neighbours);
        if (neighbours.size() > max) max = neighbours.size();
    }

    return max;
}

void TessTensor::setOutputObjects()
{
    // Create frame buffer for tensor estimation RTT pass
    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    // Create textures to store estimated tensors
    glGenTextures(NUM_TEX_OUTPUTS, m_renderTexs);
    m_texSize = square_approximation(m_numVertices);
    cout << "Textures size: " << glm::to_string(m_texSize) << endl;
    GLenum drawBuffers[NUM_TEX_OUTPUTS];
    for (int i = 0 ; i < NUM_TEX_OUTPUTS; ++i)
    {
        glBindTexture(GL_TEXTURE_2D, m_renderTexs[i]);
        // 1st 0: level, 2nd 0: no border, 3rd 0: no initial data
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_texSize.x, m_texSize.y, 0, GL_RGBA, GL_FLOAT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // 0: level
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, m_renderTexs[i], 0);
        drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
    }
    glDrawBuffers(NUM_TEX_OUTPUTS, drawBuffers);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        cout << "Error when setting frame buffer" << endl;
        // throw exception?
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // XXX: This are debug buffers to check for correctness
    /*glGenBuffers(1, &m_vertex_ssbo);
    glGenBuffers(1, &m_tesseval_ssbo);
    glGenBuffers(1, &m_tessctrl_ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_vertex_ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, m_numVertices*4*sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_tesseval_ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, m_numVertices*4*sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_tessctrl_ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, m_numVertices*4*sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);*/
}

void TessTensor::iboFromHalfMesh(Mesh &hmesh, IDIdxMap &vertexMap)
{
    std::vector<VertexID> vertices;
    hmesh.getAllVertices(&vertices);
    m_numVertices = vertices.size();

    m_maxNeighbours = getMaxNeighbours(vertices, hmesh);
    GLuint stride = m_maxNeighbours + 5;
    m_numElements = stride*m_numVertices;

    // Allocate and initialize
    GLuint *indices = new GLuint[m_numElements];
    GLuint indicesByteLen = m_numElements*sizeof(GLuint);
    for (GLuint i = 0; i < m_numVertices; ++i)
    {
        GLulong curIdx = vertexMap.getIdx(vertices[i]);
        for (GLuint j = 0; j < stride; ++j)
        {
            // initialize to self
            indices[curIdx*stride + j] = curIdx;
        }
    }

    // Add correct neighbour information.
    for (GLuint i = 0; i < m_numVertices; ++i)
    {
        // current vertex (x,y) coordinate
        // TODO: make this work for arbitrary meshes
        GLulong curIdx = vertexMap.getIdx(vertices[i]);
        int col = vertices[i] % (divisoes+1);
        int line = (int) floor((float)vertices[i] / (divisoes +1 ));

        // base a1
        if (line<divisoes)
        {
            GLulong neighbourIdx = vertexMap.getIdx(vertices[i]+divisoes+1);
            indices[curIdx*stride + 1] = neighbourIdx; //next
            indices[neighbourIdx*stride + 2] = curIdx; //prev
        }
        // base a2
        if (col<divisoes)
        {
            GLulong neighbourIdx = vertexMap.getIdx(vertices[i]+1);
            indices[curIdx*stride + 3] = neighbourIdx; //next
            indices[neighbourIdx*stride + 4] = curIdx; //prev
        }

        // my neighbours
        std::vector<VertexID> neighbours;
        hmesh.getVVertices(vertices[i], &neighbours);
        GLuint iboIdx = 5;
        for (GLuint j = 0; j < neighbours.size() && iboIdx < stride; ++j)
        {
            GLulong neighbourIdx = vertexMap.getIdx(neighbours[j]);
            indices[curIdx*stride + iboIdx++] = neighbourIdx;
        }
    }

    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesByteLen, indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete [] indices;
}
bool TessTensor::readSSBO(GLuint ssbo, GLuint bindingPoint, void *dest, GLsizei byteLen)
{
    memset(dest, 0, byteLen); // clear dest buffer

    // Read back tensors from ssbo
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPoint, ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    GLvoid *data = glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
                                    0,
                                    byteLen,
                                    GL_MAP_READ_BIT);
    if (data != NULL) {
        memcpy(dest, data, byteLen);
    } else {
        false;
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    return true;
}

bool TessTensor::readTex(GLuint tex, void *dest)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, dest);
    glBindTexture(GL_TEXTURE_2D, 0);

    // TODO: check glGetTexImage return values for error
    return true;
}

void TessTensor::calculateVecError(const glm::vec4 &a, const glm::vec4 &b,
                                   const std::string &outStr, double &minError,
                                   double &maxError, double &avgError)
{
    double err;
    glm::vec4 vecErr;

    vecErr = a - b;
    err = glm::length(vecErr);
    cout << outStr << printVec(a) << " - " << printVec(b) << " = " << printVec(vecErr) << " -> "
         << err << endl;;
    if (err < minError) { minError = err; }
    if (err > maxError) { maxError = err; }
    avgError += err;
}
