#ifndef _BLEND_TENSOR_H_
#define _BLEND_TENSOR_H_

#include <string>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "util.h"
#include "mesh.h"
#include "Timer.h"
#include "TensorEstimator.h"
#include "shaderLoader.h"
#include "rttpass.h"

// curvature, pdirk1, pdirk2
#define BLEND_NUM_TEX_OUTPUTS 3

// Intermediate textures
#define BLEND_NUM_INTERMEDIATE_TEX_OUTPUTS 4

// Multiple passes shaders
#define BLEND_NUM_SHADER_PROGS 4

/**
 * Tensor estimator based on implicit 1-ring neighborhood access using
 * alpha blending. The implementation is a port/adaptation from the HLSL
 * original code from Wesley Griffin.
 */
class BlendTensor : public TensorEstimator {
public:
    BlendTensor(GLuint numVertices);
    ~BlendTensor();

    /**
     * Initialize the necessary buffer objects indices and creates the output
     * textures for the shaders
     *
     * @param [in] hmesh The half-mesh to have its tensors estimated. For
     *                   interface compatibility
     * @param [in] vertexMap Maps vertex half-mesh ID to VBO index. For
     *                       interface compatibility
     * @param [in] mvp_ubo UBO that points to the shaders' Model-View-Projection
     *                     transformation matrices
     * @param [in] vbo The OpenGL VBO index for the mesh
     * @param [in] ibo The OpenGL IBO index for the mesh
     * @return The elapsed time of the function
     */
    double Setup(Mesh &hmesh, IDIdxMap &vertexMap, GLuint mvp_ubo, GLuint vbo,
                 GLuint ibo)
    {
        START_FUNCTION_TIMER();

        m_vbo = vbo;
        m_ibo = ibo;
        m_mvp_ubo = mvp_ubo;
        setOutputObjects();

        RETURN_FUNCTION_TIMER();
    }

    /**
     * Use Griffin's shaders algorithm to calculate the curvature tensors of
     * the previously set up mesh
     *
     * @param [in] useBarrier Use an OpenGL barrier to prevent starting new
     *                        calculations while the last one hasn't finished
     * @return The elapsed time of the function
     */
    double calculateTensors(bool useBarrier);

    /**
     * Reads the calculated tensors back from the GPU
     *
     * @return The elapsed time of the function
     */
    double readTensorsBack();

    /**
     * Compares the calculated tensors with the expected results for the
     * half-mesh
     *
     * @param [in] hmesh The half-mesh. This data structure contains the
     *                   tensors ground truth
     * @param [in] vertexMap Map half-mesh vertex ID to VBO vertex index
     * @param [out] minError The minumum encountered error
     * @param [out] maxError The maximum encountered error
     * @param [out] avgError The average encountered error
     * @return Elapsed time of the function
     */
    double compareResults(Mesh &hmesh, IDIdxMap &vertexMap, double &minError,
                          double &maxError, double &avgError);
    /**
     * Same as \sa compareResults, but ignores the error output variables
     */
    double compareResults(Mesh &hmesh, IDIdxMap &vertexMap)
    {
        double a, b, c;
        return compareResults(hmesh, vertexMap, a, b, c);
    }

    // XXX: Do nothing for now
    virtual double debugDraw(int width, int height) { return 0.0f; }

    /**
     * Returns if the class is initialized and ready for use
     * 
     * @return True if initialized, false otherwise
     */
    bool isInit() { return m_init; }

    /**
     * Get the ith shader program used by the class
     *
     * @param [in] i The index of the shader program to be retrieved
     *
     * @return The ShaderLoader object on the specified index
     */
    ShaderLoader &getShaderProgram(int i);

private:
    /**
     * Creates the output textures used by the shaders
     */
    void setOutputObjects();

    /**
     * Initializes the class
     *
     * @return True if successfully initialized, false otherwise
     */
    bool init();

    /**
     * Calculates the error between two 4 elements vectors
     *
     * @param [in] a First vector
     * @param [in] b Second vector
     * @param [in] outStr String to be written together with the erro results
     * @param [out] minError updatable min error
     * @param [out] maxError updatable max error
     * @param [out] avgError updatable average error
     */
    void calculateVecError(const glm::vec4 &a, const glm::vec4 &b, const std::string &outStr,
                           double &minError, double &maxError, double &avgError);

    bool m_init;
    GLuint m_vbo;
    GLuint m_fbo;
    GLuint m_ibo;
    GLuint m_mvp_ubo;
    GLuint m_numVertices;
    glm::ivec2 m_texSize;
    glm::vec4 *m_tensors[BLEND_NUM_TEX_OUTPUTS];
    RTTPass *m_passes[BLEND_NUM_SHADER_PROGS];

    // XXX: debug buffers
    GLuint m_vertex_ssbo, m_tessctrl_ssbo, m_tesseval_ssbo;
};

#endif // _BLEND_TENSOR_H_
