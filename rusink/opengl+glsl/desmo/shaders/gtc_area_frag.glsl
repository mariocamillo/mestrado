#version 430

in GeomData {
	vec4 normalA;
} gOut;

layout(location = 0) out vec4 normalA;

void main()
{
    normalA = gOut.normalA;
}
