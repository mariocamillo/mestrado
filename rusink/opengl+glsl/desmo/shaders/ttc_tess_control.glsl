#version 430
#extension GL_ARB_tessellation_shader : enable

#define MAX_ADJ_FACES 8
#define NEIGHBOURS_START 5

layout(vertices=1) out;

in VertexData
{
    vec4 pos;
    vec4 normal;
    uint index;
    int isBorder;
} input[];

out TCData {
    vec4 pos;
    vec4 normal;
    uint index;
    vec3 t;
    vec3 b;
    vec3 base_a[3];
    float w[3][3];
    float m[3];
} tcOut[];

layout(std140, binding=5) buffer CtrlBuffer {
    vec4 ctrlDebug[];
};

uniform vec2 vertexStep;

#define neighbour(i) (input[0].pos + (input[i].pos - input[0].pos)*0.5)

#define M_PI 3.14159265358979323846f

bool isEqual(vec3 a, vec3 b)
{
    bvec3 ret = equal(a, b);
    return (ret.x && ret.y && ret.z);
}

// BEGIN - Quaternion functions -----------------------------------------------
#if 1
// theory source: http://3dgep.com/?p=1815#Rotations
struct quaternion
{
    vec3 v;
    float  s;
};

quaternion multQuaternion(const quaternion q1, const quaternion q2)
{
    quaternion ret = {q1.s*q2.v + q2.s*q1.v + cross(q1.v, q2.v),
                      q1.s*q2.s - dot(q1.v, q2.v)};
    return ret;
}

quaternion invQuaternion(const quaternion q1)
{
    quaternion ret = {0-(q1.v), q1.s};
    return ret;
}
#endif
// END - Quaternion functions -------------------------------------------------

// BEGIN - base_a estimation --------------------------------------------------
#if 1

void rotateVector(inout vec3 base_a, const vec3 new_n)
{
    float angRot;
    vec3 axisRot;

    axisRot = normalize(cross(base_a, new_n));
    angRot = acos(dot(normalize(base_a), new_n));
    
    if (angRot<M_PI)
    {
        angRot += (- M_PI/2);
    }
    else
    {
        angRot = (-1 * ( 2 * M_PI - angRot) + M_PI/2);
    }

    if (abs(angRot)>0.01)
    {
        quaternion quatRot = {axisRot*sin(angRot/2), cos(angRot/2)};
        quaternion pure = {base_a, 0};
        base_a = multQuaternion(multQuaternion(quatRot, pure),
                                invQuaternion(quatRot)).v;
    }
}

vec3 estimateBase_a(const vec3 next, const vec3 prev,
                    const vec3 vertex, const vec3 vertexNormal, const float normalizer)
{
    vec3 base = vec3(0);
    if (!isEqual(next, vertex)) {
        // Preference: forward finite difference
        base = next - vertex;
    } else if (!isEqual(prev, vertex)) {
        // backward finite difference
        base = vertex - prev;
    }
    rotateVector(base, vertexNormal);

    return base/normalizer;
}

    void estimateBases_a(out vec3 base[3])
{
    base[0] = estimateBase_a(input[1].pos.xyz, input[2].pos.xyz, input[0].pos.xyz,
                             input[0].normal.xyz, vertexStep.x);
    base[1] = estimateBase_a(input[3].pos.xyz, input[4].pos.xyz, input[0].pos.xyz,
                             input[0].normal.xyz, vertexStep.y);
    base[2] = input[0].normal.xyz;
}
#endif
// END - base_a estimation ----------------------------------------------------

vec3 faceNormal(vec3 a, vec3 b, vec3 c)
{
    return normalize(cross(c - a, b - a));
}

vec3 getNeighbourNormal(int j, int firstNeighbour, int lastNeighbour)
{
    vec3 n_normal = vec3(0);
    uint numNormals = 0;
    int back = j == firstNeighbour ? lastNeighbour : j - 1;
    int front = j == lastNeighbour ? firstNeighbour : j + 1;

    if (input[0].isBorder == 0)
    {
        n_normal += faceNormal(input[0].pos.xyz, input[j].pos.xyz, input[front].pos.xyz);
        ++numNormals;
        n_normal += faceNormal(input[0].pos.xyz, input[back].pos.xyz, input[j].pos.xyz);
        ++numNormals;
    }
    else
    {
        if (input[j].isBorder == 0 || input[front].isBorder == 0)
        {
            n_normal += faceNormal(input[0].pos.xyz, input[j].pos.xyz, input[front].pos.xyz);
            ++numNormals;
        }
        if (input[j].isBorder == 0 || input[back].isBorder == 0)
        {
            n_normal += faceNormal(input[0].pos.xyz, input[back].pos.xyz, input[j].pos.xyz);
            ++numNormals;
        }
    }

    return n_normal/numNormals;
}

void main()
{
    int lastNeighbour = 0;
    for (int j = NEIGHBOURS_START; j < gl_PatchVerticesIn; ++j) 
    {
        // We repeat the vertex when there is no neighbour j
        if (isEqual(input[j].pos.xyz, input[0].pos.xyz))
        {
            lastNeighbour = j - 1;
            break;
        }
    }
    if (lastNeighbour == 0)
    {
        lastNeighbour = gl_PatchVerticesIn - 1;
    }

    // Create base from 1st valid neighbour (starts at NEIGHBOURS_START as 0 is the vertex, 1-4 are metric bases)
    vec3 t,b,n;
    n = input[0].normal.xyz;
    t = normalize((neighbour(NEIGHBOURS_START) - input[0].pos).xyz);
    b = cross(n, t);

    float m[3] = {0, 0, 0};
    float w[3][3] = {{0,0,0}, {0,0,0}, {0,0,0}};

    for (int j = NEIGHBOURS_START; j < gl_PatchVerticesIn; ++j) 
    {
        // We repeat the vertex when there is no neighbour j
        if (!isEqual(input[j].pos.xyz, input[0].pos.xyz))
        {
            vec3 e1 = (neighbour(j) - input[0].pos).xyz;
            float u = dot(e1, t);
            float v = dot(e1, b);
            w[0][0] += u*u;
            w[0][1] += u*v;
            w[2][2] += v*v;
            vec3 neighbourNormal = getNeighbourNormal(j, NEIGHBOURS_START, lastNeighbour);
            vec3 dn = normalize(neighbourNormal) - input[0].normal.xyz;
            float dnu = dot(dn, t);
            float dnv = dot(dn, b);
            m[0] += dnu*u;
            m[1] += dnu*v + dnv*u;
            m[2] += dnv*v;
        }
    }

    w[1][1] = w[0][0] + w[2][2];
    w[1][2] = w[0][1];

    if (gl_InvocationID == 0) {
        estimateBases_a(tcOut[gl_InvocationID].base_a);
        tcOut[gl_InvocationID].pos = input[0].pos;
        tcOut[gl_InvocationID].normal = input[0].normal;
        tcOut[gl_InvocationID].index = input[0].index;
        gl_out[gl_InvocationID].gl_Position = input[0].pos;
        tcOut[gl_InvocationID].t = t;
        tcOut[gl_InvocationID].b = b;
        for (int i = 0; i < 3; ++i)
        {
            tcOut[gl_InvocationID].m[i] = m[i];
            for (int j = 0; j < 3; ++j)
            {
                tcOut[gl_InvocationID].w[i][j] = w[i][j];
            }
        }

        gl_TessLevelOuter[0] = 1;
        gl_TessLevelOuter[1] = 1;
        gl_TessLevelOuter[2] = 1;

        gl_TessLevelInner[0] = 1;
    }
}
