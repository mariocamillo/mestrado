#version 430

in GeomData {
	vec3 initialMin;
	vec3 initialMax;
} gOut;

layout(location = 0) out vec4 initialMin;
layout(location = 1) out vec4 initialMax;

void main()
{
    initialMin = vec4(gOut.initialMin,0f);
	initialMax = vec4(gOut.initialMax,0f);
}
