#version 430

#define NEXT(index) ((index < 2) ? index + 1 : index - 2)
#define PREV(index) ((index > 0) ? index - 1 : index + 2)

layout (triangles) in;
layout (points, max_vertices = 3) out;

in VertexData {
    vec3 pos;
    vec3 normal;
	vec2 texCoord;
	float area; 
} vOut[];

out GeomData {
	vec3 curv;
} gOut;

uniform sampler2D initMinTex;
uniform sampler2D initMaxTex;

void edges(vec3 v0, vec3 v1, vec3 v2, out vec3 e[3]) {
    e[0] = v2 - v1;
    e[1] = v0 - v2;
    e[2] = v1 - v0;
}

vec3 compute_corner_area(vec3 v0, vec3 v1, vec3 v2) {
    int index;

    vec3 e[3];
    edges(v0, v1, v2, e);

    float l2[3];
    for (int index = 0; index < 3; index++) {
        l2[index] = dot(e[index], e[index]);
    }

    float ew[3];
    for (index = 0; index < 3; index++) {
        int next1 = (index + 1) % 3;
        int next2 = (index + 2) % 3;
        ew[index] = l2[index] * (l2[next1] + l2[next2] - l2[index]);
    }

    float area = 0.5f * length(cross(e[0], e[1]));
    vec3 corner_area;
	if (ew[0] <= 0.0f) {
		corner_area.y = -0.25f * l2[2] * area / dot(e[0], e[2]);
		corner_area.z = -0.25f * l2[1] * area / dot(e[0], e[1]);
		corner_area.x = area - corner_area.y - corner_area.z;
	} else if (ew[1] <= 0.0f) {
		corner_area.z = -0.25f * l2[0] * area / dot(e[1], e[0]);
		corner_area.x = -0.25f * l2[2] * area / dot(e[1], e[2]);
		corner_area.y = area - corner_area.z - corner_area.x;
	} else if (ew[2] <= 0.0f) {
		corner_area.x = -0.25f * l2[1] * area / dot(e[2], e[1]);
		corner_area.y = -0.25f * l2[0] * area / dot(e[2], e[0]);
		corner_area.z = area - corner_area.x - corner_area.y;
	} else {
		float ewscale = 0.5f * area / (ew[0] + ew[1] + ew[2]);
		corner_area.x = ewscale * (ew[1] + ew[2]);
		corner_area.y = ewscale * (ew[2] + ew[0]);
		corner_area.z = ewscale * (ew[0] + ew[1]);
	}

    return corner_area;
}

void rotate_coordinate_system(vec3 old_u, vec3 old_v, vec3 new_norm, out vec3 rot_u, out vec3 rot_v) {
	rot_u = old_u;
	rot_v = old_v;
	vec3 old_norm = cross(old_u, old_v);
	
	float ndot = dot(old_norm, new_norm);
	if (ndot <= -1.0f) {
		rot_u = -rot_u;
		rot_v = -rot_v;
	} else {
		vec3 perp_old = new_norm - ndot * old_norm;
		vec3 dperp = 1.0f / (1.0f + ndot) * (old_norm + new_norm);
		rot_u -= dperp * dot(rot_u, perp_old);
		rot_v -= dperp * dot(rot_v, perp_old);
	}
}

vec3 project_curvature(vec3 old_u, vec3 old_v, vec3 old_k, vec3 new_u, vec3 new_v) {
	vec3 rot_u, rot_v;
	rotate_coordinate_system(new_u, new_v, cross(old_u, old_v), rot_u, rot_v);
	
	float u1 = dot(rot_u, old_u);
	float v1 = dot(rot_u, old_v);
	float u2 = dot(rot_v, old_u);
	float v2 = dot(rot_v, old_v);

	return vec3(
		old_k[0] * u1 * u1 + old_k[1] * (2.0f * u1 * v1) + old_k[2] * v1 * v1,
		old_k[0] * u1 * u2 + old_k[1] * (u1 * v2 + u2 * v1) + old_k[2] * v1 * v2,
		old_k[0] * u2 * u2 + old_k[1] * (2.0f * u2 * v2) + old_k[2] * v2 * v2
	);
}

// Perform LDL^T decompos of a symmetric positive definite 3x3 matrix.
// Overwrites lower triangle of matrix.
void ldltdc3(inout float A[3][3], inout vec3 rdiag) {
    float v[2];

	for (int i = 0; i < 3; i++) {
		for (int k = 0; k < i; k++) v[k] = A[i][k] * rdiag[k];
		for (int j = i; j < 3; j++) {
			float sum = A[i][j];
			for (int l = 0; l < i; l++) sum -= v[l] * A[j][l];
			if (i == j) rdiag[i] = 1.0f / sum;
			else A[j][i] = sum;
		}
	}
}

// Solve Ax=B after ldltdc.
void ldltsl3(inout float A[3][3], inout vec3 rdiag, inout vec3 B, inout vec3 x) {
    for (int i = 0; i < 3; i++) {
        float sum = B[i];
        for (int k = 0; k < i; k++) sum -= A[i][k] * x[k];
		x[i] = sum * rdiag[i];
	}
	for (int j = 2; j >= 0; j--) {
		float sum = 0;
		for (int k = j + 1; k < 3; k++) sum += A[k][j] * x[k];
		x[j] -= sum * rdiag[j];
	}
}

vec4 texCoord_to_pos(vec2 texCoord) {
	return vec4(-1.0f + 2.0f*texCoord.x, -1.0f + 2.0f*texCoord.y, 0f, 1);
}

void main() {
	int index;

    vec3 e[3];
    edges(vOut[0].pos, vOut[1].pos, vOut[2].pos, e);

    // N-T-B coordinate system
    vec3 t = normalize(e[0]);
    vec3 n = cross(e[0], e[1]);
    vec3 b = normalize(cross(n, t));

    // Estimate curvature based on variation of normals along edges
    vec3 m = vec3(0.0f);
    float w[3][3] = { {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f} };

    for (index = 0; index < 3; index++) {
        float u = dot(e[index], t);
        float v = dot(e[index], b);
        w[0][0] += u * u;
        w[0][1] += u * v;
        w[2][2] += v * v;

        vec3 dN = vOut[PREV(index)].normal - vOut[NEXT(index)].normal;
        float dnu = dot(dN, t);
        float dnv = dot(dN, b);

        m += vec3(dnu * u, dnu * v + dnv * u, dnv * v);
    }
    w[1][1] = w[0][0] + w[2][2];
    w[1][2] = w[0][1];

    // Least squares solution
    vec3 diag = vec3(0.0f);
    ldltdc3(w, diag);
    if (any(isnan(diag))) {
        for (index = 0; index < 3; index++) {
            gl_Position = texCoord_to_pos(vOut[index].texCoord);
            gOut.curv = vec3(0.0f);
            EmitVertex();
        }
        return;
    }
    ldltsl3(w, diag, m, m);

    vec3 corner_area = compute_corner_area(vOut[0].pos, vOut[1].pos, vOut[2].pos);

    for (index = 0; index < 3; index++) {
        float weight = corner_area[index] / vOut[index].area;
        vec3 initial_max = texture2D(initMaxTex, vOut[index].texCoord).xyz;
        vec3 initial_min = texture2D(initMinTex, vOut[index].texCoord).xyz;

        gl_Position = texCoord_to_pos(vOut[index].texCoord);
        gOut.curv = weight * project_curvature(t, b, m, initial_max, initial_min);
        EmitVertex();
    }
}
