#version 430
#extension GL_ARB_shader_storage_buffer_object: require

in TEData {
    vec4 curvTensor;
    vec4 metricTensor;
    vec4 contravMetricTensor;
    vec4 pdir_k1;
    vec4 pdir_k2;
};

layout(location = 0) out vec4 curvature;
layout(location = 1) out vec4 cv_metric;
layout(location = 2) out vec4 metric;
layout(location = 3) out vec4 pdirk1;
layout(location = 4) out vec4 pdirk2;

void main()
{
    curvature = curvTensor;
    cv_metric = contravMetricTensor;
    metric = metricTensor;
    pdirk1 = pdir_k1;
    pdirk2 = pdir_k2;
}
