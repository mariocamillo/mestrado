#version 430

layout(location=0) in vec3 pos;
layout(location=2) in uint index;

out VertexData {
	vec4 maxPDir;
	vec4 minPDir;
} vOut;

uniform ivec2 size;
uniform sampler2D normal_areaTex;
uniform sampler2D curvTex;
uniform sampler2D initMaxTex;
uniform sampler2D initMinTex;

void rotate_coordinate_system(vec3 old_u, vec3 old_v, vec3 new_norm, out vec3 rot_u, out vec3 rot_v) {
	rot_u = old_u;
	rot_v = old_v;
	vec3 old_norm = cross(old_u, old_v);
	
	float ndot = dot(old_norm, new_norm);
	if (ndot <= -1.0f) {
		rot_u = -rot_u;
		rot_v = -rot_v;
	} else {
		vec3 perp_old = new_norm - ndot * old_norm;
		vec3 dperp = 1.0f / (1.0f + ndot) * (old_norm + new_norm);
		rot_u -= dperp * dot(rot_u, perp_old);
		rot_v -= dperp * dot(rot_v, perp_old);
	}
}

//
// Given a curvature tensor, find principle directions and curvatures.
// Ensures MaxPDir and MinPDir are perpendicular to normal.
//
void diagonalize(vec3 normal, vec3 k, vec3 initial_max, vec3 initial_min, out vec4 pdir[2]) {
    vec3 rot_u, rot_v;
	rotate_coordinate_system(initial_max, initial_min, normal, rot_u, rot_v);

    float c = 1.0f, s = 0.0f, tt = 0.0f;
    if (k[1] != 0.0f) {
        // Jacobi rotation to diagonalize
        float h = 0.5f * (k[2] - k[0]) / k[1];
        float hsqrt = sqrt(1.0f + h * h);
        tt = (h < 0.0f) ? 1.0f / (h - hsqrt) : 1.0f / (h + hsqrt);
        c = 1.0f / sqrt(1.0f + tt * tt);
        s = tt * c;
    }

    pdir[0].w = k[0] - tt * k[1];
    pdir[1].w = k[2] + tt * k[1];

    if (abs(pdir[0].w) >= abs(pdir[1].w)) {
        pdir[0].xyz = c * rot_u - s * rot_v;
    } else {
        float temp = pdir[0].w;
        pdir[0].w = pdir[1].w;
        pdir[1].w = temp;
        pdir[0].xyz = s * rot_u + c * rot_v;
    }

    pdir[1].xyz = cross(normal, pdir[0].xyz);
}

vec2 index_to_texCoord(uint index) {	
    // Index to x,y position in texture (x,y).
    vec2 pos = vec2(index%size.x, index/size.x);
	
	// Make it [0,1] range
	return vec2(pos.x/size.x, pos.y/size.y);
}

vec4 texCoord_to_pos(vec2 texCoord) {
	return vec4(-1.0f + 2.0f*texCoord.x, -1.0f + 2.0f*texCoord.y, 0f, 1);
}

void main() {
	vec2 texCoord = index_to_texCoord(index);
    vec3 normal = texture2D(normal_areaTex, texCoord).xyz;
    vec3 curvature = texture2D(curvTex, texCoord).xyw;
    vec3 initial_max = texture2D(initMaxTex, texCoord).xyz;
    vec3 initial_min = texture2D(initMinTex, texCoord).xyz;

    vec4 principle_directions[2];
	principle_directions[0] = principle_directions[1] = vec4(0.0f);
    diagonalize(normal, curvature, initial_max, initial_min, principle_directions);
    gl_Position = texCoord_to_pos(texCoord);
    vOut.maxPDir = principle_directions[0];
    vOut.minPDir = principle_directions[1];
}