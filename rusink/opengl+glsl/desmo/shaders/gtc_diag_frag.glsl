#version 430

in VertexData {
	vec4 maxPDir;
	vec4 minPDir;
} vOut;

layout(location = 0) out vec4 maxPDir;
layout(location = 1) out vec4 minPDir;

void main()
{
    minPDir = vOut.minPDir;
	maxPDir = vOut.maxPDir;
}
