#version 430

layout(triangles) in;

layout(points, max_vertices = 3) out;
	
in VertexData {
    vec3 pos;
    vec3 normal;
	vec2 texCoord;
	float area; 
} vOut[];

out GeomData {
	vec3 initialMin;
	vec3 initialMax;
} gOut;

uniform sampler2D normal_areaTex;

vec4 texCoord_to_pos(vec2 texCoord) {
	return vec4(-1.0f + 2.0f*texCoord.x, -1.0f + 2.0f*texCoord.y, 0f, 1);
}

void initial_coordinate_system(vec3 v0, vec3 v1, vec3 v2, vec3 n0, vec3 n1, vec3 n2,
							   out vec3 dir1[3], out vec3 dir2[3]) {
    dir1[0] = normalize(cross(v1 - v0, n0));
    dir1[1] = normalize(cross(v2 - v1, n1));
    dir1[2] = normalize(cross(v0 - v2, n2));
    
    dir2[0] = normalize(cross(n0, dir1[0]));
    dir2[1] = normalize(cross(n1, dir1[1]));
    dir2[2] = normalize(cross(n2, dir1[2]));
}

void main() {	
	vec3 initialMin[3], initialMax[3];
	initial_coordinate_system(vOut[0].pos, vOut[1].pos, vOut[2].pos,
							  vOut[0].normal, vOut[1].normal, vOut[2].normal,
							  initialMax, initialMin);
	
    for (int i = 0; i < 3; ++i) {
		gl_Position = texCoord_to_pos(vOut[i].texCoord);
		gOut.initialMin = initialMin[i];
		gOut.initialMax = initialMax[i];
		EmitVertex();
	}
}
