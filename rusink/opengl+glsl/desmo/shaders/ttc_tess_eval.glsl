#version 430
#extension GL_ARB_tessellation_shader : enable
#extension GL_ARB_shader_storage_buffer_object: require

layout(triangles, equal_spacing, cw, point_mode) in;

uniform MVP {
    mat4 ModelView;
    mat4 Projection;
};

uniform ivec2 size;

in TCData {
    vec4 pos;
    vec4 normal;
    uint index;
    vec3 t;
    vec3 b;
    vec3 base_a[3];
    float w[3][3];
    float m[3];
} tcOut[];

out TEData {
    vec4 curvTensor;
    vec4 metricTensor;
    vec4 contravMetricTensor;
    vec4 pdir_k1;
    vec4 pdir_k2;
};

layout(std140, binding=6) buffer EvalBuffer {
    vec4 evalDebug[];
};

// BEGIN - rusinkiewicz implementation ----------------------------------------
#if 1
    void swap(inout float a, inout float b)
{
    float tmp = a;
    a = b;
    b = tmp;
}

// BEGIN - Linear equation solver ---------------------------------------------
#if 1
// Szymon Rusinkiewicz
// Princeton University
// GLSL translation by Mario S Camillo, Campinas University

// Perform LDL^T decomposition of a symmetric positive definite matrix.
// Like Cholesky, but no square roots.  Overwrites lower triangle of matrix.
    bool ldltdc(inout float A[3][3], inout float rdiag[3])
{
    float v[3-1];
    for (int i = 0; i < 3; ++i)
    {
        for (int k = 0; k < i; ++k)
            v[k] = A[i][k]*rdiag[k];
        for (int j = i; j < 3; ++j)
        {
            float sum = A[i][j];
            for (int k = 0; k < i; ++k)
                sum -= v[k] * A[j][k];
            if (i == j)
            {
                if (sum <= 0.0f)
                    return false;
                rdiag[i] = 1.0f/sum;
            }
            else
            {
                A[j][i] = sum;
            }
        }
    }

    return true;
}

// Solve Ax=B after ldltdc
void ldltsl(float A[3][3], float rdiag[3], float B[3], inout float x[3])
{
    int i;
    for (i = 0; i < 3; ++i)
    {
        float sum = B[i];
        for (int k = 0; k < i; ++k)
            sum -= A[i][k]*x[k];
        x[i] = sum*rdiag[i];
    }
    for (i = 3 - 1; i >= 0; --i)
    {
        float sum = 0;
        for (int k = i + 1; k < 3; ++k)
            sum += A[k][i]*x[k];
        x[i] -= sum*rdiag[i];
    }
}
#endif
// END - Linear equation solver -----------------------------------------------

void proj_tens(const vec3 old_u, const vec3 old_v, const vec3 old_n,
    float old_a11, float old_a12, float old_a21, float old_a22,
    const vec3 new_u, const vec3 new_v, const vec3 new_n, 
    out float new_a11, out float new_a12, out float new_a21, out float new_a22)
{
    mat3 old_base_J;
    mat3 new_base_J;
    mat3 Jacobi;

    // column major
    old_base_J = mat3(old_u.x, old_u.y, old_u.z,
                      old_v.x, old_v.y, old_v.z,
                      old_n.x, old_n.y, old_n.z);

    
    new_base_J = mat3(new_u.x, new_u.y, new_u.z,
                      new_v.x, new_v.y, new_v.z,
                      new_n.x, new_n.y, new_n.z);

    mat3 inverse_old = inverse(old_base_J);
    Jacobi = inverse(old_base_J) * new_base_J ;

    new_a11 = old_a11 * Jacobi[0][0] * Jacobi[0][0] + old_a12 * Jacobi[0][0] * Jacobi[0][1] +  old_a21 * Jacobi[0][1] * Jacobi[0][0] + old_a22 * Jacobi[0][1] * Jacobi[0][1];
    new_a12 = old_a11 * Jacobi[0][0] * Jacobi[1][0] + old_a12 * Jacobi[0][0] * Jacobi[1][1] +  old_a21 * Jacobi[0][1] * Jacobi[1][0] + old_a22 * Jacobi[0][1] * Jacobi[1][1];
    new_a21 = old_a11 * Jacobi[1][0] * Jacobi[0][0] + old_a12 * Jacobi[1][0] * Jacobi[0][1] +  old_a21 * Jacobi[1][1] * Jacobi[0][0] + old_a22 * Jacobi[1][1] * Jacobi[0][1];
    new_a22 = old_a11 * Jacobi[1][0] * Jacobi[1][0] + old_a12 * Jacobi[1][0] * Jacobi[1][1] +  old_a21 * Jacobi[1][1] * Jacobi[1][0] + old_a22 * Jacobi[1][1] * Jacobi[1][1];
}

// Rotate a coordinate system to be perpendicular to the given normal
// Szymon Rusinkiewicz
// Princeton University
void rot_coord_sys(const vec3 old_u, const vec3 old_v,
    const vec3 new_norm,
    out vec3 new_u, out vec3 new_v)
{
    vec3 old_norm = cross(old_u, old_v);
    old_norm = normalize(old_norm);
    float ndot = dot(old_norm, new_norm);

    if (ndot <= -1.0f) 
    {
        new_u = 0-old_u;
        new_v = 0-old_v;
        return;
    } 
    else 
        if (abs(ndot-1.0) < 1e-6) 
        {
            new_u = old_u;
            new_v = old_v;
            return;
        }

        vec3 rot = cross(new_norm, old_norm);

        rot = normalize(rot);  // direction vec3tor must be unitary (Ting - 27/01/2013)

        float r00, r01, r02, r10, r11, r12, r20, r21, r22;
        float s = sqrt(1 - ndot*ndot);

        // Bug: 3 expressions are incorrect (Ting - 27/01/2012)
        // r00 = ndot + (1-ndot)*rot.x*rot.x;
        r00 = rot.x*rot.x + (1-rot.x*rot.x)*ndot;
        r01 = (1-ndot)*rot.x*rot.y - s*rot.z;
        r02 = (1-ndot)*rot.x*rot.z + s*rot.y;
        r10 = (1-ndot)*rot.x*rot.y + s*rot.z;
        // r11 = ndot + (1-ndot)*rot.y*rot.y;
        r11 = rot.y*rot.y+(1-rot.y*rot.y)*ndot;
        r12 = (1-ndot)*rot.y*rot.z-s*rot.x;
        r20 = (1-ndot)*rot.x*rot.z-s*rot.y;
        r21 = (1-ndot)*rot.y*rot.z+s*rot.x;
        // r22 = ndot + (1-ndot)*rot.z*rot.z;
        r22 = rot.z*rot.z+(1-rot.z*rot.z)*ndot;

        new_u.x = r00*old_u.x + r01*old_u.y + r02*old_u.z;
        new_u.y = r10*old_u.x + r11*old_u.y + r12*old_u.z;
        new_u.z = r20*old_u.x + r21*old_u.y + r22*old_u.z;
        new_v.x = r00*old_v.x + r01*old_v.y + r02*old_v.z;
        new_v.y = r10*old_v.x + r11*old_v.y + r12*old_v.z;
        new_v.z = r20*old_v.x + r21*old_v.y + r22*old_v.z;

        /*vec3 tmp;
        tmp.x = r00*new_norm.x + r01*new_norm.y + r02*new_norm.z;
        tmp.y = r10*new_norm.x + r11*new_norm.y + r12*new_norm.z;
        tmp.z = r20*new_norm.x + r21*new_norm.y + r22*new_norm.z;*/

        float t = dot(new_norm, old_norm);

        //LOG_LINE(cout, "ndot = " << ndot << "; s = " << s << "; new_norm = " << new_norm << ", " << old_norm << ", t = " << t);
}

// Given a curvature tensor, find principal directions and curvatures
// Makes sure that pdir1 and pdir2 are perpendicular to normal
// Szymon Rusinkiewicz
// Princeton University
void diagonalize_curv(const vec3 old_u, const vec3 old_v,
    float ku, float kuv, float kv,
    const vec3 new_norm,
    out vec3 pdir1, out vec3 pdir2, out float k1, out float k2)
{
    vec3 r_old_u, r_old_v;
    rot_coord_sys(old_u, old_v, new_norm, r_old_u, r_old_v);

    float c = 1, s = 0, tt = 0;
    if (kuv != 0.0f) 
    {
        // Jacobi rotation to diagonalize
        float h = 0.5f * (kv - ku) / kuv;
        tt = (h < 0.0f) ?
            1.0f / (h - sqrt(1.0f + h*h)) :
        1.0f / (h + sqrt(1.0f + h*h));
        c = 1.0f / sqrt(1.0f + tt*tt);
        s = tt * c;
    }

    k1 = ku - tt * kuv;
    k2 = kv + tt * kuv;

    if (abs(k1) >= abs(k2)) 
    {
        pdir1 = c*r_old_u - s*r_old_v;
    } 
    else 
    {
        swap(k1, k2);
        pdir1 = s*r_old_u + c*r_old_v;
    }
    pdir2 = cross(new_norm, pdir1);
}

void estimate_tensors (out vec3 pdir1, out vec3 pdir2,
    out float return_a11, out float return_a12, out float return_a22,
    out float return_b11, out float return_b12, out float return_b22, 
    out float k1, out float k2)
{
    float diag[3];
    float b11, b12, b22;

    float m[3];
    float w[3][3];
    vec3 base_a[3];
    for (int i = 0; i < 3; ++i)
    {
        base_a[i] = tcOut[0].base_a[i];
        m[i] = tcOut[0].m[i];
        for (int j = 0; j < 3; ++j)
        {
            w[i][j] = tcOut[0].w[i][j];
        }
    }


    if (ldltdc(w, diag)) 
    { 
        ldltsl(w, diag, m, m);
        b11 = m[0], b12 = m[1], b22 = m[2];
    } 
    else 
    {
        // TODO: how this case should be handled? (Ting)
    }

    diagonalize_curv(tcOut[0].t, tcOut[0].b, b11, b12, b22,
        tcOut[0].normal.xyz, pdir1, pdir2, k1, k2);

    pdir1 = normalize(pdir1);
    pdir2 = normalize(pdir2);

    // TODO: Verificar inversao de sinais em k1 em k1
    k1 *= -1;
    k2 *= -1;

    return_a11 = dot(base_a[0], base_a[0]);
    return_a12 = dot(base_a[0], base_a[1]);
    return_a22 = dot(base_a[1], base_a[1]);

    proj_tens(pdir1,pdir2,cross(pdir1, pdir2), k1,0,0,k2,base_a[0], base_a[1], base_a[2],
              return_b11,return_b12,return_b12,return_b22);
    return;
}
#endif
// END - rusinkiewicz implementation ------------------------------------------

void main()
{
    vec3 a, b;
    vec3 pdir1, pdir2;
    vec2 k;

    estimate_tensors (pdir1, pdir2, a.x, a.y,a.z, b.x, b.y, b.z, 
                      k.x, k.y);

    // Calcula tensor metrico contravariante
    float area_det_a = (a.x*a.z) - (a.y*a.y);
    vec4 c_a;

    if (abs(area_det_a) > 1e-6) 
    {
        c_a = vec4(a.z/area_det_a, a.y/area_det_a, a.y/area_det_a,
                          a.x/area_det_a);
    } 
    else 
    {
        c_a = vec4(0);
    }

    metricTensor = vec4(a.x, a.y, a.y, a.z);
    curvTensor = vec4(b.x, b.y, b.y, b.z);
    contravMetricTensor = c_a;
    pdir_k1 = vec4(pdir1, k.x);
    pdir_k2 = vec4(pdir2, k.y);

    // Index to x,y position in texture (x,y).
    vec2 pos = vec2(tcOut[0].index%size.x, tcOut[0].index/size.x);

    // Clip position ([-1,1]) so that fragCoord in frag shader is equal to texture position
    gl_Position = vec4(-1.0f + 2.0f*pos.x/size.x, -1.0f + 2.0f*pos.y/size.y, 0f, 1);
}
