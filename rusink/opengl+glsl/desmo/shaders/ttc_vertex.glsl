#version 430
#extension GL_ARB_shader_storage_buffer_object : enable

#define MAX_ADJ_FACES 8

layout(location=0) in vec3 pos;
layout(location=1) in vec3 normal;
layout(location=2) in uint index;
layout(location=3) in int isBorder;
layout(location=4) in uint numAdjFaces;

out VertexData {
    vec4 pos;
    vec4 normal;
    uint index;
    int isBorder;
} vOut;

uniform mat4 Projection;
uniform mat4 ModelView;

layout(std140, binding=4) buffer VertexBuffer {
    vec4 vertexDebug[];
};

void main() {
    vOut.pos = vec4(pos,1);
    vOut.normal = vec4(normal, 0);
    vOut.index = index;
    vOut.isBorder = isBorder;
}
