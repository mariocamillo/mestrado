#version 430

in GeomData {
	vec4 curv;
} gOut;

layout(location = 0) out vec4 curv;

void main()
{
    curv = gOut.curv.xyyz;
}
