#version 430

layout(location=0) in vec3 pos;
layout(location=2) in uint index;

out VertexData {
    vec3 pos;
    vec3 normal;
	vec2 texCoord;
	float area;
} vOut;

uniform ivec2 size;
uniform sampler2D normal_areaTex;

vec2 index_to_texCoord(uint index) {	
    // Index to x,y position in texture (x,y).
    vec2 pos = vec2(index%size.x, index/size.x);
	
	// Make it [0,1] range
	return vec2(pos.x/size.x, pos.y/size.y);
}

void main() {
	vec2 texCoord = index_to_texCoord(index);
    vec4 normal_area = texture2D(normal_areaTex, texCoord);
    vOut.pos = pos;
	vOut.normal = normal_area.xyz;
    vOut.area = normal_area.w;
    vOut.texCoord = texCoord;
}
