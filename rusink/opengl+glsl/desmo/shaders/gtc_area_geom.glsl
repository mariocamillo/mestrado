#version 430

#define NEXT(index) ((index < 2) ? index + 1 : index - 2)

layout (triangles) in;
layout (points, max_vertices = 3) out;

in VertexData {
    vec4 pos;
    vec2 texCoord; 
} vOut[];

out GeomData {
	vec4 normalA;
} gOut;

void edges(vec3 v0, vec3 v1, vec3 v2, out vec3 e[3]) {
    e[0] = v2 - v1;
    e[1] = v0 - v2;
    e[2] = v1 - v0;
}

vec3 compute_corner_area(vec3 v0, vec3 v1, vec3 v2) {
    int index;

    vec3 e[3];
    edges(v0, v1, v2, e);

    float l2[3];
    for (int index = 0; index < 3; index++) {
        l2[index] = dot(e[index], e[index]);
    }

    float ew[3];
    for (index = 0; index < 3; index++) {
        int next1 = (index + 1) % 3;
        int next2 = (index + 2) % 3;
        ew[index] = l2[index] * (l2[next1] + l2[next2] - l2[index]);
    }

    float area = 0.5f * length(cross(e[0], e[1]));
    vec3 corner_area;
	if (ew[0] <= 0.0f) {
		corner_area.y = -0.25f * l2[2] * area / dot(e[0], e[2]);
		corner_area.z = -0.25f * l2[1] * area / dot(e[0], e[1]);
		corner_area.x = area - corner_area.y - corner_area.z;
	} else if (ew[1] <= 0.0f) {
		corner_area.z = -0.25f * l2[0] * area / dot(e[1], e[0]);
		corner_area.x = -0.25f * l2[2] * area / dot(e[1], e[2]);
		corner_area.y = area - corner_area.z - corner_area.x;
	} else if (ew[2] <= 0.0f) {
		corner_area.x = -0.25f * l2[1] * area / dot(e[2], e[1]);
		corner_area.y = -0.25f * l2[0] * area / dot(e[2], e[0]);
		corner_area.z = area - corner_area.x - corner_area.y;
	} else {
		float ewscale = 0.5f * area / (ew[0] + ew[1] + ew[2]);
		corner_area.x = ewscale * (ew[1] + ew[2]);
		corner_area.y = ewscale * (ew[2] + ew[0]);
		corner_area.z = ewscale * (ew[0] + ew[1]);
	}

    return corner_area;
}

vec4 texCoord_to_pos(vec2 texCoord) {
	return vec4(-1.0f + 2.0f*texCoord.x, -1.0f + 2.0f*texCoord.y, 0f, 1);
}

void main() {
	int index;

    vec3 e[3];
    float l[3];
    for (index = 0; index < 3; index++) {
        e[index] = vOut[index].pos.xyz - vOut[NEXT(index)].pos.xyz;
        l[index] = (dot(e[index], e[index]));
    }

    vec3 fN = cross(e[0], e[1]);
    vec3 corner_area = compute_corner_area(vOut[0].pos.xyz, vOut[1].pos.xyz, vOut[2].pos.xyz);

    for (index = 0; index < 3; index++) {
		float scale = (l[index] * l[(index + 2) % 3]);
		if (scale == 0.0f) scale += 1e-8f;
		
		gl_Position = texCoord_to_pos(vOut[index].texCoord);
		gOut.normalA = vec4(fN * (1.0f / scale), corner_area[index]);
        EmitVertex();
    }
}
