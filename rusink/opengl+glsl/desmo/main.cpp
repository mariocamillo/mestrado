#ifdef WIN32
// Force use nvidia GPU
extern "C" {
    _declspec(dllexport) unsigned int NvOptimusEnablement = 0x00000001;
}
#endif

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <queue>
#include <glm/gtc/matrix_transform.hpp>
#include "util.h"
#include "glprofiler.h"
#include "TensorEstimator.h"
#include "TessTensor.h"
#include "BlendTensor.h"
#include "shaderLoader.h"
#include "deforma.h"

#ifndef WIN32
#include <unistd.h>
#define Sleep(x) usleep(x*1000)
#endif

#define CALC_AVG
#define TESS_TENSOR 0
#define BLEND_TENSOR 1

using namespace std;
using namespace glm;

extern GLfloat xmin,ymin,xmax,ymax,zmin,zmax;

/**
 * Callback for errors in GLFW
 *
 * @param [in] error The error number
 * @param [in] description C-string for the description of the error
 */
static void error_callback(int error, const char* description)
{
    cerr << description << endl;
}

/**
 * Callback for key events in GLFW
 *
 * @param [in] window The window where the event happened
 * @param [in] key the key that was pressed
 * @param [in] scancode ??
 * @param [in] action ??
 * @param [in] mods ??
 */
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

/// Structure to store a vertex information
struct VertexInfo {
    glm::vec3 pos;
    glm::vec3 normal;
    GLuint index;
    GLint isBorder;
};

/**
 * Create a half-mesh structure from an .obj file
 * @param [in][out] m The half-mesh pointer
 * @param [in] input_file path to the obj file
 * @return the elapsed time of the function
 */
double halfMeshFromObj(Mesh *m, const char *input_file) {
    START_FUNCTION_TIMER();

    FILE *objfile = fopen(input_file,"r");
    if (objfile == NULL) {
        printf ("ERROR! CANNOT OPEN '%s'\n",input_file);
        return 0.;
    }

    vector<Tri> tlist;
    vector<Tri>::iterator tIter;
    char line[200];
    char token[100];
    char atoken[100];
    char btoken[100];
    char ctoken[100];
    char dtoken[100];
    char etoken[100];
    float x,y,z;
    int a,b,c,d,e;
    Tri tt;
  
    int index = 0;
    int vert_count = 0;
    int vert_index = 1;
  
    while (fgets(line, 200, objfile) != NULL) {
    
    if (line[strlen(line)-2] == '\\') {
        if(fgets(token, 100, objfile) == NULL) break;
        int tmp = strlen(line)-2;
        strncpy(&line[tmp],token,100);
    }
    int token_count = sscanf (line, "%s\n",token);
    if (token_count == -1) continue;
    a = b = c = d = e = -1;
    if (!strcmp(token,"usemtl") ||
        !strcmp(token,"g"))
    {
        vert_index = 1; //vert_count + 1;
        index++;
    }
    else if (!strcmp(token,"v"))
    {
        vert_count++;
        sscanf (line, "%s %f %f %f\n",token,&x,&y,&z);
        Point3DGeom* node = new Point3DGeom;
        node->x = x; node->y = y; node->z = z;
        m->addVertex(vert_count,node);
        // Update bounding frame
        if (x < xmin) xmin = x;
        if (x > xmax) xmax = x;
        if (y < ymin) ymin = y;
        if (y > ymax) ymax = y;
        if (z < zmin) zmin = z;
        if (z > zmax) zmax = z;
    }
    else if (!strcmp(token,"f"))
    {
        int num = sscanf (line, "%s %s %s %s %s %s\n",token,
                          atoken,btoken,ctoken,dtoken,etoken);
        sscanf (atoken,"%d",&a);
        sscanf (btoken,"%d",&b);
        sscanf (ctoken,"%d",&c);
        if (!m->addTriangle(-1,a,b,c,NULL)) {
            tt.a=a; tt.b=b; tt.c = c;
            tlist.push_back(tt);
        };
    } else if (!strcmp(token,"vt")) {
    } else if (!strcmp(token,"vn")) {
    } else if (token[0] == '#') {
    } else {
        printf ("LINE: '%s'",line);
    }
    }

    // Novas tentativas ... 
    int iter = 10;

    while (tlist.size() > 0 && iter > 0) {
        cout << "Iteration: " << iter << endl;

        for ( tIter = tlist.begin( ) ; tIter != tlist.end( ) ; tIter++ ) {
            if (m->addTriangle(-1,tIter->a,tIter->b,tIter->c,NULL)) {
                tlist.erase(tIter);
                tIter--;
            }
        }
       iter--;
    }

    vector<FaceID> list;
    m->getAllFaces (&list);

    cout << "Quantidade total de faces:" << list.size() << endl;
    cout << "Quantidade total de faces nao inseridas:"<< tlist.size() << endl;

    RETURN_FUNCTION_TIMER();
}

/**
 * Create a half-mesh structure based on a mesh type
 *
 * @param [in][out] hmesh The half-mesh
 * @param [in] type The type of mesh to be created
 * @return The time elapsed on the function
 */
double halfMeshFromType(Mesh &hmesh, int type)
{
    START_FUNCTION_TIMER();

    Mesh ignore, ignore2;
    debug_load(&hmesh, &ignore, &ignore2, type, false);
    compute_normals(&hmesh);
    compute_vertex_metric_and_borders(&hmesh);
    compute_tensors(&hmesh);

    RETURN_FUNCTION_TIMER();
}

/**
 * Get the maximum number of adjacent faces of a group of vertices in a
 * half-mesh
 *
 * @param [in] vertices Array with the vertices indices
 * @param [in] halfmesh The half-mesh that contains the vertices
 * @return The maximum adjacent faces
 */
GLuint getMaxAdjFaces(const vector<VertexID> &vertices, Mesh &halfmesh)
{
    GLuint max = 0;
    for (vector<VertexID>::const_iterator it = vertices.begin();
         it != vertices.end(); ++it)
    {
        vector<FaceID> adjFaces;
        halfmesh.getVFaces(*it, &adjFaces);
        if (adjFaces.size() > max) max = adjFaces.size();
    }

    return max;
}

/**
 * Loads vertices data from a half-mesh into a MeshInfo struct
 *
 * @param [in] hmesh The half-mesh
 * @param [in][out] mesh The MeshInfo struct
 * @param [in] vertexMap Maps half mesh vertex ID to MeshInfo vertex index
 * @return The elapsed time of the function
 */
double loadHalfMeshVertices(Mesh &hmesh, MeshInfo<VertexInfo> &mesh, IDIdxMap &vertexMap)
{
    START_FUNCTION_TIMER();

    std::vector<VertexID> vertices;
    hmesh.getAllVertices(&vertices);
    unsigned int numVertices = vertices.size();
    mesh.vertices = new VertexInfo[numVertices];
    mesh.verticesByteLen = numVertices*sizeof(VertexInfo);
    mesh.numVertices = numVertices;
    
    std::vector<FaceID> adjFaces;
    for (unsigned int i = 0; i < numVertices; ++i)
    {
        Point3DGeom *vertex = reinterpret_cast<Point3DGeom *>(hmesh.getVGeomPtr(vertices[i]));
        unsigned int idx = vertexMap.getIdx(vertices[i]);

        mesh.vertices[idx].index = idx;
        mesh.vertices[idx].normal = glm::vec3(vertex->n[0], vertex->n[1], vertex->n[2]);
        mesh.vertices[idx].pos = glm::vec3(vertex->x, vertex->y, vertex->z);
        mesh.vertices[idx].isBorder = hmesh.isVOnBoundary(vertices[i]);
    }
    
    vector<FaceID> faces;
    hmesh.getAllFaces(&faces);
    GLulong numIndices = faces.size()*3; // 3 vertices per triangle
    mesh.indices = new GLuint[numIndices];
    mesh.indicesByteLen = numIndices*sizeof(GLuint);
    mesh.numIndices = numIndices;
    cout << "num faces:" << faces.size() << endl;
    for (unsigned int i = 0; i < faces.size(); ++i)
    {
        vector<VertexID> faceVertices;
        hmesh.getFVertices(faces[i], &faceVertices);
        for (unsigned int j = 0; j < faceVertices.size(); ++j)
        {
            mesh.indices[i*3 + j] = vertexMap.getIdx(faceVertices[j]);
        }
    }

    RETURN_FUNCTION_TIMER();
}

/**
 * Initializes the GLFW window
 *
 * @param [in] width Window width
 * @param [in] height Window height
 * @param [in] title C-string with the window title
 * @param [in] fullScreen boolean tag for full screen mode
 * @return The window handler
 */
GLFWwindow *init(int width, int height, const char *title, bool fullScreen = false)
{
    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        return NULL;
    if (fullScreen)
        window = glfwCreateWindow(1920, 1080, "desmo tessellation", glfwGetPrimaryMonitor(), NULL);
    else
        window = glfwCreateWindow(width, height, "desmo tessellation", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return NULL;
    }
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSwapInterval(0);

    glewInit();

    return window;
}

/**
 * Checks if tessallation is supported in the available GPU. OpenGL must
 * already be initialized
 *
 * @return True if supported, false otherwise
 */
bool tesselationSupported()
{
    // display some gpu info
    const GLubyte *vendor=glGetString(GL_VENDOR);
    const GLubyte *renderer=glGetString(GL_RENDERER);
    const GLubyte *version=glGetString(GL_VERSION);
    cout << "GPU: " << vendor << " " << renderer << " - " << version << endl;
    cout << "tessellation shader supported: ";
    if (glewIsSupported("GL_ARB_tessellation_shader"))
    {
        cout <<  "true" << endl;
        return true;
    }
    else
    {
        cout << "false" << endl;
        return false;
    }
}

/**
 * Cleans up the GLFW environment
 *
 * @param [in] window Window to be cleaned up
 */
void cleanup(GLFWwindow *window)
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

/**
 * Loads the mesh's IBO into the GPU
 *
 * @param [in] mesh MeshInfo struct containing the mesh data
 * @param [in] ibo the IBO index in OpenGL
 * @return The elapsed time of the function
 */
double setupTriangleIBO(MeshInfo<VertexInfo> &mesh, GLuint ibo)
{
    START_FUNCTION_TIMER();

    // Copy indices (triangles)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.indicesByteLen, mesh.indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    RETURN_FUNCTION_TIMER();
}

/**
 * Loads the mesh's VBOs into the GPU. Accepts several VBOs for mesh repetition
 * for iterative simulations
 *
 * @param [in] mesh MeshInfo struct containing the mesh data
 * @param [in] vbos C-array of OpenGL VBO indices
 * @param [in] vbosSize Number of VBOs in the array
 * @return The elapsed time of the function
 */
double setupVBOs(MeshInfo<VertexInfo> &mesh, GLuint *vbos, GLuint vbosSize)
{
    START_FUNCTION_TIMER();

    // Create buffers  for vertex data. Two so we can alternate between writing and reading
    for (GLuint i = 0; i < vbosSize; ++i)
    {
        // Bind the buffer
        glBindBuffer(GL_ARRAY_BUFFER, vbos[i]);

        // Set the vertices' attributes (see VertexInfo structure)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexInfo),
            reinterpret_cast<void*>(offsetof(VertexInfo, pos)));
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexInfo),
            reinterpret_cast<void*>(offsetof(VertexInfo, normal)));
        glEnableVertexAttribArray(1);
        glVertexAttribIPointer(2, 1, GL_UNSIGNED_INT, sizeof(VertexInfo),
            reinterpret_cast<void*>(offsetof(VertexInfo, index)));
        glEnableVertexAttribArray(2);
        glVertexAttribIPointer(3, 1, GL_INT, sizeof(VertexInfo),
            reinterpret_cast<void*>(offsetof(VertexInfo, isBorder)));
        glEnableVertexAttribArray(3);

        // Copy data to buffer
        glBufferData(GL_ARRAY_BUFFER, mesh.verticesByteLen, mesh.vertices, GL_STATIC_DRAW);

        // Unbind array
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }


    RETURN_FUNCTION_TIMER();
}

int main(int argc, char **argv)
{
    if (argc < 4)
    {
        cout << "Usage: " << argv[0] << " <num iter> <num_meshes> <mesh size>" << endl;
        return EXIT_FAILURE;
    }

    int numIter = atoi(argv[1]);
    int numMeshes = atoi(argv[2]);
    divisoes = atoi(argv[3]);
    diff_base[0] = (umax - umin)/static_cast<double>(divisoes);
    diff_base[1] = (vmax-vmin)/static_cast<double>(divisoes);
    bool compareResults = false;
    bool useBarrier = false;
    bool fullScreen = false;
    int tensorType = TESS_TENSOR;
    for (int i = 4; i < argc; ++i)
    {
        if (!strcmp(argv[i], "-c"))
        {
            compareResults = true;
        }
        else if (!strcmp(argv[i], "-g"))
        {
            tensorType = BLEND_TENSOR;
        }
        else if (!strcmp(argv[i], "-b"))
        {
            useBarrier = true;
        }
        else if (!strcmp(argv[i], "-f"))
        {
            fullScreen = true;
        }
    }

    GLFWwindow* window = init(800, 600, "desmo tessellation", fullScreen);
    if (!window)
    {
        return EXIT_FAILURE;
    }

    if (!tesselationSupported())
    {
        cleanup(window);
        return EXIT_FAILURE;
    }
    glewInit();

    // Initialize profiler. Get the current (only GL context) memory consumption
    GLProfiler glp(3);
    const GLint glfwMem = glp.GetUsedMemory();

    // Timer variables
    Timer timer;
    double elapsedus;
    
    // Shader matrices
    // TODO: change this to uniform buffer object. Maybe do the transforms in the vertex shader
    MVPUBO mvp;
    {
        float ratio;
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        mvp.p(ortho(-ratio, ratio, -2.f, 2.f, 2.f, -2.f));
        mvp.update();
    }

    Mesh hmesh;
    elapsedus = halfMeshFromType(hmesh, 0); // sela
    cout << "Time to load mesh: " << elapsedus << "us" << endl;

    vector<IDIdxMap> vertexMaps;
    vector<GLBuffers *> buffers_vector;
    vector<TensorEstimator *> estimators;

    // Load the halfmesh and linearize it into VBO and triangle IBO
    elapsedus = 0;
    double loadgpu = 0;
    double loadshaders = 0;
    double setup = 0;
    for (int i = 0; i < numMeshes; ++i)
    {
        // Helper structs
        IDIdxMap vertexMap;
        GLBuffers *buffers = new GLBuffers();
        MeshInfo<VertexInfo> mesh;
        elapsedus += loadHalfMeshVertices(hmesh, mesh, vertexMap);
        GLint glpTimer = glp.StartTimer();
        elapsedus += setupTriangleIBO(mesh, buffers->iboTriangles);
        elapsedus += setupVBOs(mesh, buffers->vbos, buffers->vbosSize);
        GLuint64 ns;
        glp.EndTimer(glpTimer);
        while (!glp.TimerElapsed(glpTimer, ns)) Sleep(10);
        loadgpu += ns/1e3;
        
        // Load tensor estimation shader
        timer.start();
        TensorEstimator *tensorEstimator;
        switch (tensorType)
        {
            case BLEND_TENSOR:
            {
                tensorEstimator = new BlendTensor(hmesh.numVertices());
                break;
            }
            case TESS_TENSOR:
            default:
            {
                tensorEstimator = new TessTensor();
                break;
            }
        }
        loadshaders += timer.getElapsedTimeInMicroSec();
        timer.stop();
        
        if (!tensorEstimator->isInit())
        {
            cout << "Failed to initialize Tensor estimator shaders" << endl;
            delete tensorEstimator;
            delete buffers;
            clean_pointer_vector(estimators);
            clean_pointer_vector(buffers_vector);
            cleanup(window);
            return EXIT_FAILURE;
        }

        // Setup the tensor estimator based on the halfmesh and BOs
        // XXX: pass the correct UBO (transform matrices) once it is added
        setup += tensorEstimator->Setup(hmesh, vertexMap, mvp.ubo(),
                                        buffers->vbos[0], buffers->iboTriangles);

        buffers_vector.push_back(buffers);
        vertexMaps.push_back(vertexMap);
        estimators.push_back(tensorEstimator);
    }
    cout << "CPU Time to load meshes in GPU: " << elapsedus << "us" << endl;
    cout << "GPU Time to load meshes in GPU: " << loadgpu << "us" << endl;
    cout << "Time to load shaders: " << loadshaders << "us" << endl;
    cout << "CPU Time to setup tensor estimators: " << setup << "us" << endl;

    GLint curTimer;
    queue<GLint> timers;
#ifdef CALC_AVG
    double calcSum = 0.;
    int itCount = 0;
    unsigned int timerCount = 0;
#endif
    do
    {
        // Oldest frame elapsed time
        GLint lastTimer = timers.size() > 0 ? timers.front() : -1;
        GLuint64 ns;
        bool observed = glp.TimerElapsed(lastTimer, ns);
        if (observed)
        {
            elapsedus = ns/1000.;
            timers.pop();
            //cout << "Time to calculate tensors: " << elapsedus << "us" << endl;
        }

#ifdef CALC_AVG
        if (itCount == 1) timer.start();
#endif

        curTimer = glp.StartTimer();
        {
            /*/ XXX: Move this inside the Draw method
            {
                float ratio;
                int width, height;
                glfwGetFramebufferSize(window, &width, &height);
                ratio = width / (float) height;
                glViewport(0, 0, width, height);
                glClear(GL_COLOR_BUFFER_BIT);
                mvp.mv(rotate(mat4(1.0f), (float) glfwGetTime() * 50.f,
                       glm::vec3(0.f, 0.f, 1.f)));
            }*/
            // XXX: Move this to before draw call (after everything is working with textures as
            // render targets)
            for (vector<TensorEstimator *>::iterator it = estimators.begin();
                 it != estimators.end(); ++it)
            {
                mvp.update();
                (*it)->calculateTensors(useBarrier);
            }
        }
        if (glp.EndTimer(curTimer))
        {
            timers.push(curTimer);
        }
#ifdef CALC_AVG
        if(itCount != 0 && observed)
        {
            calcSum += elapsedus;
            ++timerCount;
        }
#endif

        //XXX: should we compare for each mesh?
        if (compareResults)
        {
            elapsedus = (*estimators.begin())->readTensorsBack();
            cout << "Time to read results from GPU: " << elapsedus << "us" << endl;

            elapsedus = (*estimators.begin())->compareResults(hmesh,
                                                              *vertexMaps.begin());
            cout << "Time to compare results: " << elapsedus << "us" << endl;
        }

        glfwSwapBuffers(window);
        glfwPollEvents();


#ifndef CALC_AVG
    } while (0);//!glfwWindowShouldClose(window));
#else
        ++itCount;
    } while (itCount <= numIter);
    elapsedus = timer.getElapsedTimeInMicroSec();
    timer.stop();
    cout << "Used memory: " << (glp.GetUsedMemory()-glfwMem)/1024. << "MB" << endl;
    cout << "Average time to calculate tensors: " << calcSum/static_cast<double>(timerCount) <<
            "us. # Observations: " << timerCount << endl;
    cout << "Average time to calculate tensors2: " << elapsedus/static_cast<double>(numIter) <<
            "us" << endl;
#endif

    clean_pointer_vector(estimators);
    clean_pointer_vector(buffers_vector);
    cleanup(window);
    return EXIT_SUCCESS;
}
