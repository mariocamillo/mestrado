
# shader files
file(GLOB glsl "${CMAKE_CURRENT_SOURCE_DIR}/shaders/*.glsl")
source_group("Desmo_Shaders" FILES ${glsl})

# source files (C/C++)
file(GLOB src_cpp "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")
file(GLOB src_c "${CMAKE_CURRENT_SOURCE_DIR}/*.c")
source_group("Desmo_Src" FILES ${src_cpp} ${src_c})

# header files (C/C++)
file(GLOB hdr_cpp "${CMAKE_CURRENT_SOURCE_DIR}/*.hpp")
file(GLOB hdr_c "${CMAKE_CURRENT_SOURCE_DIR}/*.h")
source_group("Desmo_Include" FILES ${hdr_cpp} ${hdr_c})

# Executable generation
ADD_EXECUTABLE(desmo_glsl ${src_cpp} ${src_c} ${hdr_cpp} ${hdr_c} ${glsl} ${src_hm}
			   ${common_hdr_c} ${common_hdr_cpp} ${common_src_c} ${common_src_cpp}
               ${lib_hdr_c} ${lib_hdr_cpp} ${lib_src_c} ${lib_src_cpp})
COPY_TO_BUILD_DIR("${glsl}" desmo_glsl)
if(WIN32)
    target_link_libraries(desmo_glsl glfw3 glew32 opengl32)
    if(bits EQUAL 64)
	file(GLOB dlls "${CMAKE_CURRENT_SOURCE_DIR}/../../3rd_party/bin/Win64/*.dll")
    else(bits EQUAL 64)
	file(GLOB dlls "${CMAKE_CURRENT_SOURCE_DIR}/../../3rd_party/bin/Win32/*.dll")
    endif(bits EQUAL 64)
	file(GLOB dlls "${CMAKE_CURRENT_SOURCE_DIR}/../../3rd_party/bin/Win32/*.dll")
	COPY_TO_BUILD_DIR("${dlls}" desmo_glsl)
endif(WIN32)
if(UNIX)
    target_link_libraries(desmo_glsl GL glfw3 GLEW X11 pthread rt Xinerama Xrandr Xxf86vm Xi dl)
    #set flags
    #set(CMAKE_CXX_FLAGS "-lGL -lGLU -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi")
endif(UNIX)

# Install required system libraries
include(InstallRequiredSystemLibraries)

# set installation target dir
set(the_target "${CMAKE_CURRENT_SOURCE_DIR}/bin")

# install options
if(WIN32)
    set_target_properties(desmo_glsl PROPERTIES OUTPUT_NAME_DEBUG ${PROJECT_NAME}d)
    INSTALL(TARGETS desmo_glsl RUNTIME DESTINATION "${the_target}" PERMISSIONS OWNER_READ GROUP_READ WORLD_READ)
endif(WIN32)
if(UNIX)
    if(${CMAKE_BUILD_TYPE} MATCHES Debug)
        set(exe_path ${PROJECT_BINARY_DIR}/${PROJECT_NAME}d)
    else(${CMAKE_BUILD_TYPE} MATCHES Debug)
        set(exe_path ${PROJECT_BINARY_DIR}/${PROJECT_NAME})
    endif(${CMAKE_BUILD_TYPE} MATCHES Debug)
    INSTALL(FILES ${exe_path} DESTINATION "${the_target}" COMPONENT main)
endif(UNIX)

