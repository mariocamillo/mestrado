#include "BlendTensor.h"
#include <cmath>
#include <glm/ext.hpp>
#include "deforma.h"

#define ADD_SHADER(type, path) \
    (!path.empty() ? m_shaderProgram.addShader(type, path) : true)

static const std::string pass_vs[] = {""};

static inline int int_round(double a)
{
    return static_cast<int>((a - floor(a) >= 0.5F ) ? ceil(a) : floor(a));
}

static inline glm::ivec2 square_approximation(int a)
{
    double root = std::sqrt(a);
    return glm::ivec2(int_round(root), ceil(root));
}

using namespace glm;

BlendTensor::BlendTensor(GLuint numVertices) :
    m_vbo(0),
    m_ibo(0),
    m_fbo(0),
    m_mvp_ubo(0),
    m_numVertices(numVertices)
{
    for (int i = 0; i < BLEND_NUM_TEX_OUTPUTS; ++i)
    {
        m_tensors[i] = NULL;
    }
    m_init = init();
}

BlendTensor::~BlendTensor()
{
    for (int i = 0; i < BLEND_NUM_TEX_OUTPUTS; ++i)
    {
        delete [] m_tensors[i];
    }

    for (int i = 0; i < BLEND_NUM_SHADER_PROGS; ++i)
    {
        delete m_passes[i];
    }
}

bool BlendTensor::init()
{
    // Create RTT renderers
    glm::ivec2 texSize = square_approximation(m_numVertices);
    m_passes[0] = new RTTPass(texSize.x, texSize.y, 1);
    m_passes[1] = new RTTPass(texSize.x, texSize.y, 2);
    m_passes[2] = new RTTPass(texSize.x, texSize.y, 1);
    m_passes[3] = new RTTPass(texSize.x, texSize.y, 2);

    //init all shader programs
    m_passes[0]->SetupShaders("gtc_area_vertex.glsl", "", "",
                             "gtc_area_geom.glsl", "gtc_area_frag.glsl");
    m_passes[1]->SetupShaders("gtc_basis_vertex.glsl", "", "",
                             "gtc_basis_geom.glsl", "gtc_basis_frag.glsl");
    m_passes[2]->SetupShaders("gtc_curv_vertex.glsl", "", "",
                             "gtc_curv_geom.glsl", "gtc_curv_frag.glsl");
    m_passes[3]->SetupShaders("gtc_diag_vertex.glsl", "", "", "",
                             "gtc_diag_frag.glsl");

    // Initialize RTT outputs
    for (size_t i = 0; i < BLEND_NUM_SHADER_PROGS; ++i)
    {
        m_passes[i]->SetupOutputs();
    }

    // OneOneBS
    std::vector<GLenum> blendState(3);
    blendState[0] = GL_FUNC_ADD; blendState[1] = blendState[2] = GL_ONE;

    m_passes[0]->SetupRenderOptions(false, false, false, true, false, true,
                                   blendState);
    m_passes[1]->SetupRenderOptions(false, false, false, true, false, false,
                                   blendState); //BS is ignored bc blend=false
    m_passes[2]->SetupRenderOptions(false, false, false, true, false, true,
                                   blendState);
    m_passes[3]->SetupRenderOptions(false, false, false, true, false, false,
                                   blendState); //BS is ignored bc blend=false

    // Setup the in-out texture exchange between passes
    m_passes[0]->addUniform2i(
              m_passes[0]->ShaderProgram().getUniformLocation("size"),
              m_texSize);
    m_passes[1]->addUniform1i(
              m_passes[1]->ShaderProgram().getUniformLocation("normal_areaTex"),
              m_passes[0]->OutTexs()[0]);
    m_passes[1]->addUniform2i(
              m_passes[1]->ShaderProgram().getUniformLocation("size"),
              m_texSize);
    m_passes[2]->addUniform1i(
              m_passes[2]->ShaderProgram().getUniformLocation("normal_areaTex"),
              m_passes[0]->OutTexs()[0]);
    m_passes[2]->addUniform2i(
              m_passes[2]->ShaderProgram().getUniformLocation("size"),
              m_texSize);
    m_passes[3]->addUniform1i(
              m_passes[3]->ShaderProgram().getUniformLocation("normal_areaTex"),
              m_passes[0]->OutTexs()[0]);
    m_passes[3]->addUniform1i(
              m_passes[3]->ShaderProgram().getUniformLocation("initialMinTex"),
              m_passes[1]->OutTexs()[0]);
    m_passes[3]->addUniform1i(
              m_passes[3]->ShaderProgram().getUniformLocation("initialMaxTex"),
              m_passes[1]->OutTexs()[1]);
    m_passes[3]->addUniform1i(
              m_passes[3]->ShaderProgram().getUniformLocation("curvTex"),
              m_passes[2]->OutTexs()[0]);
    m_passes[3]->addUniform2i(
              m_passes[3]->ShaderProgram().getUniformLocation("size"),
              m_texSize);

    return true;
}


double BlendTensor::calculateTensors(bool useBarrier)
{
    START_FUNCTION_TIMER();
    for (size_t i = 0; i < BLEND_NUM_SHADER_PROGS; ++i)
    {
        m_passes[i]->Render(m_vbo, m_ibo, m_mvp_ubo, m_numVertices, useBarrier);
    }
    RETURN_FUNCTION_TIMER();
}

static void printDebugBuffer(glm::vec4 *buffer, GLuint len, std::string bufferName)
{
    for (GLuint i = 0; i < len; ++i)
    {
        if (buffer[i].x != -1)
            cout << "Vertex #" << i << " " << bufferName << ": " << printVec(buffer[i]) << endl;
    }
}


double BlendTensor::readTensorsBack()
{
    START_FUNCTION_TIMER();
    RETURN_FUNCTION_TIMER();
}

double BlendTensor::compareResults(Mesh &hmesh, IDIdxMap &vertexMap, double &minError, double &maxError,
                                 double &avgError)
{
    START_FUNCTION_TIMER();

    vector<VertexID> vertices;
    double minAErr, maxAErr, avgAErr, minBErr, maxBErr, avgBErr;

    minAErr = minBErr = 99999;
    maxAErr = maxBErr = 0.;
    avgAErr = avgBErr = 0.;
    hmesh.getAllVertices(&vertices);
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        GLulong vertexIdx = vertexMap.getIdx(vertices[i]);
        Point3DGeom *vertex = reinterpret_cast<Point3DGeom *>(hmesh.getVGeomPtr(vertices[i]));
        glm::vec4 theoryTensor, vecErr;

        {
            theoryTensor = glm::vec4(vertex->a[0][0], vertex->a[0][1], vertex->a[1][0], vertex->a[1][1]);
            stringstream ss;
            ss.clear();
            ss << "vertex#" << vertexIdx << " metric tensor: ";
            calculateVecError(m_tensors[2][vertexIdx], theoryTensor, ss.str(),
                              minAErr, maxAErr, avgAErr);
        }

        {
            theoryTensor = glm::vec4(vertex->b[0][0], vertex->b[0][1], vertex->b[1][0], vertex->b[1][1]);
            stringstream ss;
            ss.clear();
            ss << "vertex#" << vertexIdx << " curvature tensor: ";
            calculateVecError(m_tensors[0][vertexIdx], theoryTensor, ss.str(),
                              minBErr, maxBErr, avgBErr);
        }

        //cout << "vertex#" << vertexIdx << "pdir1: " << printVec(m_tensors[vertexIdx].pdir_k1) << endl;
    }

    avgAErr /= vertices.size();
    avgBErr /= vertices.size();
    
    cout << "metric tensors: min error " << minAErr << ", max error " << maxAErr << ", avgErr "
         << avgAErr << endl;
    cout << "curvature tensors: min error " << minBErr << ", max error " << maxBErr << ", avgErr "
         << avgBErr << endl;

    RETURN_FUNCTION_TIMER();
}

/*************************************** private functions ***************************************/
void BlendTensor::setOutputObjects()
{
}

void BlendTensor::calculateVecError(const glm::vec4 &a, const glm::vec4 &b,
                                   const std::string &outStr, double &minError,
                                   double &maxError, double &avgError)
{
    double err;
    glm::vec4 vecErr;

    vecErr = a - b;
    err = glm::length(vecErr);
    cout << outStr << printVec(a) << " - " << printVec(b) << " = " << printVec(vecErr) << " -> "
         << err << endl;;
    if (err < minError) { minError = err; }
    if (err > maxError) { maxError = err; }
    avgError += err;
}
