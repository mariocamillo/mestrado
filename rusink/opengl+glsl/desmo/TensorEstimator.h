#ifndef _TENSOR_ESTIMATOR_H_
#define _TENSOR_ESTIMATOR_H_

#include <GL/glew.h>
#include "util.h"
#include "mesh.h"

/**
 * Base interface class for tensor estimators
 */ 
class TensorEstimator {
public:
    virtual ~TensorEstimator() {}

    /**
     * Initialize the necessary buffer objects indices, creates the output
     * textures for the shaders and the IBO from the half-mesh
     *
     * @param [in] hmesh The half-mesh to have its tensors estimated
     * @param [in] vertexMap Maps vertex half-mesh ID to VBO index
     * @param [in] mvp_ubo UBO that points to the shaders' Model-View-Projection
     *                     transformation matrices
     * @param [in] vbo The OpenGL VBO index for the mesh
     * @param [in] ibo The OpenGL IBO index for the mesh.
     * @return The elapsed time of the function
     */
    virtual double Setup(Mesh &hmesh, IDIdxMap &vertexMap, GLuint mvp_ubo,
                         GLuint vbo, GLuint ibo) = 0;

    /**
     * Use tessellation shader to calculate the curvature tensors of
     * the previously set up mesh
     *
     * @param [in] useBarrier Use an OpenGL barrier to prevent starting new
     *                        calculations while the last one hasn't finished
     * @return The elapsed time of the function
     */
    virtual double calculateTensors(bool useBarrier) = 0;

    /**
     * Reads the calculated tensors back from the GPU
     *
     * @return The elapsed time of the function
     */
    virtual double readTensorsBack() = 0;
    
    /**
     * Compares the calculated tensors with the expected results for the
     * half-mesh
     *
     * @param [in] hmesh The half-mesh. This data structure contains the
     *                   tensors ground truth
     * @param [in] vertexMap Map half-mesh vertex ID to VBO vertex index
     * @param [out] minError The minumum encountered error
     * @param [out] maxError The maximum encountered error
     * @param [out] avgError The average encountered error
     * @return Elapsed time of the function
     */
    virtual double compareResults(Mesh &hmesh, IDIdxMap &vertexMap, double &minError,
                                  double &maxError, double &avgError) = 0;
    double compareResults(Mesh &hmesh, IDIdxMap &vertexMap)
    {
        double a, b, c;
        return compareResults(hmesh, vertexMap, a, b, c);
    }

    virtual double debugDraw(int width, int height) = 0;

    /**
     * Returns if the class is initialized and ready for use
     * 
     * @return True if initialized, false otherwise
     */
    virtual bool isInit() = 0;
protected:
    TensorEstimator() {} // Protected. This is an interface
};

#endif // _TENSOR_ESTIMATOR_H_
