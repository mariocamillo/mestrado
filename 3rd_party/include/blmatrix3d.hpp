#ifndef BL_MATRIX3D_HPP
#define BL_MATRIX3D_HPP
 
//-------------------------------------------------------------------
// FILE:            blMatrix3d.hpp
// CLASS:           blMatrix3d
// BASE CLASS:      None
// PURPOSE:         A simple 3x3 matrix class
// AUTHOR:          Vincenzo Barbato
//                  http://www.barbatolabs.com
//                  navyenzo@gmail.com
// LISENSE:         MIT-LICENCE
//                  http://www.opensource.org/licenses/mit-license.php
// NOTES:           - There are additional functions defined in this
//                    file that are external to the 3x3 matrix class
//                    - Operators that allow multiplying and
//                      dividing a scalar by a 3x3 matrix
//                    - Operators that allow multiplying the
//                      matrix by a vector and viceversa
//                    - Trace -- Sums the diagonal components
//                    - Transpose -- Returns the transposed matrix
//                    - det -- Calculates the determinant
//                    - eye3d -- Builds a 3x3 identity matrix
//                    - inv -- Calculates the inverse of a matrix
//                    - operator<< -- ostream operator to output the
//                                    matrix contents to the console
// DATE CREATED:    Oct/19/2010
// DATE UPDATED:
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
// Includes and libs needed for this file
//-------------------------------------------------------------------
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
// Enums used for this file and sub-files
//-------------------------------------------------------------------
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
class blMatrix3d
{
public: // Default constructor and destructor
 
    // Default constructor
    __device__ __host__
    blMatrix3d(const blType& InitialValue = 0);
 
    // Copy constructor from a different type matrix
    template<typename blType2>
    __device__ __host__
    blMatrix3d(const blMatrix3d<blType2>& Matrix);
 
    // Constructor using 9 distinct values
    __device__ __host__
    blMatrix3d(const blType& M00,const blType& M01,const blType& M02,
               const blType& M10,const blType& M11,const blType& M12,
               const blType& M20,const blType& M21,const blType& M22);
 
    // Constructor builds matrix from a
    // 3x3 two-dimensional array of different type
    template<typename blType2>
    __device__ __host__
    blMatrix3d(const blType2 (&MatrixArray)[3][3]);
 
    // Constructor builds matrix from a two-dimensional array
    // of different type and different dimensions
    template<typename blType2,int m,int n>
    __device__ __host__
    blMatrix3d(const blType2 (&MatrixArray)[m][n]);
 
    // Virtual destructor
    __device__ __host__
    ~blMatrix3d()
    {
    }
 
public: // Public variables
 
    // 3x3 matrix components
    blType                      M[3][3];
 
public: // Overloaded operators
 
    // Overloaded operators
    __device__ __host__
    const blMatrix3d<blType>    operator-()const;
    __device__ __host__
    blMatrix3d<blType>&         operator+=(const blMatrix3d<blType>& M);
    __device__ __host__
    blMatrix3d<blType>&         operator-=(const blMatrix3d<blType>& M);
    __device__ __host__
    blMatrix3d<blType>&         operator*=(const blType& Scalar);
    __device__ __host__
    blMatrix3d<blType>&         operator/=(const blType& Scalar);
    __device__ __host__
    const blMatrix3d<blType>   operator+(const blMatrix3d<blType>& M)const;
    __device__ __host__
    const blMatrix3d<blType>   operator-(const blMatrix3d<blType>& M)const;
    __device__ __host__
    const blMatrix3d<blType>    operator*(const blType& Scalar)const;
    __device__ __host__
    const blMatrix3d<blType>    operator/(const blType& Scalar)const;
    __device__ __host__
    const blMatrix3d<blType>   operator*(const blMatrix3d<blType>& M)const;
    __device__ __host__
    const blMatrix3d<blType>   operator/(const blMatrix3d<blType>& M)const;
};
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>::blMatrix3d(const blType& InitialValue)
{
    // Initialize all values to
    // the passed value
    M[0][0] = InitialValue;
    M[0][1] = InitialValue;
    M[0][2] = InitialValue;
    M[1][0] = InitialValue;
    M[1][1] = InitialValue;
    M[1][2] = InitialValue;
    M[2][0] = InitialValue;
    M[2][1] = InitialValue;
    M[2][2] = InitialValue;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>::blMatrix3d(const blType& M00,
                                      const blType& M01,
                                      const blType& M02,
                                      const blType& M10,
                                      const blType& M11,
                                      const blType& M12,
                                      const blType& M20,
                                      const blType& M21,
                                      const blType& M22)
{
    // Initialize all values to their
    // specified initial value
    M[0][0] = M00;
    M[0][1] = M01;
    M[0][2] = M02;
    M[1][0] = M10;
    M[1][1] = M11;
    M[1][2] = M12;
    M[2][0] = M20;
    M[2][1] = M21;
    M[2][2] = M22;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
template<typename blType2>
__device__ __host__
inline blMatrix3d<blType>::blMatrix3d(const blMatrix3d<blType2>& Matrix)
{
    // Copy the inidividual values to this matrix
    M[0][0] = blType(Matrix.M[0][0]);
    M[0][1] = blType(Matrix.M[0][1]);
    M[0][2] = blType(Matrix.M[0][2]);
    M[1][0] = blType(Matrix.M[1][0]);
    M[1][1] = blType(Matrix.M[1][1]);
    M[1][2] = blType(Matrix.M[1][2]);
    M[2][0] = blType(Matrix.M[2][0]);
    M[2][1] = blType(Matrix.M[2][1]);
    M[2][2] = blType(Matrix.M[2][2]);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
template<typename blType2>
__device__ __host__
inline blMatrix3d<blType>::blMatrix3d(const blType2 (&MatrixArray)[3][3])
{
    // Copy values from the array into this matrix
    M[0][0] = blType(MatrixArray[0][0]);
    M[0][1] = blType(MatrixArray[0][1]);
    M[0][2] = blType(MatrixArray[0][2]);
    M[1][0] = blType(MatrixArray[1][0]);
    M[1][1] = blType(MatrixArray[1][1]);
    M[1][2] = blType(MatrixArray[1][2]);
    M[2][0] = blType(MatrixArray[2][0]);
    M[2][1] = blType(MatrixArray[2][1]);
    M[2][2] = blType(MatrixArray[2][2]);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
template<typename blType2,int m,int n>
__device__ __host__
inline blMatrix3d<blType>::blMatrix3d(const blType2 (&MatrixArray)[m][n])
{
    // Initialize the values to zero
    M[0][0] = 0;
    M[0][1] = 0;
    M[0][2] = 0;
    M[1][0] = 0;
    M[1][1] = 0;
    M[1][2] = 0;
    M[2][0] = 0;
    M[2][1] = 0;
    M[2][2] = 0;
 
    // Find the smallest dimensions to loop over
    int NumOfRows = 3 < m ? 3 : m;
    int NumOfCols = 3 < m ? 3 : n;
 
    // Loop and assign the values
    for(int i = 0; i < NumOfRows; ++i)
        for(int j = 0; j < NumOfCols; ++j)
            M[i][j] = MatrixArray[i][j];
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator-()const
{
    blMatrix3d<blType> Negative;
    Negative[0][0] = -M[0][0];
    Negative[0][1] = -M[0][1];
    Negative[0][2] = -M[0][2];
    Negative[1][0] = -M[1][0];
    Negative[1][1] = -M[1][1];
    Negative[1][2] = -M[1][2];
    Negative[2][0] = -M[2][0];
    Negative[2][1] = -M[2][1];
    Negative[2][2] = -M[2][2];
 
    return Negative;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>& blMatrix3d<blType>::operator+=(const blMatrix3d<blType>& Matrix)
{
    M[0][0] += Matrix.M[0][0];
    M[0][1] += Matrix.M[0][1];
    M[0][2] += Matrix.M[0][2];
    M[1][0] += Matrix.M[1][0];
    M[1][1] += Matrix.M[1][1];
    M[1][2] += Matrix.M[1][2];
    M[2][0] += Matrix.M[2][0];
    M[2][1] += Matrix.M[2][1];
    M[2][2] += Matrix.M[2][2];
 
    return (*this);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>& blMatrix3d<blType>::operator-=(const blMatrix3d<blType>& Matrix)
{
    M[0][0] -= Matrix.M[0][0];
    M[0][1] -= Matrix.M[0][1];
    M[0][2] -= Matrix.M[0][2];
    M[1][0] -= Matrix.M[1][0];
    M[1][1] -= Matrix.M[1][1];
    M[1][2] -= Matrix.M[1][2];
    M[2][0] -= Matrix.M[2][0];
    M[2][1] -= Matrix.M[2][1];
    M[2][2] -= Matrix.M[2][2];
 
    return (*this);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>& blMatrix3d<blType>::operator*=(const blType& Scalar)
{
    M[0][0] *= Scalar;
    M[0][1] *= Scalar;
    M[0][2] *= Scalar;
    M[1][0] *= Scalar;
    M[1][1] *= Scalar;
    M[1][2] *= Scalar;
    M[2][0] *= Scalar;
    M[2][1] *= Scalar;
    M[2][2] *= Scalar;
 
    return (*this);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType>& blMatrix3d<blType>::operator/=(const blType& Scalar)
{
    M[0][0] /= Scalar;
    M[0][1] /= Scalar;
    M[0][2] /= Scalar;
    M[1][0] /= Scalar;
    M[1][1] /= Scalar;
    M[1][2] /= Scalar;
    M[2][0] /= Scalar;
    M[2][1] /= Scalar;
    M[2][2] /= Scalar;
 
    return (*this);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType> operator*(const blType& Scalar,
                                    const blMatrix3d<blType>& Matrix)
{
    return blMatrix3d<blType>(Matrix.M[0][0]*Scalar,
                              Matrix.M[0][1]*Scalar,
                              Matrix.M[0][2]*Scalar,
                              Matrix.M[1][0]*Scalar,
                              Matrix.M[1][1]*Scalar,
                              Matrix.M[1][2]*Scalar,
                              Matrix.M[2][0]*Scalar,
                              Matrix.M[2][1]*Scalar,
                              Matrix.M[2][2]*Scalar);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType> operator/(const blType& Scalar,
                                    const blMatrix3d<blType>& Matrix)
{
    return Matrix*Scalar;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator*(const blType& Scalar)const
{
    return blMatrix3d<blType>(M[0][0]*Scalar,
                              M[0][1]*Scalar,
                              M[0][2]*Scalar,
                              M[1][0]*Scalar,
                              M[1][1]*Scalar,
                              M[1][2]*Scalar,
                              M[2][0]*Scalar,
                              M[2][1]*Scalar,
                              M[2][2]*Scalar);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator/(const blType& Scalar)const
{
    return blMatrix3d<blType>(M[0][0]/Scalar,
                              M[0][1]/Scalar,
                              M[0][2]/Scalar,
                              M[1][0]/Scalar,
                              M[1][1]/Scalar,
                              M[1][2]/Scalar,
                              M[2][0]/Scalar,
                              M[2][1]/Scalar,
                              M[2][2]/Scalar);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator*(const blMatrix3d<blType>& Matrix)const
{
    // C[i][j] = Sum(A[i][k]*B[k][j])
 
    blMatrix3d<blType> Result;
 
    Result.M[0][0] = M[0][0]*Matrix.M[0][0] + M[0][1]*Matrix.M[1][0] + M[0][2]*Matrix.M[2][0];
    Result.M[0][1] = M[0][0]*Matrix.M[0][1] + M[0][1]*Matrix.M[1][1] + M[0][2]*Matrix.M[2][1];
    Result.M[0][2] = M[0][0]*Matrix.M[0][2] + M[0][1]*Matrix.M[1][2] + M[0][2]*Matrix.M[2][2];
    Result.M[1][0] = M[1][0]*Matrix.M[0][0] + M[1][1]*Matrix.M[1][0] + M[1][2]*Matrix.M[2][0];
    Result.M[1][1] = M[1][0]*Matrix.M[0][1] + M[1][1]*Matrix.M[1][1] + M[1][2]*Matrix.M[2][1];
    Result.M[1][2] = M[1][0]*Matrix.M[0][2] + M[1][1]*Matrix.M[1][2] + M[1][2]*Matrix.M[2][2];
    Result.M[2][0] = M[2][0]*Matrix.M[0][0] + M[2][1]*Matrix.M[1][0] + M[2][2]*Matrix.M[2][0];
    Result.M[2][1] = M[2][0]*Matrix.M[0][1] + M[2][1]*Matrix.M[1][1] + M[2][2]*Matrix.M[2][1];
    Result.M[2][2] = M[2][0]*Matrix.M[0][2] + M[2][1]*Matrix.M[1][2] + M[2][2]*Matrix.M[2][2];
 
    return Result;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator+(const blMatrix3d<blType>& Matrix)const
{
    // C[i][j] = A[i][j] + B[i][j])
 
    blMatrix3d<blType> Result;
 
    Result.M[0][0] = M[0][0] + Matrix.M[0][0];
    Result.M[0][1] = M[0][1] + Matrix.M[0][1];
    Result.M[0][2] = M[0][2] + Matrix.M[0][2];
    Result.M[1][0] = M[1][0] + Matrix.M[1][0];
    Result.M[1][1] = M[1][1] + Matrix.M[1][1];
    Result.M[1][2] = M[1][2] + Matrix.M[1][2];
    Result.M[2][0] = M[2][0] + Matrix.M[2][0];
    Result.M[2][1] = M[2][1] + Matrix.M[2][1];
    Result.M[2][2] = M[2][2] + Matrix.M[2][2];
 
    return Result;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator-(const blMatrix3d<blType>& Matrix)const
{
    // C[i][j] = A[i][j] - B[i][j])
 
    blMatrix3d<blType> Result;
 
    Result.M[0][0] = M[0][0] - Matrix.M[0][0];
    Result.M[0][1] = M[0][1] - Matrix.M[0][1];
    Result.M[0][2] = M[0][2] - Matrix.M[0][2];
    Result.M[1][0] = M[1][0] - Matrix.M[1][0];
    Result.M[1][1] = M[1][1] - Matrix.M[1][1];
    Result.M[1][2] = M[1][2] - Matrix.M[1][2];
    Result.M[2][0] = M[2][0] - Matrix.M[2][0];
    Result.M[2][1] = M[2][1] - Matrix.M[2][1];
    Result.M[2][2] = M[2][2] - Matrix.M[2][2];
 
    return Result;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blType Trace(const blMatrix3d<blType>& Matrix)
{
    // Trace is the sum of the diagonal components
    return (Matrix.M[0][0] + Matrix.M[1][1] + Matrix.M[2][2]);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blType det(const blMatrix3d<blType>& Matrix)
{
    // Initialize the determinant
    blType Result = 0;
 
    // Calculate the determinant
    Result += Matrix.M[0][0]*(Matrix.M[1][1]*Matrix.M[2][2] - Matrix.M[2][1]*Matrix.M[1][2]);
    Result -= Matrix.M[0][1]*(Matrix.M[1][0]*Matrix.M[2][2] - Matrix.M[2][0]*Matrix.M[1][2]);
    Result += Matrix.M[0][2]*(Matrix.M[1][0]*Matrix.M[2][1] - Matrix.M[2][0]*Matrix.M[1][1]);
 
    // Return the determinant
    return Result;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType> Transpose(const blMatrix3d<blType>& Matrix)
{
    // Initialize the transpose matrix
    blMatrix3d<blType> MatrixTranspose = 0;
 
    // Assign the components
    MatrixTranspose.M[0][0] = Matrix.M[0][0];
    MatrixTranspose.M[0][1] = Matrix.M[1][0];
    MatrixTranspose.M[0][2] = Matrix.M[2][0];
    MatrixTranspose.M[1][0] = Matrix.M[0][1];
    MatrixTranspose.M[1][1] = Matrix.M[1][1];
    MatrixTranspose.M[1][2] = Matrix.M[2][1];
    MatrixTranspose.M[2][0] = Matrix.M[0][2];
    MatrixTranspose.M[2][1] = Matrix.M[1][2];
    MatrixTranspose.M[2][2] = Matrix.M[2][2];
 
    return MatrixTranspose;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType> eye3d(const blType& DiagonalValue)
{
    return blMatrix3d<blType>(DiagonalValue,0,0,
                              0,DiagonalValue,0,
                              0,0,DiagonalValue);
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline blMatrix3d<blType> inv(const blMatrix3d<blType>& Matrix)
{
    // First:   Calculate the determinant of the Matrix
    blType D = det(Matrix);
 
    // Second:  Transpose the Matrix
    blMatrix3d<blType> Mt = Transpose(Matrix);
 
    // Third:   Calculate the matrix of minors
    blMatrix3d<blType> Minors;
    Minors.M[0][0] = Mt.M[1][1]*Mt.M[2][2] - Mt.M[2][1]*Mt.M[1][2];
    Minors.M[0][1] = Mt.M[1][0]*Mt.M[2][2] - Mt.M[2][0]*Mt.M[1][2];
    Minors.M[0][2] = Mt.M[1][0]*Mt.M[2][1] - Mt.M[2][0]*Mt.M[1][1];
    Minors.M[1][0] = Mt.M[0][1]*Mt.M[2][2] - Mt.M[2][1]*Mt.M[0][2];
    Minors.M[1][1] = Mt.M[0][0]*Mt.M[2][2] - Mt.M[2][0]*Mt.M[0][2];
    Minors.M[1][2] = Mt.M[0][0]*Mt.M[2][1] - Mt.M[2][0]*Mt.M[0][1];
    Minors.M[2][0] = Mt.M[0][1]*Mt.M[1][2] - Mt.M[1][1]*Mt.M[0][2];
    Minors.M[2][1] = Mt.M[0][0]*Mt.M[1][2] - Mt.M[1][0]*Mt.M[0][2];
    Minors.M[2][2] = Mt.M[0][0]*Mt.M[1][1] - Mt.M[1][0]*Mt.M[0][1];
 
    // Fourth:  Calculate the adjoint matrix
    Minors.M[0][1] *= blType(-1);
    Minors.M[1][0] *= blType(-1);
    Minors.M[1][2] *= blType(-1);
    Minors.M[2][1] *= blType(-1);
 
    // Fifth:   Calculate the inverse
    return Minors/D;
}
//-------------------------------------------------------------------
 
//-------------------------------------------------------------------
template<typename blType>
__device__ __host__
inline const blMatrix3d<blType> blMatrix3d<blType>::operator/(const blMatrix3d<blType>& Matrix)const
{
    return (*this) * inv(Matrix);
}
//-------------------------------------------------------------------
 
#endif // BL_MATRIX3D_HPP