#include "glprofiler.h"
#ifdef WIN32
#include <GL/wglew.h>
#define glGetGPUIDsAMD wglGetGPUIDsAMD
#define glGetGPUInfoAMD wglGetGPUInfoAMD
#define GL_GPU_RAM_AMD WGL_GPU_RAM_AMD
#else
#include <GL/glxew.h>
#define glGetGPUIDsAMD glXGetGPUIDsAMD
#define glGetGPUInfoAMD glXGetGPUInfoAMD
#define GL_GPU_RAM_AMD GLX_GPU_RAM_AMD
#endif

GLuint GLProfiler::GetFirstCardID()
{
    switch (m_vendor)
    {
        case NVIDIA:
        {
            return GetFirstCardIDNVidia();
            break;
        }
        case AMD:
        {
            return GetFirstCardIDAMD();
            break;
        }
        default:
        {
            return 0;
        }
    }
}

GLint GLProfiler::GetTotalMemory()
{
    switch (m_vendor)
    {
        case NVIDIA:
        {
            return GetTotalMemoryNVidia();
            break;
        }
        case AMD:
        {
            return GetTotalMemoryAMD();
            break;
        }
        default:
        {
            return 0;
        }
    }
}

GLint GLProfiler::GetUsedMemory()
{
    switch (m_vendor)
    {
        case NVIDIA:
        {
            return GetUsedMemoryNVidia();
            break;
        }
        case AMD:
        {
            return GetUsedMemoryAMD();
            break;
        }
        default:
        {
            return 0;
        }
    }
}

GLuint GLProfiler::GetFirstCardIDNVidia()
{
    return 0;
}

GLint GLProfiler::GetTotalMemoryNVidia()
{
    GLint totalAvail;
    glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX,
                   &totalAvail);
    return totalAvail;
}

GLint GLProfiler::GetUsedMemoryNVidia()
{
    GLint curAvail;
    glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX,
                   &curAvail);
    return (m_totalMemory - curAvail);
}

GLint GLProfiler::GetTotalMemoryAMD()
{
    GLint totalAvail;
    glGetGPUInfoAMD(m_cardID, GL_GPU_RAM_AMD, GL_INT, sizeof(GLint),
                    &totalAvail);
    return totalAvail*1024; // function above returns in MB
}

GLint GLProfiler::GetUsedMemoryAMD()
{
    GLint memInfo[4];
    // All memory pools (vbo, texture and renderbuffer) return same value
    glGetIntegerv(GL_VBO_FREE_MEMORY_ATI, memInfo);
    return m_totalMemory - memInfo[0]; // 0 is available memory
}

GLuint GLProfiler::GetFirstCardIDAMD()
{
    GLuint maxCount;
    maxCount = glGetGPUIDsAMD(0, 0);
    GLuint *IDs = new GLuint[maxCount];
    glGetGPUIDsAMD(maxCount, IDs);
    GLuint firstID = IDs[0];
    delete IDs;
    return firstID;
}
