#ifndef _GL_TIMER_H_
#define _GL_TIMER_H_

#include <GL/glew.h>

class GLTimer
{
public:
    GLTimer() : m_inUse(IDLE)
    {
        glGenQueries(1, &m_begin);
        glGenQueries(1, &m_end);
    }

    ~GLTimer()
    {
        glDeleteQueries(1, &m_begin);
        glDeleteQueries(1, &m_end);
    }

    bool Start()
    {
        if (InUse()) return false;

        glQueryCounter(m_begin, GL_TIMESTAMP);
        m_inUse = START;
        return true;
    }

    bool End()
    {
        if (m_inUse != START) return false;

        glQueryCounter(m_end, GL_TIMESTAMP);
        m_inUse = END;
        return true;
    }

    bool Elapsed(GLuint64 &ns)
    {
        if (m_inUse != END) return false;

        GLuint available[2] = {GL_FALSE, GL_FALSE};
        glGetQueryObjectuiv(m_begin, GL_QUERY_RESULT_AVAILABLE, available);
        glGetQueryObjectuiv(m_end, GL_QUERY_RESULT_AVAILABLE, available + 1);
        if (available[0] && available[1])
        {
            GLuint64 b, e;
            glGetQueryObjectui64v(m_begin, GL_QUERY_RESULT, &b);
            glGetQueryObjectui64v(m_end, GL_QUERY_RESULT, &e);
            ns = e-b;
            m_inUse = IDLE;
            return true;
        }

        return false;
    }

    inline bool InUse() { return (m_inUse != IDLE); }
    inline void Reset() { m_inUse = IDLE; }

private:     
    enum USAGE
    {
        IDLE = 0,
        START,
        END
    };
    USAGE  m_inUse;
    GLuint m_begin;
    GLuint m_end;
};

#endif //_GL_TIMER_H_
