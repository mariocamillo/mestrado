/*
*  deforma.cpp
*/

#include "deforma.h"
#include <stdlib.h>
#include <time.h>
#include <sstream>

#ifdef 	WIN32
#include <windows.h>
#include <winbase.h>
#endif
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Geometry>

#include "lineqn.h"
#include "Timer.h"
#include "utils.h"

int divisoes = 10;

static void rot_coord_sys(const vec &old_u, const vec &old_v,
    const vec &new_norm,
    vec &new_u, vec &new_v);

struct Tri 
{
    int a,b,c;
};

typedef struct TRI_struct 
{
    double nx, ny, nz;
    bool collision_current_vertex;
} Triangle;

typedef struct 
{
    int type_coll;
    vec x[4];
    vec n;
    vec v_ab[2];
    vec f[4];
    double w[3];
    double delta;
    double ab[2];
    double mu[4];
} DEBUG_COL_struct;

list<DEBUG_COL_struct> DEBUG_COL_list;
list<DEBUG_COL_struct>::iterator DEBUG_COL_list_iter;
int DEBUG_COL_selector = -1;

//#define EPSOLON 1.e-10
#define EPSOLON 1.e-6

#define TIMESTEP 1.e-4
#define NUM_ITER_STEP 1

#define DR_STOP_COND 1.e-70
#define DR_STOP_STEPSOLON 1000

double DR_STOP = DR_STOP_COND / TIMESTEP;

#define COLL_H 5.e-2
#define COLL_PARALEL_TOLERANCE 5.e-2
#define COLL_MAX 5

#define debug_u_force 0.
#define debug_v_force 0.
//#define debug_u -0.5
//#define debug_v 0.
#define debug_u 0.4
#define debug_v -0.5

//#define GAMMA 1.e-6
#define MAX_INTER 800
#define MIN_AREA 1.e-6


double area_inicial;

Mesh* m0, *m1, *m2;

int iteration;

double MESH_AREA;

// Globais de Debug
bool next_step;
int num_steps;
double current_t;
double energia_A_max, energia_A_min;
double x_energia_A_max, y_energia_A_max;
double x_energia_A_min, y_energia_A_min;
double global_errMax_a;
double global_errMax_b;
double global_errMax_k;
double MASS_VERTEX;
double diff_base[2] = {(umax - umin)/divisoes, (vmax-vmin)/divisoes};
bool force_bool = false;
bool force_render = false;
const double VIS_LIMIAR_ERROS = 0.001;
const double VIS_MAX_ERROS = 0.09;

const int TIPO_GRAFICO = -1;

float xmin=(float) 1.E12,  ymin= (float)1.E12,xmax= (float)-1.E12,ymax= (float)-1.E12,zmin=(float)1.E12, zmax=(float)-1.E12;

vector<Tri> tlist;
vector<Tri>::iterator tIter;

static void estimate_bases_tbn_borderref(Mesh *m, Point3DGeom *vptr, int index, vec &t, vec &b, vec &n)
{
    Point3DGeom *v_a_vptr, *v_vptr;
    v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->BorderRefVertex[index]);

    v_a_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->BorderRefVer_base_t[index]);


    t[0] = v_a_vptr->x - v_vptr->x;
    t[1] = v_a_vptr->y - v_vptr->y;
    t[2] = v_a_vptr->z - v_vptr->z;

    normalize(t);
    n = v_vptr->n;
    normalize(n);
    b = t CROSS n;

}

static bool is_point_debug(Point3DGeom *vptr)
{
    if ( 
        (fabs(vptr->u_T - debug_u)<0.001) && (fabs(vptr->v_T - debug_v)<0.001 )  
        )
    {
        return true;
    }
    else 
    {
        return false;
    }

}

static void rotate_vector(vec &base_a, vec &new_n)
{

    // TODO: Usando Eigen por enquanto
    float angRot;
    Eigen::Vector3f eigen_base_a,eigen_new_n,eigen_axisRot;
    Eigen::Quaternion<float> quatRot;  

    eigen_base_a(0) = (float) base_a[0]; 
    eigen_base_a(1) = (float) base_a[1]; 
    eigen_base_a(2) = (float) base_a[2];
    eigen_new_n(0) = (float) new_n[0]; 
    eigen_new_n(1) = (float) new_n[1]; 
    eigen_new_n(2) = (float) new_n[2];

    eigen_axisRot = eigen_base_a.cross(eigen_new_n).normalized();
    angRot = acos(eigen_base_a.normalized().dot(eigen_new_n));


    if (angRot<M_PI)
    {
        angRot += (float) (- M_PI/2);
    }
    else
    {
        angRot = (float)  (-1 * ( 2 * M_PI - angRot) + M_PI/2);
    }

    if (fabs(angRot)>0.01)
    {
        quatRot = Eigen::AngleAxis<float>(angRot, eigen_axisRot);
        eigen_base_a = quatRot * eigen_base_a;
        base_a[0] = eigen_base_a(0);  base_a[1] = eigen_base_a(1); 	base_a[2] = eigen_base_a(2); 
    }

}

void find_border_neighbours(Mesh *m, const VertexID &point_id, VertexID &front_id, VertexID &back_id)
{
    vector<VertexID> vlist;
    vector<VertexID>::iterator vIter;
    int i,firstpos,secondpos;
    VertexID tmp_id;
    firstpos = secondpos =-1;

    m->getVVertices(point_id,&vlist);
    for (vIter = vlist.begin( ),i=0 ; vIter != vlist.end( ) ; vIter++,i++ ) 
    {
        //vptr = (Point3DGeom *)m->getVGeomPtr(*vIter);
        if (m->isVOnBoundary(*vIter))
        {
            if (firstpos==-1)
            {
                firstpos = i;
                front_id = *vIter;
            }
            else
            {
                secondpos = i;
                back_id = *vIter;
            }
        }
    }
    vlist.clear();

    if (secondpos!=(firstpos+1))
    {
        tmp_id = back_id;
        back_id = front_id;
        front_id = back_id;
    }

}

static void estimate_bases_a(Mesh *m, Point3DGeom *vptr)
{
    Point3DGeom *v_vptr;
    vec base_a_rot, base_b_rot;
    int i;

    vptr->base_a[2] = vptr->n;

    for (i=0;i<2;i++)
    {
        vptr->base_a[i][0] = vptr->base_a[i][1] = vptr->base_a[i][2] = 0;
        // base_a[i] - olhando proximo vertice
        if (vptr->MetricVertex[i]>-1)
        {
            // Preference: forward finite difference
            v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex[i]);
            vptr->base_a[i][0] = (v_vptr->x - vptr->x);
            vptr->base_a[i][1] = (v_vptr->y - vptr->y);
            vptr->base_a[i][2] = (v_vptr->z - vptr->z);
            rotate_vector(vptr->base_a[i],vptr->base_a[2]);
        }
        // base_a[i] - olhando vertice anterior (se existir)
        else if (vptr->MetricVertex_return[i]>-1)
        {
            v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex_return[i]);
            base_a_rot[0] = vptr->x - v_vptr->x;
            base_a_rot[1] = vptr->y - v_vptr->y;
            base_a_rot[2] = vptr->z - v_vptr->z;
            rotate_vector(base_a_rot,vptr->base_a[2]);
            vptr->base_a[i] += base_a_rot;
        }
        //if ( (vptr->MetricVertex[i]>-1) && (vptr->MetricVertex_return[i]>-1) )
        //vptr->base_a[i] *= 0.5;
    }

    // base_a_rot = vptr->base_a[0];
    // base_b_rot = vptr->base_a[1];

    // rot_coord_sys(base_a_rot, base_b_rot, vptr->base_a[2], 
    //   		vptr->base_a[0], vptr->base_a[1]);

    // vec tmp = vptr->base_a[0] CROSS vptr->base_a[1];

    // if ((tmp DOT vptr->base_a[2]) < 0.0) {
    //   LOG_LINE(cout, "dot = " << (tmp DOT vptr->base_a[2]));
    //   LOG_LINE(cout, "base_a[0] = " << vptr->base_a[0] << "; base_a[1] = " << vptr->base_a[1]);
    //   base_a_rot = vptr->base_a[0];
    //   vptr->base_a[0] = vptr->base_a[1];
    //   vptr->base_a[1] = base_a_rot;
    //   tmp = vptr->base_a[0] CROSS vptr->base_a[1];
    //    if ((tmp DOT vptr->base_a[2]) < 0.0) {
    //       LOG_LINE(cout, "Normal vector incorrect direction");
    //    }
    // }

    // Multiplicando base_ax para inicial ser igual a 1
    vptr->base_a[0] *= 1/diff_base[0];
    vptr->base_a[1] *= 1/diff_base[1];
}

// Usar areas de triangulos para atualizar mu. 
/*
void calc_mu(Mesh *m, bool init)
{
// Calculo da area das faces - triangulos
// Area (triangulos)

vector<FaceID> flist;
vector<FaceID>::iterator fIter;
vector<VertexID> vlist;
vector<VertexID>::iterator vIter;
Point3DGeom  *vptr;

double area_mu;

vec v_area1;
vec v_area2;
Triangle *ftmp;
vec vt[3];
//vector<void *> Vlist;
//vector<void *>::iterator Vit;


m->getAllFaces (&flist);

area_mu = 0.;
for ( fIter = flist.begin( ) ; fIter != flist.end( ) ; fIter++ ) 
{
ftmp = (Triangle *)m->getFGeomPtr(*fIter);
m->getFVertices (*fIter, &vlist);
int i=0;
for (vIter = vlist.begin( ) ; vIter != vlist.end( ) ; vIter++ ) 
{
vptr = (Point3DGeom *)m->getVGeomPtr(*vIter);
vt[i][0] = vptr->x;
vt[i][1] = vptr->y;
vt[i][2] = vptr->z;
i++;
}
vlist.clear();

v_area1 = vt[0]-vt[1];
v_area2 = vt[2]-vt[0];

area_mu += fabs(len(v_area1 CROSS v_area2));
}
area_mu *= 0.5;
flist.clear();


if (init==true)
area_inicial = area_mu;

area_mu = (MASS/MESH_AREA) / (area_mu/area_inicial);
m->getAllVertices(&vlist);
for ( vIter = vlist.begin( ) ; vIter != vlist.end( ) ; vIter++ ) 
{
vptr = (Point3DGeom *)m->getVGeomPtr(*vIter);
vptr->mu = area_mu;
}
vlist.clear();

}
*/
double force_zero(double valor)
{
    if (fabs(valor)>EPSOLON)
    {
        return valor;
    }
    else 
    {
        return 0.;
    }

}



void proj_tens(const vec &old_u, const vec &old_v, const vec &old_n, 
    double old_a11, double old_a12, double old_a21, double old_a22,
    const vec &new_u, const vec &new_v, const vec &new_n, 
    double &new_a11, double &new_a12, double &new_a21, double &new_a22)
{
    Eigen::Matrix3d old_base_J;
    Eigen::Matrix3d new_base_J;
    Eigen::Matrix3d Jacobi;

    old_base_J(0,0) = old_u[0];
    old_base_J(1,0) = old_u[1];
    old_base_J(2,0) = old_u[2];
    old_base_J(0,1) = old_v[0];
    old_base_J(1,1) = old_v[1];
    old_base_J(2,1) = old_v[2];
    old_base_J(0,2) = old_n[0];
    old_base_J(1,2) = old_n[1];
    old_base_J(2,2) = old_n[2];


    new_base_J(0,0) = new_u[0];
    new_base_J(1,0) = new_u[1];
    new_base_J(2,0) = new_u[2];
    new_base_J(0,1) = new_v[0];
    new_base_J(1,1) = new_v[1];
    new_base_J(2,1) = new_v[2];
    new_base_J(0,2) = new_n[0];
    new_base_J(1,2) = new_n[1];
    new_base_J(2,2) = new_n[2];

    Eigen::Matrix3d inverse_old = old_base_J.inverse();
    Jacobi = old_base_J.inverse() * new_base_J ;

    new_a11 = old_a11 * Jacobi(0,0) * Jacobi(0,0) + old_a12 * Jacobi(0,0) * Jacobi(1,0) +  old_a21 * Jacobi(1,0) * Jacobi(0,0) + old_a22 * Jacobi(1,0) * Jacobi(1,0);
    new_a12 = old_a11 * Jacobi(0,0) * Jacobi(0,1) + old_a12 * Jacobi(0,0) * Jacobi(1,1) +  old_a21 * Jacobi(1,0) * Jacobi(0,1) + old_a22 * Jacobi(1,0) * Jacobi(1,1);
    new_a21 = old_a11 * Jacobi(0,1) * Jacobi(0,0) + old_a12 * Jacobi(0,1) * Jacobi(1,0) +  old_a21 * Jacobi(1,1) * Jacobi(0,0) + old_a22 * Jacobi(1,1) * Jacobi(1,0);
    new_a22 = old_a11 * Jacobi(0,1) * Jacobi(0,1) + old_a12 * Jacobi(0,1) * Jacobi(1,1) +  old_a21 * Jacobi(1,1) * Jacobi(0,1) + old_a22 * Jacobi(1,1) * Jacobi(1,1);
}



// Rotate a coordinate system to be perpendicular to the given normal
// Szymon Rusinkiewicz
// Princeton University
static void rot_coord_sys(const vec &old_u, const vec &old_v,
    const vec &new_norm,
    vec &new_u, vec &new_v)
{

    /*
    new_u = old_u;
    new_v = old_v;
    vec old_norm = old_u CROSS old_v;
    double ndot = old_norm DOT new_norm;
    if (unlikely(ndot <= -1.0f)) {
    new_u = -new_u;
    new_v = -new_v;
    return;
    } 

    // Normal components of <new_u> and <new_v> with respect to <new_norm>
    vec perp_old = new_norm - ndot * old_norm;

    // Normal component of <new_norm> with respect to <old_norm>
    // new_u = new_u - (new_norm) * (new_u DOT (new_norm))
    //       = new_u - (new_norm) * (new_u DOT (ndot * old_norm + perp_old))
    //       = new_u - (new_norm) * (new_u DOT perp_old)
    // Instead =, using <dperp> to ensure that <new_u> and <new_v>
    // are unitary

    vec dperp = 1.0f / (1 + ndot) * (old_norm + new_norm);

    new_u -= dperp * (new_u DOT perp_old);
    new_v -= dperp * (new_v DOT perp_old);
    */
    vec old_norm = old_u CROSS old_v;
    normalize(old_norm);
    double ndot = old_norm DOT new_norm;

    if (unlikely(ndot <= -1.0f)) 
    {
        new_u = -old_u;
        new_v = -old_v;
        return;
    } 
    else 
        if (fabs(ndot-1.0) < 1e-6) 
        {
            new_u = old_u;
            new_v = old_v;
            return;
        }

        vec rot = new_norm CROSS old_norm;

        normalize(rot);  // direction vector must be unitary (Ting - 27/01/2013)

        double r00, r01, r02, r10, r11, r12, r20, r21, r22;
        double s = sqrt(1 - ndot*ndot);

        // Bug: 3 expressions are incorrect (Ting - 27/01/2012)
        // r00 = ndot + (1-ndot)*rot[0]*rot[0];
        r00 = rot[0]*rot[0] + (1-rot[0]*rot[0])*ndot;
        r01 = (1-ndot)*rot[0]*rot[1] - s*rot[2];
        r02 = (1-ndot)*rot[0]*rot[2] + s*rot[1];
        r10 = (1-ndot)*rot[0]*rot[1] + s*rot[2];
        // r11 = ndot + (1-ndot)*rot[1]*rot[1];
        r11 = rot[1]*rot[1]+(1-rot[1]*rot[1])*ndot;
        r12 = (1-ndot)*rot[1]*rot[2]-s*rot[0];
        r20 = (1-ndot)*rot[0]*rot[2]-s*rot[1];
        r21 = (1-ndot)*rot[1]*rot[2]+s*rot[0];
        // r22 = ndot + (1-ndot)*rot[2]*rot[2];
        r22 = rot[2]*rot[2]+(1-rot[2]*rot[2])*ndot;

        new_u[0] = r00*old_u[0] + r01*old_u[1] + r02*old_u[2];
        new_u[1] = r10*old_u[0] + r11*old_u[1] + r12*old_u[2];
        new_u[2] = r20*old_u[0] + r21*old_u[1] + r22*old_u[2];
        new_v[0] = r00*old_v[0] + r01*old_v[1] + r02*old_v[2];
        new_v[1] = r10*old_v[0] + r11*old_v[1] + r12*old_v[2];
        new_v[2] = r20*old_v[0] + r21*old_v[1] + r22*old_v[2];

        vec tmp;
        tmp[0] = r00*new_norm[0] + r01*new_norm[1] + r02*new_norm[2];
        tmp[1] = r10*new_norm[0] + r11*new_norm[1] + r12*new_norm[2];
        tmp[2] = r20*new_norm[0] + r21*new_norm[1] + r22*new_norm[2];

        double t = new_norm DOT old_norm;

        //LOG_LINE(cout, "ndot = " << ndot << "; s = " << s << "; new_norm = " << new_norm << ", " << old_norm << ", t = " << t);
}


// Given a curvature tensor, find principal directions and curvatures
// Makes sure that pdir1 and pdir2 are perpendicular to normal
// Szymon Rusinkiewicz
// Princeton University
void diagonalize_curv(const vec &old_u, const vec &old_v,
    double ku, double kuv, double kv,
    const vec &new_norm,
    vec &pdir1, vec &pdir2, double &k1, double &k2)
{
    vec r_old_u, r_old_v;
    rot_coord_sys(old_u, old_v, new_norm, r_old_u, r_old_v);

    double c = 1, s = 0, tt = 0;
    if (likely(kuv != 0.0f)) 
    {
        // Jacobi rotation to diagonalize
        double h = 0.5f * (kv - ku) / kuv;
        tt = (h < 0.0f) ?
            1.0f / (h - sqrt(1.0f + h*h)) :
        1.0f / (h + sqrt(1.0f + h*h));
        c = 1.0f / sqrt(1.0f + tt*tt);
        s = tt * c;
    }

    k1 = ku - tt * kuv;
    k2 = kv + tt * kuv;

    if (fabs(k1) >= fabs(k2)) 
    {
        pdir1 = c*r_old_u - s*r_old_v;
    } 
    else 
    {
        swap(k1, k2);
        pdir1 = s*r_old_u + c*r_old_v;
    }
    pdir2 = new_norm CROSS pdir1;
}

void estimate_tensors (const int valence,
    const point *V, const vec *nv, 
    vec &base_a1, vec &base_a2, vec &base_n,
    vec &pdir1, vec &pdir2,
    double &return_a11, double &return_a12, double &return_a22,
    double &return_b11, double &return_b12, double &return_b22, 
    double &k1, double &k2)
{
    vec t,b,n;

    n = nv[0];
    t = V[1] - V[0];
    normalize(t);
    b = n CROSS t;

    double m[3] = {0, 0, 0};
    double w[3][3] = { {0,0,0}, {0,0,0}, {0,0,0} };

    vec ee = V[1] - V[0];

    double u = ee DOT t;
    double v = ee DOT b;

    for (int j = 1; j < (valence+1); j++) 
    {
        vec e1 = V[(j%valence+1)]-V[0];
        double u1 = e1 DOT t;
        double v1 = e1 DOT b;
        w[0][0] += u*u;
        w[0][1] += u*v;
        w[2][2] += v*v;
        vec dn = nv[j] - nv[0];
        double dnu = dn DOT t;
        double dnv = dn DOT b;
        m[0] += dnu*u;
        m[1] += dnu*v + dnv*u;
        m[2] += dnv*v;
        u = u1; v = v1;
    }

    w[1][1] = w[0][0] + w[2][2];
    w[1][2] = w[0][1];

    double diag[3];
    double b11, b12, b22;

    if (ldltdc<double,3>(w, diag)) 
    { 
        ldltsl<double,3>(w, diag, m, m);
        b11 = m[0], b12 = m[1], b22 = m[2];
    } 
    else 
    {
        LOG_LINE(cout, "LS: no solution Singular!!");
        // TODO: how this case should be handled? (Ting)
    }

    diagonalize_curv(t, b, b11, b12, b22,
        n, pdir1, pdir2, k1, k2);

    normalize(pdir1);
    normalize(pdir2);

    // TODO: Verificar inversao de sinais em k1 em k1
    k1 *= -1;
    k2 *= -1;

    return_a11 = base_a1 DOT base_a1;
    return_a12 = base_a1 DOT base_a2;
    return_a22 = base_a2 DOT base_a2;

    proj_tens(pdir1,pdir2,pdir1 CROSS pdir2, k1,0,0,k2,base_a1, base_a2, base_n,return_b11,return_b12,return_b12,return_b22);

    return;
}

double compute_tensors(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    vector<VertexID> vlist;
    vector<VertexID>::iterator vIter;
    vector<FaceID> flist;
    vector<FaceID>::iterator fIter;

    point *p;
    vec *vp;
    vec pdir1, pdir2;
    Point3DGeom *vptr, *v_vptr;
    int valence;
    Triangle *ftmp;
    vec vectmp;

    m->getAllVertices(&list);
    // Estimate the metric and curvature tensors
    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
        // Get adjacent vertices

        m->getVVertices(*Iter, &vlist);

        p = (point *)malloc(sizeof(point)*(vlist.size()+1));
        vp = (vec *)malloc(sizeof(vec)*(vlist.size()+1));

        // Array p[x] = p[0] (vertice atual) + adjacentes
        // Array vp[x] = vp[0] (normal vertice atual) + normal dos vertices adjacentes
        p[0][0] = vptr->x;
        p[0][1] = vptr->y;
        p[0][2] = vptr->z;
        vp[0][0] = vptr->n[0];
        vp[0][1] = vptr->n[1];
        vp[0][2] = vptr->n[2];
        for ( valence = 1, vIter = vlist.begin(); vIter != vlist.end(); vIter++) 
        {
            v_vptr = (Point3DGeom *)m->getVGeomPtr(*vIter);

            p[valence][0] = v_vptr->x; 
            p[valence][1] = v_vptr->y;
            p[valence][2] = v_vptr->z;

            //if (vptr->border_id==0)
            if(1)
            {
                vp[valence][0] = 0;
                vp[valence][1] = 0;
                vp[valence][2] = 0;
                m->getAdjFaces(*Iter,*vIter, &flist);
                for ( fIter = flist.begin( ) ; fIter != flist.end( ) ; fIter++ ) 
                {
                    ftmp = (Triangle *)m->getFGeomPtr(*fIter);
                    vp[valence][0] += ftmp->nx;
                    vp[valence][1] += ftmp->ny;
                    vp[valence][2] += ftmp->nz;
                }
                vp[valence] *= 1./flist.size();
                flist.clear();

                normalize(vp[valence]);

                vectmp = p[valence] - p[0];
                vectmp *= 0.5;
                p[valence] = p[0] + vectmp;
            }
            else
            {
                vp[valence][0] = v_vptr->n[0];
                vp[valence][1] = v_vptr->n[1];
                vp[valence][2] = v_vptr->n[2];
            }
            valence++;
        }
        valence--;


        estimate_bases_a(m, vptr);

        estimate_tensors (valence, p, vp, 
            vptr->base_a[0], vptr->base_a[1], vptr->base_a[2], 
            vptr->pdir1, vptr->pdir2, 
            vptr->a[0][0], vptr->a[0][1],vptr->a[1][1], 
            vptr->b[0][0], vptr->b[0][1], vptr->b[1][1], 
            vptr->k1, vptr->k2);

        vptr->a[1][0] = vptr->a[0][1];
        vptr->b[1][0] = vptr->b[0][1];

        // Calcula tensor metrico contravariante
        vptr->area_det_a = (vptr->a[0][0]*vptr->a[1][1]) - (vptr->a[0][1]*vptr->a[0][1]);

        if (fabs(vptr->area_det_a) > 1e-6) 
        {
            vptr->c_a[0][0] = vptr->a[1][1]/vptr->area_det_a;
            vptr->c_a[0][1] = -vptr->a[0][1]/vptr->area_det_a;
            vptr->c_a[1][1] = vptr->a[0][0]/vptr->area_det_a;
        } 
        else 
        {
            vptr->c_a[0][0] = vptr->c_a[0][1] = vptr->c_a[1][1] = 0.;
        }

        vptr->area_det_a = sqrt(vptr->area_det_a);

        vptr->c_a[1][0] = vptr->c_a[0][1];

        // Clear list of adjacent vertices
        free((char *)p);
        free((char *)vp);
        vlist.clear();
    }
    list.clear();

    /*
    // Tratamento da borda
    m->getBorderVertices(&list);
    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
    // copia tensor de curvatura (para bordas)
    if (*Iter>-1)
    {
    vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
    v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->BorderCopyVertex);
    vptr->b[0][0] = v_vptr->b[0][0];			
    vptr->b[1][0] = v_vptr->b[1][0];			
    vptr->b[0][1] = v_vptr->b[0][1];			
    vptr->b[1][1] = v_vptr->b[1][1];			
    }

    }
    list.clear();
    */

    RETURN_FUNCTION_TIMER();
} 

double compute_phi_psi_theta(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    Point3DGeom *vptr;
    m->getAllVertices(&list);
    int alpha,beta,gamma,delta;

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
        for (alpha=0; alpha<2; alpha++)
            for (beta=0; beta<2; beta++)
                for (gamma=0; gamma<2; gamma++)
                    for (delta=0; delta<2; delta++)
                    {
                        vptr->phi[alpha][beta][gamma][delta] = vptr->psi[alpha][beta][gamma][delta] = vptr->theta[alpha][beta][gamma][delta] = 
                            vptr->c_a[alpha][beta] * vptr->c_a[gamma][delta]
                        + vptr->c_a[alpha][gamma] * vptr->c_a[beta][delta] 
                        + vptr->c_a[alpha][delta] * vptr->c_a[beta][gamma];


                        vptr->phi[alpha][beta][gamma][delta] *= ZETA;
                        vptr->psi[alpha][beta][gamma][delta] *= XI;
                        vptr->theta[alpha][beta][gamma][delta] *= PHI;
                    }
    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}


double compute_tensors_n_m(Mesh *m, Mesh *m0)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    Point3DGeom *vptr, *vptr_inicial;

    m->getAllVertices (&list);

    energia_A_min = 1.e20;
    energia_A_max = -1.e20;

    int alpha,beta,gamma,delta;

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
        vptr_inicial = (Point3DGeom *)m0->getVGeomPtr(vptr->previous);

        vptr->epsilon[0][0] = 0.5 * (vptr->a[0][0] - vptr_inicial->a[0][0]);
        vptr->epsilon[0][1] = vptr->epsilon[1][0] = 0.5 * (vptr->a[0][1]- vptr_inicial->a[0][1]);
        vptr->epsilon[1][1] = 0.5 * (vptr->a[1][1] - vptr_inicial->a[1][1]);

        vptr->kappa[0][0] = -1 * (vptr->b[0][0] - vptr_inicial->b[0][0]);
        vptr->kappa[0][1] = vptr->kappa[1][0]= -1 * (vptr->b[0][1] - vptr_inicial->b[0][1]);
        vptr->kappa[1][1] = -1 * (vptr->b[1][1] - vptr_inicial->b[1][1]);

        // aproveita para atualizar vptr->mu e vptr->damp

        //vptr->mu =  vptr_inicial->mu / vptr->area_det_a/vptr_inicial->area_det_a;
        vptr->mu =  vptr_inicial->mu / (vptr->area_det_a/vptr_inicial->area_det_a);
        //vptr->damp = DAMP / vptr->area_det_a/(vptr_inicial->area_det_a);

        vptr->energia_A = 0;
        for (alpha=0; alpha<2; alpha++)
        {
            for (beta=0; beta<2; beta++)
            {
                vptr->M[alpha][beta] = 0.;
                vptr->NN[alpha][beta] = 0.;
                if (vptr->MetricVertex[alpha] > -1) {
                    for (gamma=0; gamma<2; gamma++)
                    {
                        for (delta=0; delta<2; delta++)
                        {
                            vptr->energia_A += 
                                (vptr->phi[alpha][beta][gamma][delta] * vptr->epsilon[alpha][beta] * vptr->epsilon[gamma][delta] +
                                vptr->psi[alpha][beta][gamma][delta] * vptr->kappa[alpha][beta] * vptr->kappa[gamma][delta] +
                                vptr->theta[alpha][beta][gamma][delta] * vptr->epsilon[alpha][beta] * vptr->kappa[gamma][delta]);		
                            vptr->M[alpha][beta] += (vptr->psi[alpha][beta][gamma][delta] * vptr->kappa[gamma][delta]
                            + vptr->theta[alpha][beta][gamma][delta] * vptr->epsilon[gamma][delta]);
                            vptr->NN[alpha][beta] += (vptr->phi[alpha][beta][gamma][delta] * vptr->epsilon[gamma][delta] 
                            + vptr->theta[alpha][beta][gamma][delta] * vptr->kappa[gamma][delta]);
                        }
                    }
                } else {
                    // Natural boundary condition
                    for (gamma=0; gamma<2; gamma++)
                    {
                        for (delta=0; delta<2; delta++)
                        {
                            vptr->energia_A += 
                                vptr->theta[alpha][beta][gamma][delta] * vptr->epsilon[alpha][beta] * vptr->kappa[gamma][delta];

                            vptr->M[alpha][beta] += vptr->theta[alpha][beta][gamma][delta] * vptr->epsilon[gamma][delta];
                        }
                    }
                }
            }
        }


        for (alpha=0; alpha<2; alpha++)
        {
            for (beta=0; beta<2; beta++)
            {
                vptr->N[alpha][beta] = vptr->NN[alpha][beta];
                for (gamma=0; gamma<2; gamma++)
                {
                    for (delta=0; delta<2; delta++)
                    {
                        vptr->N[alpha][beta] += - vptr->M[beta][gamma] * (vptr->c_a[alpha][delta] * vptr->b[delta][gamma]); 
                    }
                }
            }
        }
        // Comentei a linha abaixo, pois s�o redundantes. (Ting)
        //vptr->N[2][0] = vptr->N[2][1] =  0;

        // Guarda m�ximo nas globais

        if (vptr->energia_A>energia_A_max) 
        {
            energia_A_max = vptr->energia_A;
            x_energia_A_max = vptr->x;
            y_energia_A_max = vptr->y;
        }
        if (vptr->energia_A<energia_A_min) 
        {
            energia_A_min = vptr->energia_A;
            x_energia_A_min = vptr->x;
            y_energia_A_min = vptr->y;
        }
    }

    //LOG_LINE(cout, "Energia_A_max: " << energia_A_max << "(" << x_energia_A_max << ";" << y_energia_A_max << ")");
    //LOG_LINE(cout, "Energia_A_min: " << energia_A_min << "(" << x_energia_A_min << ";" << y_energia_A_min << ")");

    list.clear();

    RETURN_FUNCTION_TIMER();
}

void debug_print_values(Mesh *m)
{
    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    Point3DGeom *vptr;

    m->getAllVertices(&list);
    //int alpha,beta,gamma,delta;

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {

        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);


        if ( 
            is_point_debug(vptr)
            )
        {
            vptr->debug_ponto=1;
            LOG_LINE(cout, "**************************** ");
            LOG_LINE(cout, "u: " << vptr->u_T << " ; v: " << vptr->v_T << " ; t: " << current_t);
            LOG_LINE(cout, "x: " << vptr->x << " ; y: " << vptr->y << " ; z: " << vptr->z);
            LOG_LINE(cout, "base_a1 : " << vptr->base_a[0] << " base_a2 : " << vptr->base_a[1] << "  du : " << diff_base[0] << " dv : " << diff_base[1]);
            LOG_LINE(cout, "k: " << vptr->k1 << "  , " << vptr->k2);
            LOG_LINE(cout, "a  : " << vptr->a[0][0] << "  , " << vptr->a[0][1] << " , "  << vptr->a[1][1]);
            LOG_LINE(cout, "b: " << vptr->b[0][0] << "  , " << vptr->b[0][1] << " , "  << vptr->b[1][1]);
            //LOG_LINE(cout, "c_a  : " << vptr->c_a[0][0] << "  , " << vptr->c_a[0][1] << " , "  <<  vptr->c_a[0][1] << " , " << vptr->c_a[1][1]);
            //LOG_LINE(cout, "b_T: " << vptr->b11_T << " , " << vptr->b12_T << " , " << vptr->b22_T);
            //LOG_LINE(cout, "bw: " << vptr->bw[0][0] << " ; " << vptr->bw[0][1] << " ; " << vptr->bw[1][0] << " ; " << vptr->bw[1][1]);
            //LOG_LINE(cout, "area_det_a : " << vptr->area_det_a << "    mu : " << vptr->mu << " damp : " << vptr->damp);
            LOG_LINE(cout, "epsilon: " << vptr->epsilon[0][0] << " ; " << vptr->epsilon[0][1] << " ; " << vptr->epsilon[1][1]);
            LOG_LINE(cout, "kappa: " << vptr->kappa[0][0] << " ; " <<  vptr->kappa[0][1] << " ; " << vptr->kappa[1][1]);
            /*
            LOG_LINE(cout, "phi: ";
            for ( alpha=0;alpha<2;alpha++) 
            for ( beta=0;beta<2;beta++) 
            for ( gamma=0;gamma<2;gamma++) 
            for ( delta=0;delta<2;delta++) 
            LOG_LINE(cout, vptr->phi[alpha][beta][gamma][delta] << ", ";
            LOG_LINE(cout, endl;

            LOG_LINE(cout, "psi: ";
            for ( alpha=0;alpha<2;alpha++) 
            for ( beta=0;beta<2;beta++) 
            for ( gamma=0;gamma<2;gamma++) 
            for ( delta=0;delta<2;delta++) 
            LOG_LINE(cout, vptr->psi[alpha][beta][gamma][delta] << ", ";
            LOG_LINE(cout, endl;

            LOG_LINE(cout, "theta: ";
            for ( alpha=0;alpha<2;alpha++) 
            for ( beta=0;beta<2;beta++) 
            for ( gamma=0;gamma<2;gamma++) 
            for ( delta=0;delta<2;delta++) 
            LOG_LINE(cout, vptr->theta[alpha][beta][gamma][delta] << ", ";
            LOG_LINE(cout, endl;
            */
            //LOG_LINE(cout, "chris1_xx1: " << vptr->chris_tipo1[0][0][0] << " , " << vptr->chris_tipo1[0][1][0] << " , " << vptr->chris_tipo1[1][0][0] << " , " << vptr->chris_tipo1[1][1][0]);
            //LOG_LINE(cout, "chris1_xx2: " << vptr->chris_tipo1[0][0][1] << " , " << vptr->chris_tipo1[0][1][1] << " , " << vptr->chris_tipo1[1][0][1] << " , " << vptr->chris_tipo1[1][1][1]);
            LOG_LINE(cout, "chris2_xx1: " << vptr->chris_tipo2[0][0][0] << " , " << vptr->chris_tipo2[0][1][0] << " , " << vptr->chris_tipo2[1][0][0] << " , " << vptr->chris_tipo2[1][1][0]);
            LOG_LINE(cout, "chris2_xx2: " << vptr->chris_tipo2[0][0][1] << " , " << vptr->chris_tipo2[0][1][1] << " , " << vptr->chris_tipo2[1][0][1] << " , " << vptr->chris_tipo2[1][1][1]);
            LOG_LINE(cout, "M1: (" << vptr->M[0][0] << " , " << vptr->M[1][0] <<  " )");
            LOG_LINE(cout, "M2: (" << vptr->M[0][1] << " , " << vptr->M[1][1] << " )");
            LOG_LINE(cout, "NN1: (" << vptr->NN[0][0] << " , " << vptr->NN[1][0] << " )");
            LOG_LINE(cout, "NN2: (" << vptr->NN[0][1] << " , " << vptr->NN[1][1] << " )");
            LOG_LINE(cout, "N1: (" << vptr->N[0][0] << " , " << vptr->N[1][0] << " , " << vptr->N[2][0] << " )");
            LOG_LINE(cout, "N2: (" << vptr->N[0][1] << " , " << vptr->N[1][1] << " , " << vptr->N[2][1] << " )");
            LOG_LINE(cout, "dc_M_0xx: (" << vptr->dc_M[0][0][0] << " , " << vptr->dc_M[0][0][1] << " , " << vptr->dc_M[0][1][0] << " , " << vptr->dc_M[0][1][1] << " )");
            LOG_LINE(cout, "dc_M_1xx: (" << vptr->dc_M[1][0][0] << " , " << vptr->dc_M[1][0][1] << " , " << vptr->dc_M[1][1][0] << " , " << vptr->dc_M[1][1][1] << " )");
            LOG_LINE(cout, "diff_N_0xx: (" << vptr->diff_N[0][0][0] << " , " << vptr->diff_N[0][0][1] << " , " << vptr->diff_N[0][1][0] << " , " << vptr->diff_N[0][1][1] << " )");
            LOG_LINE(cout, "diff_N_1xx: (" << vptr->diff_N[1][0][0] << " , " << vptr->diff_N[1][0][1] << " , " << vptr->diff_N[1][1][0] << " , " << vptr->diff_N[1][1][1] << " )");
            LOG_LINE(cout, "dc_N_0xx: (" << vptr->dc_N[0][0][0] << " , " << vptr->dc_N[0][0][1] << " , " << vptr->dc_N[0][1][0] << " , " << vptr->dc_N[0][1][1] << " )");
            LOG_LINE(cout, "dc_N_1xx: (" << vptr->dc_N[1][0][0] << " , " << vptr->dc_N[1][0][1] << " , " << vptr->dc_N[1][1][0] << " , " << vptr->dc_N[1][1][1] << " )");
            LOG_LINE(cout, "dc2_Mxx (00,01,10,11): (" << vptr->dc2_M[0][0] << " , " << vptr->dc2_M[0][1] << " , " << vptr->dc2_M[1][0] << " , " << vptr->dc2_M[1][1] << " )");
            LOG_LINE(cout, "dcNx_x: " << vptr->dc_N_cart[0] << "  ; " << vptr->dc_N_cart[1]);
            LOG_LINE(cout, "force external: " << vptr->force_external_cart <<  "  forces: "<< vptr->force_vertex_cart <<  "  forces with damping: "<< vptr->force_vertex_cart - DAMP * vptr->v_cart);

            LOG_LINE(cout, "v (cart): " << vptr->v_cart);

        }
    }
}

// TODO: Colocar novamente tudo na mesma funcao para fazer uma passada s�
double compute_chris(Mesh *m) 
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;

    Point3DGeom *vptr;
    Point3DGeom *diff_vptr_prev, *diff_vptr_next;

    int i,j,k;
    int alpha,beta,gamma;

    double diff_g[3][3][3]; // diff_g[gamma][alpha][beta] - derivada de g[alpha][beta] em relacao a gamma
    //double c_g[3][3];

    m->getAllVertices(&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        // Atualiza diferenciais para a, N e M.
        for (alpha=0;alpha<2;alpha++)
        {
            for (beta=0;beta<2;beta++)
            {
                for (gamma=0;gamma<2;gamma++)
                {
                    vptr->diff_a[alpha][beta][gamma] = 0;
                    vptr->diff_N[alpha][beta][gamma] = 0;
                    vptr->diff_M[alpha][beta][gamma] = 0;
                }
            }

            if (vptr->MetricVertex[alpha]>-1)
            {
                diff_vptr_next = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex[alpha]);
                for (beta=0;beta<2;beta++)
                {
                    for (gamma=0;gamma<2;gamma++)
                    {
                        vptr->diff_a[alpha][beta][gamma] += (diff_vptr_next->a[beta][gamma] - vptr->a[beta][gamma]);
                    }
                }
            }
            else if (vptr->MetricVertex_return[alpha]>-1)
            {
                diff_vptr_prev = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex_return[alpha]);
                for (beta=0;beta<2;beta++)
                {
                    for (gamma=0;gamma<2;gamma++)
                    {
                        vptr->diff_a[alpha][beta][gamma] += (vptr->a[beta][gamma] - diff_vptr_prev->a[beta][gamma]);
                    }
                }
            }

            for (beta=0;beta<2;beta++)
            {
                for (gamma=0;gamma<2;gamma++)
                {
                    vptr->diff_N[alpha][beta][gamma] += vptr->N[beta][gamma];
                    vptr->diff_M[alpha][beta][gamma] += vptr->M[beta][gamma];
                    if (vptr->MetricVertex_return[alpha]>-1) {
                        // Backward finite difference (Ting - 16/02/2013)
                        diff_vptr_prev = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex_return[alpha]);
                        vptr->diff_N[alpha][beta][gamma] -= diff_vptr_prev->N[beta][gamma];
                        vptr->diff_M[alpha][beta][gamma] -= diff_vptr_prev->M[beta][gamma];
                    } else {
                        // Natural boundary conditions: no restrictions (Ting - 16/02/2013)	
                    }
                }
            } 

            for (beta=0;beta<2;beta++)
            {
                for (gamma=0;gamma<2;gamma++)
                {
                    vptr->diff_a[alpha][beta][gamma] *= 1/(diff_base[alpha]);
                    vptr->diff_N[alpha][beta][gamma] *= 1/(diff_base[alpha]);
                    vptr->diff_M[alpha][beta][gamma] *= 1/(diff_base[alpha]);
                }
            }
        }

        // Continua calculo de christoff

        for (i=0;i<2;i++)
        {
            for (j=0;j<2;j++)
            {
                for (k=0;k<2;k++)
                {
                    diff_g[i][j][k] = vptr->diff_a[i][j][k];
                }
            }

            diff_g[i][0][2] = 0.;
            diff_g[i][1][2] = 0.;
            diff_g[i][2][0] = 0.;
            diff_g[i][2][1] = 0.;
            diff_g[i][2][2] = 0.;

        }

        diff_g[2][0][0] = 0.;
        diff_g[2][0][1] = 0.;
        diff_g[2][0][2] = 0.;
        diff_g[2][1][0] = 0.;
        diff_g[2][1][1] = 0.;
        diff_g[2][1][2] = 0.;
        diff_g[2][2][0] = 0.;
        diff_g[2][2][1] = 0.;
        diff_g[2][2][2] = 0.;

        for (i=0; i<3; i++)
        {
            for (j=0; j<3; j++)
            {
                for (k=0; k<3; k++)
                {
                    vptr->chris_tipo1[i][j][k] = 
                        0.5 * ( diff_g[i][j][k] + diff_g[j][k][i] - diff_g[k][i][j]);
                }
            }
        }

        for (alpha=0; alpha<2; alpha++)
        {
            for (beta=0; beta<2; beta++)
            {
                for (gamma=0; gamma<2; gamma++)
                {
                    vptr->chris_tipo2[alpha][beta][gamma] = 
                        ( vptr->c_a[0][gamma] * vptr->chris_tipo1[alpha][beta][0] +
                        vptr->c_a[1][gamma] * vptr->chris_tipo1[alpha][beta][1]);
                }
            }
        }
    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}

double compute_dm(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    vec t,b,n;

    Point3DGeom *vptr;

    int alpha,beta,gamma,lambda;

    //double diff_M[2][2][2]; // diff_M[gamma][alpha][beta] - derivada de M[alpha][beta] em relacao a gamma

    m->getAllVertices(&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        for (alpha=0; alpha<2; alpha++)
        {
            for (beta=0; beta<2; beta++)
            {
                for (gamma=0; gamma<2; gamma++)
                {

                    vptr->dc_M[gamma][alpha][beta] = vptr->diff_M[gamma][alpha][beta];
                    for (lambda=0; lambda<2; lambda++)
                    {
                        vptr->dc_M[gamma][alpha][beta] += vptr->chris_tipo2[gamma][lambda][alpha] * vptr->M[lambda][beta] + vptr->chris_tipo2[gamma][lambda][beta] * vptr->M[alpha][lambda];
                    }
                }
            }
        }

        // Natural boundary condition
        // Bordas: Zerando todas as derivadas com falta de informacao:
        // TODO: Parar de usar loops for... no calculo da derivada para ja embutir esses zeros.
        // Acho que n�o faz efeito j� que voc� zera na frente ... (Ting, 5/1/2013)
        if ( (vptr->MetricVertex[0]==-1) || (vptr->MetricVertex_return[0]==-1) )
        {
            //	vptr->dc_M[0][0][0] = vptr->dc_M[0][0][1] = vptr->dc_M[0][1][0] = vptr->dc_M[0][1][1] = 0;
        }

        if ( (vptr->MetricVertex[1]==-1) || (vptr->MetricVertex_return[1]==-1) )
        {
            //	vptr->dc_M[1][0][0] = vptr->dc_M[1][0][1] = vptr->dc_M[1][1][0] = vptr->dc_M[1][1][1] = 0;
        }

    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}

// TODO: Altamente n�o otimizado.
double compute_d2m_dn(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    Point3DGeom *vptr;
    //Point3DGeom *a_vptr[2];
    Point3DGeom *diff_vptr_prev, *diff_vptr_next;
    int alpha,beta,lambda,gamma;

    m->getAllVertices(&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        // Primeiro passo, calcula differenciais de dc_M

        for (alpha=0;alpha<2;alpha++)
        {

            if (vptr->MetricVertex_return[alpha]>-1)
                diff_vptr_prev = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex_return[alpha]);

            if (vptr->MetricVertex[alpha]>-1)
                diff_vptr_next = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex[alpha]);


            for (beta=0;beta<2;beta++)
            {
                for (gamma=0;gamma<2;gamma++)
                {
                    for (lambda=0;lambda<2;lambda++)
                    {
                        vptr->diff_dc_M[alpha][beta][gamma][lambda] = 0;
                    }
                }
            }


            // TODO: Desnecessario calcular todas as diferenciais, s� algumas s�o utilizadas. 
            // TODO: Desnecessario guardar na estrutura da malha essa diferencial se for so utilizada aqui.
            // diff_sum = 0;
            // diff_sum++;
            // if (vptr->MetricVertex[alpha]>-1)
            //   {
            //     //diff_sum++;
            //     for (beta=0;beta<2;beta++)
            // 	{
            // 	  for (gamma=0;gamma<2;gamma++)
            // 	    {
            // 	      for (lambda=0;lambda<2;lambda++)
            // 		{
            // 		  vptr->diff_dc_M[alpha][beta][gamma][lambda] += (diff_vptr_next->dc_M[beta][gamma][lambda] - vptr->dc_M[beta][gamma][lambda]);
            // 		}
            // 	    }
            // 	}
            //   }

            // diff_sum++;
            // if (vptr->MetricVertex_return[alpha]>-1)
            //   {
            //     //diff_sum++;
            //     for (beta=0;beta<2;beta++)
            // 	{
            // 	  for (gamma=0;gamma<2;gamma++)
            // 	    {
            // 	      for (lambda=0;lambda<2;lambda++)
            // 		{
            // 		  vptr->diff_dc_M[alpha][beta][gamma][lambda] += (vptr->dc_M[beta][gamma][lambda] - diff_vptr_prev->dc_M[beta][gamma][lambda]);
            // 		}
            // 	    }
            // 	}
            //   }

            // for (beta=0;beta<2;beta++)
            //   {
            //     for (gamma=0;gamma<2;gamma++)
            // 	{
            // 	  for (lambda=0;lambda<2;lambda++)
            // 	    {
            // 	      vptr->diff_dc_M[alpha][beta][gamma][lambda] *= 1/(diff_sum * diff_base[alpha]);
            // 	    }
            // 	}
            //   }

            for (beta=0;beta<2;beta++)
            {
                // Forward finite difference (Ting - 16/02/2013)
                vptr->diff_dc_M[alpha][beta][0][0] -= (vptr->dc_M[beta][0][0]);
                vptr->diff_dc_M[alpha][beta][1][1] -= (vptr->dc_M[beta][1][1]);
                // Backward finite difference (Ting - 16/02/2013)
                vptr->diff_dc_M[alpha][beta][0][1] += (vptr->dc_M[beta][0][1]);
                vptr->diff_dc_M[alpha][beta][1][0] += (vptr->dc_M[beta][1][0]);
                if (vptr->MetricVertex[alpha]>-1) {
                    vptr->diff_dc_M[alpha][beta][0][0] += (diff_vptr_next->dc_M[beta][0][0]);
                    vptr->diff_dc_M[alpha][beta][1][1] += (diff_vptr_next->dc_M[beta][1][1]);
                } else {
                    // Natural boundary condition: no internal forces (Ting)
                }
                if (vptr->MetricVertex_return[alpha]>-1) {
                    vptr->diff_dc_M[alpha][beta][0][1] -= (diff_vptr_prev->dc_M[beta][0][1]);
                    vptr->diff_dc_M[alpha][beta][1][0] -= (diff_vptr_prev->dc_M[beta][1][0]);
                } else {
                    // Natural boundary condition: no internal forces (Ting)
                }
            }

            for (beta=0;beta<2;beta++)
            {
                for (gamma=0;gamma<2;gamma++)
                {
                    for (lambda=0;lambda<2;lambda++)
                    {
                        vptr->diff_dc_M[alpha][beta][gamma][lambda] *= 1/(diff_base[alpha]);
                    }
                }
            }
        }

        for (beta=0;beta<2;beta++)
        {
            for (alpha=0;alpha<2;alpha++)
            {
                // Para derivada covariante dupla de M[alpha][beta]
                // termo derivada ordinaria

                vptr->dc2_M[alpha][beta] = vptr->diff_dc_M[beta][alpha][beta][alpha];
                for (lambda=0;lambda<2;lambda++)
                {
                    // Soma das correcoes de christoff
                    vptr->dc2_M[alpha][beta] += vptr->chris_tipo2[lambda][beta][beta] * vptr->dc_M[alpha][lambda][alpha] +
                        vptr->chris_tipo2[lambda][beta][alpha] * vptr->dc_M[alpha][beta][lambda] -
                        vptr->chris_tipo2[alpha][beta][lambda] * vptr->dc_M[lambda][beta][alpha];

                }		
            }

        }

        // Calculo de N^{alpha}{beta}|{alpha}

        for (alpha=0; alpha<2; alpha++)
        {
            for (beta=0; beta<2; beta++)
            {
                for (gamma=0; gamma<2; gamma++)
                {
                    vptr->dc_N[gamma][alpha][beta] = vptr->diff_N[gamma][alpha][beta];
                    for (lambda=0; lambda<2; lambda++)
                    {
                        vptr->dc_N[gamma][alpha][beta] += vptr->chris_tipo2[gamma][lambda][alpha] * vptr->N[lambda][beta] + vptr->chris_tipo2[gamma][lambda][beta] * vptr->N[alpha][lambda];
                    }
                }
            }
        }

        // Natural boundary condition: tangential forces are null
        // Bordas: Zerando todas as derivadas com falta de informacao:
        // TODO: Parar de usar loops for... no calculo da derivada para ja embutir esses zeros.
        if ( (vptr->MetricVertex[0]==-1) || (vptr->MetricVertex_return[0]==-1) )
        {
            //	vptr->dc2_M[0][1] = vptr->dc2_M[1][1] = 0.;
            //	vptr->dc_N[0][0][0] = vptr->dc_N[0][0][1] = vptr->dc_N[0][1][0] = vptr->dc_N[0][1][1] = 0.;
        }

        if ( (vptr->MetricVertex[1]==-1) || (vptr->MetricVertex_return[1]==-1) )
        {
            //	vptr->dc2_M[0][0] = vptr->dc2_M[1][0] = 0.;
            //	vptr->dc_N[1][0][0] = vptr->dc_N[1][0][1] = vptr->dc_N[1][1][0] = vptr->dc_N[1][1][1] = 0.;

        }

    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}


double compute_n3(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;

    Point3DGeom *vptr;

    int alpha,beta;

    m->getAllVertices(&list);


    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        for (beta=0;beta<2;beta++)
        {
            vptr->N[2][beta] = 0;
            for (alpha=0;alpha<2;alpha++)
            {
                vptr->N[2][beta] += vptr->dc_M[alpha][beta][alpha];
            }
        }

    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}


double compute_new_positions(Mesh *m, bool stopcond)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    Point3DGeom *vptr,*v_vptr;
    int alpha,beta,gamma;
    vec dr_cart;
    vec t,b,n,e,border_delta_pos;
    int i;
    m->getAllVertices(&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        vptr->external_force = false;

        for (alpha=0;alpha<2;alpha++)
        {
            for (beta=0;beta<2;beta++)
            {
                vptr->bw[alpha][beta] = 0;
                for (gamma=0;gamma<2;gamma++)
                {
                    vptr->bw[alpha][beta] += vptr->c_a[gamma][beta] * vptr->b[alpha][gamma];
                }
            }
        }


        // calculo de dc_N_cart
        vptr->dc_N_cart[0] =  force_zero(vptr->dc_N[0][0][0] - vptr->N[2][0] * vptr->bw[0][0]) * vptr->base_a[0];
        vptr->dc_N_cart[0] += force_zero(vptr->dc_N[0][1][0] - vptr->N[2][0] * vptr->bw[0][1]) * vptr->base_a[1];
        vptr->dc_N_cart[0] += force_zero(vptr->N[0][0] * vptr->b[0][0] + vptr->N[1][0] * vptr->b[1][0] + vptr->dc2_M[0][0] + vptr->dc2_M[1][0]) * vptr->base_a[2];

        vptr->dc_N_cart[1] =  force_zero(vptr->dc_N[1][0][1] - vptr->N[2][1] * vptr->bw[1][0]) * vptr->base_a[0];
        vptr->dc_N_cart[1] += force_zero(vptr->dc_N[1][1][1] - vptr->N[2][1] * vptr->bw[1][1]) * vptr->base_a[1];
        vptr->dc_N_cart[1] += force_zero(vptr->N[0][1] * vptr->b[0][1] + vptr->N[1][1] * vptr->b[1][1] + vptr->dc2_M[0][1] + vptr->dc2_M[1][1]) * vptr->base_a[2];

        vptr->force_external_cart[0] = 0.;
        vptr->force_external_cart[1] = 0.;
        vptr->force_external_cart[2] = 0.;

        if ( (fabs(vptr->u_T - debug_u_force)<0.001 ) && (fabs(vptr->v_T - debug_v_force)<0.001 ) )	
        {
            if (force_bool==true)
            {
                vptr->force_external_cart[2] = 500. / vptr->mu;
                vptr->external_force = true;
            }
        }

        vptr->force_external_cart[2] += -9.8;   // adicionar forca gravitacional
        // if(vptr->border_id == 5 || vptr->border_id == 6 || vptr->border_id == 7) {
        //   vptr->force_external_cart[1] = -10.;
        // }

        //  if(vptr->border_id == 2 || vptr->border_id == 1 || vptr->border_id == 3) {
        //   vptr->force_external_cart[1] = 10.;
        //  }

        vptr->force_vertex_cart =  + vptr->dc_N_cart[0] + vptr->dc_N_cart[1] + vptr->mu * vptr->force_external_cart;

        if (vptr->collision==true)
        {
            vptr->force_vertex_cart += vptr->force_collision_cart;
        }


        // Condicao para aplicar movimento. Decide quais pontos vao ser fixos ou nao
        // (C1) 
        //if ( ( !((fabs(vptr->u_T)<0.6 ) && (fabs(vptr->v_T)<0.6 )) ) && ( (fabs(vptr->u_T)<2) && (fabs(vptr->v_T)<2) ) )
        // (C2) Borda fixa (ok)
        //if ( (vptr->u_T<umax) && (vptr->u_T>umin) && (vptr->v_T<vmax) && (vptr->v_T>vmin) )
        // (C3)
        //if ( (fabs(vptr->u_T - debug_u_force)<0.001 ) && (fabs(vptr->v_T - debug_v_force)<0.001 ) )
        // (C4)
        //if ( !((fabs(vptr->u_T)<1.2 ) && (fabs(vptr->v_T)<1.2 ))	&& ( (vptr->u_T<umax) && (vptr->u_T>umin) && (vptr->v_T<vmax) && (vptr->v_T>vmin) ) )
        // (C5) Pontos do meio e da borda fixos (ok)
        //if ( !((fabs(vptr->u_T)<0.3 ) && (fabs(vptr->v_T)<0.3 ))	&& ( (vptr->u_T<umax) && (vptr->u_T>umin) && (vptr->v_T<vmax) && (vptr->v_T>vmin) ) )
        // (C6) Pontos no meio fixos
        //if ( (fabs(vptr->u_T)>0.2) || (fabs(vptr->v_T)>0.2 ) )
        // (C7) Pontos do meio e da borda fixos
        //if ( ( (fabs(vptr->u_T)>0.2) || (fabs(vptr->v_T)>0.2 ) ) &&  ( (vptr->u_T<umax) && (vptr->u_T>umin) && (vptr->v_T<vmax) && (vptr->v_T>vmin) ) )
        // (C8) 4 pontas fixas (ok) 
        // if ( !( 
        //        ( fabs(fabs(vptr->u_T)-0.5)<0.01  && fabs(fabs(vptr->v_T)-0.5)<0.01) 
        //        ) )
        // (C8a) 4 pontas (formadas por 3 amostras) fixas
        /* if ( !( 
        ( fabs(fabs(vptr->u_T)-0.5)<0.01 ) && ( fabs(fabs(vptr->v_T)-0.5)<0.01 ) ||
        ( fabs(fabs(vptr->u_T)-0.45)<0.01 ) && ( fabs(fabs(vptr->v_T)-0.5)<0.01 ) ||
        ( fabs(fabs(vptr->u_T)-0.5)<0.01 ) && ( fabs(fabs(vptr->v_T)-0.45)<0.01 ) 
        ) ) */      
        // (C9) Duas bordas paralelas fixas (ok)
        //if ( !(fabs(vptr->u_T)==0.5))
        //if ( !(fabs(vptr->v_T)==0.5))
        // (C10) Uma borda fixa
        //if ( !(vptr->u_T==0.5))
        //if ( !(vptr->u_T==-0.5))
        //if ( !(vptr->v_T==0.5))
        //if ( !(vptr->v_T==-0.5))
        // (C11) Tres bordas fixas (ok)
        //if ( !(vptr->u_T==0.5 || vptr->u_T==-0.5 || vptr->v_T==0.5))
        //if ( !(vptr->u_T==0.5 || vptr->v_T==-0.5 || vptr->v_T==0.5))
        //if ( !(vptr->u_T==-0.5 || vptr->v_T==-0.5 || vptr->v_T==0.5))
        //if ( !(vptr->u_T==-0.5 || vptr->v_T==-0.5 || vptr->u_T==0.5))
        //if (1)
        // (C12) 1 ponta fixa 
        if ( !( ( fabs(vptr->u_T-0.5)<0.01 && fabs(vptr->v_T+0.5)<0.01 ) ) )  
            //if ( !( ( fabs(vptr->u_T-0.5)<0.01 && fabs(vptr->v_T-0.5)<0.01 ) ) )  
            //if ( !( ( fabs(vptr->u_T+0.5)<0.01 && fabs(vptr->v_T+0.5)<0.01 ) ) )  
            //if ( !( ( fabs(vptr->u_T+0.5)<0.01 && fabs(vptr->v_T-0.5)<0.01 ) ) )  
        {

            //vptr->v_next_cart = vptr->mu * vptr->v_cart + TIMESTEP * ( vptr->force_vertex_cart - DAMP * vptr->v_cart );
            //vptr->v_next_cart *= 1/vptr->mu;

            // Tentando de outra forma:
            //vptr->v_next_cart = ( 1./( (vptr->mu/TIMESTEP) + (DAMP) ) ) * (vptr->force_vertex_cart + vptr->mu * vptr->v_cart / TIMESTEP );

            // Tentando a alternativa Eq. 192 do relat�rio t�cnico ... (Ting - 4/1/2013)
            vec tforce = vptr->force_vertex_cart - DAMP * vptr->v_cart;

            vptr->v_next_cart = vptr->v_cart + (TIMESTEP * tforce)/vptr->mu;

            //if (len(vptr->v_next_cart)<EPSOLON)
            if (len(tforce)<EPSOLON)
                // || (current_t > 1000 && len(vptr->v_cart) < 1.e-6))
            {
                vptr->v_next_cart[0] = 0;
                vptr->v_next_cart[1] = 0;
                vptr->v_next_cart[2] = 0;
            }


            dr_cart = vptr->v_next_cart * TIMESTEP;

            if (TIPO_GRAFICO < 0) 
            {
                if ( stopcond && (len(dr_cart)<DR_STOP) )
                {
                    dr_cart[0] = dr_cart[1] = dr_cart[2]= 0.;
                }
                vptr->x += dr_cart[0];
                vptr->y += dr_cart[1];
                vptr->z += dr_cart[2];

            }
            vptr->v_cart = vptr->v_next_cart;
        }
    }

    list.clear();

    // Reposicionamento das bordas, testando ideia de manter posicao em relacao ao ponto vizinho, 
    // criando assim tensao 0;

    // Desabilitado no momento
    //m->getBorderVertices(&list);
    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        if (*Iter>-1)
        {

            vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

            if (*Iter == 0)
            {
                //vptr->debug = 1;
                //v_vptr->debug = 1;
            }

            e[0] = e[1] = e[2] = 0;
            border_delta_pos[0] = border_delta_pos[1] = border_delta_pos[2] = 0;
            if ( vptr->external_force==false )	
            {
                for (i=0;i<3;i++)
                {
                    v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->BorderRefVertex[i]);
                    estimate_bases_tbn_borderref(m, vptr, i, t,b,n);
                    e = vptr->BorderRefVertexCoords[i][0] * t + vptr->BorderRefVertexCoords[i][1] * b + vptr->BorderRefVertexCoords[i][2] * n;

                    border_delta_pos[0] +=  ( v_vptr->x + e[0]) - vptr->x; 
                    border_delta_pos[1] += ( v_vptr->y + e[1]) - vptr->y ; 
                    border_delta_pos[2] += ( v_vptr->z + e[2]) - vptr->z; 

                }

                border_delta_pos *= 1/3. * 0.5;
                if (is_point_debug(vptr))
                {	
                    LOG_LINE(cout, "delta_pos_sum" << border_delta_pos);
                }

                vptr->x = vptr->x + border_delta_pos[0];		
                vptr->y = vptr->y + border_delta_pos[1];		
                vptr->z = vptr->z + border_delta_pos[2];
            }
        }
    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}

double compute_normals (Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<FaceID> list;
    vector<FaceID>::iterator Iter;

    vector<void *> Vlist;
    vector<void *>::iterator Vit;
    vector<VertexID> vlist;
    vector<VertexID>::iterator vIter;
    Point3DGeom *tmp;
    Triangle *ftmp;
    vec pt[3], vv;

    m->getAllFaces(&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        m->getFVertices (*Iter, &Vlist);
        int i=0;
        for (Vit = Vlist.begin( ) ; Vit != Vlist.end( ) ; Vit++ ) 
        {
            tmp = (Point3DGeom *)(*Vit);
            pt[i][0] = tmp->x;
            pt[i][1] = tmp->y;
            pt[i][2] = tmp->z;
            i++;
        }
        ftmp = (Triangle *)m->getFGeomPtr(*Iter);
        pt[1][0] -= pt[0][0];
        pt[1][1] -= pt[0][1];
        pt[1][2] -= pt[0][2];
        pt[2][0] -= pt[0][0];
        pt[2][1] -= pt[0][1];
        pt[2][2] -= pt[0][2];
        pt[0] = pt[1] CROSS pt[2];
        normalize(pt[0]);
        ftmp->nx = pt[0][0]; ftmp->ny = pt[0][1]; ftmp->nz = pt[0][2];
        Vlist.clear();
    }

    list.clear();

    m->getAllVertices (&vlist);

    for ( vIter = vlist.begin( ) ; vIter != vlist.end( ) ; vIter++ ) 
    {
        m->getVFaces(*vIter, &list);
        vv[0] = vv[1] = vv[2] = 0.;
        for (Iter = list.begin(); Iter != list.end(); Iter++) 
        {
            ftmp = (Triangle *)m->getFGeomPtr(*Iter);
            vv[0] += ftmp->nx;
            vv[1] += ftmp->ny;
            vv[2] += ftmp->nz;
        }
        normalize(vv);
        tmp = (Point3DGeom *)m->getVGeomPtr(*vIter);   
        tmp->n[0] = vv[0];
        tmp->n[1] = vv[1];
        tmp->n[2] = vv[2];

        list.clear();
    }
    vlist.clear();

    RETURN_FUNCTION_TIMER();
}


//TODO : Tornar gen�rico
double compute_vertex_metric_and_borders(Mesh *m)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list, vlist;
    vector<VertexID>::iterator Iter, vIter;
    Point3DGeom *vptr, *v_vptr;
    int col,line,i;
    vec vecTmp1, vecTmp2;
    vec t,b,n,e;
    m->getAllVertices (&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
        col = *Iter % (divisoes+1);
        line = (int) floor((float)*Iter / (divisoes +1 ));

        // defini��o dos tensores metricos

        if (line<divisoes)
        {
            vptr->MetricVertex[0] = *Iter+divisoes+1;
            v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex[0]);
            v_vptr->MetricVertex_return[0] = *Iter;
        }

        if ( col<divisoes) 
        {
            vptr->MetricVertex[1] = *Iter+1;
            v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->MetricVertex[1]);
            v_vptr->MetricVertex_return[1] = *Iter;
        }


        // bordas
        // TODO: integrar na condicionais de tensores metricos
        // TODO: reduzir comparacoes

        vptr->BorderCopyVertex = -1;
        vptr->border_id = 0;
        vptr->BorderRefVer_base_t[0] = vptr->BorderRefVer_base_t[1] = vptr->BorderRefVer_base_t[2] = 0;
        if (m->isVOnBoundary(*Iter))
        {
            if ((line==0) && (!(col==0) && !(col==divisoes)) )
            {
                vptr->BorderCopyVertex = *Iter+divisoes+1;

                vptr->BorderRefVertex[0] = *Iter+divisoes;
                vptr->BorderRefVer_base_t[0] = vptr->BorderRefVertex[0] + divisoes+1;

                vptr->BorderRefVertex[1] = *Iter+divisoes+1;
                vptr->BorderRefVer_base_t[1] = vptr->BorderRefVertex[1] + divisoes+1;

                vptr->BorderRefVertex[2] = *Iter+divisoes+2;
                vptr->BorderRefVer_base_t[2] = vptr->BorderRefVertex[2] + divisoes+1;

                vptr->border_id = 8;	

            }
            if ( (col==0) && (!(line==0) && !(line==divisoes)) )
            {
                vptr->BorderCopyVertex = *Iter +1;

                vptr->BorderRefVertex[0] = *Iter - (divisoes+1) +1 ;
                vptr->BorderRefVer_base_t[0] = *Iter - (divisoes+1) +2;

                vptr->BorderRefVertex[1] = *Iter+1;
                vptr->BorderRefVer_base_t[1] = *Iter +2;

                vptr->BorderRefVertex[2] = *Iter + divisoes+2;
                vptr->BorderRefVer_base_t[2] = *Iter + divisoes+3;

                vptr->border_id = 2;

            }
            if ((line==divisoes) && (!(col==divisoes) && !(col==0) ) )
            {
                vptr->BorderCopyVertex = *Iter-divisoes-1;

                vptr->BorderRefVertex[0] = *Iter - (divisoes+1)-1;
                vptr->BorderRefVer_base_t[0] = vptr->BorderRefVertex[0] - (divisoes+1);

                vptr->BorderRefVertex[1] = *Iter - (divisoes+1);
                vptr->BorderRefVer_base_t[1] = vptr->BorderRefVertex[1] - (divisoes+1);

                vptr->BorderRefVertex[2] = *Iter - (divisoes+1)+1;
                vptr->BorderRefVer_base_t[2] = vptr->BorderRefVertex[2] - (divisoes+1);
                vptr->border_id = 4;
            }
            if ( (col==divisoes) && ( !(line==divisoes) && !(line==0) ))
            {
                vptr->BorderCopyVertex = *Iter -1;

                vptr->BorderRefVertex[0] = *Iter - (divisoes+1) -1 ;
                vptr->BorderRefVer_base_t[0] = *Iter - (divisoes+1) -2;

                vptr->BorderRefVertex[1] = *Iter-1;
                vptr->BorderRefVer_base_t[1] = *Iter -2;

                vptr->BorderRefVertex[2] = *Iter + (divisoes+1) -1 ;
                vptr->BorderRefVer_base_t[2] = *Iter + (divisoes+1) -2;
                vptr->border_id = 6;
            }

            if ( (line==0) && (col==0) )
            {
                vptr->BorderCopyVertex = *Iter +divisoes+2;

                vptr->BorderRefVertex[0] = *Iter +divisoes+2;
                vptr->BorderRefVer_base_t[0] = *Iter +(divisoes+1)*2 +2;

                vptr->BorderRefVertex[1] = *Iter +divisoes+3;
                vptr->BorderRefVer_base_t[1] = *Iter +divisoes+4;

                vptr->BorderRefVertex[2] = *Iter +(divisoes+1)*2 +1;
                vptr->BorderRefVer_base_t[2] = *Iter +(divisoes+1)*3 +1;


                vptr->border_id = 1;
            }
            if ( (line==0) && (col==divisoes) )
            {
                vptr->BorderCopyVertex = *Iter +divisoes;

                vptr->BorderRefVertex[0] = *Iter +divisoes;
                vptr->BorderRefVer_base_t[0] = *Iter +(divisoes+1)*2-2;

                vptr->BorderRefVertex[1] = *Iter + divisoes -1;
                vptr->BorderRefVer_base_t[1] = *Iter +divisoes -2;

                vptr->BorderRefVertex[2] = *Iter +(divisoes+1)*2-1;
                vptr->BorderRefVer_base_t[2] = *Iter +(divisoes+1)*3-1;


                vptr->border_id = 7;
            }
            if ( (line==divisoes) && (col==0) )
            {
                vptr->BorderCopyVertex = *Iter -divisoes;

                vptr->BorderRefVertex[0] = *Iter - (divisoes+1) +1;
                vptr->BorderRefVer_base_t[0] = *Iter -(divisoes+1)*2 +2;

                vptr->BorderRefVertex[1] = *Iter -(divisoes+1)+2;
                vptr->BorderRefVer_base_t[1] = *Iter -(divisoes+1)+3;

                vptr->BorderRefVertex[2] = *Iter -(divisoes+1)*2 +1;
                vptr->BorderRefVer_base_t[2] = *Iter -(divisoes+1)*3 +1;

                vptr->border_id = 3;
            }
            if ( (line==divisoes) && (col==divisoes) )
            {
                vptr->BorderCopyVertex = *Iter -divisoes-2;

                vptr->BorderRefVertex[0] = *Iter - (divisoes+1) -1;
                vptr->BorderRefVer_base_t[0] = *Iter -(divisoes+1)*2-2;

                vptr->BorderRefVertex[1] = *Iter - (divisoes+1) -2;
                vptr->BorderRefVer_base_t[1] = *Iter - (divisoes+1) -3;

                vptr->BorderRefVertex[2] = *Iter -(divisoes+1)*2-1;
                vptr->BorderRefVer_base_t[2] = *Iter -(divisoes+1)*3-1;

                vptr->border_id = 5;

            }

            // teste de diagnoais

            if ((line==0) && ((col>1) && (col<(divisoes-1))) )
            {
                vptr->BorderRefVer_base_t[0] = *Iter + (divisoes+1)*2 - 2;
                vptr->BorderRefVer_base_t[1] = *Iter + (divisoes+1)*2;
                vptr->BorderRefVer_base_t[2] = *Iter + (divisoes+1)*2 +2;

            }

            if ( (col==0) && ((line>1) && (line<(divisoes-1))) )
            {

                vptr->BorderRefVer_base_t[0] = *Iter - (divisoes+1)*2 +2;
                vptr->BorderRefVer_base_t[1] = *Iter +2;
                vptr->BorderRefVer_base_t[2] = *Iter + (divisoes+1)*2+2;
            }


            if ( (col==divisoes) && ((line>1) && (line<(divisoes-1))) )
            {
                vptr->BorderRefVer_base_t[0] = *Iter - (divisoes+1)*2 -2;
                vptr->BorderRefVer_base_t[1] = *Iter -2;
                vptr->BorderRefVer_base_t[2] = *Iter + (divisoes+1)*2 -2;
            }


            if ((line==divisoes) && ((col>1) && (col<(divisoes-1))) )
            {
                vptr->BorderRefVer_base_t[0] = vptr->BorderRefVertex[0] - (divisoes+1)*2;
                vptr->BorderRefVer_base_t[1] = vptr->BorderRefVertex[1] - (divisoes+1);
                vptr->BorderRefVer_base_t[2] = vptr->BorderRefVertex[2] - (divisoes+1)*2;

            }

        }

    }

    list.clear();

    // Processamento das coordenativas relativas para os pontos das bordas.
    // Fazendo em outra iteracao para garantir que as base_a[x] estao definidas para os pontos internos.
    // Aproveitando para criar "ordem" dos pontos das bordas
    m->getBorderVertices(&list);
    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        if (*Iter>-1)
        {
            vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
            //estimate_bases_a(m,v_vptr);


            for (i=0;i<3;i++)
            {
                estimate_bases_tbn_borderref(m, vptr, i, t,b,n);
                v_vptr = (Point3DGeom *)m->getVGeomPtr(vptr->BorderRefVertex[i]);

                e[0] = vptr->x - v_vptr->x;
                e[1] = vptr->y - v_vptr->y;
                e[2] = vptr->z - v_vptr->z;

                vptr->BorderRefVertexCoords[i][0] = e DOT t;
                vptr->BorderRefVertexCoords[i][1] = e DOT b;
                vptr->BorderRefVertexCoords[i][2] = e DOT n;


            }

        }
    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}


//TODO : Funcao pega de http://www.softsurfer.com/Archive/algorithm_0102/algorithm_0102.htm#Distance%20to%20Ray%20or%20Segment
//TODO: Implementar/melhorar

void proj_Point_to_Line(const vec &P, const vec &P0, const vec &P1, double &b)
{
    vec v = P0 - P1;
    vec w = P0 - P;

    double c1 = w DOT v;
    double c2 = v DOT v;
    b = c1/c2;

    if (b>1)
    {
        b = 1.;
    }
    else 
    {
        if (b<0)
            b = 0.;
    }
    //P_proj = P0 + b * v;

}

bool point_proximity(const vec &x1, const vec &x2)
{
    return (len(x2-x1)<0.01);		


}

bool find_elist(vector<EdgeID> &elist, EdgeID &elem)
{
    bool resp;
    vector<EdgeID>::iterator eIter;

    resp = false;
    eIter = elist.begin();
    while (eIter!=elist.end() && resp==false)
    {
        if (*eIter==elem)
        {
            resp = true;
        }
        eIter++;
    }
    return resp;

}

double copy_mesh (Mesh *m1, Mesh *m2) 
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    vec tf;
    VertexID prev, next;
    Point3DGeom *ptr, *vptr;

    m1->getAllVertices (&list);

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        ptr = (Point3DGeom *)m1->getVGeomPtr(*Iter);
        vptr = (Point3DGeom *)m2->getVGeomPtr(ptr->next);
        prev = vptr->previous;
        next = vptr->next;
        *vptr = *ptr;
        vptr->previous = prev;
        vptr->next = next;
    }
    list.clear();

    RETURN_FUNCTION_TIMER();
}

// DebugLoad
double debug_load(Mesh *m0, Mesh *m1, Mesh *m2, int mesh_type, bool full) 
{
    START_FUNCTION_TIMER();

    long long unsigned int memory_size = 0;
    double x,y,z;
    int a,b,c,d;

    int index = 0;
    int vert_count = 0;
    int vert_index = 1;

    VertexID v0, v1, v2;
    FaceID f0, f1, f2;
    Point3DGeom *node0, *node1, *node2;
    vec d0, d1;
    bool col_even;

    int i = 0;
    int k = 0;

    double u = 0.;
    double v = 0.;

    double a11,a12,a22 = 0;
    double b11,b12,b22 = 0;
    double w11,w12,w21,w22 = 0;

    double k1_T,k2_T = 0;
    vec ru, rv;

    double g_a = 1;
    double g_c = 0.1;
    double e_r = 1;
    double t = 0;

    clock_t start, finish;
    for ( i=0; i<=divisoes;i++) 
    {
        start = clock();
        LOG(cout, "debug_load i:" << i << " (" <<divisoes<< ")");
        u = umin + (umax - umin)/divisoes * i ;

        for (k=0;k<=divisoes;k++)
        {
            v = vmin + (vmax - vmin)/divisoes * k;

            switch (mesh_type){
            case 0: // Sela  (u,v, u^2 - v^2)
                x = u;
                y = v;
                z = (u * u - v * v);
                ru[0] = 1; ru[1]= 0; ru[2] = 2*u; 
                rv[0] = 0; rv[1]= 1; rv[2] = -2*v; 
                a11 = 1 + 4 * u*u;
                a12 = -4 * u * v;
                a22 = 1 + 4 * v * v;
                b11 = 2/sqrt(1+4*u*u+4*v*v);
                b12 = 0;
                b22 = -2/sqrt(1+4*u*u+4*v*v);
                break;

            case 1:  // Cossenoidal {u, v, Cos[2u]*Cos[2u]}
                x = u;
                y = v;
                z = cos(2*u)*cos(2*v);
                ru[0] = 1; ru[1]= 0; ru[2] =- 2*cos(2*v)*sin(2*u); 
                rv[0] = 0; rv[1]= 1; rv[2] = -2*cos(2*u)*sin(2*v); 

                a11 = 1 + 4 * pow(cos(2*v),2)*pow(sin(2*u),2);
                a12 = 4 * cos(2*u)*cos(2*v)*sin(2*u)*sin(2*v);
                a22 = 1 + 4 * pow(cos(2*u),2)*pow(sin(2*v),2);
                b11 = - 4 * cos(2*u) * cos(2*v) / sqrt(1+4*pow(cos(2*v)*sin(2*u),2)+4*pow(cos(2*u)*sin(2*v),2));
                b12 = 4 * sin(2*u) * sin(2*v) / sqrt(1+4*pow(cos(2*v)*sin(2*u),2)+4*pow(cos(2*u)*sin(2*v),2));
                b22 = - 4 * cos(2*u) * cos(2*v) / sqrt(1+4*pow(cos(2*v)*sin(2*u),2)+4*pow(cos(2*u)*sin(2*v),2));
                break;
            case 2:  // Senoidal x 2 ({u, v, Sin[2 u]*Cos[2 v]})
                x = u;
                y = v;
                z = sin(2*u)*cos(2*v);
                ru[0] = 1; ru[1]= 0; ru[2] = 2*cos(2*u)*cos(2*v); 
                rv[0] = 0; rv[1]= 1; rv[2] = -2*sin(2*u)*sin(2*v); 

                a11 = 1 + 4 * pow(cos(2*u),2)*pow(cos(2*v),2);
                a12 = -4 * cos(2*u)*cos(2*v)*sin(2*u)*sin(2*v);
                a22 = 1 + 4 * pow(sin(2*u),2)*pow(sin(2*v),2);
                b11 = - 4 * cos(2*v) * sin(2*u) / sqrt(1+4*pow(cos(2*u)*cos(2*v),2)+4*pow(sin(2*u)*sin(2*v),2));
                b12 = - 4 * cos(2*u) * sin(2*v) / sqrt(1+4*pow(cos(2*u)*cos(2*v),2)+4*pow(sin(2*u)*sin(2*v),2));
                b22 = - 4 * cos(2*v) * sin(2*u) / sqrt(1+4*pow(cos(2*u)*cos(2*v),2)+4*pow(sin(2*u)*sin(2*v),2));
                break;

            case 3: // Gaussiana

                x = u;
                y = v;
                z = t*g_a*exp(-(u*u+v*v)/(2*g_c*g_c));
                ru[0] = 1; ru[1]= 0; ru[2] = -g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * u / ( g_c * g_c);
                rv[0] = 0; rv[1]= 1; rv[2] = -g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * v / ( g_c * g_c);

                a11 = 1 + g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * u*u / ( g_c*g_c*g_c*g_c);
                a12 = g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * u*v / ( g_c*g_c*g_c*g_c);
                a22 = 1 + g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * v*v / ( g_c*g_c*g_c*g_c);
                b11 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * ( -(g_c*g_c) + u*u ) / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
                b12 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * u * v  / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
                b22 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * ( -(g_c*g_c) + v*v ) / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
                break;

            case 4: // {u + v, u - v, (u + v)^2 - (u - v)^2}
                //u *= .5;
                //v *= .5;

                x = u + v;
                y = u - v;
                z = pow(u + v,2) - pow(u - v,2);

                ru[0] = 1; ru[1]= 1; ru[2] = -2 * ( u + v) + 2 * ( u + v ); 
                rv[0] = 1; rv[1]= -1; rv[2] = 2 * ( u - v) + 2 * ( u + v );

                a11 = 2 + pow(( -2 * ( u - v ) + 2 * (u + v)),2);
                a12 = ( -2 * ( u - v ) + 2 * (u + v) ) * ( 2 * (u -v) + 2 * (u + v) );
                a22 = 2 + pow(( 2 * ( u - v ) + 2 * (u + v) ),2);
                b11 = 0;
                b12 = - 4 / ( 1 + 8 * u*u + 8 * v*v );
                b22 = 0;

                break;

            case 5: // Calha:  {u, v, u^2}

                x = u;
                y = v;
                z = u * u;
                ru[0] = 1; ru[1]= 0; ru[2] = 2*u; 
                rv[0] = 0; rv[1]= 1; rv[2] = 0; 
                a11 = 1 + 4 * u*u;
                a12 = 0;
                a22 = 1;
                b11 = 2/sqrt(1+4*u*u);
                b12 = 0;
                b22 = 0;

                break;

            case 6: // Calha2  { u,v, v^2}
                x = u;
                y = v;
                z = v * v;
                ru[0] = 1; ru[1]= 0; ru[2] = 0; 
                rv[0] = 0; rv[1]= 1; rv[2] = 2*v; 
                a11 = 1;
                a12 = 0;
                a22 = 1 + 4 * v*v;
                b11 = 0;
                b12 = 0;
                b22 = 2/sqrt(1+4*v*v);

                break;

            case 7: // Cilindro
                {
                    x = u;
                    y = v;
                    z = 0;

                    ru[0] = 1; ru[1]= 0; ru[2] = 0; 
                    rv[0] = 0; rv[1]= 1; rv[2] = 0; 

                    a11 = 1;
                    a12 = 0;
                    a22 = 1;
                    b11 = 0;
                    b12 = 0;
                    b22 = 0;

                }
                break;


            case 8: // plano esticado

                x = 2*u;
                y = v;
                z = 0;

                ru[0] = 2; ru[1]= 0; ru[2] = 0; 
                rv[0] = 0; rv[1]= 1; rv[2] = 0; 

                a11 = 4;
                a12 = 0;
                a22 = 1;
                b11 = 0;
                b12 = 0;
                b22 = 0;
                break;

            case 9: // plano encolhendo

                x = 2*u;
                y = v;
                z = 0;

                ru[0] = 2; ru[1]= 0; ru[2] = 0; 
                rv[0] = 0; rv[1]= 1; rv[2] = 0; 

                a11 = 4;
                a12 = 0;
                a22 = 1;
                b11 = 0;
                b12 = 0;
                b22 = 0;
                break;

            case 10: // sela cisalhada (u+v/2,v, u^2 - v^2)
                x = u + v/2;
                y = v;
                z = (u * u - v * v);
                ru[0] = 1; ru[1]= 0; ru[2] = 2*u; 
                rv[0] = 0; rv[1]= 1; rv[2] = -2*v; 
                a11 = 1 + 4 * u*u;
                a12 = 1/2 - 4 * u * v;
                a22 = 5/4 + 4 * v * v;
                b11 = 2/sqrt(1+4*u*u+pow(u+2*v,2));
                b12 = 0;
                b22 = -2/sqrt(1+4*u*u+pow(u+2*v,2));
                break;

            case 11: // teste derivada

                x = u;
                y = v;
                z = 0;

                ru[0] = 1; ru[1]= 0; ru[2] = 0; 
                rv[0] = 0; rv[1]= 1; rv[2] = 0; 

                a11 = 1;
                a12 = 0;
                a22 = 1;
                b11 = 0;
                b12 = 0;
                b22 = 0;
                break;

            }


            w11 = (b12*a12 - b11*a22)/(a11*a22-a12*a12);
            w12 = (b22*a12 - b12*a22)/(a11*a22-a12*a12);

            w21 = (b11*a12 - b12*a11)/(a11*a22-a12*a12);
            w22 = (b12*a12 - b22*a11)/(a11*a22-a12*a12);


            vec AVec_1,AVec_2,RuRv_norm,pdir1_T,pdir2_T,pdir_T_norm;


            if ( (fabs(w21)>0.0001)) 
            {
                AVec_1[0] = ( +w11 - w22 - sqrt( pow(w11-w22,2)+ 4*w12*w21 )) / (2*w21) ;
                AVec_1[1] = 1;
                AVec_2[0] = ( +w11 - w22 + sqrt( pow(w11-w22,2)+ 4*w12*w21 )) / (2*w21) ;
                AVec_2[1] = 1;
                k1_T =   ( ( w11 + w22 ) - sqrt( w11*w11 + 4*w12*w21 - 2*w11*w22 + w22*w22))/2;
                k2_T =   ( ( w11 + w22 ) + sqrt( w11*w11 + 4*w12*w21 - 2*w11*w22 + w22*w22))/2;

                //k1_T = (w11 + w22)/2 + sqrt(4*w12*w21+(pow(w11-w22,2)))/2;
            }
            else
            {
                AVec_1[0] = 1 ;
                AVec_1[1] = 0;
                AVec_2[1] = - w12 / (w11 - w22);
                AVec_2[1] = 1;
                k1_T =   w11;
                k2_T =   w22;
            }


            RuRv_norm = ru CROSS rv;
            normalize(RuRv_norm);

            pdir1_T = AVec_1[0] * ru + AVec_1[1] * rv;
            pdir2_T = AVec_2[0] * ru + AVec_2[1] * rv;
            normalize(pdir1_T);
            normalize(pdir2_T);
            pdir_T_norm = pdir1_T CROSS pdir2_T;

            
            node0 = new Point3DGeom;
            memory_size += sizeof(Point3DGeom);
            if ( (pdir_T_norm DOT RuRv_norm)<0 )
            {
                node0->pdir1_T = pdir1_T;
                node0->pdir2_T = pdir2_T;
                node0->k1_T = - k1_T; 
                node0->k2_T = - k2_T; 
            }
            else
            {
                node0->pdir1_T = pdir2_T;
                node0->pdir2_T = pdir1_T;
                node0->k1_T = - k2_T; 
                node0->k2_T = - k1_T; 
            }
            normalize(node0->pdir1_T);
            normalize(node0->pdir2_T);

            node0->a11_T = a11; 
            node0->a12_T = a12; 
            node0->a22_T = a22; 

            node0->w11_T = w11; 
            node0->w12_T = w12; 
            node0->w21_T = w21; 
            node0->w22_T = w22; 

            node0->b11_T = b11; 
            node0->b12_T = b12; 
            node0->b22_T = b22; 

            node0->u_T = u;
            node0->v_T = v;

            node0->ru = ru;
            node0->rv = rv;

            node0->x = x; 
            node0->y = y; 
            node0->z = z;

            node0->mu = MASS/MESH_AREA;

            node0->area_det_a = sqrt(a11*a22 - a12*a12);

            node0->MetricVertex[0] = node0->MetricVertex[1] = -1;
            node0->MetricVertex_return[0] = node0->MetricVertex_return[1] = -1;

            vec f;
            f[0] = f[1] = f[2] = 0.0;
            node0->force_external_cart = f;
            node0->v_cart = f;
            //node0->acc = f;

            v0 = m0->addVertex(vert_count,node0);
            node0->previous = -1; node0->next = -1;
            if (full)
            {
                node1 = new Point3DGeom;
                node2 = new Point3DGeom;
                node1->pdir1_T = node2->pdir1_T = node0->pdir1_T ;
                node1->pdir2_T = node2->pdir2_T = node0->pdir2_T;
                node1->k1_T = node2->k1_T = node0->k1_T;
                node1->k2_T = node2->k2_T = node0->k2_T;
                node1->a11_T = node2->a11_T = node0->a11_T;
                node1->a12_T = node2->a12_T = node0->a12_T;
                node1->a22_T = node2->a22_T = node0->a22_T;
                node1->w11_T = node2->w11_T = node0->w11_T;
                node1->w12_T = node2->w12_T = node0->w12_T;
                node1->w21_T = node2->w21_T = node0->w21_T;
                node1->w22_T = node2->w22_T = node0->w22_T;
                node1->b11_T = node2->b11_T = node0->b11_T; 
                node1->b12_T = node2->b12_T = node0->b12_T;
                node1->b22_T = node2->b22_T = node0->b22_T;
                node1->u_T = node2->u_T = node0->u_T;
                node1->v_T = node2->v_T = node0->v_T;
                node1->ru = node2->ru = node0->ru;
                node1->rv = node2->rv = node0->rv;
                node1->x = node2->x = node0->x;
                node1->y = node2->y = node0->y;
                node1->z = node2->z = node0->z;
                node1->mu = node2->mu = node0->mu;
                node1->MetricVertex[0] = node1->MetricVertex[1] = node2->MetricVertex[0] = node2->MetricVertex[1] = node0->MetricVertex[0];
                node1->MetricVertex_return[0] = node1->MetricVertex_return[1] = node2->MetricVertex_return[0] = node2->MetricVertex_return[1] = node0->MetricVertex_return[0];
                node1->force_external_cart = node2->force_external_cart = node0->force_external_cart;
                node1->v_cart = node2->v_cart = node0->v_cart;
                //node1->acc = node2->acc = node0->acc;

                v1 = m1->addVertex(vert_count,node1);
                v2 = m2->addVertex(vert_count,node2);
                node0->previous = -1; node0->next = v1;
                node1->previous = v0; node1->next = v2;
                node2->previous = v1; node2->next = -1;
            }

            // Update bounding frame
            if (x < xmin) xmin = (float)x;
            if (x > xmax) xmax = (float)x;
            if (y < ymin) ymin = (float)y;
            if (y > ymax) ymax = (float)y;
            if (z < zmin) zmin = (float)z;
            if (z > zmax) zmax = (float)z;
            vert_count++;

            // Create faces
            if ( (i>0) && (k>0) ) 
            {

                col_even = (((k + i )%2)==0);

                a = (i) * (divisoes+1) + k;
                b = (i-1) * (divisoes+1) + k;
                c = (i-1) * (divisoes+1) + k - 1;
                d = (i) * (divisoes+1) + k - 1;


                if (col_even)
                {
                    f0 = m0->addTriangle_(-1,a,b,c,NULL);
                    if (full)
                    {
                        f1 = m1->addTriangle_(-1,a,b,c,NULL);
                        f2 = m2->addTriangle_(-1,a,b,c,NULL);
                    }
                    node0 = (Point3DGeom *)m0->getVGeomPtr(a); 
                    node1 = (Point3DGeom *)m0->getVGeomPtr(b); 
                    node2 = (Point3DGeom *)m0->getVGeomPtr(c);
                }
                else
                {
                    f0 = m0->addTriangle_(-1,b,c,d,NULL);
                    if (full)
                    {
                        f1 = m1->addTriangle_(-1,b,c,d,NULL);
                        f2 = m2->addTriangle_(-1,b,c,d,NULL);
                    }
                    node0 = (Point3DGeom *)m0->getVGeomPtr(b); 
                    node1 = (Point3DGeom *)m0->getVGeomPtr(c); 
                    node2 = (Point3DGeom *)m0->getVGeomPtr(d);
                }

                d0[0] = node1->x - node0->x;
                d0[1] = node1->y - node0->y;
                d0[2] = node1->z - node0->z;
                d1[0] = node2->x - node0->x;
                d1[1] = node2->y - node0->y;
                d1[2] = node2->z - node0->z;
                vec tmp = d0 CROSS d1;
                Triangle* t0 = new Triangle;
                t0->nx = tmp[0];
                t0->ny = tmp[1];
                t0->nz = tmp[2];
                m0->setFGeomPtr(f0, (void *)t0);
                memory_size += sizeof(Triangle);
                if (full)
                {
                    Triangle* t1 = new Triangle;
                    Triangle* t2 = new Triangle;
                    t1->nx = t2->nx = tmp[0];
                    t1->ny = t2->ny = tmp[1];
                    t1->nz = t2->nz = tmp[2];
                    m1->setFGeomPtr(f1, (void *)t1);
                    m2->setFGeomPtr(f2, (void *)t2);
                }

                // face 2
                if (col_even)
                {
                    f0 = m0->addTriangle_(-1,a,c,d,NULL);
                    if (full)
                    {
                        f1 = m1->addTriangle_(-1,a,c,d,NULL);
                        f2 = m2->addTriangle_(-1,a,c,d,NULL);
                    }
                    node0 = (Point3DGeom *)m0->getVGeomPtr(a); 
                    node1 = (Point3DGeom *)m0->getVGeomPtr(c); 
                    node2 = (Point3DGeom *)m0->getVGeomPtr(d);
                }
                else
                {
                    f0 = m0->addTriangle_(-1,a,b,d,NULL);
                    if (full)
                    {
                        f1 = m1->addTriangle_(-1,a,b,d,NULL);
                        f2 = m2->addTriangle_(-1,a,b,d,NULL);
                    }
                    node0 = (Point3DGeom *)m0->getVGeomPtr(a); 
                    node1 = (Point3DGeom *)m0->getVGeomPtr(b); 
                    node2 = (Point3DGeom *)m0->getVGeomPtr(d);
                }
                // face 2

                d0[0] = node1->x - node0->x;
                d0[1] = node1->y - node0->y;
                d0[2] = node1->z - node0->z;
                d1[0] = node2->x - node0->x;
                d1[1] = node2->y - node0->y;
                d1[2] = node2->z - node0->z;
                tmp = d0 CROSS d1;
                t0 = new Triangle;
                t0->nx = tmp[0];
                t0->ny = tmp[1];
                t0->nz = tmp[2];
                m0->setFGeomPtr(f0, (void *)t0);
                memory_size += sizeof(Triangle);
                if (full)
                {
                    Triangle* t1 = new Triangle;
                    Triangle* t2 = new Triangle;
                    t1->nx = t2->nx = tmp[0];
                    t1->ny = t2->ny = tmp[1];
                    t1->nz = t2->nz = tmp[2];
                    m1->setFGeomPtr(f1, (void *)t1);
                    m2->setFGeomPtr(f2, (void *)t2);
                }
            }

        }
        finish = clock();
        LOG_LINE(cout, " Time: " << (double) 1000 * (finish - start) / CLOCKS_PER_SEC << "ms");
    }

    vector<FaceID> list;
    m0->getAllFaces(&list);
    vector<VertexID> vertices;
    m0->getAllVertices(&vertices);
    LOG_LINE(cout, " Quantidade total de faces na malha 0:" << list.size());
    LOG_LINE(cout, "Quantidade total de faces nao inseridas:"<< tlist.size());
    LOG_LINE(cout, "Total memory for mesh geometry structs: " << memory_size/1024./1024. << "MB");
    LOG_LINE(cout, "Vertices: " << vertices.size() << " | Faces: " << list.size());
    vertices.clear();
    list.clear();

    if (full)
    {
        m1->getAllFaces (&list);
        LOG_LINE(cout, " Quantidade total de faces na malha 1:" << list.size());
        LOG_LINE(cout, "Quantidade total de faces nao inseridas:"<< tlist.size());
        list.clear();

        m2->getAllFaces (&list);
        LOG_LINE(cout, " Quantidade total de faces na malha 1:" << list.size());
        LOG_LINE(cout, "Quantidade total de faces nao inseridas:"<< tlist.size());
        list.clear();
    }

    RETURN_FUNCTION_TIMER();
}

double debug_update(Mesh *m, double t, int mesh_type)
{
    START_FUNCTION_TIMER();

    vector<VertexID> list;
    vector<VertexID>::iterator Iter;

    Point3DGeom *vptr;

    double x,y,z;

    double u = 0.;
    double v = 0.;

    double a11,a12,a22 = 0;
    double c_a11,c_a12,c_a22 = 0;
    double k1_T,k2_T;
    double det_a = 0;
    double b11,b12,b22 = 0;
    double w11,w12,w21,w22 = 0;

    // Para gaussiana
    double g_a;
    double g_c;

    vec ru, rv;

    double u_unit = 10./(umax - umin);
    double v_unit = 10./(vmax - vmin);

    m->getAllVertices(&list);
    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);
        switch (mesh_type){
        case 0: // Sela  (u,v, t*(u^2 - v^2))
            u = x = vptr->u_T;
            v = y =  vptr->v_T;
            //x = (vptr->u_T+umin)*u_unit;
            //y =  (vptr->v_T+vmin)*v_unit;
            //u = v = 10.;
            z = t * (u*u - v*v);

            ru[0] = 1; ru[1]= 0; ru[2] = 2*t*u; 
            rv[0] = 0; rv[1]= 1; rv[2] = -2*t*v; 
            a11 = 1 + 4 * t*t * u*u;
            //a11 = u*u + 4 * t*t * u*u;
            a12 = -4 * t*t * u * v;
            a22 = 1 + 4 * t*t * v * v;
            //a22 = v*v + 4 * t*t * v * v;
            b11 = 2*t/sqrt(1+4*t*u*t*u+4*t*v*t*v);
            b12 = 0;
            b22 = -2*t/sqrt(1+4*t*u*t*u+4*t*v*t*v);

            break;
        case 1:  // Cos
            u = x = vptr->u_T;
            v = y =  vptr->v_T;	

            x = u;
            y = v;
            z = t*cos(2*u)*cos(2*v);

            ru[0] = 1; ru[1]= 0; ru[2] = -2*t*cos(2*v)*sin(2*u); 
            rv[0] = 0; rv[1]= 1; rv[2] = -2*t*cos(2*u)*sin(2*v); 

            a11 = 1 + 4 * t*t*pow(cos(2*v),2)*pow(sin(2*u),2);
            a12 = 4 * t*t*cos(2*u)*cos(2*v)*sin(2*u)*sin(2*v);
            a22 = 1 + 4 * t*t*pow(cos(2*u),2)*pow(sin(2*v),2);
            b11 = - 4 * t*cos(2*u) * cos(2*v) / sqrt(1+4*pow(t*cos(2*v)*sin(2*u),2)+4*pow(t*cos(2*u)*sin(2*v),2));
            b12 = 4 * t*sin(2*u) * sin(2*v) / sqrt(1+4*pow(t*cos(2*v)*sin(2*u),2)+4*pow(t*cos(2*u)*sin(2*v),2));
            b22 = - 4 * t*cos(2*u) * cos(2*v) / sqrt(1+4*pow(t*cos(2*v)*sin(2*u),2)+4*pow(t*cos(2*u)*sin(2*v),2));
            break;
        case 2:  // Senoidal x 2 {u, v, t*Sin[2 u]*Cos[2 v]}

            u = x = vptr->u_T;
            v = y =  vptr->v_T;	

            z = t*sin(2*u)*cos(2*v);

            ru[0] = 1; ru[1]= 0; ru[2] =  2*t*cos(2*u)*cos(2*v); 
            rv[0] = 0; rv[1]= 1; rv[2] = -2*t*sin(2*u)*sin(2*v); 

            a11 = 1 + 4 * t*t* pow(cos(2*u),2)*pow(cos(2*v),2);
            a12 = -4 * t*t* cos(2*u)*cos(2*v)*sin(2*u)*sin(2*v);
            a22 = 1 + 4 * t*t* pow(sin(2*u),2)*pow(sin(2*v),2);
            b11 = - 4 * t * cos(2*v) * sin(2*u) / sqrt(1+4*pow(t*cos(2*u)*cos(2*v),2)+4*pow(t*sin(2*u)*sin(2*v),2));
            b12 = - 4 * t * cos(2*u) * sin(2*v) / sqrt(1+4*pow(t*cos(2*u)*cos(2*v),2)+4*pow(t*sin(2*u)*sin(2*v),2));
            b22 = - 4 * t * cos(2*v) * sin(2*u) / sqrt(1+4*pow(t*cos(2*u)*cos(2*v),2)+4*pow(t*sin(2*u)*sin(2*v),2));



            break;

        case 3: // Gaussiana
            g_a = 1.0;
            g_c = 0.01;

            u = x = vptr->u_T;
            v = y =  vptr->v_T;	
            z = t*g_a*exp(-(u*u+v*v)/(2*g_c*g_c));

            ru[0] = 1; ru[1]= 0; ru[2] = -g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * u / ( g_c * g_c);
            rv[0] = 0; rv[1]= 1; rv[2] = -g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * v / ( g_c * g_c);

            a11 = 1 + g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * u*u / ( g_c*g_c*g_c*g_c);
            a12 = g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * u*v / ( g_c*g_c*g_c*g_c);
            a22 = 1 + g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * v*v / ( g_c*g_c*g_c*g_c);
            b11 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * ( -(g_c*g_c) + u*u ) / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
            b12 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * u * v  / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
            b22 = g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t * ( -(g_c*g_c) + v*v ) / (g_c*g_c*g_c*g_c *sqrt(1+ g_a*g_a * exp(-(u*u+v*v)/(2*g_c*g_c)) * t*t * (u*u + v*v) / (g_c*g_c*g_c*g_c)));
            break;

        case 4: // {u + v, u - v, t * ((u + v)^2 - (u - v)^2)}

            //u *= .5;
            //v *= .5;

            u = vptr->u_T;
            v = vptr->v_T;	
            x = u + v;
            y = u - v;
            z = t*(pow(u + v,2) - pow(u - v,2));


            ru[0] = 1; ru[1]= 1; ru[2] = t * (-2 * ( u - v) + 2 * ( u + v ));
            rv[0] = 1; rv[1]= -1; rv[2] =t *  (2 * ( u - v) + 2 * ( u + v ));

            a11 = 2 + t*t *pow(( -2 * ( u - v ) + 2 * (u + v)),2);
            a12 = t*t * ( -2 * ( u - v ) + 2 * (u + v) ) * ( 2 * (u -v) + 2 * (u + v) );
            a22 = 2 + t*t* pow(( 2 * ( u - v ) + 2 * (u + v) ),2);
            b11 = 0;
            b12 = -8*t  / sqrt( 4 + pow(t*(u+v),2) + pow(-4*t*u+4*t*v,2));
            b22 = 0;

            break;

        case 5: // Calha:  {u, v, t*u^2}

            u = vptr->u_T;
            v = vptr->v_T;	
            x = u;
            y = v;
            z = t*u * u;
            ru[0] = 1; ru[1]= 0; ru[2] = 2*t*u; 
            rv[0] = 0; rv[1]= 1; rv[2] = 0; 
            a11 = 1 + 4 * u*u *t*t;
            a12 = 0;
            a22 = 1;
            b11 = 2*t/sqrt(1+4*u*u*t*t);
            b12 = 0;
            b22 = 0;

            break;

        case 6: // Calha2:  {u, v, t*v^2}

            u	 = vptr->u_T;
            v = vptr->v_T;	
            x = u;
            y = v;
            z = t*v * v;
            ru[0] = 1; ru[1]= 0; ru[2] = 0; 
            rv[0] = 0; rv[1]= 1; rv[2] = 2*t*v; 
            a11 = 1;
            a12 = 0;
            a22 = 1 + 4 * v*v *t*t;
            b11 = 0;
            b12 = 0;
            b22 = 2*t/sqrt(1+4*v*v*t*t);

            break;
        case 7: // Cilindro

            u = vptr->u_T;
            v = vptr->v_T;	

            if (t>0)
            {
                double M = (umax-umin);
                double R = M/(M_PI*t);
                double alpha = u/R;

                x = sin(alpha)*R;
                y = v;
                z = -cos(alpha)*R+R;
                ru[0] = cos(t*M_PI*u/M); ru[1]= 0; ru[2] = sin(t*M_PI*u/M); 
                rv[0] = 0; rv[1]= 1; rv[2] = 0; 
                a11 = pow(cos(t*M_PI*u/M),2) + pow(sin(t*M_PI*u/M),2); // supostamente 1
                a12 = 0;
                a22 = 1;
                b11 = M_PI*  t /M;
                b12 = 0;
                b22 = 0;

            } 
            else
            {
                x = u;
                y = v;
                z = 0;
                a11 = 1;
                a12 = 0;
                a22 = 1;
                b11 = 0;
                b12 = 0;
                b22 = 0;
            }
            break;
        case 8: // plano esticado

            u = vptr->u_T;
            v = vptr->v_T;	

            x = (1+t) * u;
            y =  v;
            z = 0.;

            ru[0] = 1+t; ru[1]= 0; ru[2] = 0; 
            rv[0] = 0; rv[1]= 1; rv[2] = 0; 

            a11 = pow(1+t,2);
            a12 = 0;
            a22 = 1;
            b11 = 0;
            b12 = 0;
            b22 = 0;
            break;

        case 9: // plano encolhendo

            u = vptr->u_T;
            v = vptr->v_T;	

            x = (1-t) * u;
            y =  v;
            z = 0.;

            ru[0] = 1-t; ru[1]= 0; ru[2] = 0; 
            rv[0] = 0; rv[1]= 1; rv[2] = 0; 

            a11 = pow(1-t,2);
            a12 = 0;
            a22 = 1;
            b11 = 0;
            b12 = 0;
            b22 = 0;
            break;

        case 10: // Sela cisalhada:	{u + (t*v/2), v, t*(u^2 - v^2)}
            u = x = vptr->u_T + t * vptr->v_T/2;
            v = y =  vptr->v_T;	
            z = t * (u*u - v*v);

            ru[0] = 1; ru[1]= 0; ru[2] = 2*t*u; 
            rv[0] = t/2; rv[1]= 1; rv[2] = -2*t*v; 
            a11 = 1 + 4 * t*t * u*u;
            a12 = t/2 -4 * t*t * u * v;
            a22 = 1 + t*t/4 + 4 * t*t * v * v;
            b11 = 2*t/sqrt(1+4*t*u*t*u+4*pow(t*(t*u+2*v),2));
            b12 = 0;
            b22 = -2*t/sqrt(1+4*t*u*t*u+4*pow(t*(t*u+2*v),2));

            break;

        case 11: // teste derivada

            u = vptr->u_T;
            v = vptr->v_T;	

            if (u<0)
            {
                x=u;
                y=v;
                z=0;
            }
            else
            {

                if (t>0)
                {
                    x=u;
                    y=v;
                    z=(t)*u*u;
                }
                else
                {
                    x=u;
                    y=v;
                    z=0;
                }


            }

            ru[0] = 1+t; ru[1]= 0; ru[2] = 0; 
            rv[0] = 0; rv[1]= 1; rv[2] = 0; 

            a11 = pow(1+t,2);
            a12 = 0;
            a22 = 1;
            b11 = 0;
            b12 = 0;
            b22 = 0;
            break;					

        }



        det_a = (a11*a22) - (a12*a12);
        c_a11 = a22 / det_a;
        c_a12 = - a12 / det_a;
        c_a22 = a11 / det_a;

        w11 = (b12*a12 - b11*a22)/(a11*a22-a12*a12);
        w12 = (b22*a12 - b12*a22)/(a11*a22-a12*a12);
        w21 = (b11*a12 - b12*a11)/(a11*a22-a12*a12);
        w22 = (b12*a12 - b22*a11)/(a11*a22-a12*a12);


        vec AVec_1,AVec_2,RuRv_norm,pdir1_T,pdir2_T,pdir_T_norm;


        if ( (fabs(w21)>0.0001)) 
        {
            AVec_1[0] = ( +w11 - w22 - sqrt( pow(w11-w22,2)+ 4*w12*w21 )) / (2*w21) ;
            AVec_1[1] = 1;
            AVec_2[0] = ( +w11 - w22 + sqrt( pow(w11-w22,2)+ 4*w12*w21 )) / (2*w21) ;
            AVec_2[1] = 1;
            k1_T =   ( ( w11 + w22 ) - sqrt( w11*w11 + 4*w12*w21 - 2*w11*w22 + w22*w22))/2;
            k2_T =   ( ( w11 + w22 ) + sqrt( w11*w11 + 4*w12*w21 - 2*w11*w22 + w22*w22))/2;

            //k1_T = (w11 + w22)/2 + sqrt(4*w12*w21+(pow(w11-w22,2)))/2;
        }
        else
        {
            AVec_1[0] = 1 ;
            AVec_1[1] = 0;
            AVec_2[1] = - w12 / (w11 - w22);
            AVec_2[1] = 1;
            k1_T =   w11;
            k2_T =   w22;
        }


        RuRv_norm = ru CROSS rv;
        normalize(RuRv_norm);

        pdir1_T = AVec_1[0] * ru + AVec_1[1] * rv;
        pdir2_T = AVec_2[0] * ru + AVec_2[1] * rv;
        normalize(pdir1_T);
        normalize(pdir2_T);
        pdir_T_norm = pdir1_T CROSS pdir2_T;

        if ( (pdir_T_norm DOT RuRv_norm)<0 )
        {
            vptr->pdir1_T = pdir1_T;
            vptr->pdir2_T = pdir2_T;
            vptr->k1_T = - k1_T;
            vptr->k2_T = - k2_T;
        }
        else
        {
            vptr->pdir1_T = pdir2_T;
            vptr->pdir2_T = pdir1_T;
            vptr->k1_T = - k2_T; 
            vptr->k2_T = - k1_T; 

        }


        normalize(vptr->pdir1_T);
        normalize(vptr->pdir2_T);


        vptr->a11_T = a11;
        vptr->a12_T = a12; 
        vptr->a22_T = a22; 



        vptr->c_a11_T = c_a11;
        vptr->c_a12_T = c_a12;
        vptr->c_a12_T = c_a22;


        vptr->w11_T = w11; 
        vptr->w12_T = w12; 
        vptr->w21_T = w21; 
        vptr->w22_T = w22; 

        vptr->b11_T = b11; 
        vptr->b12_T = b12; 
        vptr->b22_T = b22; 

        vptr->ru = ru;
        vptr->rv = rv;

        vptr->x =  x;
        vptr->y =  y;
        vptr->z =  z;
    }

    list.clear();

    RETURN_FUNCTION_TIMER();
}


void debug_test(Mesh *m) 
{
    vector<VertexID> list;
    vector<VertexID>::iterator Iter;
    vector<VertexID> vlist;
    vector<VertexID>::iterator vIter;
    Point3DGeom *vptr;

    m->getAllVertices(&list);



    double tmp1 = 0.;
    double tmp2 = 0.;
    double tmp3 = 0.;
    double tmp4 = 0.;
    double max_tmpa1 = 0.;
    double max_tmpa2 = 0.;
    double max_tmpa3 = 0.;

    double max_tmpb1 = 0.;
    double max_tmpb2 = 0.;
    double max_tmpb3 = 0.;
    double err_k1_medio,err_k2_medio;
    double err_a11_medio,err_a12_medio, err_a22_medio;
    double err_b11_medio,err_b12_medio, err_b22_medio;
    double err_a11_medio_relativo,err_a12_medio_relativo, err_a22_medio_relativo;
    double err_b11_medio_relativo,err_b12_medio_relativo, err_b22_medio_relativo;
    int err_contagem_pontos = 0;

    err_k1_medio = err_k2_medio = 0;
    err_a11_medio = err_a12_medio = err_a22_medio = 0;
    err_b11_medio = err_b12_medio = err_b22_medio = 0 ;
    err_a11_medio_relativo =err_a12_medio_relativo = err_a22_medio_relativo = 0;
    err_b11_medio_relativo =err_b12_medio_relativo = err_b22_medio_relativo = 0;
    err_contagem_pontos = 0;

    global_errMax_a = 0;
    global_errMax_b = 0;
    global_errMax_k	= 0;

    for ( Iter = list.begin( ) ; Iter != list.end( ) ; Iter++ ) 
    {
        vptr = (Point3DGeom *)m->getVGeomPtr(*Iter);

        vptr->debug = 0;
        //if ( (vptr->x==1) && (vptr->y==1) )
        //if ( (fabs(vptr->x - 0)<0.001)  && (fabs(vptr->y - 0)<0.001) ) 
        //if ( (fabs(vptr->x)<2)  && (fabs(vptr->y)<2) ) 
        if (1)
        {
            err_contagem_pontos++;


            proj_tens(vptr->ru, vptr->rv, vptr->ru CROSS vptr->rv, vptr->a11_T,vptr->a12_T, vptr->a12_T, vptr->a22_T, vptr->base_a[0], vptr->base_a[1], vptr->base_a[2], tmp1, tmp2, tmp3, tmp4);

            vptr->err_a11 = vptr->err_a12 = vptr->err_a22 = 0;

            vptr->err_a11 = fabs(vptr->a[0][0] - tmp1);
            vptr->err_a12 = fabs(vptr->a[0][1] - tmp2);
            vptr->err_a22 = fabs(vptr->a[1][1] - tmp4);

            vptr->errMax_a = max(vptr->err_a11,vptr->err_a12);
            vptr->errMax_a = max(vptr->errMax_a,vptr->err_a22);

            global_errMax_a = max(vptr->errMax_a,global_errMax_a);

            err_a11_medio += vptr->err_a11;
            err_a12_medio += vptr->err_a12; 
            err_a22_medio += vptr->err_a22;
            err_a11_medio_relativo += vptr->err_a11 / fabs(tmp1);
            err_a12_medio_relativo += vptr->err_a12 / fabs(tmp2); 
            err_a22_medio_relativo += vptr->err_a22 / fabs(tmp4); 



            if (0)
                //if ( (fabs(vptr->u_T - debug_u)<0.01 ) && (fabs(vptr->v_T - debug_v)<0.01 ) )	
                //if (vptr->errMax_a>0.0005)
            {
                vptr->debug = 1;		

                LOG_LINE(cout, "--- Tensor Metrico ---");
                LOG_LINE(cout, "x:" << vptr->x << " y:" << vptr->y << " z:" << vptr->z);
                LOG_LINE(cout, "a11= " << vptr->a[0][0] << " a12= " << vptr->a[0][1] << " a22= " << vptr->a[1][1] );
                LOG_LINE(cout, "a11_T= " << tmp1 << " a12_T= " << tmp2 << " a22_T= " << tmp4 );
                LOG_LINE(cout, "a_T (ru rv) = " << vptr->a11_T << " ; " << vptr->a12_T << " ; " << vptr->a22_T);
                LOG_LINE(cout, "b  = " << vptr->b[0][0] << " ; " << vptr->b[0][1] << " ; " << vptr->b[1][1]);
                LOG_LINE(cout, "k  = " << vptr->k1 << " ; " << vptr->k2 << " ; ");
                LOG_LINE(cout, "----------------------");

            }



            //proj_tens(vptr->pdir1_T, vptr->pdir2_T, vptr->pdir1_T CROSS vptr->pdir2_T, vptr->k1_T,vptr->b12, vptr->b12,vptr->k2_T, vptr->pdir1, vptr->pdir2, vptr->pdir1 CROSS vptr->pdir2, tmp1, tmp2, tmp3, tmp4);

            //tmp1 = vptr->k1_T;
            //tmp2 = vptr->b12;
            //tmp3 = vptr->k2_T;



            vptr->err_k1 = fabs(vptr->k1 - vptr->k1_T);
            vptr->err_k2 = fabs(vptr->k2 - vptr->k2_T);

            global_errMax_k = max(vptr->errMax_k,global_errMax_k);
            // troca ordem das curvaturas teoricas se for o caso
            if (max(vptr->err_k1, vptr->err_k2)>max(fabs(vptr->k1 - vptr->k2_T), fabs(vptr->k2 - vptr->k1_T)))
            {
                double tmp_k = vptr->k1_T;
                vptr->k1_T = vptr->k2_T;
                vptr->k2_T = tmp_k;
                vptr->err_k1 = fabs(vptr->k1 - vptr->k1_T);
                vptr->err_k2 = fabs(vptr->k2 - vptr->k2_T);
            }


            err_k1_medio += vptr->err_k1;
            err_k2_medio += vptr->err_k2;
            vptr->errMax_k = max(vptr->err_k1, vptr->err_k2);

            // if (0)
            if (vptr->errMax_k>0.2)
                //if ( ( fabs(vptr->x - 0.2)<0.001 ) && ( fabs(vptr->y - 0.2)<0.001 ) )
            {
                //vptr->debug = 1;				
                /*

                LOG_LINE(cout, "--- Curvatura ---");
                LOG_LINE(cout, "x:" << vptr->x << " y:" << vptr->y << " z:" << vptr->z);
                LOG_LINE(cout, "k1= " << vptr->k1 << " k2= " << vptr->k2);
                LOG_LINE(cout, "k1_T= " << vptr->k1_T << " k2_T= " << vptr->k2_T);
                LOG_LINE(cout, "k1_R= " << vptr->k1_R << " k2_R= " << vptr->k2_R);
                LOG_LINE(cout, vptr->errMax_k);
                LOG_LINE(cout, "--base ru rv------");
                LOG_LINE(cout, "a11= " << vptr->a11_T << " a12= " << vptr->a12_T << " a22 " << vptr->a22_T );
                LOG_LINE(cout, "b11= " << vptr->b11_T << " b12= " << vptr->b12_T << " b22 " << vptr->b22_T );
                LOG_LINE(cout, "w11= " << vptr->w11_T << " w12= " << vptr->w12_T << " w21= " << vptr->w21_T << " w22 " << vptr->w22_T );
                LOG_LINE(cout, "-----------------");
                */
            }



            vptr->err_b11 = vptr->err_b12 = vptr->err_b22 = 0;


            proj_tens(vptr->ru, vptr->rv, vptr->ru CROSS vptr->rv, vptr->b11_T,vptr->b12_T, vptr->b12_T, vptr->b22_T, vptr->base_a[0], vptr->base_a[1], vptr->base_a[2], tmp1, tmp2, tmp3, tmp4);
            vptr->err_b11 = fabs(vptr->b[0][0] - tmp1);
            vptr->err_b12 = fabs(vptr->b[0][1] - tmp2);
            vptr->err_b22 = fabs(vptr->b[1][1] - tmp4);


            vptr->errMax_b = max(vptr->err_b11,vptr->err_b12);
            vptr->errMax_b = max(vptr->errMax_b,vptr->err_b22);

            global_errMax_b = max(vptr->errMax_b,global_errMax_b);

            err_b11_medio += vptr->err_b11;
            err_b12_medio += vptr->err_b12; 
            err_b22_medio += vptr->err_b22;
            err_b11_medio_relativo += vptr->err_b11 / fabs(tmp1);
            err_b12_medio_relativo += vptr->err_b12 / fabs(tmp2); 
            err_b22_medio_relativo += vptr->err_b22 / fabs(tmp4); 

            if (0)
                //if ( (fabs(vptr->u_T - debug_u)<0.01 ) && (fabs(vptr->v_T - debug_v)<0.01 ) )	
                //if (vptr->errMax_b>0.01)
            {
                vptr->debug = 1;				
                LOG_LINE(cout, "--- Tensor Curvatura ---");
                LOG_LINE(cout, "x:" << vptr->x << " y:" << vptr->y << " z:" << vptr->z);
                LOG_LINE(cout, "b11= " << vptr->b[0][0] << " b12= " << vptr->b[0][1] << " b22 " << vptr->b[1][1] );
                LOG_LINE(cout, "b11_T= " << tmp1 << " b12_T= " << tmp2 << " b22_T= " << tmp4 );
                LOG_LINE(cout, "b11_T (ru x rv)= " << vptr->b11_T << " b12_T (ru x rv)= " << vptr->b12_T << " b22_T (ru x rv)= " << vptr->b22_T );
                LOG_LINE(cout, "k: "  << vptr->k1 << " ; "  << vptr->k2);
                LOG_LINE(cout, "k_T: "  << vptr->k1_T << " ; "  << vptr->k2_T);
                LOG_LINE(cout, "------------------------");

            }


            //LOG_LINE(cout, vptr->x << ";" << vptr->y << ";" << vptr->z << ";" << vptr->b11 << ";" << vptr->b12 << ";" << vptr->b22  << ";" << vptr->k1_T << ";" << vptr->b12 << ";" << vptr->k2_T);
            //LOG_LINE(cout, vptr->x << ";" << vptr->y << ";" << vptr->z);




        }


    }


    err_k1_medio = err_k1_medio / err_contagem_pontos;
    err_k2_medio = err_k2_medio / err_contagem_pontos;
    err_a11_medio = err_a11_medio / err_contagem_pontos;
    err_a12_medio = err_a12_medio / err_contagem_pontos;
    err_a22_medio = err_a22_medio / err_contagem_pontos;
    err_a11_medio_relativo = err_a11_medio_relativo / err_contagem_pontos;
    err_a12_medio_relativo = err_a12_medio_relativo / err_contagem_pontos;
    err_a22_medio_relativo = err_a22_medio_relativo / err_contagem_pontos;

    err_b11_medio = err_b11_medio / err_contagem_pontos;
    err_b12_medio = err_b12_medio / err_contagem_pontos;
    err_b22_medio = err_b22_medio / err_contagem_pontos;
    err_b11_medio_relativo = err_b11_medio_relativo / err_contagem_pontos;
    err_b12_medio_relativo = err_b12_medio_relativo / err_contagem_pontos;
    err_b22_medio_relativo = err_b22_medio_relativo / err_contagem_pontos;


    //printf("Max dif E=%lf , Max dif F=%lf , Max dif G=%lf \n",max_tmpa1,max_tmpa2,max_tmpa3);
    //printf("Max dif e=%lf , Max dif f=%lf , Max dif g=%lf \n",max_tmpb1,max_tmpb2,max_tmpb3);
    LOG_LINE(cout, "-------");
    LOG_LINE(cout, "Erros Abs. Medios:");
    LOG_LINE(cout, "a (err)  : " << err_a11_medio << "; " << err_a12_medio << "; " << err_a22_medio);
    LOG_LINE(cout, "b (err)  : " << err_b11_medio << "; " << err_b12_medio << "; " << err_b22_medio);
    LOG_LINE(cout, "k (err)  : " << err_k1_medio << "; " << err_k2_medio << "; " );
    //LOG_LINE(cout, err_contagem_pontos);
    LOG_LINE(cout, "Erros Abs. Max:");
    LOG_LINE(cout, "a (err)  : " << global_errMax_a);
    LOG_LINE(cout, "b (err)  : " << global_errMax_b);
    LOG_LINE(cout, "k (err)  : " << global_errMax_k);
    LOG_LINE(cout, "-------");
    list.clear();

}
