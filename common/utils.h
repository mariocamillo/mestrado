#ifndef _UTILS_H_
#define _UTILS_H_

#include <cstdio>
#include <iostream>

namespace desmo {
namespace utils {

#ifdef _DEBUG
#define LOG_LINE(out, x) out << x << std::endl
#define LOG(out, x) out << x
#else
#define LOG_LINE(out, x)
#define LOG(out, x)
#endif

#define FREE_CPU_RESOURCE(x, f) \
    if (x != NULL) {            \
        f(x);                   \
        x = NULL;               \
    }

#ifdef WIN32
#include <conio.h>
#else
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

inline int _kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}
#endif

} //namespace utils
} //namespace desmo
#endif //_UTILS_H_
