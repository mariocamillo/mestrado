/*
*  deforma.h
*/
#ifndef _DEFORMA_H_
#define _DEFORMA_H_

#include <cstring>
#include <cmath>
#include <iostream>
//#include <stdio.h>
#include <list>
#include "mesh.h"
#include "Vec.h"

extern int divisoes;
extern double diff_base[2];

#define ZETA 80.0
#define XI 0.001
#define PHI 0.00001
#define DAMP 0.0001
// #define ZETA 0.1
// #define XI 0.0
// #define PHI 0.0
// #define DAMP 0.05

#define umin  -0.5f
#define umax  0.5f 
#define vmin  -0.5f
#define vmax  0.5f

//#define MASS 0.111 // Massa geral
#define MASS 0.2 // Massa geral

using namespace std;

// Usado no Screenshot
struct Rect
{
    int left,top,right,bottom;
};

// Estrutura de cada vertex. 
// ** => TODO: Para ajudar o debug essas variaveis s�o armazenadas na malha, mas na verdade podem ser usadas s� no processamento local.
typedef struct POINT_GEOM_struct 
{   /* points */
    double x, y, z;					// Coordenadas
    vec n;							// Normal
    double k1, k2;					// Curvaturas
    vec pdir1, pdir2;
    vec base_a[3];
    double mu;       // mass density
    double area_det_a;
    double damp;
    double bw[2][2];     // Weingartem: b[alpha][beta] = b_{alpha}^{beta} **
    vec force_external_cart;         // forca externa aplicada no vertex
    vec v_cart;						 // velocidade
    vec v_next_cart;				 // velocity no proximo instante  // TODO: pode ser processado localmente e nao armazenado
    vec force_vertex_cart;			 // Forcas totais (internas+externas) no vertex
    VertexID next;					 // Para guardar relacao com estado inicial
    VertexID previous;				 // Para guardar relacao com estado inicial
    VertexID MetricVertex[2];		 // Vertexs de referencia para deterinar base_a[x]. Usado para o sentido positivo das diferencas finitas.
    VertexID MetricVertex_return[2]; // Vertexs de "retorno" em relacao as bases_a[x]. Usado para o sentido negativo das diferencas finitas. Armazenando pois eh muito custoso determinar em realtime. 
    VertexID BorderCopyVertex;		 // TODO: Depreciado, sendo removido. Referencia um vertice interior em um da borda. Usado originalmente para copiar o tensor de curvatura.
    VertexID BorderRefVertex[3];	 // TODO: Ver se vai ser mantido. Vertices de referencia para a tecnica de planificacao. No momento em duvida se vai ser aplicado nos cantos.
    VertexID BorderRefVer_base_t[3]; // TODO: Ver se vai ser mantido. A base_t usado para a tecnica de planificacao.
    vec BorderRefVertexCoords[3];    // TODO: Ver se vai ser mantido. Cordenadas da tecnica de planificacao.
    int border_id;					 // id da borda, de 0 a 8. 0 eh a regiao interior. 1 a 8 sao as bordas (cantos tem ids proprios).
    double a[2][2];					 // Tensor Metrico covariante.        FUTURO: Para poupar memroai, usar estrutura propria que possua apenas 3 elementos mas possa ser referenciada com [i][j], pois a[0][1]=a[1][0].
    double c_a[2][2];				 // Tensor Metrico contravariante.    FUTURO: Idem
    double b[2][2];					 // Tensor de curvatura.			  FUTURO: Idem
    double a11_T,a12_T,a22_T;	     // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    double c_a11_T,c_a12_T,c_a22_T;  // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    double w11_T,w12_T,w21_T,w22_T;  // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    double b11_T,b12_T,b22_T;        // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    vec pdir1_T, pdir2_T;			 // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    vec ru,rv;						 // DEBUG: Guarda valor teorico para superficies de deformacao conhecida (sela, calha, etc).
    double err_a11,err_a12,err_a22;  // DEBUG: Erro entre teorico e estimado.
    double err_b11,err_b12,err_b22;  // DEBUG: Erro entre teorico e estimado.
    double err_k1,err_k2;			 // DEBUG: Erro entre teorico e estimado.
    double errMax_a;				 // DEBUG: Erro entre teorico e estimado.
    double errMax_b;				 // DEBUG: Erro entre teorico e estimado.
    double errMax_k;				 // DEBUG: Erro entre teorico e estimado.	
    double k1_T, k2_T;				 // DEBUG: Erro entre teorico e estimado.	
    double phi[2][2][2][2];			 // Constante para cada vertex. 
    double psi[2][2][2][2];			 // Constante para cada vertex. 
    double theta[2][2][2][2];        // Constante para cada vertex. 
    double N[3][2];					 // Tensor N, no formato 3x2
    double NN[2][2];                 // Tensor N', no formato 2x2.
    double M[2][2];					 // Tensor M
    double kappa[2][2];				 // Tensor kappa
    double epsilon[2][2];            // Tensor espilon
    double diff_a[2][2][2];			 // Derivada ordinaria. TODO: Retirar da estrutura global. Esta aqui no momento para ajudar debugs.
    double diff_N[2][2][2];			 // Derivada ordinaria. TODO: Retirar da estrutura global. Esta aqui no momento para ajudar debugs.
    double diff_M[2][2][2];          // Derivada ordinaria. TODO: Retirar da estrutura global. Esta aqui no momento para ajudar debugs.
    double diff_dc_M[2][2][2][2];    // Derivada ordinaria da derivada covariante de M. TODO: Retirar da estrutura global. Esta aqui no momento para ajudar debugs.
    double dc_M[2][2][2];		     // Derivada covariante. Formato: dc_M[gamma][alpha][beta] Derivada Covariante de M^{alpha}{beta} em rela��o a gamma ... 
    double dc_N[2][2][2];			 // Derivada covariante. Formato: dc_N[gamma][alpha][beta] Derivada Covariante de N^{alpha}{beta} em rela��o a gamma ... 
    double dc2_M[2][2];				 // Derivada covariante. Formato: dc2_M[alpha][beta] Derivada Covariante dupla de M^{beta}{alpha} em rela��o a alpha e beta M^{beta,alpha}|{alpha,beta} ...
    vec dc_N_cart[2];				 // Derivada covariante. Formato: Derivada N[alpha] em relacao a [alpha]. TODO: Retirar da estrutura global. Esta aqui no momento para ajudar debugs.

    double chris_tipo1[3][3][3];     // Simbolos de Christoffel tipo1.
    double chris_tipo2[2][2][2];     // Simbolos de Christoffel tipo1.
    double energia_A;				 // DEBUG: Para o calculo de energia.
    double debug;					 // DEBUG: Debugs para visualizacao.
    double debug2;
    double debug3;
    double debug_ponto;
    double debug_force;
    double debug_force2;
    double debug_border;
    double u_T,v_T;					 // Coordenada original u,v no plano.
    bool external_force;			 // DEBUG: Controle se aplica forca externa 

    // Variaveis de controle de colisao. Desabilitado no momento.
    bool debug_collision_vertex;
    bool debug_collision_edge;
    bool collision;
    int coll_count;
    vec force_collision_cart;
} Point3DGeom;

double debug_load(Mesh *m0, Mesh *m1, Mesh *m2, int mesh_type, bool full = true);
double debug_update(Mesh *m, double t, int mesh_type);
void debug_print_values(Mesh *m);
double copy_mesh (Mesh *m1, Mesh *m2);
double compute_vertex_metric_and_borders(Mesh *m);
double compute_normals (Mesh *m);
double compute_tensors(Mesh *m);
double compute_phi_psi_theta(Mesh *m);
double compute_tensors_n_m(Mesh *m, Mesh *m0);
double compute_chris(Mesh *m) ;
double compute_dm(Mesh *m);
double compute_d2m_dn(Mesh *m);
double compute_n3(Mesh *m);
double compute_new_positions(Mesh *m, bool stopcond);

#endif //_DEFORMA_H_
