#ifndef _GL_PROFILER_H_
#define _GL_PROFILER_H_

#include <string>
#include <GL/glew.h>
#include "gltimer.h"

class GLProfiler {
public:
    enum CARD_VENDOR {
        NVIDIA = 0,
        AMD
    };

    GLProfiler(GLint numTimers) : m_cardID(0), m_curTimer(0), m_numTimers(0), m_timers(NULL)
    {
        // Get card vendor
        std::string vendorString(reinterpret_cast<const char *>(glGetString(GL_VENDOR)));
        if (vendorString.find("NVIDIA") != std::string::npos)
        {
            m_vendor = NVIDIA;
        }
        else if (vendorString.find("AMD") != std::string::npos ||
                 vendorString.find("ATI") != std::string::npos)
        {
            m_vendor = AMD;
        } 

        m_cardID = GetFirstCardID();
        m_totalMemory = GetTotalMemory();

        NumTimers(numTimers);
    }

    ~GLProfiler()
    {
        if (m_timers != NULL)
        {
            delete [] m_timers;
        }
    }

    GLint StartTimer()
    {
        if (m_curTimer >= m_numTimers)
        {
            m_curTimer = 0;
            m_timers[m_curTimer].Reset();
        }

        if (m_timers[m_curTimer].Start())
        {
            return m_curTimer++;
        }
        
        return -1;
    }

    bool EndTimer(GLint timer)
    {
        if (timer >=0 && timer < m_numTimers) return m_timers[timer].End();

        return false;
    }

    bool TimerElapsed(GLint timer, GLuint64 &ns)
    {
        if (timer >= 0 && timer < m_numTimers) return m_timers[timer].Elapsed(ns);

        return false;
    }

    GLuint GetFirstCardID();

    GLint GetTotalMemory();
    
    GLint GetUsedMemory();

    inline CARD_VENDOR vendor() { return m_vendor; }
    inline GLint NumTimers(GLint num)
    {
        if (m_timers != NULL)
        {
            // Check that no timer is in use
            for (GLint i = 0; i < m_numTimers; ++i)
            {
                if (m_timers[i].InUse()) return m_numTimers;
            }
            delete [] m_timers;
        }
        m_timers = new GLTimer[num];
        m_numTimers = num;

        return num;
    }

    inline int NumTimers() { return m_numTimers; }

private:

    GLint GetTotalMemoryNVidia();
    GLint GetUsedMemoryNVidia();
    GLuint GetFirstCardIDNVidia();

    GLint GetTotalMemoryAMD();
    GLint GetUsedMemoryAMD();
    GLuint GetFirstCardIDAMD();

    CARD_VENDOR m_vendor;
    GLuint m_cardID;
    GLint m_curTimer;
    GLint m_numTimers;
    GLTimer *m_timers;
    GLint m_totalMemory;
};

#endif //_GL_PROFILER_H_
