#include "tfpass.h"
#include <cstring>
#include <iostream>
#include "util.h"

#define SWITCH_RENDER_OPT(on, opt) (on ? glEnable(opt) : glDisable(opt))
#define ADD_SHADER(type, path) \
    (!path.empty() ? m_shaderProg.addShader(type, path) : true)

using namespace std;

static inline bool readTex(GLuint tex, void *dest)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, dest);
    glBindTexture(GL_TEXTURE_2D, 0);

    // TODO: check glGetTexImage return values for error
    return true;
}

TFPass::TFPass()
{
}

TFPass::~TFPass()
{
    // TODO free ogl resources
}

// TODO: fix this method
bool TFPass::SetupShaders(const std::string &vs, const std::string &tcs,
                          const std::string &tes, const std::string &gs,
                          const char *varyings[], const int varyings_size)
{
    if (m_shaderProg.init() &&
        ADD_SHADER(GL_VERTEX_SHADER, vs) &&
        ADD_SHADER(GL_TESS_CONTROL_SHADER, tcs) &&
        ADD_SHADER(GL_TESS_EVALUATION_SHADER, tes) &&
        ADD_SHADER(GL_GEOMETRY_SHADER, gs))
    {
        glTransformFeedbackVaryings(m_shaderProg.shaderProgram(),
                                    varyings_size, varyings,
                                    GL_INTERLEAVED_ATTRIBS);
        return m_shaderProg.link();
    }

    return false;
}

bool TFPass::Render(GLuint vao, GLuint ibo, GLuint numElems,
                     bool useBarrier, GLenum elemType)
{
    // invalid input
    if (vao == 0 || ibo == 0)
    {
        return false;
    }

    // Output was not setup correctly
    if (m_outBOs.empty())
    {
        return false;
    }

    if (!m_shaderProg.use())
    {
        return false;
    }

    // Wait all transform feedback operations finish
    if (useBarrier) {
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT |
                        GL_TRANSFORM_FEEDBACK_BARRIER_BIT |
                        GL_TEXTURE_FETCH_BARRIER_BIT |
                        GL_TEXTURE_UPDATE_BARRIER_BIT);
    }

    // Bind draw buffers
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // Set uniforms
    for (std::map<GLint,GLint>::iterator it = m_uniform1is.begin();
         it != m_uniform1is.end(); ++it)
    {
        glUniform1i(it->first, it->second);
    }
    for (std::map<GLint,glm::ivec2>::iterator it = m_uniform2is.begin();
         it != m_uniform2is.end(); ++it)
    {
        glUniform2i(it->first, it->second.x, it->second.y);
    }
    for (std::map<GLint,GLfloat>::iterator it = m_uniform1fs.begin();
         it != m_uniform1fs.end(); ++it)
    {
        glUniform1f(it->first, it->second);
    }

    // Make textures active
    size_t i = 0;
    for (std::map<GLint,GLuint>::iterator it = m_inTexs.begin();
         it != m_inTexs.end(); ++it, ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, it->second);
        glUniform1i(it->first, i);
    }

    // Bind transform feedback buffers
    for (size_t i = 0; i < m_outBOs.size(); ++i)
    {
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, i, m_outBOs[i]);
    }

    // no need to pass index array 'cause ibo is bound already
    glBeginTransformFeedback(elemType);
    glDrawElements(elemType, numElems, GL_UNSIGNED_INT, 0);
    glEndTransformFeedback();

    return true;
}
