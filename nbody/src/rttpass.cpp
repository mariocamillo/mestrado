#include "rttpass.h"
#include <cstring>
#include <iostream>
#include "util.h"

#define SWITCH_RENDER_OPT(on, opt) (on ? glEnable(opt) : glDisable(opt))
#define ADD_SHADER(type, path) \
    (!path.empty() ? m_shaderProg.addShader(type, path) : true)

using namespace std;

static inline bool readTex(GLuint tex, void *dest)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, dest);
    glBindTexture(GL_TEXTURE_2D, 0);

    // TODO: check glGetTexImage return values for error
    return true;
}

RTTPass::RTTPass(GLsizei width, GLsizei height, size_t numOutTexs) :
    m_width(width),
    m_height(height),
    m_fbo(0),
    m_outTexs(numOutTexs, 0),
    m_cull(true),
    m_depth(true),
    m_stencil(true),
    m_wireframe(false),
    m_alpha(true),
    m_blend(false),
    m_blendState(3, 0)
{
}

RTTPass::~RTTPass()
{
    // TODO free ogl resources
}

bool RTTPass::SetupShaders(const std::string &vs, const std::string &tcs,
                           const std::string &tes, const std::string &gs,
                           const std::string &fs)
{
    if (m_shaderProg.init() &&
        ADD_SHADER(GL_VERTEX_SHADER, vs) &&
        ADD_SHADER(GL_TESS_CONTROL_SHADER, tcs) &&
        ADD_SHADER(GL_TESS_EVALUATION_SHADER, tes) &&
        ADD_SHADER(GL_GEOMETRY_SHADER, gs) &&
        ADD_SHADER(GL_FRAGMENT_SHADER, fs) &&
        m_shaderProg.link())
    {
        return true;
    }

    return false;
}

bool RTTPass::SetupRenderOptions(bool cull, bool depth, bool stencil, bool wireframe,
                                 bool alpha, bool blend,
                                 const std::vector<GLenum> &blendState)
{
    if (blendState.size() != 3)
    {
        return false;
    }

    m_cull = cull;
    m_depth = depth;
    m_stencil = stencil;
    m_wireframe = wireframe;
    m_alpha = alpha;
    m_blend = blend;
    m_blendState = blendState;

    return true;
}

bool RTTPass::SetupOutputs()
{
    // Create frame buffer for tensor estimation RTT pass
    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    // Create textures to store estimated tensors
    glGenTextures(m_outTexs.size(), &(m_outTexs[0]));
    std::vector<GLenum> drawBuffers(m_outTexs.size(), 0);
    for (size_t i = 0 ; i < m_outTexs.size(); ++i)
    {
        glBindTexture(GL_TEXTURE_2D, m_outTexs[i]);
        // 1st 0: level, 2nd 0: no border, 3rd 0: no initial data
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, 0);
        // Poor filtering. Needed to make a complete texture that is readable on later passes.
        // see https://www.opengl.org/wiki/Common_Mistakes#Creating_a_complete_texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glBindTexture(GL_TEXTURE_2D, 0);

        // 0: level
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
                               GL_TEXTURE_2D, m_outTexs[i], 0);
        drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
    }
    glDrawBuffers(m_outTexs.size(), &(drawBuffers[0]));
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        return false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return true;
}

bool RTTPass::Render(GLuint vao, GLuint ibo, GLuint numElems,
                     bool useBarrier, GLenum elemType,
                     const vector<pair<GLint, GLint> > &ssbos,
                     GLint drawStep, GLint numInstances, bool clear)
{
    // invalid input
    if (vao == 0 || ibo == 0)
    {
        return false;
    }

    if (drawStep == 0)
    {
        drawStep = numElems;
    }

    // Output was not setup correctly
    if (m_fbo == 0)
    {
        return false;
    }

    if (!m_shaderProg.use())
    {
        return false;
    }

    glPushAttrib(GL_ENABLE_BIT | GL_VIEWPORT_BIT | GL_POLYGON_BIT);

    // setup render options
    if (m_wireframe)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    SWITCH_RENDER_OPT(m_cull, GL_CULL_FACE);
    SWITCH_RENDER_OPT(m_depth, GL_DEPTH_TEST);
    SWITCH_RENDER_OPT(m_stencil, GL_STENCIL_TEST);
    SWITCH_RENDER_OPT(m_alpha, GL_ALPHA_TEST);

    glViewport(0, 0, m_width, m_height); // setup viewport (equal to textures size)

    // Wait all writes to texture to finish
    if (useBarrier) {
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT |
                        GL_TEXTURE_FETCH_BARRIER_BIT |
                        GL_TEXTURE_UPDATE_BARRIER_BIT |
                        GL_TEXTURE_UPDATE_BARRIER_BIT);
    }

    glUniform2i(m_shaderProg.getUniformLocation("size"), m_width, m_height);
   
    // Bind buffers
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    SWITCH_RENDER_OPT(m_blend, GL_BLEND);
    if (m_blend)
    {
        glBlendEquation(m_blendState[0]);
        glBlendFunc(m_blendState[1], m_blendState[2]);
    }

    // Set uniforms
    for (std::map<GLint,GLint>::iterator it = m_uniform1is.begin();
         it != m_uniform1is.end(); ++it)
    {
        glUniform1i(it->first, it->second);
    }
    for (std::map<GLint,glm::ivec2>::iterator it = m_uniform2is.begin();
         it != m_uniform2is.end(); ++it)
    {
        glUniform2i(it->first, it->second.x, it->second.y);
    }
    for (std::map<GLint,GLfloat>::iterator it = m_uniform1fs.begin();
         it != m_uniform1fs.end(); ++it)
    {
        glUniform1f(it->first, it->second);
    }

    // Make textures active
    size_t i = 0;
    for (std::map<GLint,GLuint>::iterator it = m_inTexs.begin();
         it != m_inTexs.end(); ++it, ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, it->second);
        glUniform1i(it->first, i);
    }

    // Clear texture before draw
    if (clear)
    {
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    for (auto ssbo : ssbos)
    {
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, ssbo.second, ssbo.first);
    }

    // no need to pass index array 'cause ibo is bound already
    for (int i = 0; i < numElems; i += drawStep)
    {
        glDrawArraysInstanced(elemType, i, drawStep, numInstances);
    }

    for (auto ssbo : ssbos)
    {
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, ssbo.second, 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glPopAttrib();

    return true;
}

void RTTPass::ReadTexBack(std::vector<glm::vec4 *> &outTexs)
{
    // Wait all writes to shader storage and texture to finish
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT |
                    GL_TEXTURE_UPDATE_BARRIER_BIT |
                    GL_TEXTURE_FETCH_BARRIER_BIT);

    size_t texSize = m_width*m_height;
    for (size_t i = 0; i < std::max<size_t>(outTexs.size(), m_outTexs.size());
         ++i)
    {
        if (outTexs[i] == NULL) {
            outTexs[i] = new glm::vec4[texSize];
        }
        memset(outTexs[i], 0, texSize*sizeof(glm::vec4));
        readTex(m_outTexs[i], outTexs[i]);
    }
}
