#version 440

layout(location=0) in vec4 oldPos;
layout(location=1) in vec4 oldVel;

out vec4 newPos;
out vec4 newVel;

uniform ivec2 size;
uniform float dt;
uniform float damping;
uniform sampler2D accelTex;

ivec2 index_to_texelCoord(uint index) {
    // Index to x,y position in texture (x,y).
    return ivec2(index%size.x, index/size.x);
}

void main() {
    vec3 accel = texelFetch(accelTex, index_to_texelCoord(gl_VertexID), 0).xyz;
    vec3 vel = (oldVel.xyz + accel * dt) * damping;
    vec3 pos = oldPos.xyz + vel * dt;
    newPos = vec4(pos, oldPos.w);
    newVel = vec4(vel, oldVel.w);
}
