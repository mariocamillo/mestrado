#version 430
#extension GL_ARB_tessellation_shader : enable
#extension GL_ARB_shader_storage_buffer_object: require

layout(quads, equal_spacing, cw, point_mode) in;

uniform float dt;
uniform float softeningSq;

in VertexData {
    vec4 pos;
    vec4 pos2;
    vec2 texCoord;
} vOut[];

out TEData {
    vec4 force;
};

vec3 interaction(vec4 a, vec4 b)
{
    vec3 r_ij = b.xyz - a.xyz; // 3 FLOPs
    float softDist = dot(r_ij, r_ij) + softeningSq; // 6 FLOPs
    float invSqrtSoftDist = inversesqrt(softDist); // 2 FLOPs
    vec3 accel_wo_mass = r_ij * invSqrtSoftDist * invSqrtSoftDist * invSqrtSoftDist; // 5 FLOPs
    return accel_wo_mass * b.w;
}

void main()
{
    // TODO: Calculate my index from tesselator output
    int my_idx = int(round(24*gl_TessCoord.y + 7*gl_TessCoord.x));
    vec3 accel = vec3(0);

    for (int i = 0; i < 32; ++i)
    {
        accel += interaction(vOut[my_idx].pos, vOut[i].pos2);
    }

    // Index to x,y position in texture (x,y).
    vec2 pos = vOut[my_idx].texCoord;

    // Clip position ([-1,1]) so that fragCoord in frag shader is equal to texture position
    force = vec4(accel, 0);
    gl_Position = vec4(-1.0f + 2.0f*pos.x, -1.0f + 2.0f*pos.y, 0f, 1);
}
