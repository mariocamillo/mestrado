#version 440

layout(location=0) in vec4 pos;
layout(location=1) in vec4 vel;

out VertexData {
    vec4 pos;
    vec4 pos2;
    vec2 texCoord;
} vOut;

uniform ivec2 size;

layout(std140, binding=0) buffer PosBuffer {
    vec4 positions[];
};
layout(std140, binding=1) buffer VelBuffer {
    vec4 velocities[];
};

vec2 index_to_texCoord(uint index) {	
    // Index to x,y position in texture (x,y).
    vec2 pos = vec2(index%size.x, index/size.x);
	
	// Make it [0,1] range
	return vec2(pos.x/size.x, pos.y/size.y);
}

void main() {
    vOut.pos = pos;
    vOut.pos2 = positions[gl_InstanceID*32 + gl_VertexID%32];
    vOut.texCoord = index_to_texCoord(gl_VertexID);
}
