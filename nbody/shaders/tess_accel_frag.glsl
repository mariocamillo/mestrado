#version 430
#extension GL_ARB_shader_storage_buffer_object: require

in TEData {
    vec4 force;
};

layout(location = 0) out vec4 accel;

void main()
{
    accel = force;
}
