#version 440

layout(location=0) in vec4 pos;

out VertexData {
    vec4 pos;
    vec2 texCoord;
} vOut;

uniform ivec2 size;

vec2 index_to_texCoord(uint index) {	
    // Index to x,y position in texture (x,y).
    vec2 pos = vec2(index%size.x, index/size.x);
	
	// Make it [0,1] range
	return vec2(pos.x/size.x, pos.y/size.y);
}

void main() {
    vOut.pos = pos;
    vOut.texCoord = index_to_texCoord(gl_VertexID);
}
