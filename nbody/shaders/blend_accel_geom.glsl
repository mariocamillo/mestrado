#version 440

layout (lines) in;
layout (points, max_vertices = 2) out;

in VertexData {
    vec4 pos;
    vec2 texCoord;
} vOut[];

out GeomData {
    vec3 accel;
} gOut;

uniform float dt;
uniform float softeningSq;

vec4 texCoord_to_pos(vec2 texCoord) {
    return vec4(-1.0f + 2.0f*texCoord.x, -1.0f + 2.0f*texCoord.y, 0f, 1);
}

void main() {
    vec3 r_ij = vOut[0].pos.xyz - vOut[1].pos.xyz; // 3 FLOPs
    float softDist = dot(r_ij, r_ij) + softeningSq; // 6 FLOPs
    float invSqrtSoftDist = inversesqrt(softDist); // 2 FLOPs
    vec3 accel_wo_mass = r_ij * invSqrtSoftDist * invSqrtSoftDist * invSqrtSoftDist; // 5 FLOPs

    // accel for i
    gl_Position = texCoord_to_pos(vOut[0].texCoord);
    gOut.accel = accel_wo_mass * vOut[1].pos.w; // 1 FLOP
    EmitVertex();

    // accel for j
    gl_Position = texCoord_to_pos(vOut[1].texCoord);
    gOut.accel = - (accel_wo_mass * vOut[0].pos.w); // 1 FLOP
    EmitVertex();
}
