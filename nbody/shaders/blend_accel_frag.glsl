#version 440

in GeomData {
    vec3 accel;
} gOut;

layout(location = 0) out vec4 force;

void main()
{
    force = vec4(gOut.accel, 0);
}
