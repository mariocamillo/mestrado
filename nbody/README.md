This is an implementation of gravitational n-body simulation.

It has 3 different methods of implementation:

* Cuda all-pairs: This is the implementation provided by NVidia Corporation in
their CUDA samples package, specifically v8.0, and is used as the basis for the
other implementations as to provide a fair comparison ground for all of them.

* Tessellation: This is a port of the CUDA implementation to use tessellation
shaders. This avoids context swapping between CUDA and OpenGL and leaves the
shared memory management for each 32-body "patch" to the driver/hardware

* Blend: This is an implementation based on Griffin et al work on a
blending technique for curvature tensor estimation. This also avoids context
swapping and aims to be quicker by leveraging the geometry shader for
pair-wise computation and the blend hardware for body velocity accumulation.

## How to build
Pre-requisites:

* OpenGL
* CUDA SDK

Linux specific pre-requisites (pre-compiled libs are provided in 3rdParty dir for Windows but may need to be rebuilt depending on MSVC version):

* glew
* freeglut

Run CMake to generate msvc/make files:

``` bash
$ mkdir build && cd build
$ cmake ../
```

The application can than be built with:

* Windows: Visual Studio using the project file `build/nbody.sln` 
* Linux: `cd build && make`

## How to run

After building the application you should have a binary called `nbody`.
It has the same options as the original from the CUDA SDK but also an additional two options:

* `-blend`: run n-body simulation on the OpenGL using Blend technique
* `-tess`: run n-body simulation on the OpenGL using Tessellation shaders with instanced rendering

The tessellation without instancing method is available as a historical version in git commit **05e47dd**.