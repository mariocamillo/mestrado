#!/bin/bash

echo "Execute $1 seconds per application, per num bodies size"

tess=true
blend=true
cuda=true

if [ $2 == "-t" ]; then
  echo "Exec only tessellation tests"
  cuda=false
  blend=false
fi

if [ $2 == "-c" ]; then
  echo "Exec only cuda tests"
  tess=false
  blend=false
fi

if [ $2 == "-b" ]; then
  echo "Exec only blend tests"
  tess=false
  cuda=false
fi

mesh_sizes=(256 512 768 1024 1280 1536 1792 2048 2304 2560 2816 3072 3328 3584)
#mesh_sizes=(3072 3328 3584)

if [ $tess = true ]; then
  echo > tess.avg.out
fi

if [ $blend = true ]; then
  echo > blend.avg.out
fi

if [ $cuda = true ]; then
  echo > cuda.avg.out
fi

average=0
for size in ${mesh_sizes[@]}; do

  echo "Running for mesh size $size"
  if [ $tess = true ]; then
    echo > tess.out
    ./nbody --tess --numbodies=$size >> tess.out &
    pid=$!
    sleep $1
    kill $pid
    average=`grep "FPS" tess.out | awk 'NR>1 {sum+=$2}END{print sum/(NR-1)}'`
    echo "$size, $average" >> tess.avg.out
  fi
  if [ $blend = true ]; then
    echo > blend.out
    ./nbody --blend --numbodies=$size >> blend.out &
    pid=$!
    sleep $1
    kill $pid
    average=`grep "FPS" blend.out | awk 'NR>1 {sum+=$2}END{print sum/(NR-1)}'`
    echo "$size, $average" >> blend.avg.out
  fi
  if [ $cuda = true ]; then
    echo > cuda.out
    ./nbody --numbodies=$size >> cuda.out &
    pid=$!
    sleep $1
    kill $pid
    average=`grep "FPS" cuda.out | awk 'NR>1 {sum+=$2}END{print sum/(NR-1)}'`
    echo "$size, $average" >> cuda.avg.out
  fi

done
