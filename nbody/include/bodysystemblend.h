#ifndef __BODYSYSTEMBLEND_H__
#define __BODYSYSTEMBLEND_H__

#include "bodysystem.h"
#include "rttpass.h"
#include "tfpass.h"

// GLSL Blend based BodySystem: runs on the GPU
template <typename T>
class BodySystemBlend : public BodySystem<T>
{
    public:
        BodySystemBlend(unsigned int numBodies);
        virtual ~BodySystemBlend();

        virtual void loadTipsyFile(const std::string &filename);

        virtual void update(T deltaTime);

        virtual void setSoftening(T softening);
        virtual void setDamping(T damping);

        virtual T *getArray(BodyArray array);
        virtual void setArray(BodyArray array, const T *data);

        virtual unsigned int getCurrentReadBuffer() const
        {
            return m_ppbo[m_currentRead];
        }

        virtual unsigned int getNumBodies() const
        {
            return m_numBodies;
        }

    protected: // methods
        BodySystemBlend() {}

        virtual void _initialize(int numBodies);
        virtual void _finalize();

    protected: // data
        unsigned int m_numBodies;
        size_t m_numElements[2];
        bool m_bInitialized;

        RTTPass *m_accelPass;
        TFPass *m_posPass;
        unsigned int m_ppbo[2];
        unsigned int m_vpbo[2];
        unsigned int m_ipbo[1];
        unsigned int m_vao[2];
        unsigned int m_ibo[2];
        unsigned int m_currentRead;
        unsigned int m_currentWrite;
        T *m_hpos;
        T *m_hvel;
};

#include "bodysystemblend_impl.h"

#endif // __BODYSYSTEMBLEND_H__
