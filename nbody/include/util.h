/*

	Copyright 2011 Etay Meiri

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _UTIL_H
#define _UTIL_H


#define HELPERGL_EXTERN_GL_FUNC_IMPLEMENTATION
#include "helper_gl.h" 
#ifdef WIN32
#include <Windows.h> // avoid warning of APIENTRY redefinition in glfw
#else
#include <unistd.h>
#endif
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <map>
#include <string>
#include <sstream>
#include <glm/glm.hpp>

#define ZERO_MEM(a) memset(a, 0, sizeof(a))

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

#ifdef WIN32
#define SNPRINTF _snprintf_s
#define RANDOM rand
#define SRANDOM srand((unsigned)time(NULL))
#else
#define SNPRINTF snprintf
#define RANDOM random
#define SRANDOM srandom(getpid())
#endif

#define INVALID_OGL_VALUE 0xFFFFFFFF

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }
#define SAFE_DELETE_ARRAY(p) if (p) { delete [] p; p = NULL; }

#define GLExitIfError() {}/*                                                         \
{                                                                               \
    GLenum Error = glGetError();                                                \
                                                                                \
    if (Error != GL_NO_ERROR) {                                                 \
        printf("OpenGL error in %s:%d: 0x%x\n", __FILE__, __LINE__, Error);     \
        exit(0);                                                                \
    }                                                                           \
}*/

#define GLCheckError() (true) //(glGetError() == GL_NO_ERROR)

#define clean_pointer_vector(vec) \
    while (!vec.empty())          \
    {                             \
        delete *(vec.begin());    \
        vec.erase(vec.begin());   \
    }

static inline int int_round(double a)
{
    return static_cast<int>((a - floor(a) >= 0.5F ) ? ceil(a) : floor(a));
}

static inline glm::ivec2 square_approximation(int a)
{
    double root = std::sqrt(a);
    return glm::ivec2(int_round(root), ceil(root));
}

inline std::ostream &operator<<(std::ostream &out, const glm::vec3 &v)
{
    out << "(" << v.x << "," << v.y << "," <<  v.z << ")";
    return out;
}
inline std::ostream &operator<<(std::ostream &out, const glm::vec4 &v)
{
    out << "(" << v.x << "," << v.y << "," <<  v.z << "," <<  v.w <<  ")";
    return out;
}

static bool readSSBO(GLuint ssbo, GLuint bindingPoint, void *dest, GLsizei byteLen)
{
    memset(dest, 0, byteLen); // clear dest buffer

    // Read back tensors from ssbo
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPoint, ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    GLvoid *data = glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
                                    0,
                                    byteLen,
                                    GL_MAP_READ_BIT);
    if (data != NULL) {
        memcpy(dest, data, byteLen);
    } else {
        false;
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    return true;
}

static void printDebugBuffer(glm::vec4 *buffer, GLuint len, std::string bufferName)
{
    for (GLuint i = 0; i < len; ++i)
    {
        if (buffer[i].x != -1)
            std::cout << "Vertex #" << i << " " << bufferName << ": " << buffer[i] << std::endl;
    }
}

static void genArrayBuffers(GLuint *ids, GLuint numBuffers, GLuint memSize, GLenum mode)
{
    glGenBuffers(numBuffers, (GLuint *)ids);
    for (GLuint i = 0; i < numBuffers; ++i)
    {
        glBindBuffer(GL_ARRAY_BUFFER, ids[i]);
        glBufferData(GL_ARRAY_BUFFER, memSize, 0, mode);

        int size = 0;
        glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, (GLint *)&size);

        if ((GLuint)size != memSize)
        {
            fprintf(stderr, "WARNING: Pixel Buffer Object allocation failed!\n");
        }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}

static void updateBOData(GLuint bo, GLuint memSize, void *data)
{
    glBindBuffer(GL_ARRAY_BUFFER, bo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, memSize, data);

    int size = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, (GLint *)&size);

    if ((unsigned)size != memSize)
    {
        fprintf(stderr, "WARNING: Pixel Buffer Object download failed!\n");
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

static void readBOData(GLint bo, void *hdata, GLsizei size)
{
    glBindBuffer(GL_ARRAY_BUFFER, bo);
    glGetBufferSubData(GL_ARRAY_BUFFER, 0, size, hdata);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#endif // _UTIL_H
