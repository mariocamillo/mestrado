#ifndef _TF_PASS_H_
#define _TF_PASS_H_
#include <map>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#define HELPERGL_EXTERN_GL_FUNC_IMPLEMENTATION
#include "helper_gl.h" 
#include "shaderLoader.h"

/**
 * Abstracts a Render-to-Texture draw pass in the GPU
 */
class TFPass {
public:
    /**
     * Construct a TFPass object that will render to a texture of size
     * width x height.
     *
     * @param width  Width of the output texture.
     * @param height Height of the output texture.
     * @param numOutTexs Number of output textures in this pass.
     */
    TFPass();
    ~TFPass();

    /**
     * Creates a shader program using the shader files passed as parameters.
     *
     * If any shader is passed as an empty string the shader program is created
     * without that shader step. Underneath it uses \sa ShaderLoader
     *
     * @param vs  Vertex shader path.
     * @param tcs Tesselation Control shader path.
     * @param tes Tesselation Evaluation shader path.
     * @param gs Geometry shader path.
     *
     * @return Successfulness of the shader program creation.
     */
    bool SetupShaders(const std::string &vs, const std::string &tcs,
                      const std::string &tes, const std::string &gs,
                      const char *varyings[], const int varyings_size);

    /**
     * Renders the given vbo and ibo to texture with the shaders and options setup.
     *
     * @param vao The vertex array object to be rendered.
     * @param ibo The index buffer object to be rendered.
     * @param numElems Number of elements to be drawn in this call.
     * @param useBarrier Use glBarrier during render call
     * @param elemType Type of element being drawn. Default is triangles
     *
     * @return Boolean indicating if the render was successful.
     */
    bool Render(GLuint vao, GLuint ibo, GLuint numElems,
                bool useBarrier, GLenum elemType = GL_TRIANGLES);

    ShaderLoader &ShaderProgram() { return m_shaderProg; }

    void clearOutputBOs() { m_outBOs.clear(); }
    void addOutputBO(GLuint bo)
    {
        m_outBOs.push_back(bo);
    }
    void addInputTex(std::string location, GLuint tex)
    {
        m_inTexs[m_shaderProg.getUniformLocation(location.c_str())] = tex;
    }
    void addUniform1i(std::string location, GLint value)
    {
        m_uniform1is[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

    void addUniform2i(std::string location, glm::ivec2 value)
    {
        m_uniform2is[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

    void addUniform1f(std::string location, GLfloat value)
    {
        m_uniform1fs[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

private:
    std::vector<GLuint> m_outBOs;
    ShaderLoader m_shaderProg;
    std::map<GLint, GLuint> m_inTexs;
    std::map<GLint, GLint> m_uniform1is;
    std::map<GLint, GLfloat> m_uniform1fs;
    std::map<GLint, glm::ivec2> m_uniform2is;
};
#endif // _TF_PASS_H_
