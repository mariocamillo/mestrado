/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include <assert.h>
#include <math.h>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>

#include "util.h"

template<typename T>
BodySystemBlend<T>::BodySystemBlend(unsigned int numBodies)
    : m_numBodies(numBodies),
      m_bInitialized(false),
      m_currentRead(0),
      m_currentWrite(1)
{
    _initialize(numBodies);
    setSoftening(0.00125f);
    setDamping(0.995f);
}

template<typename T>
BodySystemBlend<T>::~BodySystemBlend()
{
    _finalize();
    m_numBodies = 0;
}

template<typename T>
void BodySystemBlend<T>::_initialize(int numBodies)
{
    assert(!m_bInitialized);

    m_numBodies = numBodies;

    unsigned int memSize = 4 * sizeof(T) * numBodies;

    // allocate host memory
    m_hpos = new T[4 * numBodies];
    m_hvel = new T[4 * numBodies];

    // create the pixel buffer objects for rendering
    genArrayBuffers((GLuint *)m_ppbo, 2, memSize, GL_DYNAMIC_COPY); //position
    genArrayBuffers((GLuint *)m_vpbo, 2, memSize, GL_DYNAMIC_COPY); //velocity

    // setup VAOs
    glGenVertexArrays(2, (GLuint *) m_vao);
    auto setupVAO = [&](int idx) {
        glBindVertexArray(m_vao[idx]);
        glBindBuffer(GL_ARRAY_BUFFER, m_ppbo[idx]);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, m_vpbo[idx]);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    };
    setupVAO(0);
    setupVAO(1);

    glGenBuffers(2, (GLuint*)m_ibo);
    std::vector<GLuint> indices;
    indices.reserve(m_numBodies*m_numBodies);

    // Initialize (n^2)/2 line segments IBO for interaction computation
    for (unsigned int i = 0; i < m_numBodies; ++i)
    {
        for (unsigned j = i + 1; j < m_numBodies; ++j)
        {
            indices.push_back(i);
            indices.push_back(j);
        }
    }

    m_numElements[0] = indices.size();
    // Copy indices (lines)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo[0]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(GLuint),
                 &indices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    // Initialize IBO for pos update N points
    indices.clear();
    for (unsigned int i = 0; i < m_numBodies; ++i)
    {
        indices.push_back(i);
    }
    m_numElements[1] = indices.size();
    // Copy indices (points)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(GLuint),
                 &indices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    // Set RTT class with correct shaders (2-pass)
    //create renderers
    glm::ivec2 texSize = square_approximation(m_numBodies);
    cout << "texSize: (" << texSize.x << "," << texSize.y << ")" << endl;
    m_accelPass = new RTTPass(texSize.x, texSize.y, 1);
    m_posPass = new TFPass();

    //init all shader programs
    m_accelPass->SetupShaders("blend_accel_vertex.glsl", "", "",
                              "blend_accel_geom.glsl", "blend_accel_frag.glsl");
    const char *varyings[] = {"newPos","gl_NextBuffer","newVel"};
    m_posPass->SetupShaders("blend_pos_vertex.glsl", "", "", "", varyings,
                            ARRAY_SIZE_IN_ELEMENTS(varyings));

    // Initialize RTT outputs
    m_accelPass->SetupOutputs();

    // OneOneBS
    std::vector<GLenum> blendState(3);
    blendState[0] = GL_FUNC_ADD; blendState[1] = blendState[2] = GL_ONE;
    m_accelPass->SetupRenderOptions(false, false, false, true, false, true,
                                    blendState);

    // Setup the in-out texture exchange between passes
    m_posPass->addInputTex("accelTex", m_accelPass->OutTexs()[0]);

    // Setup fixed uniform parameters
    m_accelPass->addUniform1f("softening", 1);
    m_posPass->addUniform1f("damping", 1);
    m_posPass->addUniform2i("size", texSize);

    m_bInitialized = true;
}

template<typename T>
void BodySystemBlend<T>::_finalize()
{
    assert(m_bInitialized);

    glDeleteBuffers(2, (const GLuint *)m_ppbo);
    glDeleteBuffers(2, (const GLuint *)m_vpbo);
    glDeleteBuffers(2, (const GLuint *)m_vao);
    glDeleteBuffers(2, (const GLuint *)m_ibo);
    SAFE_DELETE(m_accelPass);
    SAFE_DELETE(m_posPass);
    SAFE_DELETE_ARRAY(m_hpos);
    SAFE_DELETE_ARRAY(m_hvel);

    m_bInitialized = false;
}

template <typename T>
void BodySystemBlend<T>::loadTipsyFile(const std::string &filename)
{
    if (m_bInitialized)
        _finalize();

    std::vector< typename vec4<T>::Type > positions;
    std::vector< typename vec4<T>::Type > velocities;
    std::vector< int > ids;

    int nBodies = 0;
    int nFirst=0, nSecond=0, nThird=0;

    read_tipsy_file(positions,
                    velocities,
                    ids,
                    filename,
                    nBodies,
                    nFirst,
                    nSecond,
                    nThird);

    _initialize(nBodies);

    setArray(BODYSYSTEM_POSITION, (T *)&positions[0]);
    setArray(BODYSYSTEM_VELOCITY, (T *)&velocities[0]);
}


template<typename T>
void BodySystemBlend<T>::setSoftening(T softening)
{
    T softeningSq = softening*softening;
    m_accelPass->addUniform1f("softeningSq", softeningSq);
}

template<typename T>
void BodySystemBlend<T>::setDamping(T damping)
{
    m_posPass->addUniform1f("damping", damping);
}

template<typename T>
void BodySystemBlend<T>::update(T deltaTime)
{
    assert(m_bInitialized);

    // Run GLSL code
    m_accelPass->addUniform1f("dt", deltaTime);
    m_accelPass->Render(m_vao[m_currentRead], m_ibo[0],
                        m_numElements[0], false, GL_LINES);
    m_posPass->addUniform1f("dt", deltaTime);
    m_posPass->clearOutputBOs();
    m_posPass->addOutputBO(m_ppbo[m_currentWrite]);
    m_posPass->addOutputBO(m_vpbo[m_currentWrite]);
    m_posPass->Render(m_vao[m_currentRead], m_ibo[1],
                      m_numElements[1], false, GL_POINTS);

    // Do we need a barrier here? Theoretically a sync (flush) will happen when
    // we try to access the PBO
    std::swap(m_currentRead, m_currentWrite);
}

template<typename T>
T *BodySystemBlend<T>::getArray(BodyArray array)
{
    assert(m_bInitialized);

    T *hdata = 0;

    switch (array)
    {
        default:
        case BODYSYSTEM_POSITION:
            readBOData(m_ppbo[m_currentRead], (void *)m_hpos,
                       4 * sizeof(T) * m_numBodies);
            hdata = m_hpos;
            break;

        case BODYSYSTEM_VELOCITY:
            readBOData(m_vpbo[m_currentRead], (void *)m_hvel,
                       4 * sizeof(T) * m_numBodies);
            hdata = m_hvel;
            break;
    }

    return hdata;
}

template<typename T>
void BodySystemBlend<T>::setArray(BodyArray array, const T *data)
{
    assert(m_bInitialized);

    m_currentRead = 0;
    m_currentWrite = 1;

    switch (array)
    {
        default:
        case BODYSYSTEM_POSITION:
        {
            updateBOData(m_ppbo[m_currentRead], 4 * sizeof(T) * m_numBodies, (void *)data);
            break;
        }
        case BODYSYSTEM_VELOCITY:
        {
            updateBOData(m_vpbo[m_currentRead], 4 * sizeof(T) * m_numBodies, (void *)data);
            break;
        }
    }
}
