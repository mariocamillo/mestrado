#ifndef _RTT_PASS_H_
#define _RTT_PASS_H_
#include <map>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#define HELPERGL_EXTERN_GL_FUNC_IMPLEMENTATION
#include "helper_gl.h"
#include "shaderLoader.h"

/**
 * Abstracts a Render-to-Texture draw pass in the GPU
 */
class RTTPass {
public:
    /**
     * Construct a RTTPass object that will render to a texture of size
     * width x height.
     *
     * @param width  Width of the output texture.
     * @param height Height of the output texture.
     * @param numOutTexs Number of output textures in this pass.
     */
    RTTPass(GLsizei width, GLsizei height, size_t numOutTexs);
    ~RTTPass();

    /**
     * Creates a shader program using the shader files passed as parameters.
     *
     * If any shader is passed as an empty string the shader program is created
     * without that shader step. Underneath it uses \sa ShaderLoader
     *
     * @param vs  Vertex shader path.
     * @param tcs Tesselation Control shader path.
     * @param tes Tesselation Evaluation shader path.
     * @param gs Geometry shader path.
     * @param fs Fragment shader path.
     *
     * @return Successfulness of the shader program creation.
     */
    bool SetupShaders(const std::string &vs, const std::string &tcs,
                      const std::string &tes, const std::string &gs,
                      const std::string &fs);

    /**
     * Set the OpenGL rendering options for this pass
     */
    bool SetupRenderOptions(bool cull, bool depth, bool stencil, bool wireframe,
                            bool alpha, bool blend,
                            const std::vector<GLenum> &blendState);

    /**
     * Creates the output textures
     *
     * @return True if successfull, false otherwise
     */
    bool SetupOutputs();

    /**
     * Renders the given vbo and ibo to texture with the shaders and options setup.
     *
     * @param vao The vertex buffer object to be rendered.
     * @param ibo The index buffer object to be rendered.
     * @param mvp_ubo UBO for the MVP matrix.
     * @param numElems Number of elements to be drawn in this call.
     * @param useBarrier Use glBarrier during render call
     * @param elemType Type of element being drawn. Default is triangles
     * @param clear True to clear the texture with zeroes before draw call
     *
     * @return Boolean indicating if the render was successful.
     */
    bool Render(GLuint vao, GLuint ibo, GLuint numElems,
                bool useBarrier, GLenum elemType = GL_TRIANGLES,
                const std::vector<std::pair<GLint, GLint> > &ssbos = std::vector<std::pair<GLint, GLint> >(),
                GLint drawStep = 0, GLint numInstances = 1,
                bool clear = true);

    /**
     * Reads the output texture back from the GPU.
     *
     * Allocates enough memory to read the texture back and read it using OpenGL.
     * The ownership of the allocated memory is passed to the caller.
     *
     * @return Float4 array with the texture data (row major).
     */
    void ReadTexBack(std::vector<glm::vec4 *> &outTexs);

    ShaderLoader &ShaderProgram() { return m_shaderProg; }

    const std::vector<GLuint> &OutTexs() { return m_outTexs; }

    void addInputTex(std::string location, GLuint tex)
    {
        m_inTexs[m_shaderProg.getUniformLocation(location.c_str())] = tex;
    }
    void addUniform1i(std::string location, GLint value)
    {
        m_uniform1is[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

    void addUniform2i(std::string location, glm::ivec2 value)
    {
        m_uniform2is[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

    void addUniform1f(std::string location, GLfloat value)
    {
        m_uniform1fs[m_shaderProg.getUniformLocation(location.c_str())] = value;
    }

    GLuint fbo() const { return m_fbo; }

    GLsizei width() const { return m_width; }

    GLsizei height() const { return m_height; }

private:
    GLsizei m_width;
    GLsizei m_height;
    GLuint m_fbo;
    std::vector<GLuint> m_outTexs;
    ShaderLoader m_shaderProg;
    bool m_cull;
    bool m_depth;
    bool m_stencil;
    bool m_wireframe;
    bool m_alpha;
    bool m_blend;
    std::vector<GLenum> m_blendState;
    std::map<GLint, GLuint> m_inTexs;
    std::map<GLint, GLint> m_uniform1is;
    std::map<GLint, GLfloat> m_uniform1fs;
    std::map<GLint, glm::ivec2> m_uniform2is;
};
#endif // _RTT_PASS_H_
