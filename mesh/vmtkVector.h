/*
 * vmtkVector.h
 *
 *  Created on: 06/01/2011
 *      Author: ra100300
 */

#ifndef VMTKVECTOR_H_
#define VMTKVECTOR_H_

#include <stdio.h>
#include <cmath>


typedef struct _Vector {
	float x;
	float y;
	float z;
} Vector3;

Vector3 Vector_NewVector(float x, float y, float z);
Vector3 Vector_normalized(Vector3 vector);
float Vector_magnitude(Vector3 vector);
//float Vector_magnitudeSquared(Vector vector);
Vector3 Vector_add(Vector3 vector1, Vector3 vector2);
Vector3 Vector_sub(Vector3 vector1, Vector3 vector2);
float Vector_dot(Vector3 vector1, Vector3 vector2);
Vector3 Vector_cross(Vector3 vector1, Vector3 vector2);
void Vector_print(char* msg, Vector3 v);
float Vector_distance2points(Vector3 vec1, Vector3 vec2) ;
Vector3 Vector_scalarmult(float scalar, Vector3 vec);
Vector3 Vector_mult(Vector3 vec1, Vector3 vec2) ;
bool Vector_equal(Vector3 vec1, Vector3 vec2);
void Vector_normalize(Vector3 * vector);
float Vector_angle(Vector3 vec1, Vector3 vec2);

#endif /* VMTKVECTOR_H_ */
