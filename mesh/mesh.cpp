/*
 *  mesh.cpp
 *
 *  Basic methods of a half-edge mesh class
 *
 *  author: Wu Shin - Ting (25/05/09; 09/07/09; 5/5/2011)
 *          DCA - FEEC - Unicamp
 */

#include "mesh.h"
#include<fstream>
#include <cmath>
#include <climits>

ofstream file("pares.txt");

#define PRINT(x) x
#define OR ||
#define AND &&

// =======================================================================
// CONSTRUCTORS & DESTRUCTORS
// =======================================================================


Mesh::Mesh() {
  typedef pair <int,Loop*> Pair;

  mesh_exist = 0;
  face_paint = -1;
  _outer_loop = new Loop;
  _outer_loop->ID = (long)(_outer_loop); _outer_loop->half = NULL; _outer_loop->type = 2; 
  _outer_loop->GeomPtr = NULL;
  loops.insert(Pair((long)(_outer_loop),_outer_loop));
  _nCycles = 1;
}

Mesh::~Mesh() {
  removeMesh();
}
void Mesh::clear(){
	 HalfEdge *he;
	//Clear halfedges
	for (eIterator = edges.begin(); eIterator != edges.end(); eIterator++) {
		he = (eIterator->second)->half;
		delete he->mate;
		delete he;
	}

	vertices.clear();
	edges.clear();
	loops.clear();
}

void Mesh::load(char *input_file) {

  FILE *objfile = fopen(input_file,"r");
  if (objfile == NULL) {
    printf ("ERROR! CANNOT OPEN '%s'\n",input_file);
    return;
  }


  char line[200];
  char token[100];
  char atoken[100];
  char btoken[100];
  char ctoken[100];
  char dtoken[100];
  char etoken[100];
  float x,y,z;
  int a,b,c,d,e;

  int index = 0;
  int vert_count = 0;
  int vert_index = 1;

  xmax = ymax = zmax = 0;
  xmin = ymin = zmin = 100000000;

  while (fgets(line, 200, objfile)) {

    if (line[strlen(line)-2] == '\\') {
      fgets(token, 100, objfile);
      int tmp = strlen(line)-2;
      strncpy(&line[tmp],token,100);
    }
    int token_count = sscanf (line, "%s\n",token);
    if (token_count == -1) continue;
    a = b = c = d = e = -1;
    if (!strcmp(token,"usemtl") ||
	!strcmp(token,"g")) {
      vert_index = 1; //vert_count + 1;
      index++;
    } else if (!strcmp(token,"v")) {
      vert_count++;
      sscanf (line, "%s %f %f %f\n",token,&x,&y,&z);
      Point3D* node = new Point3D;
      node->x = x; node->y = y; node->z = z;
      addVertex(vert_count,node);

      // Update bounding frame
      if (x < xmin) xmin = x;
      if (x > xmax) xmax = x;
      if (y < ymin) ymin = y;
      if (y > ymax) ymax = y;
      if (z < zmin) zmin = z;
      if (z > zmax) zmax = z;
    } else if (!strcmp(token,"f")) {
      int num = sscanf (line, "%s %s %s %s %s %s\n",token,
			atoken,btoken,ctoken,dtoken,etoken);
      sscanf (atoken,"%d",&a);
      sscanf (btoken,"%d",&b);
      sscanf (ctoken,"%d",&c);

      FaceID fid = addTriangle_(-1,a,b,c,NULL);

    } else if (!strcmp(token,"vt")) {
    } else if (!strcmp(token,"vn")) {
    } else if (token[0] == '#') {
    } else {
      printf ("LINE: '%s'",line);
    }
  }
}
// =======================================================================
// MODIFIERS:   ADD & REMOVE
// =======================================================================
VertexID Mesh::addVertex(const VertexID ID, void *ptr) {
  typedef pair <int,Vertex*> Pair;


  if (ID != -1) {
    // Decide what to do if ID exists
    if (vertices.find(ID) != vertices.end( ) )
       return -1;
  }

  Vertex *v = new Vertex;
  v->half = 0;
  for (int ii = 0; ii < 16; ii++)v->q.m[ii] = 0;
  v->ID = (ID == -1)?(long)v:ID; v->half = NULL;
  v->GeomPtr = ptr; 
  vertices.insert(Pair(v->ID,v));


  return v->ID;
}
void Mesh::flipEdge (HalfEdge *half) {

	HalfEdge *half_a;

	//side 1
	(half)->next->previous = (half)->mate->previous;
	(half)->next->next = (half);
	(half)->mate->previous->next = (half)->next;
	(half)->mate->previous->previous = (half);
	(half)->next->left = (half)->left;
	(half)->mate->previous->left = (half)->left;
	(half)->left->half = half;
	//side 2
	(half)->mate->next->previous = (half)->previous;
	(half)->previous->next = (half)->mate->next;
	(half)->mate->next->next = (half)->mate;
	(half)->previous->previous = (half)->mate;
	(half)->previous->left = (half)->mate->left;
	(half)->previous->next->left = (half)->mate->left;
	(half)->mate->left->half = (half)->mate;

	//halfedges
	half_a = (half)->previous;
	(half)->next = (half)->mate->previous;
	(half)->previous = (half)->next->next;
	(half)->origin = (half)->previous->mate->origin;

	(half)->mate->next = half_a;
	(half)->mate->previous = half->mate->next->next;
	(half)->mate->origin = (half)->mate->previous->mate->origin;
}

HalfEdge* Mesh::addHalfEdge_(const VertexID a, const VertexID b) {

	Edge* edge;
	Vertex *v1, *v2;
	HalfEdge *he1pt, *he2pt;
	typedef pair <int,Edge*> Pair;

    vIterator = vertices.find(a);
    if (vIterator != vertices.end())
      v1 = vIterator->second;
    else {
      cout << "Add vertex "<< a << " before." << endl;
      return(0);
    }
    vIterator = vertices.find(b);
    if (vIterator != vertices.end())
      v2 = vIterator->second;
    else {
      cout << "Add vertex "<< b << "before." << endl;
      return(0);
    }


	if (vertices.find(b)->second->half){
		he1pt = findHalf(b, a);
		if (he1pt){

			//cout << "encontrou!" << endl;
			return he1pt;
		}
	}
//	cout << "nao encontrou!" << endl;

    //Add edge AND its halfedges
    he1pt = new HalfEdge;
    he1pt->next = he1pt->previous = he1pt->mate = NULL;
    he1pt->origin = NULL;
    he1pt->edge = NULL;
    he1pt->left = NULL;
    he2pt = new HalfEdge;
    he2pt->next = he2pt->previous = he2pt->mate = NULL;
    he2pt->origin = NULL;
    he2pt->edge = NULL;
    he2pt->left = NULL;

    edge = new Edge;
    edge->ID = (long)edge; edge->half = he1pt; edge->GeomPtr = NULL;
    edges.insert(Pair(edge->ID,edge));

    he1pt->edge = he2pt->edge = edge;
    he1pt->mate = he2pt;
    he2pt->mate = he1pt;

    //Add vertex AND loop entries
    he1pt->origin = v1;
    he1pt->left = _outer_loop;
    he1pt->previous = he2pt;
    he2pt->next = he1pt;

    he2pt->origin = v2;
    he2pt->left = _outer_loop;
    he2pt->previous = he1pt;
    he1pt->next = he2pt;

    return he1pt;
}

HalfEdge* Mesh::addHalfEdge(const VertexID a, const VertexID b, Loop *outer) {
  Edge* edge;
  Vertex *v1, *v2;
  HalfEdge *he1pt, *he2pt;
  typedef pair <int,Edge*> Pair;

  he1pt = getHalfEdge(a,b);

 /* cout << "\naddhalfedge\n";
  if (!he1pt)cout << "nulo\n";
  else cout << he1pt->origin->ID << " Type:" << he1pt->left->type<< endl;*/

  cout << endl;

  if (he1pt && he1pt->left->type == 1) {
    // Only 2D manifold is allowed
    cout << "Halfedge (" << a << "," << b <<") is already occupied." << endl;
    return 0;
  }

  if (!he1pt) {
    //Get vertices 
	 // cout << "adicona half\n";
    vIterator = vertices.find(a);
    if (vIterator != vertices.end()) 
      v1 = vIterator->second;
    else {
      cout << "Add vertex "<< a << " before." << endl;
      return(0);
    }
    vIterator = vertices.find(b);
    if (vIterator != vertices.end()) 
      v2 = vIterator->second;
    else {
      cout << "Add vertex "<< b << "before." << endl;
      return(0);
    }
    //Add edge AND its halfedges
    he1pt = new HalfEdge;
    he1pt->next = he1pt->previous = he1pt->mate = NULL;
    he1pt->origin = NULL;
    he1pt->edge = NULL;
    he1pt->left = NULL;
    he2pt = new HalfEdge;
    he2pt->next = he2pt->previous = he2pt->mate = NULL;
    he2pt->origin = NULL;
    he2pt->edge = NULL;
    he2pt->left = NULL;
 
    edge = new Edge;
    edge->ID = (long)edge; edge->half = he1pt; edge->GeomPtr = NULL;
    edges.insert(Pair(edge->ID,edge));

    he1pt->edge = he2pt->edge = edge;
    he1pt->mate = he2pt;
    he2pt->mate = he1pt;

    //Add vertex AND loop entries
    he1pt->origin = v1;
    he1pt->left = outer;
    he1pt->previous = he2pt;
    he2pt->next = he1pt;

    he2pt->origin = v2;
    he2pt->left = outer;
    he2pt->previous = he1pt;
    he1pt->next = he2pt;

    //if(he1pt->origin->half)cout << "Nao eh nulo, id: " << a << endl;
    //else cout << "Eh nulo, id: " << a << endl;
  } 

  return (he1pt);
}
void Mesh::adjust_half(HalfEdge *h){

	if (h->mate != h->mate->previous->next){
		h->mate->previous->next = h->mate;
	}
	if (h->mate != h->mate->next->previous){
		h->mate->next->previous = h->mate;
	}
}
FaceID Mesh::addTriangle_(LoopID ID, const VertexID a, const VertexID b, const VertexID c, void *ptr) {

	HalfEdge *half_edge_ab, *half_edge_bc, *half_edge_ca, *h1, *h2, *h3, *h;
	vector<pair<HalfEdge **, HalfEdge *> > connections;

	int va, vb, vc, v;

	if (ID != -1) {
		// Decide what to do if ID exists
		if (loops.find(ID) != loops.end( ) )
			return -1;
	}

	//verificar se o vertice ja foi inserido
	half_edge_ab = half_edge_bc = half_edge_ca = 0;

	//face sentido anti-horario(interno)
	half_edge_ab = addHalfEdge_(a, b);
	half_edge_bc = addHalfEdge_(b, c);
	half_edge_ca = addHalfEdge_(c, a);

//	if(triangle_inserted(half_edge_ab, half_edge_bc, half_edge_ca, a, b, c)){
//
//		return -1;
//	}

	if(!triangle_ok(half_edge_ab, half_edge_bc, half_edge_ca, a, b) OR
	   !triangle_ok(half_edge_bc, half_edge_ca, half_edge_ab, b, c) OR
	   !triangle_ok(half_edge_ca, half_edge_ab, half_edge_bc, c, a)){
		return -1;
	}

	//verificar se ja foi inserida
	outer_halfedge(half_edge_ab, half_edge_bc, half_edge_ca, a, b, &connections);
	outer_halfedge(half_edge_bc, half_edge_ca, half_edge_ab, b, c, &connections);
	outer_halfedge(half_edge_ca, half_edge_ab, half_edge_bc, c, a, &connections);
	inner_halfedge(half_edge_ab, half_edge_bc, half_edge_ca, &connections);
	vertice_half(a, b, c, half_edge_ab, half_edge_bc, half_edge_ca);

	for (int i = 0; i < connections.size(); i++){
		(*(connections[i].first)) = ((connections[i].second));
	}

	loop_half(ID, half_edge_ab, half_edge_bc, half_edge_ca, ptr);

	return ID;
}

bool Mesh::triangle_inserted(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b, VertexID c){

	if (hab->next != hab->mate AND
			hbc->next != hbc->mate AND
			hca->next != hca->mate){


		if (hab->left->type == 1 AND hab->mate->left->type == 1){
			if (hab->next != hbc){
				hab = hab->mate;
			}
		}
		else{
			if (hab->left->type == 2){
				hab = hab->mate;
			}
		}

		if (hbc->left->type == 1 AND hbc->mate->left->type == 1){
			if (hbc->next != hca){
				hbc = hbc->mate;
			}
		}
		else{
			if (hbc->left->type == 2){
				hbc = hbc->mate;
			}
		}

		if (hca->left->type == 1 AND hca->mate->left->type == 1){
			if (hca->next != hab){
				hca = hca->mate;
			}
		}
		else{
			if (hca->left->type == 2){
				hca = hca->mate;
			}
		}

		if ((hab->left->type == 1 AND hbc->left->type == 1 AND hca->left->type == 1)AND
			 (hab->left == hbc->left AND  hbc->left == hca->left)){
			return true;
		}

	}
	return false;
}

bool Mesh::triangle_ok(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b){
	//tentando colocar aresta sobre face
	int flag_a, flag_b;

	if (hab->next != hab->mate AND
		hbc->next != hbc->mate AND
		hca->next == hca->mate){

		flag_a = 0;
		if (hab->origin->ID == a){
			if (hab->left->type == 1){
				flag_a = 1;
			}
		}
		else{
			if (hab->mate->left->type == 1){
				flag_a = 1;
			}
		}

		flag_b = 0;
		if (hbc->origin->ID == b){
			if (hbc->left->type == 1){
				flag_b = 1;
			}
		}
		else{
			if (hbc->mate->left->type == 1){
				flag_b = 1;
			}
		}

		if (flag_a OR flag_b) return false;
		else return true;
	}
	return true;
}

void Mesh::outer_halfedge(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b, vector<pair<HalfEdge **, HalfEdge *> > *c){

	Vertex *va, *vb;
	HalfEdge *h, *h1, *h2;
	int flag;

	va = vertices.find(a)->second;
	vb = vertices.find(b)->second;

	if (hab->mate != hab->next){

		if(hab->left->type == 1)hab = hab->mate;
		if (hbc->mate == hbc->next)return;
		if(hbc->left->type == 1)hbc = hbc->mate;

		if((hab->previous->left->type == 2 AND hbc->next->left->type == 2 AND hca->mate == hca->next) OR (hbc->mate != hbc->next AND hca->mate != hca->next)){
			if (hab->next != hbc){
				//return;
				h1 = hbc->mate;
				while(h1->left->type != 2){
					h1 = h1->next->mate;
				}

				c->push_back(make_pair(&h1->next, hab->next));
				c->push_back(make_pair(&((hab->next)->previous), h1));
				c->push_back(make_pair(&((hbc->previous)->next), h1->next));
				c->push_back(make_pair(&((h1->next)->previous), (hbc->previous)));
			}
		}

		return;
	}

	//conectar dois vertices
	if (!va->half){
		c->push_back(make_pair(&((hab->mate)->next), (hca->mate)));
	}
	else{
		if (hca->next != hca->mate){
			if (hca->left->type == 2){
				h = hca->next;
			}
			else{
				h = hca->mate->next;
			}
		}
		else{
			h = outHalf(va, _outer_loop, flag);
		}
		c->push_back(make_pair(&((hab->mate)->next), h));
		c->push_back(make_pair(&h->previous, (hab->mate)));
	}

	if (!vb->half){
		c->push_back(make_pair(&((hab->mate)->previous), hbc->mate));
	}
	else{

		if (hbc->next != hbc->mate){
			if (hbc->left->type == 2){
				h = hbc;
			}
			else h = hbc->mate;
		}
		else{

			h = outHalf(vb, _outer_loop, flag);
		}

		c->push_back(make_pair(&((hab->mate)->previous), h->previous));
		c->push_back(make_pair(&((h->previous)->next), hab->mate));
	}
}
HalfEdge *Mesh::articulate_vertex(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca){
    return NULL;
}
bool Mesh::check_triangle(FaceID id){

	HalfEdge *hf, *iter;

	hf = (loops.find(id)->second)->half;

	iter = hf;
	cout << "Face" << id << endl;
	do{
		cout << iter->origin->ID <<" ";
		iter = iter->next;
	}while(iter != hf);

	return true;
}
void Mesh::adjust_inner_half(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca){

	cout << "adjust_inner_half" << endl;
	HalfEdge *h, *hp, *hn;
	if (hab->mate != hab->next AND hbc->mate != hbc->next){
		if (hab->previous->left->type == 2 AND hbc->next->left->type == 2 AND hca->mate->next == hbc->next AND hca->mate->previous == hab->previous){
			cout << "ajusta" << endl;
			h = hbc->origin->half;
			hp = hbc->previous;
			hn = hab->next;

			do{
				if (h != hbc AND h->left->type != 1 AND h!= hn AND  avoid_loop(h, hab, hbc)){
					break;
				}
				h = h->mate->next;
			}while(h != hbc->origin->half);

			cout << "h escolhido:" << h->origin->ID << " mate:" << h->mate->origin->ID << endl;
			cout << "h next:" << h->next->origin->ID << endl;
			cout << "h next next:" << h->next->next->origin->ID << endl;
			cout << "h next next next:" << h->next->next->next->origin->ID << endl;

			if (hab->next != hbc){
				hn->previous = h->previous;
				h->previous->next = hn;
			}
			else cout << "nao muda ab" << endl;

			if (hbc->previous != hab){
				hp->next = h;
				h->previous = hp;
			}
			else cout << "nao muda bc" << endl;
		}
	}
}

bool Mesh::avoid_loop(HalfEdge *h, HalfEdge *hab, HalfEdge *hbc){

	int flag;
	HalfEdge *h_sv[4], *hn, *hp, *it;

	hp = hbc->previous;
	hn = hab->next;



	cout << "avoid" << endl;
	cout << "endereco antes hn->previous:" << hn->previous << endl;
	cout << "endereco antes h->previous->next:" << h->previous->next << endl;
	cout << "endereco antes hp->next:" << hp->next << endl;
	cout << "endereco antes h->previous:" << h->previous << endl;

	h_sv[0] = hn->previous;
	h_sv[1] = h->previous->next;
	flag = 1;
	if (hab->next != hbc){


		cout << "salva #1:" << h_sv[0] << endl;
		cout << "salva #2:" << h_sv[1] << endl;

		hn->previous = h->previous;
		h->previous->next = hn;

		flag = 0;
		it = h->previous;
		do{
			if (it->origin->ID == h->origin->ID){
				flag = 1;
			}
			it = it->next;
		}while(it != h->previous AND !flag);

	}

	if (!flag){
		 hn->previous = h_sv[0];
		 h->previous->next = h_sv[1];

		 cout << "endereco depois hn->previous:" << hn->previous << endl;
		 cout << "endereco depois h->previous->next:" << h->previous->next << endl;
		 return false;
	}

	flag = 1;
	h_sv[2] = hp->next;
	h_sv[3] = h->previous;
	if (hbc->previous != hab){

		cout << "salva #3:" << h_sv[2] << endl;
		cout << "salva #4:" << h_sv[3] << endl;

		hp->next = h;
		h->previous = hp;

		flag = 0;
		it = h->previous;
		do{
			if (it->origin->ID == h->origin->ID){
				flag = 1;
			}
			it = it->next;
		}while(it != h->previous AND !flag);
	}

	hn->previous = h_sv[0];
	h->previous->next = h_sv[1];
	hp->next = h_sv[2];
	h->previous = h_sv[3];

	cout << endl;
	cout << "endereco depois hn->previous:" << hn->previous << endl;
	cout << "endereco depois h->previous->next:" << h->previous->next << endl;
	cout << "endereco depois hp->next:" << hp->next << endl;
	cout << "endereco depois h->previous:" << h->previous << endl;

	if (flag){
		return true;
	}
	else return false;
}

void Mesh::inner_halfedge(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, vector<pair<HalfEdge **, HalfEdge *> > *c){


	if (hab->left AND hab->left->type == 1){
		hab = hab->mate;
	}

	if (hbc->left AND hbc->left->type == 1){
		hbc = hbc->mate;
	}

	if (hca->left AND hca->left->type == 1){
		hca = hca->mate;
	}

	//halfedge dentro da face

	c->push_back(make_pair(&hab->next, hbc));
	c->push_back(make_pair(&hab->previous, hca));

	c->push_back(make_pair(&hbc->next, hca));
	c->push_back(make_pair(&hbc->previous, hab));

	c->push_back(make_pair(&hca->next, hab));
	c->push_back(make_pair(&hca->previous, hbc));
}

void Mesh::half_out_from_vertice(VertexID a, vector<pair<HalfEdge *, HalfEdge *> >* list){

	Vertex *v;
	HalfEdge *it_h;

	v = vertices.find(a)->second;
	if (!v->half)return;

	it_h = v->half;

	do{
		if (it_h->previous->left->type != 1){
			(*list).push_back(make_pair(it_h->previous, it_h));
		}
		it_h = it_h->mate->next;
	}while(it_h != v->half);
}

void Mesh::vertice_half(VertexID a, VertexID b, VertexID c, HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca){

	Vertex *v;

	v = vertices.find(a)->second;
	if (!v->half) v->half = hab;

	v = vertices.find(b)->second;
	if (!v->half) v->half = hbc;

	v = vertices.find(c)->second;
	if (!v->half) v->half = hca;
}

void Mesh::loop_half(LoopID &ID,  HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, void *ptr){

	Loop *face;
	HalfEdge *it_h, *h;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	typedef pair <int,Loop*> Pair;


	//cout << "triangulo" << endl;
	//cout << hab->mate->origin->ID << endl;
	//cout << hab->mate->previous->origin->ID << endl;
	//cout << hab->next->origin->ID << endl;
	//cout << hab->previous->mate->next->next->next->next->next->origin->ID << endl;

	if (hab->left AND hab->left->type == 1){
		hab = hab->mate;
	}

	if (hbc->left AND hbc->left->type == 1){
		hbc = hbc->mate;
	}

	if (hca->left AND hca->left->type == 1){
		hca = hca->mate;
	}

	//loop inner
	face = new Loop;
	//face -> half
	face->ID = (ID == -1)?(long)face:ID; face->half = hab; face->type = 1; face->GeomPtr = ptr;
	ID = face->ID;
	//half->face
	hab->left = hbc->left = hca->left = face;
	loops.insert(Pair(face->ID,face));

//	//loop externo (outer_loop)
//	cout << "loop externo" << endl;
//	 getAllVertices (&vvlist);
//	if (!_outer_loop->half){
//		cout << "loop externo nulo" << endl;
//		 for (vvIter = vvlist.begin();!_outer_loop->half AND vvIter != vvlist.end(); vvIter++) {
//			 it_h = vertices.find(*vvIter)->second->half;
//			 cout << "Vertice:" << *vvIter << endl;
//			 cout << "passou!" << endl;
//			 if (it_h){
//				 h = it_h;
//				 do{
//					 cout << "do" << endl;
//					 if (it_h->left->type == 2){
//						 cout << "encontrou" << endl;
//						 _outer_loop->half = it_h;
//						 break;
//					 }
//					 it_h = it_h->mate->next;
//				 }while(it_h != h);
//			 }
//		 }
//	}
//	cout << "sai do" << endl;
//
//	if (_outer_loop->half->left->type != 2){
//		cout << "atualizar loop externo" << endl;
//		for (vvIter = vvlist.begin();_outer_loop->half->left->type != 2 AND vvIter != vvlist.end(); vvIter++) {
//			it_h = vertices.find(*vvIter)->second->half;
//			h = it_h;
//			if (it_h){
//				do{
//					if (it_h->left->type == 2){
//						_outer_loop->half = it_h;
//						break;
//					}
//					it_h = it_h->mate->next;
//				}while(it_h != h);
//			}
//		}
//	}
//	cout << "outer_loop:" << _outer_loop->half->origin->ID << endl;
//	cout << "outer_loop:" << _outer_loop->half->mate->origin->ID << endl;
//	cout << "sai loop" << endl;
}

HalfEdge *Mesh::outHalf(Vertex *v, Loop *f, int &flag){

	HalfEdge *it_h, *h;

	int n_out = 0;
	flag = 0;
	//vertice(x) articulado
	/*
	 * 0    0
	 * | \ /|
	 * |  x |
	 * | / \|
	 * 0    0
	 */

	h = 0;
	if(v->half){
		it_h = v->half;
		do{
			//cout << "it_h:" << it_h->origin->ID << endl;
			//cout << "it_h mate:" << it_h->mate->origin->ID << endl;
			if (it_h->left == f){
				h = it_h;
				//cout << "entrou" << endl;
				n_out++;
			}

			it_h = it_h->mate->next;
		}while(it_h != v->half);
	}

	if(n_out > 1){
		flag = 1;
	}

//	cout << "outHalf" << endl;
//	cout << h->origin->ID << endl;
//	cout << h->mate->origin->ID << endl;
	return h;
}

FaceID Mesh::addTriangle(const LoopID ID, const VertexID a, const VertexID b, const VertexID c, void *ptr) {

	/*Input: ID loop, AND ID of the vertices a, b, c
	 * Add the triangle with vertices: a, b, c. The ID loop,
	 * will be the ID of the face. The direction of internal loop
	 * is: a->b->c.
	 * Output: ID face*/

	Loop *face, *outer;
	HalfEdge *half_edge_ab, *half_edge_bc, *half_edge_ca, *half_edge, *half_edge1, *half_edge2, *half_edge3;
	vector<VertexID> list;
	typedef pair <int,Loop*> Pair;

	if (ID != -1) {
		// Decide what to do if ID exists
		if (loops.find(ID) != loops.end( ) )
			return -1;
	}

	outer = getOuterLoop(a,b,c);

	if (!outer) outer = _outer_loop;

	HalfEdge *half_cpy;
	half_cpy = outer->half;

	HalfEdge *outer_half;

	half_edge_ab = addHalfEdge(a,b,outer);
	half_edge_bc = addHalfEdge(b,c,outer);
	half_edge_ca = addHalfEdge(c,a,outer);

	if (!half_edge_ab || !half_edge_bc || !half_edge_ca) return 0;

	if (half_edge_ab->next == half_edge_bc && half_edge_bc->next == half_edge_ca
			&& half_edge_ca->next == half_edge_ab && half_edge_ab->left != _outer_loop) {
		// (ab,bc,ca) build a closed loop different from _outer_loop. It only changes status
		if (half_edge_ab->left->GeomPtr) delete (char *)(half_edge_ab->left->GeomPtr);
		if (ID != -1) {
			remove_loop(half_edge_ab->left);
			face = new Loop;
			face->ID = (ID == -1)?(long)face:ID; face->half = half_edge_ab;
			loops.insert(Pair(face->ID,face));
		} else {
			face = half_edge_ab->left;
		}
		face->type = 1;
		face->GeomPtr = ptr;
		half_edge_ab->left = half_edge_bc->left = half_edge_ca->left = face;
		return face->ID;
	}

	if (!half_edge_ab || !half_edge_bc || !half_edge_ca) return 0;

	half_edge1 = half_edge2 = half_edge3 = NULL;

	obtain_pointer(&half_edge1, &half_edge2, &half_edge3, half_edge_ab, half_edge_bc,half_edge_ca, outer);
	if (!half_edge1 || !half_edge2 || !half_edge3) return 0;

	if (!(half_edge_ab->origin)->half) (half_edge_ab->origin)->half = half_edge_ab;
	if (!(half_edge_bc->origin)->half) (half_edge_bc->origin)->half = half_edge_bc;
	if (!(half_edge_ca->origin)->half) (half_edge_ca->origin)->half = half_edge_ca;

	face = new Loop;
	face->ID = (ID == -1)?(long)face:ID; face->half = half_edge_ab; face->type = 1; face->GeomPtr = ptr;
	loops.insert(Pair(face->ID,face));

	// Buid edge-edge relations
	buid_edge_relations(half_edge_ab, half_edge_bc, half_edge_ca, half_edge1, half_edge2, half_edge3);
	half_edge_ab->left = half_edge_bc->left = half_edge_ca->left = face;
	//Buid face-edge relations
	buid_face_relations(half_edge_ab, half_edge_bc, half_edge_ca, outer, face);

	return face->ID;
}

void Mesh::remove_vertex (Vertex* v) {
  if (v->GeomPtr) delete (char *)(v->GeomPtr);
  vertices.erase(v->ID);
  delete v;
}

void Mesh::remove_edge (Edge *edge) {
   if (edge->GeomPtr) delete (char *)(edge->GeomPtr);
   edges.erase((edge)->ID);
   delete (edge);
}

void Mesh::remove_loop (Loop *f) {
   lIterator = loops.find(f->ID);
   if (lIterator != loops.end() && f != _outer_loop) {
     if (f->GeomPtr) delete (char *)(f->GeomPtr);
       loops.erase(f->ID);
       delete (f);
   }
}

VertexID Mesh::replace_vertex (Vertex *old_v, Vertex **new_v, void *ptr,
		const VertexID vID) {
	VertexID index;
	typedef pair <int,Vertex*> Pair;
	HalfEdge *he, *mhe, *che;

	// Update the new point entry
	if (old_v->GeomPtr) delete (char *)(old_v->GeomPtr);//ele deleta aqui, porem se o vertice ja estiver sendo utilizado ele retorna -1. mas ele jah deletou!
	old_v->GeomPtr = NULL;

	if (vID == -1) {
		// ID starts with 0
		index = old_v->ID;
		old_v->GeomPtr = ptr;
		*new_v = old_v;
	} else {
		// Decide what to do if ID exists
		if (vertices.find(vID) == vertices.end( ) ) {
			index = vID;
			*new_v = new Vertex;
			(*new_v)->ID = index; (*new_v)->half = old_v->half;
			(*new_v)->GeomPtr = ptr;
			vertices.insert(Pair(index,*new_v));
			// Update vertex pointers
			he = (old_v)->half;
			he->origin = *new_v;
			che = he->mate;
			if (che) che = che->next;
			while (che && che != he) {
				if (che->origin == (old_v)) che->origin = *new_v;
				che = che->mate;
				che = che->next;
			}
		} else {
			cout << "Vertex "<< vID << " is already used." << endl;
			return -1;
		}
	}
	return index;
}

void Mesh::ReduceTtoP_caseIII (Loop *f, Loop *f1, Loop *f2, Loop *f3,
	HalfEdge *e10, HalfEdge *e20, HalfEdge *e30, Vertex *va, Vertex *vb,
	Vertex *vc, Vertex **new_v)
{
  Loop * outer;
  Vertex *vv;

  if (f1 && f1 == f2) {
    if (f == f1) {
      *new_v = e30->mate->previous->origin;
      if (e30->mate->next->next != e30->mate->previous) {
	(*new_v)->half = e30->mate->next->next;
	remove_loop(f3);
      } else {
	if (f3 != _outer_loop) {
	  remove_loop(f3);
	  remove_vertex (*new_v);
	  *new_v = NULL;
	} else {
	  f3->half = NULL;
	  (*new_v)->half = NULL;
	}
      }
      remove_vertex(va);
      remove_vertex(vb);
      remove_edge(e10->edge);
      remove_edge(e30->edge);
      remove_edge(e30->mate->previous->edge);
      delete (e30->mate->previous);
      delete (e30->mate->next);
      delete (e30->mate);
    } else {
      // f AND f1 reduced to an edge
      va->half = e30->mate;
      vc->half = e10->mate->next->mate;
      *new_v = vc;
      remove_vertex (vb);
      remove_loop (f1);
      e30->mate->mate = e10->mate->next->mate;
      e10->mate->next->mate->mate = e30->mate;
      e30->mate->edge = e10->mate->next->mate->edge;
      //     e30->mate->next->mate->edge->half = e10->mate->next->mate;
      e30->mate->edge->half = e10->mate->next->mate;
      delete (e10->mate->next);
      delete(e10->mate);
      delete(e20->mate);
      if (va->half->left->type == 2 &&
	  (va->half->previous == va->half->mate ||
	   va->half->next == va->half->mate)) {
	outer = va->half->left;
	if (va->half->previous == va->half->mate &&
	    va->half->next == va->half->mate) {
	  if (outer != _outer_loop) {
	    remove_loop(outer);
	  } else {
	    _outer_loop->half = NULL;
	  }
	  vv = va;
	  (*new_v)->half = NULL;
	} else if (va->half->previous == va->half->mate) {
	  vv = va;
	  vc->half->previous->next = vc->half->mate->next;
	  vc->half->mate->next->previous = vc->half->previous;
	  (*new_v)->half = vc->half->mate->next;
	} else {
	  vv = vc;
	  *new_v = va;
	  va->half->previous->next = va->half->mate->next;
	  va->half->mate->next->previous = va->half->previous;
	  (*new_v)->half = va->half->mate->next;
	}
	remove_edge (vv->half->edge);
	delete (vv->half->mate);
	delete (vv->half);
	remove_vertex (vv);
      }
      remove_edge(e10->edge);
      remove_edge(e20->edge);
      remove_edge(e30->edge);
    }
  } else if (f2 && f2 == f3) {
    if (f == f2) {
      *new_v = e10->mate->previous->origin;
      if (e10->mate->next->next != e10->mate->previous) {
	(*new_v)->half = e10->mate->next->next;
	remove_loop(f1);
      } else {
	if (f1 != _outer_loop) {
	  remove_loop(f1);
	  remove_vertex (*new_v);
	  *new_v = NULL;
	} else {
	  f1->half = NULL;
	  (*new_v)->half = NULL;
	}
      }
      remove_vertex(va);
      remove_vertex(vc);
      remove_edge(e10->edge);
      remove_edge(e20->edge);
      remove_edge(e10->mate->previous->edge);
      delete (e10->mate->previous);
      delete (e10->mate->next);
      delete (e10->mate);
    } else {
      // f AND f2 reduced to an edge
      vb->half = e10->mate;
      va->half = e20->mate->next->mate;
      *new_v = va;
      remove_vertex (vc);
      remove_loop (f2);
      e10->mate->mate = e20->mate->next->mate;
      e20->mate->next->mate->mate = e10->mate;
      e10->mate->edge = e20->mate->next->mate->edge;
      //e20->mate->next->mate->edge->half = e20->mate->next->mate;
      e10->mate->edge->half = e20->mate->next->mate;
      delete (e20->mate->next);
      delete(e20->mate);
      delete(e30->mate);
      if (vb->half->left->type == 2 &&
	  (vb->half->previous == vb->half->mate ||
	   vb->half->next == vb->half->mate)) {
	outer = vb->half->left;
	if (vb->half->previous == vb->half->mate &&
	    vb->half->next == vb->half->mate) {
	  if (outer != _outer_loop) {
	    remove_loop(outer);
	  } else {
	    _outer_loop->half = NULL;
	  }
	  vv = vb;
	  (*new_v)->half = NULL;
	} else if (vb->half->previous == vb->half->mate) {
	  vv = vb;
	  va->half->previous->next = va->half->mate->next;
	  va->half->mate->next->previous = va->half->previous;
	  (*new_v)->half = va->half->mate->next;
	} else {
	  vv = va;
	  *new_v = vb;
	  vb->half->previous->next = vb->half->mate->next;
	  vb->half->mate->next->previous = vb->half->previous;
	  (*new_v)->half = vb->half->mate->next;
	}
	remove_edge (vv->half->edge);
	delete (vv->half->mate);
	delete (vv->half);
	remove_vertex (vv);
      }
      remove_edge(e10->edge);
      remove_edge(e20->edge);
      remove_edge(e30->edge);
    }
  } else if (f3 && f3 == f1) {
    if (f == f3) {
      *new_v = e20->mate->previous->origin;
      if (e20->mate->next->next != e20->mate->previous) {
	(*new_v)->half = e20->mate->next->next;
	remove_loop(f2);
      } else {
	if (f2 != _outer_loop) {
	  remove_loop(f2);
	  remove_vertex (*new_v);
	  *new_v = NULL;
	} else {
	  f2->half = NULL;
	  (*new_v)->half = NULL;
	}
      }
      remove_vertex(vc);
      remove_vertex(va);
      remove_edge(e10->edge);
      remove_edge(e20->edge);
      remove_edge(e20->mate->previous->edge);
      delete (e20->mate->previous);
      delete (e20->mate->next);
      delete (e20->mate);
    } else {
      vc->half = e20->mate;
      vb->half = e30->mate->next->mate;
      *new_v = vb;
      remove_vertex (va);
      remove_loop (f3);
      e20->mate->mate = e30->mate->next->mate;
      e30->mate->next->mate->mate = e20->mate;
      e20->mate->edge = e30->mate->next->mate->edge;
      //e30->mate->next->mate->edge->half = e30->mate->next->mate;
      e20->mate->edge->half = e30->mate->next->mate;
      delete (e30->mate->next);
      delete (e10->mate);
      delete (e30->mate);
      if (vc->half->left->type == 2 &&
	  (vc->half->previous == vc->half->mate ||
	   vc->half->next == vc->half->mate)) {
	outer = vc->half->left;
	if (vc->half->previous == vc->half->mate &&
	    vc->half->next == vc->half->mate) {
	  if (outer != _outer_loop) {
	    remove_loop(outer);
	  } else {
	    _outer_loop->half = NULL;
	  }
	  vv = vc;
	  (*new_v)->half = NULL;
	} else if (vc->half->previous == vc->half->mate) {
	  vv = vc;
	  vb->half->previous->next = vb->half->mate->next;
	  vb->half->mate->next->previous = vb->half->previous;
	  (*new_v)->half = vb->half->mate->next;
	} else {
	  vv = vb;
	  *new_v = vc;
	  vc->half->previous->next = vc->half->mate->next;
	  vc->half->mate->next->previous = vc->half->previous;
	  (*new_v)->half = vc->half->mate->next;
	}
	remove_edge (vv->half->edge);
	delete (vv->half->mate);
	delete (vv->half);
	remove_vertex (vv);
      }
      remove_edge(e10->edge);
      remove_edge(e20->edge);
      remove_edge(e30->edge);
    }
  }
  delete(e10);
  delete(e20);
  delete(e30);
  remove_loop(f);
}

// he_ref leaving v
unsigned Mesh::remove_inner_structures (HalfEdge *he_ref, Vertex *v)
{
  HalfEdge *he1, *he2;
  Loop *f_ref, *f;

  f_ref=he_ref->mate->left;

  he1 = he_ref->mate->previous;
  he2 = he_ref->mate->next;

  if (he2->mate == he1 && he1->left->type == 1) {
    // f_ref is a degenerated face with two bounding edges
    remove_edge (he1->edge);
    remove_loop (he1->left);
    remove_vertex (he1->origin);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin != v && he1->mate->left->type == 1
	     && he1->mate->next == he2->mate) {
    // f_ref is "strangulated" in two loops AND
    // he1->mate->left AND he2->mate->left is the same
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_edge (he2->mate->next->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he2->mate->left);
    he2->mate->next->mate->edge = he_ref->edge;
    he_ref->mate = he2->mate->next->mate;
    he2->mate->next->mate = he_ref;
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he1->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 0;
  } else if (he1->origin != v && he1->mate->left->type == 2
	     && he2->mate->left->type == 2 && he1->mate->next == he2->mate) {
    // f_ref is "strangulated" in two loops AND
    // he1->mate->left AND he2->mate->left is outer loop
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_vertex (he1->origin);
    remove_loop (he_ref->mate->left);
    delete(he1->mate);
    delete(he2->mate);
    delete(he1);
    delete(he2);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin != v && he1->mate->left->type == 1
	     && he2->mate->left->type == 1 && he1->mate->next != he2->mate) {
    // f_ref is "strangulated" in two loops AND
    // he1->mate->left AND he2->mate->left are distinct
    remove_edge (he2->edge);
    remove_loop (he_ref->mate->left);
    he2->mate->edge = he1->edge;
    he2->mate->mate = he1->mate;
    he1->mate->mate = he2->mate;
    delete(he1);
    delete(he2);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he2->origin == v &&
	     he1->mate->left->type == 1 &&
	     he1->mate->left == he2->mate->left) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left is the same
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_edge (he2->mate->next->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he2->mate->left);
    he2->mate->next->mate->edge = he_ref->edge;
    he_ref->mate = he2->mate->next->mate;
    he2->mate->next->mate = he_ref;
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he1->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 0;
  } else if (he1->origin == v && he1->mate->left->type == 2
	     && he2->mate->left->type == 2 && he1->mate->next == he2->mate) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left are outer loop
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_loop (he_ref->mate->left);
    delete(he1->mate);
    delete(he2->mate);
    delete(he1);
    delete(he2);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->mate->left->type == 1
	     && he2->mate->left->type == 2 && he1->mate->next == he2->mate) {
    // f_ref is "strangulated" in three loops AND
    // he2->mate->left is outer loop
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_edge (he1->mate->next->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he1->mate->left);
    he1->mate->next->edge = he1->mate->previous->edge;
    he1->mate->next->mate->mate = he1->mate->previous->mate;
    he1->mate->previous->mate->mate = he1->mate->next->mate;
    he1->mate->next->mate->next =  he2->mate->next;
    he2->mate->next->previous = he1->mate->next->mate;
    delete(he1->mate->previous);
    delete(he1->mate->next);
    delete(he1->mate);
    delete(he2->mate);
    delete(he1);
    delete(he2);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->mate->left->type == 2
	     && he2->mate->left->type == 2 && he1->mate->next == he2->mate) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left is outer loop
    remove_edge (he2->edge);
    remove_edge (he1->edge);
    remove_edge (he2->mate->next->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he2->mate->left);
    he2->mate->next->edge = he2->mate->previous->edge;
    he2->mate->next->mate->mate = he2->mate->previous->mate;
    he2->mate->previous->mate->mate = he2->mate->next->mate;
    he1->mate->previous->next =  he2->mate->previous->mate;
    he2->mate->previous->mate->previous = he1->mate->previous;
    delete(he2->mate->previous);
    delete(he2->mate->next);
    delete(he1->mate);
    delete(he2->mate);
    delete(he1);
    delete(he2);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->left->type == 1 && he2->left->type == 1
	     && he1->mate->left != he2->mate->left &&
	     he1->mate->previous->mate != he1->mate->next &&
	     he2->mate->previous->mate != he2->mate->next) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left are distinct loops
    remove_edge (he1->edge);
    remove_edge (he2->edge);
    remove_edge (he1->mate->previous->edge);
    remove_edge (he2->mate->previous->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he1->mate->left);
    remove_loop (he2->mate->left);
    he1->mate->previous->edge = he1->mate->next->edge;
    he1->mate->previous->mate->mate = he1->mate->next->mate;
    he1->mate->next->mate->mate = he1->mate->previous->mate;
    he2->mate->previous->edge = he2->mate->next->edge;
    he2->mate->previous->mate->mate = he2->mate->next->mate;
    he2->mate->next->mate->mate = he2->mate->previous->mate;
    delete(he1->mate->previous);
    delete(he1->mate->next);
    delete(he1->mate);
    delete(he2->mate->previous);
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->left->type == 1 && he2->left->type == 1
	     && he1->mate->left != he2->mate->left &&
	     he1->mate->previous->mate == he1->mate->next &&
	     he2->mate->previous->mate != he2->mate->next) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left are distinct loops
    // he1->mate->left is an inner loop
    remove_vertex (he1->mate->previous->mate->origin);
    remove_edge (he1->edge);
    remove_edge (he2->edge);
    remove_edge (he1->mate->previous->edge);
    remove_edge (he2->mate->previous->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he1->mate->left);
    remove_loop (he2->mate->left);
    he2->mate->previous->edge = he2->mate->next->edge;
    he2->mate->previous->mate->mate = he2->mate->next->mate;
    he2->mate->next->mate->mate = he2->mate->previous->mate;
    delete(he1->mate->previous);
    delete(he1->mate->next);
    delete(he1->mate);
    delete(he2->mate->previous);
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->left->type == 1 && he2->left->type == 1
	     && he1->mate->left != he2->mate->left &&
	     he2->mate->previous->mate == he2->mate->next &&
	     he1->mate->previous->mate != he1->mate->next) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left are distinct loops
    // he2->mate->left is an inner loop
    remove_vertex (he2->mate->previous->mate->origin);
    remove_edge (he1->edge);
    remove_edge (he2->edge);
    remove_edge (he1->mate->previous->edge);
    remove_edge (he2->mate->previous->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he1->mate->left);
    remove_loop (he2->mate->left);
    he1->mate->previous->edge = he1->mate->next->edge;
    he1->mate->previous->mate->mate = he1->mate->next->mate;
    he1->mate->next->mate->mate = he1->mate->previous->mate;
    delete(he1->mate->previous);
    delete(he1->mate->next);
    delete(he1->mate);
    delete(he2->mate->previous);
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 1;
  } else if (he1->origin == v && he1->left->type == 1 && he2->left->type == 1
	     && he1->mate->left != he2->mate->left &&
	     he2->mate->previous->mate == he2->mate->next &&
	     he1->mate->previous->mate == he1->mate->next) {
    // f_ref is "strangulated" in three loops AND
    // he1->mate->left AND he2->mate->left are distinct inner loops
    remove_vertex (he1->mate->next->mate->origin);
    remove_vertex (he2->mate->next->mate->origin);
    remove_edge (he1->edge);
    remove_edge (he2->edge);
    remove_edge (he1->mate->previous->edge);
    remove_edge (he2->mate->previous->edge);
    remove_loop (he_ref->mate->left);
    remove_loop (he1->mate->left);
    remove_loop (he2->mate->left);
    delete(he1->mate->previous);
    delete(he1->mate->next);
    delete(he1->mate);
    delete(he2->mate->previous);
    delete(he2->mate->next);
    delete(he2->mate);
    delete(he2);
    delete(he1);
    delete(he_ref->mate);
    return 1;
  }

  cout << "Situation not handled yet." << endl;

  return 0;
}

// he1: half-edge leaving v1
// he2: half-edge entering v1
unsigned Mesh::remove_inner_structures (HalfEdge *he1, HalfEdge *he2, Vertex *v)
{
  if (he1->mate == he2 && he1->next == he2) {
    remove_vertex (he2->origin);
    return 1;
  }
  return 0;
}

// void Mesh::remove_degenerated_faceset (Vertex *v, HalfEdge *he, Loop *f) {
//   HalfEdge *he, *mhe, *che;
//
//   he = (old_v)->half;
//   he->origin = new_v;
//   che = he->mate;
//   if (che) che = che->next;
//   while (che && che != he) {
//     if (che->origin == (old_v)) che->origin = new_v;
//     che = che->mate;
//     che = che->next;
//   }
// }

VertexID Mesh::Reduce3(const FaceID ID, const VertexID vID, void *ptr) {

	Loop *f;
	HalfEdge *half;
	Vertex * v;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}

	half = f->half;
	v = half_collapse (half);

	ptr = v;
	return -1;
	return v->ID;
}

Vertex *Mesh :: half_collapse (HalfEdge *half) {
	Vertex *vertice;
	HalfEdge *half_travel;
	int tail, head, flag_face;

	tail = Point_Border(half);
	head = Point_Border(half->next);

	if ((tail + head == 0) || (tail + head == 1)) {

		if (tail + head == 1) {
			if (head) {
				half = half->mate;
			}
		}
		else cout << "Extremidades do half nao estao na borda" << endl;
		cout << "2 faces degeneradas" << endl;

		cout << "Atualizar vertice-half" << endl;
		cout << "Atualizar vertice:" << half->origin->ID << endl;
		flag_face = 0;
		half_travel = half->mate->next;
		while (!flag_face && half_travel != half) {
			if (half->left != half_travel->left && half->mate->left != half_travel->left) {
				if (half_travel->left->type == 1) {
					flag_face = 1;
					half->origin->half = half_travel;
				}
			}
			half_travel = half_travel->mate->next;
		}

		cout << "Atualizar vertice:" << half->mate->next->next->origin->ID << endl;
		flag_face = 0;
		half_travel = half->mate->next->next->mate->next;
		while (!flag_face && half_travel != half->mate->next->next) {
			if (half->mate->left != half_travel->left) {
				if (half_travel->left->type == 1) {
					flag_face = 1;
					half->mate->next->next->origin->half = half_travel;
				}
			}
			half_travel = half_travel->mate->next;
		}

		cout << "Atualizar vertice:" << half->previous->origin->ID << endl;
		flag_face = 0;
		half_travel = half->previous->mate->next;
		while (!flag_face && half_travel != half->previous) {
			if (half->left != half_travel->left) {
				if (half_travel->left->type == 1) {
					flag_face = 1;
					half->previous->origin->half = half_travel;
				}
			}
			half_travel = half_travel->mate->next;
		}

		cout << "Atualizar ORigem dos half" << endl;
		half_travel = half->next->mate->next;
		while (half_travel != half->next) {
			half_travel->origin = half->origin;
			half_travel = half_travel->mate->next;
		}

		cout << "Mate" << endl;
		half->previous->mate->mate = half->next->mate;
		half->next->mate->mate = half->previous->mate;

		half->mate->next->mate->mate = half->mate->previous->mate;
		half->mate->previous->mate->mate = half->mate->next->mate;

		vertice = half->origin;

		remove_edge(half->edge);
		remove_vertex(half->next->origin);
		remove_loop(half->left);
		remove_loop(half->mate->left);
		delete half->mate->next;
		delete half->mate->previous;
		delete half->mate;
		delete half->next;
		delete half->previous;
		delete half;
	}
	else { // extreminades estao na borda

		cout << "Extremidades na borda" << endl;

	}

	return vertice;
}

HalfEdge * Mesh::SameFace(HalfEdge *h) {

	HalfEdge *h_travel;
	int flag;

	if ((h)->next->mate->previous == (h)->previous->mate->next->mate ||
		(h)->mate->next->mate->previous == (h)->mate->previous->mate->next->mate	) {
		cout << "A funcao SameFace mudou!\n";
		h_travel = (h)->next;
		flag = 0;

		while (!flag && h_travel != (h)) {
			if (h_travel->next->mate->previous != h_travel->previous->mate->next->mate &&
				h_travel->mate->next-> mate->previous != h_travel->mate->previous->mate->next->mate	) {
				h = h_travel;
				flag = 1;
			}
			h_travel = h_travel->next;
		}
	}
	cout << "----------------------------\n";
	cout << "Face:" << h->origin->ID << " " << h->next->origin->ID << " " << h->previous->origin->ID << endl;
	cout << "----------------------------\n";

	return h;
}
bool Mesh::ok_collapse(HalfEdge *h) {

	vector<FaceID> Face;
	HalfEdge *it_h;

	if(!h)return false;
	int v0, vr;
	/*
	 * v0 --------\
	 *  | \        \
	 *  |   v2 ---- v3
	 *  | /        /
	 * v1 --------/
	 */
	//v0----v1, nao pode ser contraida
	if (h->next->mate->left != _outer_loop AND h->previous->mate->left != _outer_loop
		AND h->next->mate->left != h->previous->mate->left
		AND h->next->mate->previous == h->previous->mate->next->mate){
		return false;
	}
	//boundary
	v0 = Point_Border(h);
	vr = Point_Border(h->next);
	//nao pode contrair um vertice que esta na borda para dentro
	if(vr AND !v0)return false;
//	if (h->left->type == 2) return false;
	//colapse e feito: (v0, v1) -> v0
	//aresta entre dois vertices que estao na borda
	if((v0 AND vr) AND h->left->type == 1 AND h->mate->left->type == 1)return false;
	if (h->left->type == 2 AND h->mate->left->type == 2) return false;
	if (h == (h->mate->next)->mate->next) return false;
	if (h->previous->mate->left->type == 2 AND h->next->mate->left->type == 2) return false;

	it_h = h->next;
	do{
		if (it_h->left != _outer_loop){
			Face.push_back(it_h->left->ID);
		}
		it_h = it_h->mate->next;
	}while(it_h != h->next);

	if(change_orientation(Face, h)) return false;

	return true;
}
/*
 * Pensar em um lugar melhor para essa funcao. Ela esta na classe mesh, aqui podem ficar
 * as funcoes que operam na malha
 */
double Mesh::dist_point(VertexID idA, VertexID idB) {

	Point3D *tmp;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	float pt[3][3];
	int ind_pt, ab;

	ind_pt = 0;
	tmp = (Point3D *)(getVGeomPtr(idA));
	pt[ind_pt][0] = tmp->x; pt[ind_pt][1] = tmp->y; pt[ind_pt][2] = tmp->z;
	ind_pt++;
	tmp = (Point3D *)(getVGeomPtr(idB));
	pt[ind_pt][0] = tmp->x; pt[ind_pt][1] = tmp->y; pt[ind_pt][2] = tmp->z;
	ind_pt++;

	ab = sqrt( (pow((pt[0][0]-pt[1][0]),2)) + (pow((pt[0][1]-pt[1][1]),2)) + (pow((pt[0][2]-pt[1][2]),2)));
	return ab;
}
bool Mesh:: makeFlip(double ab, double bc, double ca) {

	double threshold = 0.1;

	if (ab > bc && ab > ca) {
		if ((bc/ca) >= (1-threshold) && (bc/ca) <= (1+threshold)) {
			return true;
		}
		else return false;
	}
	else {
		if (bc > ab && bc > ca) {
			if ((ab/ca) >= (1-threshold) && (ab/ca) <= (1+threshold)) {
				return true;
			}
			else return false;
		}
		else {
			if ((ab/bc) >= (1-threshold) && (ab/bc) <= (1+threshold)) {
				return true;
			}
			else return false;
		}
	}
}
bool Mesh::equal_sides(double ab, double bc, double ca) {
	double rate_a, rate_b, rate_c;
	double threshold = 0.2;

	rate_a = ab/bc;
	rate_b = bc/ca;
	rate_c = ab/ca;

	cout << "RATE A:" << rate_a << endl;
	cout << "RATE B:" << rate_b << endl;
	cout << "RATE C:" << rate_c << endl;

	if ((rate_a <= (1+threshold) && rate_a >= (1-threshold)) &&
		(rate_b <= (1+threshold) && rate_b >= (1-threshold)) &&
		(rate_c <= (1+threshold) && rate_c >= (1-threshold))) {
		return true;
	}
	return false;
}
double Mesh::computeSignedFaceArea(VertexID vA, VertexID vB, VertexID vC) {
	Point3D *tmp;
	double v[3][3];

	tmp=(Point3D *)getVGeomPtr (vA);
	v[0][0] = (double)tmp->x;   v[0][1] = (double)tmp->y;
	v[0][2] = (double)tmp->z;
	tmp=(Point3D *)getVGeomPtr (vB);
	v[1][0] = (double)tmp->x;   v[1][1] = (double)tmp->y;
	v[1][2] = (double)tmp->z;
	tmp=(Point3D *)getVGeomPtr (vC);
	v[2][0] = (double)tmp->x;   v[2][1] = (double)tmp->y;
	v[2][2] = (double)tmp->z;

	return ((0.5f*(v[0][0]*v[1][1]*v[2][2] + v[0][2]*v[1][0]*v[2][1]
	        + v[0][1]*v[1][2]*v[2][0] - ( v[0][2]*v[1][1]*v[2][0]
	        + v[0][0]*v[1][2]*v[2][1] + v[0][1]*v[1][0]*v[2][2]))));
}
bool Mesh::change_orientation(vector<FaceID> Face, HalfEdge* half) {

	Point3D *pr;
	vector<VertexID> vvlist;
	vector<FaceID>::iterator FIter;
	double area, area_after;
	int id_vr;

	id_vr =  half->next->origin->ID;
	pr = (Point3D *)(getVGeomPtr(id_vr));

	for (FIter = Face.begin(); FIter != Face.end(); FIter++) {
		getFVertices (*FIter, &vvlist);
		if (vvlist[0] == id_vr || vvlist[1] == id_vr || vvlist[2] == id_vr){

			area = computeSignedFaceArea(vvlist[0], vvlist[1], vvlist[2]);

			half->next->origin->GeomPtr = half->origin->GeomPtr;

			area_after = computeSignedFaceArea(vvlist[0], vvlist[1], vvlist[2]);

			half->next->origin->GeomPtr = pr;

			if ((area > 0 && area_after < 0) || (area < 0 && area_after > 0)) {
				return true;
			}
		}
		vvlist.clear();
	}

	half->next->origin->GeomPtr = pr;
	return false;
}//-----------------------------------------------------------------------------------------
bool Mesh::crossEdge(vector<FaceID> Face, HalfEdge* half) {

	Point3D *pr;
	vector<VertexID> vvlist;
	vector<FaceID>::iterator FIter;
	double area, area_after;
	int id_vr;

	id_vr =  half->next->origin->ID;
	pr = (Point3D *)(getVGeomPtr(id_vr));

	for (FIter = Face.begin(); FIter != Face.end(); FIter++) {
		getFVertices (*FIter, &vvlist);
		if (vvlist[0] == id_vr || vvlist[1] == id_vr || vvlist[2] == id_vr){

			area = computeSignedFaceArea(vvlist[0], vvlist[1], vvlist[2]);

			half->next->origin->GeomPtr = half->origin->GeomPtr;

			area_after = computeSignedFaceArea(vvlist[0], vvlist[1], vvlist[2]);

			half->next->origin->GeomPtr = pr;

			if ((area > 0 && area_after < 0) || (area < 0 && area_after > 0)) {
				return true;
			}
		}
		vvlist.clear();
	}

	half->next->origin->GeomPtr = pr;
	return false;
}//-----------------------------------------------------------------------------------------
void Mesh::computePlane(VertexID vA, VertexID vB, VertexID vC, double &a, double &b, double &c, double &d){
	Point3D *tmp;
	double v[3][3], BA[3], BC[3], normal;

	tmp=(Point3D *)getVGeomPtr (vA);
	v[0][0] = (double)tmp->x;   v[0][1] = (double)tmp->y;
	v[0][2] = (double)tmp->z;
	tmp=(Point3D *)getVGeomPtr (vB);
	v[1][0] = (double)tmp->x;   v[1][1] = (double)tmp->y;
	v[1][2] = (double)tmp->z;
	tmp=(Point3D *)getVGeomPtr (vC);
	v[2][0] = (double)tmp->x;   v[2][1] = (double)tmp->y;
	v[2][2] = (double)tmp->z;

	BA[0] = v[0][0] - v[1][0]; BA[1] = v[0][1] - v[1][1]; BA[2] = v[0][2] - v[1][2];
	BC[0] = v[2][0] - v[1][0]; BC[1] = v[2][1] - v[1][1]; BC[2] = v[2][2] - v[1][2];

	a = BA[1]*BC[2] - BA[2]*BC[1];
	b = BA[2]*BC[0] - BA[0]*BC[2];
	c = BA[0]*BC[1] - BA[1]*BC[0];

	normal = a*a + b*b + c*c;
	normal = sqrt(normal);

	a = a/normal;
	b = b/normal;
	c = c/normal;

	d = -(a*v[0][0] + b*v[0][1] + c*v[0][2]);
}
//-----------------------------------------------------------------------------------------
VertexID Mesh::ReduceQEM(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) ) {

	cout << "ReduceQEM" << endl;
	int index;
	Loop *f;
	double cost_min, a, b, c, d, q[4][4];
	double v[4], dist, dist_temp_x, dist_temp_y, dist_temp_z, dist_temp_h;
	HalfEdge *half_iter, *half, *half_min, *half_vert_adj, *half_plane, *h_tmp;
	vector<HalfEdge *> list_half;
	VertexID vA, vB, vC;
	Point3D *tmp;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}

	half_iter = half = f->half;
	half_min = 0;
	do{
		tmp = (Point3D *)getVGeomPtr (half_iter->origin->ID);
		v[0] = tmp->x; v[1] = tmp->y; v[2] = tmp->z; v[3] = 1;
		cout << "V:" << half_iter->origin->ID << endl;
		half_vert_adj = half_iter->next;
		while(half_vert_adj != half_iter){
			cout << "adj:" << half_vert_adj->origin->ID << endl;
			if (half_vert_adj->previous == half_iter){
				h_tmp = half_iter;
			}
			else h_tmp = half_vert_adj;

			if (ok_collapse(h_tmp)){
				cout << "Ok\n";
				memset(q,0,sizeof(q));
				half_plane = half_vert_adj;
				do{
					//calcular planos
					if (half_plane->left->type != 2){
						vA = half_plane->origin->ID;
						vB = half_plane->next->origin->ID;
						vC = half_plane->previous->origin->ID;
						computePlane(vA, vB, vC, a, b, c, d);
						//cout << "Plano:" << a << " " << b << " " << c << " " << d << endl;
						q[0][0] += a*a; q[0][1] += a*b; q[0][2] += a*c; q[0][3] += a*d;
						q[1][0] += a*b; q[1][1] += b*b; q[1][2] += b*c; q[1][3] += b*d;
						q[2][0] += a*c; q[2][1] += b*c; q[2][2] += c*c; q[2][3] += c*d;
						q[3][0] += a*d; q[3][1] += b*d; q[3][2] += c*d; q[3][3] += d*d;
					}
					half_plane = half_plane->mate->next;
				}while(half_plane != half_vert_adj);
				//calcula erro
				//dist = vt.Q.v
				dist_temp_x = v[0]*q[0][0] + v[1]*q[1][0] + v[2]*q[2][0] + v[3]*q[3][0];
				dist_temp_y = v[0]*q[0][1] + v[1]*q[1][1] + v[2]*q[2][1] + v[3]*q[3][1];
				dist_temp_z = v[0]*q[0][2] + v[1]*q[1][2] + v[2]*q[2][2] + v[3]*q[3][2];
				dist_temp_h = v[0]*q[0][3] + v[1]*q[1][3] + v[2]*q[2][3] + v[3]*q[3][3];

				dist = dist_temp_x*v[0] + dist_temp_y*v[1] + dist_temp_z*v[2] + dist_temp_h*v[3];
				dist = fabs(dist);
				cout << "dist:" << dist << endl;
				if(!half_min){
					cost_min = dist;
					if (half_iter == half_vert_adj->previous){
						half_min = half_iter->mate;
					}
					else {
						half_min = half_vert_adj;
					}
				}
				else {
					if (cost_min > dist){
						cost_min = dist;
						if (half_iter == half_vert_adj->previous){
							half_min = half_iter->mate;
						}
						else {
							half_min = half_vert_adj;
						}
					}
				}
			}
			half_vert_adj = half_vert_adj->next;
		}
		cout << endl;
		half_iter = half_iter->next;
	}while(half_iter != half);

	cout << "Dist minima:" << cost_min << endl;
	cout << "Mantem vertice:" << half_min->mate->origin->ID << endl;
	cout << "Retira vertice:" << half_min->origin->ID << endl;
	cout << "Elimina aresta" << endl;
	index = eliminate_edge(half_min->mate, &h_tmp);
	cout << "fim elima aresta" << endl;
	cout << "index:" << index << endl;
	return index;
}
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
VertexID Mesh::ReduceQEM2(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) ) {

	cout << "ReduceQEM" << endl;
	int index, id_q;
	Loop *f;
	double cost_min, cost[4], cost_tmp, a, b, c, d, q[4][4][3];
	Matrix16 Q[3], Qbarra, Qinv;
	double v[4], dist, dist_temp_x, dist_temp_y, dist_temp_z, dist_temp_h, vbarra[4];
	HalfEdge *half_iter, *half, *half_min, *half_vert_adj, *half_plane, *h_tmp;
	vector<HalfEdge *> list_half;
	VertexID vA, vB, vC;
	Point3D *tmp, *tmp2;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}

	//1. Compute the Q matrices for all the initial vertices
	half_iter = half = f->half;
	cout << "CHECK1" << endl;
	id_q = 0;
	do{
		tmp = (Point3D *)getVGeomPtr (half_iter->origin->ID);
		v[0] = tmp->x; v[1] = tmp->y; v[2] = tmp->z; v[3] = 1;
		cout << "V:" << half_iter->origin->ID << endl;
		half_vert_adj = half_iter->next;
		while(half_vert_adj != half_iter){
			cout << "adj:" << half_vert_adj->origin->ID << endl;
			if (half_vert_adj->previous == half_iter){
				h_tmp = half_iter;
			}
			else h_tmp = half_vert_adj;

			if (ok_collapse(h_tmp)){
				cout << "Ok\n";
				memset(q,0,sizeof(q));
				memset(Q[id_q].m,0,sizeof(Q[id_q].m));
				half_plane = half_vert_adj;
				do{
					//calcular planos
					if (half_plane->left->type != 2){
						vA = half_plane->origin->ID;
						vB = half_plane->next->origin->ID;
						vC = half_plane->previous->origin->ID;
						computePlane(vA, vB, vC, a, b, c, d);
						cout << "Plano:" << a << " " << b << " " << c << " " << d << endl;
						Q[id_q].m[0]  += a*a; Q[id_q].m[1]  += a*b; Q[id_q].m[2]  += a*c; Q[id_q].m[3]  += a*d;
						Q[id_q].m[4]  += a*b; Q[id_q].m[5]  += b*b; Q[id_q].m[6]  += b*c; Q[id_q].m[7]  += b*d;
						Q[id_q].m[8]  += a*c; Q[id_q].m[9]  += b*c; Q[id_q].m[10] += c*c; Q[id_q].m[11] += c*d;
						Q[id_q].m[12] += a*d; Q[id_q].m[13] += b*d; Q[id_q].m[14] += c*d; Q[id_q].m[15] += d*d;
					}
					half_plane = half_plane->mate->next;
				}while(half_plane != half_vert_adj);
				//calcula erro
			}
			half_vert_adj = half_vert_adj->next;
		}
		cout << endl;
		id_q++;
		half_iter = half_iter->next;
	}while(half_iter != half);

	cout << "Matriz Q" << endl;

	for (int kk = 0; kk < 3; kk++){
		cout << "Q" << kk << endl;
		for (int ii = 0; ii < 4; ii++){
			for (int jj = 0; jj < 4; jj++){
				cout << Q[kk].m[ii*4+jj] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}

	cout << "CHECK2" << endl;
	//3. Compute the optimal contraction target v for each valid pair
	//(v1, v2) /. The error v(Q1 + Q2)v of this target vertex becomes
	//the cost of contracting that pair.

	cost_min = 1000000000;
	half_min = 0;
	half_iter = f->half;
	int i, j;
	i = 0;
	do{
		j = i + 1;
		half_vert_adj = half_iter->next;
		while(half_vert_adj != half_iter && j < 3){
			cout << "(i, j):" << i << " " << j << endl;
			Qbarra = Matrix_sum(Q[i], Q[j]);
			Qbarra.m[12] = 0; Qbarra.m[13] = 0; Qbarra.m[14] = 0; Qbarra.m[15] = 1;

			Matrix_print("Q[i]", Q[i]);
			Matrix_print("Q[j]", Q[j]);
			Matrix_print("Soma", Qbarra);
			if(Matrix_determinant(Qbarra)){
				Qinv = Matrix_inverted(Qbarra);
				Matrix_print("Inv", Qinv);

				Matrix16 QQ =  Matrix_multiplied(Qinv, Qbarra);
				Matrix_print("Qinv * Qbarra", QQ);

				vbarra[0] = Qinv.m[3]; vbarra[1] = Qinv.m[7]; vbarra[2] = Qinv.m[11]; vbarra[3] = Qinv.m[15];
			}
			else {
				tmp = (Point3D *)getVGeomPtr (half_iter->origin->ID);
				tmp2 = (Point3D *)getVGeomPtr (half_vert_adj->origin->ID);
				vbarra[0] = (tmp->x+tmp2->x)/2.0; vbarra[1] = (tmp->y+tmp2->y)/2.0; vbarra[2] = (tmp->z+tmp2->z)/2.0; vbarra[3] = 1;
			}
			j++;

			cost[0] = vbarra[0]*Qbarra.m[0] + vbarra[1]*Qbarra.m[4] + vbarra[2]*Qbarra.m[8] + vbarra[3]*Qbarra.m[12];
			cost[1] = vbarra[0]*Qbarra.m[1] + vbarra[1]*Qbarra.m[5] + vbarra[2]*Qbarra.m[9] + vbarra[3]*Qbarra.m[13];
			cost[2] = vbarra[0]*Qbarra.m[2] + vbarra[1]*Qbarra.m[6] + vbarra[2]*Qbarra.m[10] + vbarra[3]*Qbarra.m[14];
			cost[3] = vbarra[0]*Qbarra.m[3] + vbarra[1]*Qbarra.m[7] + vbarra[2]*Qbarra.m[11] + vbarra[3]*Qbarra.m[15];

			cost_tmp = cost[0]*vbarra[0] + cost[1]*vbarra[1] + cost[2]*vbarra[2] + cost[3]*vbarra[3];

			if(fabs(cost_min) > fabs(cost_tmp)){
				cost_min = cost_tmp;
				half_min = half_iter;
				for (int ii = 0; ii < 4; ii++)
					v[ii] = vbarra[ii];

			}

			half_vert_adj = half_vert_adj->next;
		}


		cout << "Cost min:" << cost_min << endl;
		cout << "vbarra:" << v[0] << " " << v[1] << " " << v[2] << endl;
		half_iter = half_iter->next;
		i++;
	}while(half_iter != f->half);

	Vertex *vert;
	tmp = (Point3D *)(getVGeomPtr( half_min->mate->origin->ID));
	vert = vertices.find(half_min->mate->origin->ID)->second;
	tmp->x = v[0]; tmp->y = v[1]; tmp->z = v[2];
	vert->GeomPtr = tmp;
	cout << "CHECK3" << endl;
	cout << "Dist minima:" << cost_min << endl;
	cout << "Mantem vertice:" << half_min->mate->origin->ID << endl;
	cout << "Retira vertice:" << half_min->origin->ID << endl;
	cout << "Elimina aresta" << endl;
	index = eliminate_edge(half_min->mate, &h_tmp);
	cout << "fim elima aresta" << endl;
	cout << "index:" << index << endl;
	cout << "CHECK4" << endl;
	return index;
}
//-----------------------------------------------------------------------------------------
HalfEdge *Mesh::findHalf(int i, int j){

	Vertex *v;
	HalfEdge *half_iter;
	//cout << "i:" << i << endl;
	//cout << "j:" << j << endl;

	if (vertices.find(i) == vertices.end())return 0;
	if (vertices.find(j) == vertices.end())return 0;

	v = vertices.find(i)->second;//pega o vertice x
	if(!v)return 0;
	if(!v->half) return 0;

	half_iter = v->half;

	do{
		if (half_iter->next->origin->ID == j)break;
		half_iter = half_iter->mate->next;
	}while (half_iter != v->half);



	if(half_iter->next->origin->ID != j)return 0;
	return half_iter;
}
//-----------------------------------------------------------------------------------------
int compare (pair<double, int> a, pair<double, int> b)
{
	return (a.first > b.first);
}
//-----------------------------------------------------------------------------------------
int cmp (pair<HalfEdge *, double> a, pair<HalfEdge *, double> b)
{
	return (a.first > b.first);
}
//-----------------------------------------------------------------------------------------
int compare_q (node *a, node  *b)
{
	return (a->c > b->c);
}
//-----------------------------------------------------------------------------------------
void Mesh :: intialize(vector<FaceID> list){

	vector<FaceID>::iterator FIter;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	int vd[3], index;
	double a, b, c, d;
	Vertex *vt;

	for (FIter = list.begin(); FIter != list.end(); FIter++) {
		getFVertices (*FIter, &vvlist);
		index = 0;
		for ( vvIter = vvlist.begin(); vvIter != vvlist.end(); vvIter++ ) {
			vd[index++] = *vvIter;
		}

		computePlane(vd[0], vd[1], vd[2], a, b, c, d);
		vt = vertices.find(vd[0])->second;

		vt->q.m[0]  += a*a; vt->q.m[1]  += a*b; vt->q.m[2]  += a*c; vt->q.m[3]  += a*d;
		vt->q.m[4]  += a*b; vt->q.m[5]  += b*b; vt->q.m[6]  += b*c; vt->q.m[7]  += b*d;
		vt->q.m[8]  += a*c; vt->q.m[9]  += b*c; vt->q.m[10] += c*c; vt->q.m[11] += c*d;
		vt->q.m[12] += a*d; vt->q.m[13] += b*d; vt->q.m[14] += c*d; vt->q.m[15] += d*d;

		vt = vertices.find(vd[1])->second;

		vt->q.m[0]  += a*a; vt->q.m[1]  += a*b; vt->q.m[2]  += a*c; vt->q.m[3]  += a*d;
		vt->q.m[4]  += a*b; vt->q.m[5]  += b*b; vt->q.m[6]  += b*c; vt->q.m[7]  += b*d;
		vt->q.m[8]  += a*c; vt->q.m[9]  += b*c; vt->q.m[10] += c*c; vt->q.m[11] += c*d;
		vt->q.m[12] += a*d; vt->q.m[13] += b*d; vt->q.m[14] += c*d; vt->q.m[15] += d*d;

		vt = vertices.find(vd[2])->second;

		vt->q.m[0]  += a*a; vt->q.m[1]  += a*b; vt->q.m[2]  += a*c; vt->q.m[3]  += a*d;
		vt->q.m[4]  += a*b; vt->q.m[5]  += b*b; vt->q.m[6]  += b*c; vt->q.m[7]  += b*d;
		vt->q.m[8]  += a*c; vt->q.m[9]  += b*c; vt->q.m[10] += c*c; vt->q.m[11] += c*d;
		vt->q.m[12] += a*d; vt->q.m[13] += b*d; vt->q.m[14] += c*d; vt->q.m[15] += d*d;

		vvlist.clear();
	}
}
//-----------------------------------------------------------------------------------------
void Mesh::QEM_(double max_err, void (func)(void), vector<FaceID> fd){

	int vr;
	vector<node *> heap;
	HalfEdge *half, *h_tmp;
	Matrix16 mtx;
	Vertex *vv;

	initializeQEM();
	make_pairs(fd, &heap);
	sort(heap.begin(), heap.end(), compare_q);

	for(int i = 0; i < heap.size(); i++){
		half = heap[heap.size()-1]->h;
		vr = half->next->origin->ID;
		vv = vertices.find(heap[heap.size()-1]->v0)->second;
		heap.pop_back();
		cout << "Collapse:" << "(" << half->origin->ID << " " << half->next->origin->ID << ")" << endl;
		//if(half->left != _outer_loop AND half->left != _outer_loop)
		mtx = half->next->origin->q;
			eliminate_edge(half, &h_tmp);
			//half->origin->q = Matrix_sum(half->origin->q, half->next->origin->q);
		updateHeap(max_err, &heap, vr, vv, mtx);
		cout << "Heap" << endl;
		for (int i = 0; i < heap.size(); i++){
			cout << "(" << heap[i]->v0<< " " << heap[i]->v1 << "):" << heap[i]->c << endl;
		}
		//return;
	}

}
void Mesh::QEM(double max_err){


	int vr, i;
	vector<node *> heap;
	HalfEdge *half, *h_tmp;
	Matrix16 mtx;
	Vertex *vv;
	Point3D *ptt;

	initializeQEM();
	initializeHeap(max_err, &heap);

	//for( i = 0; i < heap.size(); i++){
	while(heap.size() > 0){
		half = heap[heap.size()-1]->h;
		vv = vertices.find(heap[heap.size()-1]->v0)->second;
		assert(half AND (half->origin) AND (half->next));
		vr = half->next->origin->ID;
		heap.pop_back();
		mtx = half->next->origin->q;

		if (ok_collapse(half)){
			eliminate_edge(half,&h_tmp);
			//i = 0;
		}
		updateHeap(max_err, &heap, vr,vv, mtx);
		update_normal();
	}
}
//-----------------------------------------------------------------------------------------
void Mesh::initializeQEM(){

	vector<FaceID> Flist;
	vector<FaceID>::iterator FIter;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	Vertex *v;
	Matrix16 kp;
	double norma, area, n[3], a, b, c, d;
	Point3D *p;

	getAllVertices(&vvlist);
	for ( vvIter = vvlist.begin(); vvIter != vvlist.end(); vvIter++ ) {
		v = vertices.find(*vvIter)->second;
		memset(v->q.m, 0, sizeof(v->q.m));
	}
	vvlist.clear();

	getAllFaces (&Flist);
	for ( FIter = Flist.begin(); FIter != Flist.end(); FIter++) {
		getFVertices (*FIter, &vvlist);

		computeFaceArea(vvlist[0], vvlist[1], vvlist[2], n);
		norma =  sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
		area = norma;
		if(norma > FLT_MIN){
			n[0] /= norma; n[1] /= norma; n[2] /= norma;
			area *= 0.5;
		}

		p = (Point3D *)getVGeomPtr (vvlist[0]);
		a = n[0];
		b = n[1];
		c = n[2];
		d = -(a*p->x + b*p->y + c*p->z);

		kp.m[0] = a*a; kp.m[1] = a*b; kp.m[2] = a*c; kp.m[3] = a*d;
		kp.m[4] = a*b; kp.m[5] = b*b; kp.m[6] = b*c; kp.m[7] = b*d;
		kp.m[8] = a*c; kp.m[9] = b*c; kp.m[10] = c*c; kp.m[11] = c*d;
		kp.m[12] = a*d; kp.m[13] = b*d; kp.m[14] = c*d; kp.m[15] = d*d;

		for (int i = 0; i < 16; i++)kp.m[i] *= area;

		v = vertices.find(vvlist[0])->second;
		v->q = Matrix_sum(v->q, kp);

		v = vertices.find(vvlist[1])->second;
		v->q = Matrix_sum(v->q, kp);

		v = vertices.find(vvlist[2])->second;
		v->q = Matrix_sum(v->q, kp);

		vvlist.clear();
	}
}
//-----------------------------------------------------------------------------------------
void Mesh::initializeHeap(double max_err,  vector<node*> *heap){

	Vertex *v;
	node *v0v1;
	HalfEdge *h, *iterh;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	double cost, best_cost;

	getAllVertices(&vvlist);

	for (vvIter = vvlist.begin(); vvIter != vvlist.end(); vvIter++){
		v = vertices.find(*vvIter)->second;
		iterh = v->half;
		best_cost = FLT_MAX;
		h = 0;
		//cout << "v:" << v->ID << endl;
		do{
			if(ok_collapse(iterh)){
				//cout << "collapse\n";
				cost = collapseCost(iterh->origin->ID, (iterh->next)->origin->ID);
				if(cost > max_err)cost = -1;
				if (cost >= 0.0 && cost < best_cost){
					best_cost = cost;
					h = iterh;
				}
			}
			iterh = (iterh->mate)->next;
		}while(iterh != v->half);

		if(h){
			v0v1 = new node;
			v0v1->v0 = h->origin->ID;
			v0v1->v1 = h->next->origin->ID;
			v0v1->c = best_cost;
			v0v1->h = h;
			heap->push_back(v0v1);
			//cout << "(" << h->origin->ID << " " << h->next->origin->ID << "):" << best_cost << endl;
		}
	}
	sort(heap->begin(), heap->end(), compare_q);
}
//-----------------------------------------------------------------------------------------------
void Mesh::Heap(double max_err, vector<node*> *heap){

	Vertex *v;
	vector<int> remove;
	HalfEdge *h, *iterh;
	double cost, best_cost;

	for (int i = 0; i < heap->size(); i++){
		//file << "---" << endl;
		//file << "Heap:" << (*heap)[i]->v0 << endl;

		//file << v->ID << endl;
		//file << "---" << endl;
		v = vertices.find((*heap)[i]->v0)->second;//como o half foi atualizado, eu tenho certeza de q esse half esta entre v0 e v1(feito no update)
		iterh = v->half;
		best_cost = FLT_MAX;
		h = 0;
		if(ok_collapse(iterh)){
			//cout << "collapse\n";
			cost = collapseCost(iterh->origin->ID, (iterh->next)->origin->ID);
			if(cost > max_err)cost = -1;
			if (cost >= 0.0 && cost < best_cost){
				best_cost = cost;
				h = iterh;
			}
		}
		if(!h)remove.push_back(i);//illegal collapse
		else{
			(*heap)[i]->c = best_cost;//atualiza custo
		}
	}
	//remover

	//file << "REMOVER:" << remove.size() << endl;
	//cout << "heap:" << heap->size() << endl;

	for (int i = 0; i < remove.size(); i++){
		(*heap).erase((*heap).begin()+remove[i]);
		for (int j = i+1; j < remove.size(); j++){
			remove[j]--;
		}
	}
}
//-----------------------------------------------------------------------------------------------
void Mesh::updateHeap(double max_err,  vector<node*> *heap, int vr, Vertex *vv, Matrix16 mtx){

	HalfEdge *iterh;
	vector<int> remove;
	vector<VertexID> vs;
	vector<VertexID>::iterator it_vs;

	//cout << "VR:" << vr << endl;
	//file << "VR:" << vr << endl;

	for (int i = 0; i < heap->size(); i++){
		if((*heap)[i]->v0 == vr OR (*heap)[i]->v1 == vr)
			remove.push_back(i);
	}

	//file << "remover size:" << remove.size() << endl;
	//remover
	//cout << "remover" << endl;
	for (int i = 0; i < remove.size(); i++){
		(*heap).erase((*heap).begin()+remove[i]);
		for (int j = i+1; j < remove.size(); j++){
			remove[j]--;
		}
	}

	iterh = vv->half;
	do {
		vs.push_back(iterh->mate->origin->ID);
		iterh = iterh->mate->next;
	}while (iterh != vv->half);

	vs.push_back(vv->ID);
//	cout << "atualizar custos" << endl;
//atualizar os custos e os pares que ficaram no heap
//	cout << "atualizando matriz" << endl;
	//atualizando matriz
	for(it_vs = vs.begin(); it_vs != vs.end(); it_vs++){
		updateMatrix(*it_vs);
	}
	//cout << "atualizando halfedge" << endl;
	//atualizando halfedge
	//file << "#1 heap:" << heap->size() << endl;
	for (int i = 0; i < heap->size(); i++){
		//vertices.find((*heap)[i]->v0)->second->half = findHalf((*heap)[i]->v0, (*heap)[i]->v1);
		(*heap)[i]->h = findHalf((*heap)[i]->v0, (*heap)[i]->v1);
		vertices.find((*heap)[i]->v0)->second->half = (*heap)[i]->h;
		//cout << "half i-j:" << vertices.find((*heap)[i]->v0)->second->half << endl;
	}
	//file << "#2 heap:" << heap->size() << endl;
	//atualizar custo
//	cout << "atualizar custo" << endl;
	//vv->q = Matrix_sum(vv->q, mtx);//contracao (a, b) => a, matrix de b somada com a matrix de a
	//file << "#3 heap:" << heap->size() << endl;
	Heap(max_err, heap);
	//file << "#4 heap:" << heap->size() << endl;
	//cout << "sort" << endl;
	sort(heap->begin(), heap->end(), compare_q);

}
//-----------------------------------------------------------------------------------------
void Mesh::updateMatrix(VertexID vid){

	Vertex *v;
	HalfEdge *h, *iterh;
	double norma, area, n[3], a, b, c, d;
	Point3D *p;
	Matrix16 kp;

//	cout << "vid:" << vid << endl;
	v = vertices.find(vid)->second;
	//cout << "vertice:" << v->ID << endl;
	memset(v->q.m, 0, sizeof(v->q.m));

	iterh = v->half;
	//if(iterh)cout << "nao nulo" << endl;
	///else cout << "nulo" << endl;
	//cout << "iterh face:" << iterh->left->ID << endl;

	do{
		if(iterh->left != _outer_loop){
		//	cout << "iterh face:" << iterh->left->ID << endl;
		//	cout << "iterh->origin->ID:" << iterh->origin->ID << endl;
		//	cout << "area" << endl;
			computeFaceArea(iterh->origin->ID, iterh->next->origin->ID, iterh->previous->origin->ID, n);
			norma =  sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
			area = norma;
			if(norma > FLT_MIN){
				n[0] /= norma; n[1] /= norma; n[2] /= norma;
				area *= 0.5;
			}
			//cout << "plano" << endl;
			p = (Point3D *)getVGeomPtr (iterh->origin->ID);
			a = n[0];
			b = n[1];
			c = n[2];
			d = -(a*p->x + b*p->y + c*p->z);

			kp.m[0] = a*a; kp.m[1] = a*b; kp.m[2] = a*c; kp.m[3] = a*d;
			kp.m[4] = a*b; kp.m[5] = b*b; kp.m[6] = b*c; kp.m[7] = b*d;
			kp.m[8] = a*c; kp.m[9] = b*c; kp.m[10] = c*c; kp.m[11] = c*d;
			kp.m[12] = a*d; kp.m[13] = b*d; kp.m[14] = c*d; kp.m[15] = d*d;

			for (int i = 0; i < 16; i++)kp.m[i] *= area;
		//	cout << "matrix sum" << endl;

			v->q = Matrix_sum(v->q, kp);
		}
		iterh = iterh->mate->next;

		//if(iterh)cout << "nao nulo" << endl;
		//else cout << "nulo" << endl;


		//cout << "iterh:" << iterh << endl;
		//cout << "iterh face:" << iterh->left->ID << endl;
		//cout << "v->half:" << v->half << endl;

	}while(iterh != v->half);
	//cout << "sai up" << endl;
}
//-----------------------------------------------------------------------------------------
double Mesh::collapseCost(VertexID v0, VertexID v1){

	Point3D *pt;
	Matrix16 qs;
	Vertex *vA, *vB;
	double x, y, z, err;

	vA = vertices.find(v0)->second;
	vB = vertices.find(v1)->second;

	//cout << "(" << vA->ID << " " << vB->ID << ")" << endl;
	qs = Matrix_sum(vA->q, vB->q);
	/*
	cout << "soma matriz" << endl;
	for (int ii = 0; ii < 4; ii++){
		for (int jj = 0; jj < 4; jj++){
			cout << qs.m[ii*4+jj] << " ";
		}
		cout << endl;
	}*/

	pt = (Point3D *)getVGeomPtr (vA->ID);
	x = pt->x; y = pt->y; z = pt->z;
	//cout << "pt:" << x << " " << y << " " << z << endl;
	long double p1, p2, p3, p4;
	p1 = qs.m[0]*x*x +  2*qs.m[1]*x*y  +  2*qs.m[2]*x*z   +   2*qs.m[3]*x;
	p2 = qs.m[5]*y*y  +  2*qs.m[6]*y*z   +   2*qs.m[7]*y;
	p3 =  qs.m[10]*z*z  +   2*qs.m[11]*z;
	p4 = qs.m[15];

	/*cout << "p1:" << p1 << endl;
	cout << "p2:" << p2 << endl;
	cout << "p3:" << p3 << endl;
	cout << "p4:" << p4 << endl;
	cout << "p1+p2:" << p1+p2 << endl;
	cout << "p1+p2+p3:" << p1+p2+p3 << endl;
	cout << "p1+p2+p3+p4:" << p1+p2+p3+p4 << endl;*/

	err =  qs.m[0]*x*x +  2*qs.m[1]*x*y  +  2*qs.m[2]*x*z   +   2*qs.m[3]*x
			           +    qs.m[5]*y*y  +  2*qs.m[6]*y*z   +   2*qs.m[7]*y
			                             +    qs.m[10]*z*z  +   2*qs.m[11]*z
			                                                +     qs.m[15];
//	cout << "err:" << err << endl;
	return err;
}
//-----------------------------------------------------------------------------------------
void Mesh::metric1area_side(vector<FaceID> *list, double t, double &ee){

	vector<FaceID> Flist;
	vector<FaceID>::iterator FIter;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	Point3D *tmp;
	double area, side_a, side_b, side_c, v[3][3], greater_side, n[3];
	ee = 0;
	getAllFaces (&Flist);
	for ( FIter = Flist.begin(); FIter != Flist.end(); FIter++) {
		/// check whether the normal vector is inverted
		getFVertices (*FIter, &vvlist);
		area = computeFaceArea(vvlist[0], vvlist[1], vvlist[2],n);

		tmp=(Point3D *)getVGeomPtr (vvlist[0]);
		v[0][0] = (double)tmp->x;   v[0][1] = (double)tmp->y;
		v[0][2] = (double)tmp->z;
		tmp=(Point3D *)getVGeomPtr (vvlist[1]);
		v[1][0] = (double)tmp->x;   v[1][1] = (double)tmp->y;
		v[1][2] = (double)tmp->z;
		tmp=(Point3D *)getVGeomPtr (vvlist[2]);
		v[2][0] = (double)tmp->x;   v[2][1] = (double)tmp->y;
		v[2][2] = (double)tmp->z;

		side_a = sqrt( pow(v[0][0] - v[1][0],2) + pow(v[0][1] - v[1][1],2) + pow(v[0][2] - v[1][2],2));//a
		side_b = sqrt( pow(v[0][0] - v[2][0],2) + pow(v[0][1] - v[2][1],2) + pow(v[0][2] - v[2][2],2)); //b
		side_c = sqrt( pow(v[1][0] - v[2][0],2) + pow(v[1][1] - v[2][1],2) + pow(v[1][2] - v[2][2],2)); //c

		if(side_a > side_b AND side_a > side_c){
			greater_side = side_a;
		}
		else {
			if (side_b > side_a AND side_b > side_c){
				greater_side = side_b;
			}
			else greater_side = side_c;
		}
		cout << "area:" << area << endl;
		cout << "(area/greater_side):" << (area/greater_side) << endl;
		if((area/greater_side) < t){
			(*list).push_back(*FIter);
			if (ee < (area/greater_side)){
				ee = (area/greater_side);
			}
		}
		vvlist.clear();
	}

}
//-----------------------------------------------------------------------------------------
double Mesh::computeFaceArea(VertexID vA, VertexID vB, VertexID vC, double *n){

	Point3D *tmp;
	double p1[3], p2[3], p3[3], p1p2[3], p1p3[3], nn;

	tmp=(Point3D *)getVGeomPtr (vA);
	p1[0] = (double)tmp->x;   p1[1] = (double)tmp->y;
	p1[2] = (double)tmp->z;

	tmp=(Point3D *)getVGeomPtr (vB);
	p2[0] = (double)tmp->x;   p2[1] = (double)tmp->y;
	p2[2] = (double)tmp->z;

	tmp=(Point3D *)getVGeomPtr (vC);
	p3[0] = (double)tmp->x;  p3[1] = (double)tmp->y;
	p3[2] = (double)tmp->z;

	//p1p2
	p1p2[0] = p2[0] - p1[0]; p1p2[1] = p2[1] - p1[1]; p1p2[2] = p2[2] - p1[2];
	//p1p3
	p1p3[0] = p3[0] - p1[0]; p1p3[1] = p3[1] - p1[1]; p1p3[2] = p3[2] - p1[2];

	//|p1p2 x p1p3|
	n[0] = p1p2[1]*p1p3[2] - p1p3[1]*p1p2[2];
	n[1] = p1p2[2]*p1p3[0] - p1p2[0]*p1p3[2];
	n[2] = p1p3[1]*p1p2[0] - p1p2[1]*p1p3[0];

	//norma
	nn = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
	return (nn/2.0);
}
//-----------------------------------------------------------------------------------------
void Mesh::update_normal(){

	Point3D *ptr;
	double n[3], mod;
	vector<FaceID> Flist;
	vector<FaceID>::iterator FIter;
	vector<VertexID> vvlist;

	getAllFaces (&Flist);
	//cout << "up normal" << endl;

	for ( FIter = Flist.begin(); FIter != Flist.end(); FIter++) {
		getFVertices (*FIter, &vvlist);
		computeFaceArea(vvlist[0], vvlist[1], vvlist[2], n);
		/*Normalizar*/
		mod = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
		if (mod > FLT_MIN){
			n[0] /= mod; n[1] /= mod; n[2] /= mod;
		}
		//cout << "computed" << endl;
		//cout << "face:" << *FIter << endl;
		ptr = ((Point3D *)getFGeomPtr(*FIter));
		if (ptr){
			//cout << "ptr->nx" << ptr->nx << endl;
			//cout << "ptr" << endl;
			ptr->nx = n[0]; ptr->nx = n[1]; ptr->nx = n[2];
			cout << "ptr2" << endl;
			setFGeomPtr (*FIter, ptr);
			//cout << "ptr3" << endl;
		}
		vvlist.clear();
	}
	//cout << "sai up normal" << endl;
}
//-----------------------------------------------------------------------------------------
void Mesh::checkRateTopology (vector<FaceID> *list) {

	vector<FaceID> Flist;
	vector<FaceID>::iterator FIter;
	vector<VertexID> vvlist;
	vector<VertexID>::iterator vvIter;
	Point3D *tmp;
	double d;
	double base, heigth;
	double side_a, side_b, side_c;
	double v[3][3];
	double rate;
	double angle_A, angle_B, angle_C;

	getAllFaces (&Flist);

	for ( FIter = Flist.begin(); FIter != Flist.end(); FIter++) {
		/// check whether the normal vector is inverted
		getFVertices (*FIter, &vvlist);

		d = computeSignedFaceArea(vvlist[0], vvlist[1], vvlist[2]);

		tmp=(Point3D *)getVGeomPtr (vvlist[0]);
		v[0][0] = (double)tmp->x;   v[0][1] = (double)tmp->y;
		v[0][2] = (double)tmp->z;
		tmp=(Point3D *)getVGeomPtr (vvlist[1]);
		v[1][0] = (double)tmp->x;   v[1][1] = (double)tmp->y;
		v[1][2] = (double)tmp->z;
		tmp=(Point3D *)getVGeomPtr (vvlist[2]);
		v[2][0] = (double)tmp->x;   v[2][1] = (double)tmp->y;
		v[2][2] = (double)tmp->z;

		base = sqrt( pow(v[0][0] - v[1][0],2) + pow(v[0][1] - v[1][1],2) + pow(v[0][2] - v[1][2],2));//a

		side_a = base;

		side_b = sqrt( pow(v[0][0] - v[2][0],2) + pow(v[0][1] - v[2][1],2) + pow(v[0][2] - v[2][2],2)); //b

		side_c = sqrt( pow(v[1][0] - v[2][0],2) + pow(v[1][1] - v[2][1],2) + pow(v[1][2] - v[2][2],2)); //c

		PRINT(cout << "side a:" << side_a << endl);
		PRINT(cout << "side b:" << side_b << endl);
		PRINT(cout << "side c:" << side_c << endl);

		//lei dos cossenos
		angle_A = (((side_a*side_a) - (side_b*side_b) - (side_c*side_c)) / (-2*side_b*side_c));
		angle_B = (((side_b*side_b) - (side_a*side_a) - (side_c*side_c)) / (-2*side_a*side_c));
		angle_C = (((side_c*side_c) - (side_a*side_a) - (side_b*side_b)) / (-2*side_a*side_b));

		angle_A = acos(angle_A);
		angle_B = acos(angle_B);
		angle_C = acos(angle_C);

		angle_A = (angle_A * 180)/M_PI;
		angle_B = (angle_B * 180)/M_PI;
		angle_C = (angle_C * 180)/M_PI;

		heigth = (2*fabs(d))/base;
		rate = heigth/base;
		//base is h = 2A/b
		//A = d
		if (d < EPS) {//verificar esse limite
			(*list).push_back(*FIter);
		}
		else{
			if (angle_A < 15 || angle_B < 15 || angle_C < 15) {
				(*list).push_back(*FIter);
			}
		}
		vvlist.clear();
	}

	Flist.clear();
}
//-----------------------------------------------------------------------------------------
double Mesh::evaluate(HalfEdge *hh){

	Vertex *v0, *v1;
	Matrix16 qs;
	Point3D *pt;
	double x, y, z;

	//cout << "calcula" << endl;
	v0 = hh->origin;
	v1 = (hh->next)->origin;
	/*cout << "calcula 2" << endl;
	if(v0)cout << "v0 nao nulo" << endl;
	else cout << "v0 nulo" << endl;

	if(v1)cout << "v1 nao nulo" << endl;
	else cout << "v1 nulo" << endl;*/

//	cout << "v1:" << v1->ID << endl;
//	cout << "v0v1:" << v0->ID << " " << v1->ID << endl;
  //	cout << "v1:" << v1->q.m[1] << endl;

	//Matrix_print("v0", v0->q);
	//Matrix_print("v1", v1->q);

	qs = Matrix_sum(v0->q, v1->q);
	//cout << "calcula 3" << endl;
	pt = (Point3D *)getVGeomPtr (v0->ID);
	x = pt->x; y = pt->y; z = pt->z;

	return qs.m[0]*x*x +  2*qs.m[1]*x*y  +  2*qs.m[2]*x*z   +   2*qs.m[3]*x
			           +    qs.m[5]*y*y  +  2*qs.m[6]*y*z   +   2*qs.m[7]*y
			                             +    qs.m[10]*z*z  +   2*qs.m[11]*z
			                                                +     qs.m[15];
}
//-----------------------------------------------------------------------------------------
void Mesh::computeQ(Vertex *v){

	HalfEdge *h, *hiter;
	int vd[3];
	double a, b, c, d;

	memset(v->q.m, 0, sizeof(v->q.m));

	h = v->half;
	//if(h)cout << "h nao nulo" << endl;
	//else cout << "h nulo" << endl;
	hiter = h;
	do{
		//cout << "do" << endl;
		if(hiter->left != _outer_loop){
			//cout << "IF" << endl;

			vd[0] = hiter->origin->ID;
			//cout << "vd0:" << vd[0] << endl;
			vd[1] = hiter->next->origin->ID;
			//cout << "vd1:" << vd[1] << endl;
			vd[2] = hiter->previous->origin->ID;
			//cout << "vd2:" << vd[2] << endl;
			//cout << "vs:" << vd[0] << " " << vd[1] << " " << vd[2] << endl;
			computePlane(vd[0], vd[1], vd[2], a, b, c, d);
			v->q.m[0]  += a*a; v->q.m[1]  += a*b; v->q.m[2]  += a*c; v->q.m[3]  += a*d;
			v->q.m[4]  += a*b; v->q.m[5]  += b*b; v->q.m[6]  += b*c; v->q.m[7]  += b*d;
			v->q.m[8]  += a*c; v->q.m[9]  += b*c; v->q.m[10] += c*c; v->q.m[11] += c*d;
			v->q.m[12] += a*d; v->q.m[13] += b*d; v->q.m[14] += c*d; v->q.m[15] += d*d;
		}

		hiter = hiter->mate->next;
	//	if(!hiter)cout << "hiter nulo" << endl;

	}while(h != hiter);
}
//-----------------------------------------------------------------------------------------
void Mesh::update(vector <node *> *f, double err, pair <VertexID, VertexID> p){

	vector<VertexID> vlist;
	vector<VertexID>::iterator vIter;
	vector<FaceID> flist;
	Vertex *v;
	HalfEdge *h, *hiter;
	double cost;
	vector <int> pos;
	node *pp;
	int rv;

	//Descobre qual vertice foi removido
	if(vertices.find(p.first) == vertices.end()){
		v = vertices.find(p.second)->second;
		rv = p.first;
	}
	else{
		v = vertices.find(p.first)->second;
		rv = p.second;
	}

	//cout << "Vertice que ficou:" << v->ID << endl;
	//cout << "rv:" << rv << endl;

	getAllVertices (&vlist);
	for (vIter = vlist.begin(); vIter != vlist.end(); vIter++) {
		if(vertices.find(*vIter) != vertices.end()){
			//cout << "viter:" << *vIter << endl;
			computeQ(vertices.find(*vIter)->second);
			//cout << "--sai computeQ--" << endl;
		}
	}

	//cout << "fim for" << endl;
	for (int i = 0; i < (*f).size(); i++){
		pp = (*f)[i];
		if(pp->v0 == rv OR pp->v1 == rv){
			pos.push_back(i);
		}
	}
/*	cout << "vetor" << endl;
	for (int i = 0; i < (*f).size(); i++){
		cout << "(" << (*f)[i]->v0 << " " << (*f)[i]->v1 << ") ";
	}
	cout << endl;

	cout << "Pares q serao retirados" << endl;
	for (int i = 0; i < pos.size(); i++){
		cout << pos[i] << ":"<< "(" << (*f)[pos[i]]->v0 << " " << (*f)[pos[i]]->v1 << ")  ";
	}
	cout << endl;

	cout << "--apagar--" << endl;
	cout << "tam vetor:" << (*f).size() << endl;
	cout << "numeros de pares:" << pos.size() << endl;*/
	for (int i = 0; i < pos.size(); i++){

		/*for (int ii = 0; ii < (*f).size(); ii++){
			cout << ii << ":" << "(" << (*f)[ii]->v0 << " " << (*f)[ii]->v1 << ") " << endl;
		}*/

		/*cout << "indice par:" << pos[i] << endl;
		cout << "par:" << "(" << (*f)[pos[i]]->v0 << " " << (*f)[pos[i]]->v1 << ") "<<endl;
		cout << "-----------" << endl;*/
		(*f).erase((*f).begin()+pos[i]);

		for (int ii = i+1; ii < pos.size(); ii++){
			//cout << "pos[i] > pos[ii]:" << pos[i] << " " << pos[ii] << endl;
			if (pos[i] < pos[ii]){
				//cout << "pos[ii]:" << pos[ii] << endl;
				pos[ii]--;
			}
		}

	}

	//atualizar halfedges
	for (int i = 0; i < (*f).size(); i++){
		(*f)[i]->h = findHalf((*f)[i]->v0, (*f)[i]->v1);
	}
	//atualizar custo
	for (int i = 0; i < (*f).size(); i++){
		(*f)[i]->c = evaluate((*f)[i]->h);
	}

	sort((*f).begin(), (*f).end(), compare_q);
	cout << "---fim up ----" << endl;


}
//-----------------------------------------------------------------------------------------
void Mesh::make_pairs(vector<FaceID> list, vector <node *> *f){

	vector<VertexID> vlist;
	vector<VertexID>::iterator vIter;
	vector<FaceID> flist;
	vector<FaceID>::iterator fIter;
	double minn, c;
	HalfEdge *h1, *h2, *h3;
	node *ptr;
	node *pr = new node;

	for ( fIter = list.begin(); fIter != list.end(); fIter++) {
		getFVertices (*fIter, &vlist);
		minn = DBL_MAX;
		cout << "--+++++++++++++++++------" << endl;
		h1 = findHalf(vlist[0], vlist[1]);
		c = evaluate(h1);
		cout << "(" << vlist[0] << " " << vlist[1] << ")" << endl;
		cout << "c:" << c << endl;
		if(c < minn && c > 0 && h1->left != _outer_loop && h1->mate->left != _outer_loop AND ok_collapse(h1)){
			minn = c;
			pr->v0 = vlist[0];
			pr->v1 = vlist[1];
			pr->h = h1;
			pr->c = c;
		}

		h2 = findHalf(vlist[0], vlist[2]);
		c = evaluate(h2);
		cout << "(" << vlist[0] << " " << vlist[2] << ")" << endl;
		cout << "c:" << c << endl;
		if (c < minn && c > 0  && h2->left != _outer_loop && h2->mate->left != _outer_loop AND ok_collapse(h2)){
			minn = c;
			pr->v0 = vlist[0];
			pr->v1 = vlist[2];
			pr->h = h2;
			pr->c = c;
		}

		h3 = findHalf(vlist[1], vlist[2]);
		c = evaluate(h3);
		cout << "(" << vlist[1] << " " << vlist[2] << ")" << endl;
		cout << "c:" << c << endl;
		if(c < minn && c > 0 && h3->left != _outer_loop && h3->mate->left != _outer_loop AND ok_collapse(h3)){
			minn = c;
			pr->v0 = vlist[1];
			pr->v1 = vlist[2];
			pr->h = h3;
			pr->c = c;
		}
		cout << "--+++++++++++++++++------" << endl;
		if (minn != DBL_MAX){

			ptr = new node;
			ptr->v0 = pr->v0;
			ptr->v1 = pr->v1;
			ptr->c = minn;
			ptr->h = pr->h;

			(*f).push_back(ptr);

			ptr = new node;
			ptr->v0 = (*f)[(*f).size()-1]->v1;
			ptr->v1 = (*f)[(*f).size()-1]->v0;
			ptr->c = minn;
			ptr->h = (*f)[(*f).size()-1]->h->mate;
			(*f).push_back(ptr);
		}

		vlist.clear();
	}
}
//-----------------------------------------------------------------------------------------
void Mesh::decimater_(vector<FaceID> flist, int min_f, void (func)(void)){

	node *ptr;
	vector<VertexID> vlist;
	vector<FaceID>::iterator fIter;
	vector<node *> p_q;
	vector<VertexID>::iterator vIter;
	Vertex *v;
	HalfEdge *h, *hiter;
	double cost;
	node *pp;
	pair <VertexID, VertexID> v0v1;
	int len;


	for (fIter = flist.begin(); fIter != flist.end(); fIter++){
		getFVertices (*fIter, &vlist);

		computeQ(vertices.find(vlist[0])->second);
		computeQ(vertices.find(vlist[1])->second);
		computeQ(vertices.find(vlist[2])->second);
		vlist.clear();
	}

	//inicializar heap (heap ainda nao esta implementado)
	make_pairs(flist, &p_q);
	sort(p_q.begin(), p_q.end(), compare_q);

	for(int kk = 0; kk < p_q.size(); kk++){
		cout << "(" << p_q[kk]->v0 << " " << p_q[kk]->v1 << "):" << p_q[kk]->c << endl;
	}
	getchar();
	cout << "realizando collapse" << endl;

	while (p_q.size() > 0){
		pp = p_q[p_q.size()-1];
		cout << "retira:" << "(" << pp->v0 << " " << pp->v1 << ")" << endl;
		cout << "custo:" << pp->c << endl;
		p_q.pop_back();

		if (vertices.find(pp->v0) != vertices.end() && vertices.find(pp->v1) != vertices.end()){
			cout << "-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=" << endl;
			v0v1.first = pp->v0;
			//if(v0v1.first == 5)return;
			cout << "v0v1.first:" << v0v1.first << endl;
			cout << "-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=" << endl;
			v0v1.second = pp->v1;
			cout << "v0v1.second:" << v0v1.second << endl;
			cout << "IF" << endl;

			//if(pp->h->left != _outer_loop && pp->h->mate->left != _outer_loop){
				if(pp->h->mate->previous->mate->left != _outer_loop && pp->h->next->mate->left != _outer_loop && !Point_Border (pp->h->next)){
					cout << "ENTRA" << endl;
					eliminate_edge(pp->h, &h);
					cout << "update 1" << endl;
					update(&p_q, 1000, v0v1);
					cout << "vetor atualizado" << endl;
					for (int ii = 0; ii < p_q.size(); ii++){
						cout << "(" << p_q[ii]->v0 << " " << p_q[ii]->v1 << "):" << p_q[ii]->c << endl;
					}
				}
			//}
		}
		delete pp;
	}

}
//-----------------------------------------------------------------------------------------
void Mesh::decimater(double err, int min_f, void (func)(void)){

	node *ptr;
	vector<VertexID> vlist;
	vector<FaceID> flist;
	vector<FaceID>::iterator fIter;
	vector<node *> p_q;
	vector<VertexID>::iterator vIter;
	Vertex *v;
	HalfEdge *h, *hiter;
	double cost, bestcost;
	node *pp;
	pair <VertexID, VertexID> v0v1;
	int len;

	getAllFaces (&flist);
	intialize(flist);
	cout << "ERR:" << err << endl;
	//inicializar heap (heap ainda nao esta implementado)
	getAllVertices (&vlist);
	for (vIter = vlist.begin(); vIter != vlist.end(); vIter++) {
		v = vertices.find(*vIter)->second;
		h = v->half;
		hiter = h;
		bestcost = FLT_MAX;
		do{
			cost = evaluate(hiter);
			//cout << "=-=-=-=-=-=-=-=-=--=-=" << endl;
			cout << "cost:" << cost << endl;
			//cout << "best:" << bestcost << endl;
			//cout << "err:" << err << endl;
		//	cout << "=-=-=-=-=-=-=-=-=--=-=" << endl;
			if(cost >= 0 && cost < bestcost && cost < err){
				ptr = new node;
				ptr->h = hiter;
				ptr->v0 = hiter->origin->ID;
				ptr->v1 = hiter->next->origin->ID;
				ptr->c = cost;
				p_q.push_back(ptr);
				bestcost = cost;
				cout << "insere ";
				cout << "(" << hiter->origin->ID << " " << hiter->next->origin->ID << "):" << cost << endl;
			}
			hiter = hiter->mate->next;
		}while(h != hiter);

	}
	sort(p_q.begin(), p_q.end(), compare_q);

	for(int kk = 0; kk < p_q.size(); kk++){
		cout << "(" << p_q[kk]->v0 << " " << p_q[kk]->v1 << "):" << p_q[kk]->c << endl;
	}

	cout << "realizando collapse" << endl;

	int contt = 0;
	while (p_q.size() > 0){
		pp = p_q[p_q.size()-1];
		cout << "retira:" << "(" << pp->v0 << " " << pp->v1 << ")" << endl;
		cout << "custo:" << pp->c << endl;
		p_q.pop_back();

		if (vertices.find(pp->v0) != vertices.end() && vertices.find(pp->v1) != vertices.end()){
			cout << "-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=" << endl;
			v0v1.first = pp->v0;
			//if(v0v1.first == 5)return;
			cout << "v0v1.first:" << v0v1.first << endl;
			cout << "-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=" << endl;
			v0v1.second = pp->v1;
			cout << "v0v1.second:" << v0v1.second << endl;
			cout << "IF" << endl;

			if(pp->h->left != _outer_loop && pp->h->mate->left != _outer_loop){
				if(pp->h->mate->previous->mate->left != _outer_loop && pp->h->next->mate->left != _outer_loop && !Point_Border (pp->h)){
					cout << "ENTRA" << endl;
					eliminate_edge(pp->h, &h);
					cout << "update 1" << endl;
					update(&p_q, err, v0v1);
				}
			}
		}
		delete pp;
		contt++;
	}

}

//-----------------------------------------------------------------------------------------
void Mesh::decimate(double t, void (func)(void), int min){

	Vertex *v,*vtt;
	HalfEdge *half, *h_tmp;
	vector<VertexID> list;
	vector<pair<Point3D*, int> > vbarra;
	Matrix16 Q[500];
	vector<pair<VertexID, VertexID> > list_pair;
	vector<pair<double,int> >cost;
	map<int, int> hash_ind;
	VertexID vi, n_faces;

	cout << "Aqui"<< endl;
	/*Teste*/
	vtt = vertices.find(0)->second;
	HalfEdge *h_tt, *h_tt2;
	h_tt = vtt->half;

	do{
		cout << h_tt->origin->ID << endl;
		cout << "left:" << h_tt->left->ID << endl;
		h_tt2 = h_tt->next;
		while(h_tt2 != h_tt){
			cout << h_tt2->origin->ID << " ";
			h_tt2 = h_tt2->next;
		}
		cout << endl;
		h_tt = h_tt->mate->next;
	}
	while(h_tt!=vtt->half);


	getAllVertices(&list);
	n_faces = list.size();
	if(n_faces < min) return;

	for (int i = 0; i < list.size(); i++){
		hash_ind[list[i]] = i;
	}
	computeQ(list, Q, hash_ind);
	validPairs(list, t, &list_pair);
	computeCost(list_pair, &cost, &vbarra, Q);

	sort(cost.begin(), cost.end(), compare);
	//Iteratively remove the pair (v1,v2) of least cost fromthe heap,
	//contract this pair, AND UPDATE the costs of all valid pairs involv-
	//ing v1
	cout << "size vet:" << cost.size() << endl;
/*
	for (int ii = cost.size()-1; ii >= 0; ii--){
		cout << list_pair[(cost[ii].second)].first << " " << list_pair[(cost[ii].second)].second << endl;
	}
	return;
	*/
	bool flag = 1;
	while(flag && n_faces > min){
		cout << "vai pegar half" << endl;
		cout << "par:" << list_pair[(cost[cost.size()-1].second)].first << " " << list_pair[(cost[cost.size()-1].second)].second << endl;
		vi = list_pair[(cost[cost.size()-1].second)].first;
		half = findHalf(list_pair[(cost[cost.size()-1].second)].first, list_pair[(cost[cost.size()-1].second)].second);
		cout << "pegou half" << endl;
		cost.pop_back();
		if (half){
			cout << "par valido" << endl;
			if(half->mate->left->type == 2){
				cout << "Half na borda" << endl;
				half = half->mate;
			}
			if(half->left != _outer_loop){
				v = vertices.find(half->origin->ID)->second;
				cout << "v = vertice" << endl;
				v->GeomPtr = vbarra[(cost[cost.size()-1].second)].first;
				cout << "Vertice modificado:" << half->mate->origin->ID << endl;
				cout << "aqui2" << endl;
				cout << "Left:" << _outer_loop->ID << endl;
				cout << "Left 1:" << half->left->ID << endl;
				cout << "Left 2:" << half->mate->left->ID << endl;
				eliminate_edge(half, &h_tmp);
				n_faces--; // pode ser q tenha removido mais que uma
			}
			/*Update*/
			cout << "UPDATE" << endl;
			updateCost(vi, Q, &cost, &vbarra, &list_pair,t, hash_ind);
			sort(cost.begin(), cost.end(), compare);
		}
		else {
			cout << "par invalido" << endl;
		}


		//--------
		if(cost.size() == 0)flag = 0;

		cout << "size vet:" << cost.size() << endl;
	}

	list_pair.clear();
	hash_ind.clear();

	cout << "fim decimate" << endl;
}
//---------------------------------------------------------------
void Mesh::updateCost(VertexID vid, Matrix16 *Qq,vector<pair<double,int> >*c, vector<pair<Point3D*, int> > *vb, vector<pair<VertexID, VertexID> > *lp, double t, map<int, int> hash){

	vector<VertexID> np;
	vector<int> index_pair;
	Matrix16 qsum, qinv;
	Point3D *pt, *tmp;
	double cost;

	for(int i = 0; i < (*lp).size(); i++){
		if((*lp)[i].first == vid || (*lp)[i].second == vid){

			if((*lp)[i].first == vid){
				if(vertices.find((*lp)[i].second) != vertices.end()){
					np.push_back((*lp)[i].second);
					index_pair.push_back(i);
				}
			}
			else{
				if((*lp)[i].second == vid){
					if(vertices.find((*lp)[i].first) != vertices.end()){
						np.push_back((*lp)[i].first);
						index_pair.push_back(i);
					}
				}
			}
		}
	}
	//os vertices que tem que atualizar
	computeQ(np, Qq, hash);
	//matrix Q atualizada

	for (int i = 0; i < index_pair.size(); i++){//atualizando custos

		qsum =  Matrix_sum(Qq[(*lp)[index_pair[i]].first], Qq[(*lp)[index_pair[i]].second]);//Qi + Qj
		qsum.m[12] = 0; qsum.m[13] = 0; qsum.m[14] = 0; qsum.m[15] = 1;
		pt = new Point3D;
		if (Matrix_determinant(qsum)){//inverse?
			qinv = Matrix_inverted(qsum);
			pt->x = qinv.m[3]; pt->y = qinv.m[7]; pt->z = qinv.m[11];
		}
		else{
			tmp = (Point3D *)(getVGeomPtr((*lp)[index_pair[i]].first));
			pt->x = tmp->x; pt->y = tmp->y; pt->z = tmp->z;
		}
		(*vb)[index_pair[i]].first = pt;
		(*vb)[index_pair[i]].second = index_pair[i];

		cost = (qsum.m[0]*pt->x*pt->x) + (2*qsum.m[1]*pt->x*pt->y) + (2*qsum.m[2]*pt->x*pt->z) +
				(2*qsum.m[3]*pt->x) + (qsum.m[5]*pt->y*pt->y) + (2*qsum.m[6]*pt->y*pt->z) + (qsum.m[7]*pt->y) +
				(qsum.m[10]*pt->z*pt->z) + (2*qsum.m[11]*pt->z) + qsum.m[15];

		(*c)[index_pair[i]].first = cost;
		(*c)[index_pair[i]].second = index_pair[i];
	}
}
//---------------------------------------------------------------
void Mesh::computeQ(vector<VertexID> list, Matrix16 *Q, map<int, int> hash){
	Vertex *v;
	HalfEdge *half_plane, *half_iter, *half_adj, *h;
	VertexID vA, vB, vC;
	double a, b, c, d;
	int id_q;

	id_q = 0;
	for(int i = 0; i < list.size(); i++){
		v = vertices.find(list[i])->second;//pega o vertice x
		half_iter = h = v->half;
		id_q = hash.find(v->ID)->second;
		memset(Q[id_q].m,0,sizeof(Q[id_q].m));
		do{//distancia do ponto para os planos
			half_plane = half_iter->next;
			do{
				if(half_plane->left->type != 2){
					vA = half_plane->origin->ID;
					vB = half_plane->next->origin->ID;
					vC = half_plane->previous->origin->ID;
					computePlane(vA, vB, vC, a, b, c, d);
					Q[id_q].m[0]  += a*a; Q[id_q].m[1]  += a*b; Q[id_q].m[2]  += a*c; Q[id_q].m[3]  += a*d;
					Q[id_q].m[4]  += a*b; Q[id_q].m[5]  += b*b; Q[id_q].m[6]  += b*c; Q[id_q].m[7]  += b*d;
					Q[id_q].m[8]  += a*c; Q[id_q].m[9]  += b*c; Q[id_q].m[10] += c*c; Q[id_q].m[11] += c*d;
					Q[id_q].m[12] += a*d; Q[id_q].m[13] += b*d; Q[id_q].m[14] += c*d; Q[id_q].m[15] += d*d;
				}
				half_plane = half_plane->mate->next;
			}while(half_plane != half_iter->next);
			half_iter = half_iter->mate->next;
		}while(half_iter != h);
	}
}
//-----------------------------------------------------------------------------------------
HalfEdge *Mesh::findHalf(VertexID vid, HalfEdge *half){

	HalfEdge *half_iter;
	if (!half)cout << "half nulo" << endl;
	half_iter = half;
	do{
		cout << "half_iter->origin->ID:" << half_iter->origin->ID << endl;
		if (half_iter->origin->ID == vid)return half_iter;
		half_iter = half_iter->next;
	}while(half_iter != half);
}
//-----------------------------------------------------------------------------------------
void Mesh::validPairs(vector<VertexID> list, double tt, vector<pair<VertexID, VertexID> > *list_p){

	Vertex *v;
	HalfEdge  *half_iter, *half_adj;
	Point3D *tmp, *tmp2;
	double len;
	(*list_p).clear();

	for(int i = 0; i < list.size(); i++){
		v = vertices.find(list[i])->second;//pega o vertice x
		half_iter = v->half;
		half_adj = half_iter;
		do{
			if(half_adj->left->type!=2){
				tmp = (Point3D *)(getVGeomPtr(list[i]));
				tmp2 = (Point3D *)(getVGeomPtr(half_adj->next->origin->ID));
				len = sqrt( (tmp->x - tmp2->x)*(tmp->x - tmp2->x) + (tmp->y - tmp2->y)*(tmp->y - tmp2->y) + (tmp->z - tmp2->z)*(tmp->z - tmp2->z) );
				if (len < tt){
					(*list_p).push_back(make_pair(list[i],half_adj->next->origin->ID));
				}
			}
			half_adj = half_adj->mate->next;
		}while(half_adj != half_iter);
	}
}
//-----------------------------------------------------------------------------------------
HalfEdge *Mesh::findHalf(HalfEdge *half){
	HalfEdge *half_t;

	half_t = half->next;
	while (half_t != half){
		if (ok_collapse(half_t))return half_t;
		half_t = half_t->next;
	}
	return 0;
}
//-----------------------------------------------------------------------------------------
void Mesh::computeCost(vector<pair<VertexID, VertexID> > list_p, vector< pair<double,int> > *c, vector<pair<Point3D *, int> > *v_b, Matrix16 *q){

	Matrix16 qsum, qinv;
	Point3D *pt, *tmp;
	Vertex *v;
	double cost;

	for (int i = 0; i < list_p.size(); i++){//pair(vi, vj)
		qsum =  Matrix_sum(q[list_p[i].first], q[list_p[i].second]);//Qi + Qj
		qsum.m[12] = 0; qsum.m[13] = 0; qsum.m[14] = 0; qsum.m[15] = 1;
		pt = new Point3D;
		if (Matrix_determinant(qsum)){//inverse?
			qinv = Matrix_inverted(qsum);
			pt->x = qinv.m[3]; pt->y = qinv.m[7]; pt->z = qinv.m[11];
		}
		else{
			tmp = (Point3D *)(getVGeomPtr(list_p[i].first));
			pt->x = tmp->x; pt->y = tmp->y; pt->z = tmp->z;
		}
		(*v_b).push_back(make_pair(pt,i));

		cost = (qsum.m[0]*pt->x*pt->x) + (2*qsum.m[1]*pt->x*pt->y) + (2*qsum.m[2]*pt->x*pt->z) +
			   (2*qsum.m[3]*pt->x) + (qsum.m[5]*pt->y*pt->y) + (2*qsum.m[6]*pt->y*pt->z) + (qsum.m[7]*pt->y) +
			   (qsum.m[10]*pt->z*pt->z) + (2*qsum.m[11]*pt->z) + qsum.m[15];
		(*c).push_back(make_pair(cost, i));
	}
}
//-----------------------------------------------------------------------------------------
VertexID Mesh::Reduce(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) ) {

	VertexID index;
	bool cross;
	Loop *f;
	vector<FaceID> Flist;
	HalfEdge *half, *half_a, *half_face, *h, *half_t;
	int a, b, c; // vertices do triangulo
	double ab, bc, ca, vd[3];
	int id_v1, id_v2;
	Point3D *p1, *p2, *n_point;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}

	index = -1;
	half = f->half;
	a = Point_Border (half);
	b = Point_Border (half->next);
	c = Point_Border (half->previous);

	//calcula tamanho dos lados
	ab = dist_point(half->origin->ID, half->next->origin->ID);
	bc = dist_point(half->next->origin->ID, half->previous->origin->ID);
	ca = dist_point(half->previous->origin->ID, half->origin->ID);

	getAllFaces (&Flist);
	cross = 0;
	//Caso 1 e 2: Nenhum ponto na borda ou os 3 pontos na borda
	if (a + b + c == 0 || a + b + c == 3) {//tratando do mesmo jeito por enquanto nenhum e todos na borda
		if (a + b + c == 0) {
			if (ok_collapse(half)){
				index = eliminate_edge(half, &half_face);
			}
			else {
				half_t = findHalf(half);
				if(half_t)
					index = eliminate_edge(half_t, &half_face);
			}
		}
	}
	else {
		if (a + b + c == 2){
			if (a && b){ // a e b na borda
				if (ok_collapse(half->next)){
					index = eliminate_edge(half->next, &half_face);
				}
				else {
					half_t = findHalf(half->next);
					if(half_t)
						index = eliminate_edge(half_t,  &half_face);
				}
			}
			else{
				if (b && c){
					if (ok_collapse(half->previous)){
						index = eliminate_edge(half->previous,  &half_face);
					}
					else {
						half_t = findHalf(half->previous);
						if(half_t)
							index = eliminate_edge(half_t,  &half_face);
					}
				}
				else {
					if (ok_collapse(half)){
						index = eliminate_edge(half->previous,  &half_face);
					}
					else {
						half_t = findHalf(half);
						if(half_t)
							index = eliminate_edge(half_t,  &half_face);
					}
				}
			}
		}
		else{
			if (a){
				if (ok_collapse(half->next)){
					index = eliminate_edge(half->next,  &half_face);
				}
				else {
					half_t = findHalf(half->next);
					if(half_t)
						index = eliminate_edge(half_t,  &half_face);
				}
			}
			else {
				if (b){
					if (ok_collapse(half->previous)){
						index = eliminate_edge(half->previous, &half_face);
					}
					else {
						half_t = findHalf(half->previous);
						if(half_t)
							index = eliminate_edge(half_t, &half_face);
					}
				}
				else {
					if (ok_collapse(half)){
						index = eliminate_edge(half, &half_face);
					}
					else {
						half_t = findHalf(half);
						if(half_t)
							index = eliminate_edge(half_t, &half_face);
					}
				}
			}
		}
	}
	return index;
}
//VertexID Mesh::Reduce(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) ) {
//
//	PRINT(cout << "Reduce\n");
//	VertexID index;
//	bool cross;
//	Loop *f;
//	vector<FaceID> Flist;
//	HalfEdge *half, *half_a, *half_face, *h;
//	int a, b, c; // vertices do triangulo
//	double ab, bc, ca, vd[3];
//	int id_v1, id_v2;
//	Point3D *p1, *p2, *n_point;
//
//	lIterator = loops.find(ID);
//	if (lIterator != loops.end()) {
//		f = lIterator->second;
//		if (f->type != 1) {
//			cout << ID << " is not a face." << endl;
//			return -1;
//		}
//	} else {
//		cout << "Face "<< ID << " does not exist." << endl;
//		return -1;
//	}
//
//	index = -1;
//	half = f->half;
//	a = Point_Border (half);
//	b = Point_Border (half->next);
//	c = Point_Border (half->previous);
//
//	cout << "a:" << half->origin->ID << endl;
//	cout << "b:" << half->next->origin->ID << endl;
//	cout << "c:" << half->previous->origin->ID << endl;
//
//	//calcula tamanho dos lados
//	ab = dist_point(half->origin->ID, half->next->origin->ID);
//	bc = dist_point(half->next->origin->ID, half->previous->origin->ID);
//	ca = dist_point(half->previous->origin->ID, half->origin->ID);
//
//	getAllFaces (&Flist);
//	cout << "ab:" << ab << " bc:" << bc << " ca:" << ca << endl;
//
//	cross = 0;
//	//if (makeFlip(ab, bc, ca)) {
//	if (0) {
//		cout << "Flip" << endl;
//		if (ab >bc && ab > ca) {
//			cout << "ab:" << half->origin->ID << endl;
//			flipEdge(half);
//		}
//		else {
//			if (bc > ab && bc > ca) {
//				cout << "bc:" << half->origin->ID << endl;
//				flipEdge(half->next);
//			}
//			else {
//				cout << "ca:" << half->origin->ID << endl;
//				flipEdge(half->previous);
//			}
//		}
//	}
//	else{
//		//Caso 1 e 2: Nenhum ponto na borda ou os 3 pontos na borda
//		if (a + b + c == 0 || a + b + c == 3) {
//
//			if (a + b + c == 0) {
//				cout << "3 pontos" << endl;
//				if (equal_sides(ab, bc, ca)) { //lados mais ou menos iguais
//					//cross = crossEdge(Flist,half);
//					if (cross)cout << "CRUZA ARESTA" << endl;
//					else cout << "NAO CRUZA ARESTA" << endl;
//					cout << "Elimina FACE" << endl;
//					index = eliminate_edge(half, func, &half_face);
//					cout << "INDEX:" << index << endl;
//					//cross = crossEdge(Flist,half_face);
//					if (cross)cout << "CRUZA ARESTA" << endl;
//					else cout << "NAO CRUZA ARESTA" << endl;
//					//index = eliminate_edge(half_face, func, &h);
//					cout << "INDEX:" << index << endl;
//				}
//				else {//fecha menor lado
//					cout << "Fecha MENOR" << endl;
//					if (ab < bc && ab < ca){
//						//ab menor lado
//						PRINT(cout << "Menor lado ab\n");
//						half = SameFace(half);
//					}
//					else {
//						if (bc < ab && bc < ca) {
//							//bc menor lado
//							PRINT(cout << "Menor lado bc\n");
//							half = SameFace(half->next);
//						}
//						else {
//							//ca menor lado
//							PRINT(cout << "Menor lado ca\n");
//							half = SameFace(half->previous);
//						}
//					}
//					/*
//					 * Se cruza aresta
//					 */
//					//cross = crossEdge(Flist,half);
//					if (cross)cout << "CRUZA ARESTA" << endl;
//					else cout << "NAO CRUZA ARESTA" << endl;
//					index = eliminate_edge(half, func, &half_face);
//
//				}
//			}
//			else {
//				cout << "3 pontos" << endl;
//				if (ab < bc && ab < ca) {
//					half_a = SameFace(half);
//				}
//				else {
//					if (bc < ab && bc < ca) {
//						half_a = SameFace(half->next);
//					}
//					else {
//						half_a = SameFace(half->previous);
//					}
//				}
//				//cross = crossEdge(Flist,half_a);
//				if (cross)cout << "CRUZA ARESTA" << endl;
//				else cout << "NAO CRUZA ARESTA" << endl;
//				index = eliminate_edge(half_a, func, &half_face);
//				/*if (half->mate->left->type == 2) {
//				half = SameFace(half);
//				index = eliminate_edge(half);
//			}
//			else {
//				if (half->next->mate->left->type == 2) {
//					half = SameFace(half->next);
//					index = eliminate_edge(half);
//				}
//				else {
//					half = SameFace(half->previous);
//					index = eliminate_edge(half);
//				}
//			}*/
//			}
//		}
//		else {//Caso 3: Dois pontos na borda
//			if (a + b + c == 2) {
//				cout << "2 pontos" << endl;
//				/*if (ab < bc && ab < ca) {
//					cout << "Passou half" << endl;
//					half_a = SameFace(half);
//				}
//				else {
//					if (bc < ab && bc < ca) {
//						cout << "Passou Next" << endl;
//						half_a = SameFace(half->next);
//					}
//					else {
//						cout << "Passou Prev" << endl;
//						half_a = SameFace(half->previous);
//					}
//				}*/
//				//cross = crossEdge(Flist,half_a);
//				//if (cross)cout << "CRUZA ARESTA" << endl;
//				//else cout << "NAO CRUZA ARESTA" << endl;
//				//index = eliminate_edge(half_a, func, &half_face);
//				if (a && b) {
//					half = half->previous;
//					half_a = SameFace(half);
//					if (half == half_a) {
//						//cross = crossEdge(Flist,half);
//						if (!cross)
//							index = eliminate_edge(half, func, &half_face);
//						else index = -1;
//					}
//					else {
//						//cross = crossEdge(Flist,half_a);
//						if (!cross)
//							index = eliminate_edge(half_a, func, &half_face);
//						else index = -1;
//					}
//				}
//				else {
//					if (b && c) {
//						half_a = SameFace(half);
//						//cross = crossEdge(Flist,half_a);
//						if (!cross)
//							index = eliminate_edge(half_a, func, &half_face);
//						else index = -1;
//					}
//					else {
//						half_a = SameFace(half->previous);
//						cout << "HALf:" << half->origin->ID << endl;
//						cout << "HALF_A:" << half_a->origin->ID << endl;
//						//cross = crossEdge(Flist,half_a);
//						if (!cross)
//							index = eliminate_edge(half_a, func, &half_face);
//						else index = -1;
//					}
//				}
//			}
//			else {//Caso 4: Um ponto na borda
//				cout << "1 ponto" << endl;
//				/*if (ab < bc && ab < ca) {
//					half_a = SameFace(half);
//				}
//				else {
//					if (bc < ab && bc < ca) {
//						half_a = SameFace(half->next);
//					}
//					else {
//						half_a = SameFace(half->previous);
//					}
//				}
//				cross = crossEdge(Flist,half_a);
//				if (cross)cout << "CRUZA ARESTA" << endl;
//				else cout << "NAO CRUZA ARESTA" << endl;
//				index = eliminate_edge(half_a, func, &half_face);*/
//				if (a) {
//					cout << "a" << endl;
//					half_a = SameFace(half->next);
//				}
//				else {
//					if (b) {
//						cout << "b" << endl;
//						half_a = SameFace(half);
//					}
//					else {
//						cout << "c" << endl;
//						half_a = SameFace(half);
//					}
//				}
//				//cross = crossEdge(Flist,half_a);
//				if (!cross)
//					index = eliminate_edge(half_a, func, &half_face);
//				else index = -1;
//			}
//		}
//	}
//	return index;
//}
int Mesh :: Point_Border (HalfEdge *half) {

	if (half->left->type == 2) return 1;

	HalfEdge *half_travel;
	half_travel = half->mate->next;

	while (half_travel != half) {
		if (half_travel->left->type == 2) return 1;
		half_travel = half_travel->mate->next;
	}

	return 0;
}

int Mesh :: eliminate_edge(HalfEdge *half, HalfEdge **half_f) {
	//Input: half da aresta que sera removida (halfedge caso seja possivel precisa estar dentro da face)
	//Output: Id do vertice anterior ao vertice removido


	float vd[3], point[3][3];
	Point3D *p1,*p2,*p;
	int id_vertice_1, id_vertice_2;

	id_vertice_1 = half->origin->ID;
	id_vertice_2 =  half->next->origin->ID;

	p1 = (Point3D *)(getVGeomPtr(id_vertice_1));
	p2 = (Point3D *)(getVGeomPtr(id_vertice_2));


	point[0][0] = p1->x; point[0][1] =  p1->y; point[0][2] = p1->z;
	point[1][0] = p2->x; point[1][1] =  p2->y; point[1][2] = p2->z;


	Vertex *vertice, *vertice_fica;
	HalfEdge *half_travel, *half_update;
	int id_prev_vertex, id_face, face;

	id_prev_vertex = half->origin->ID;

	vertice = half->next->origin;//vertice que vai ser retirado

	*half_f = half->previous->mate;
	vertice_fica = half->origin;

	//Update HalfEdge

	half_travel = half->next;

	do{
		if (half_travel->left == _outer_loop)cout << "Loop borda" << endl;
		half_travel->origin = half->origin;
		half_travel = half_travel->mate->next;
	}while(half_travel != half->next);

	//Edge share two faces
	if (half->left->type == 1 && half->mate->left->type == 1) {
		//cout << "Edge share two faces" << endl;
		//Update Vertex-Half

		half->origin->half = change_origin(half->origin->half, half->left, half->mate->left);
		half->mate->previous->origin->half = change_origin(half->mate->previous->origin->half, half->mate->left, 0);
		half->previous->origin->half = change_origin(half->previous->origin->half, half->left, 0);

		//Mate
		half->next->mate->mate = half->previous->mate;
		half->previous->mate->mate = half->next->mate;
		//side 2
		half->mate->next->mate->mate = half->mate->previous->mate;
		half->mate->previous->mate->mate = half->mate->next->mate;

		if (half->origin->half->left->type == 2) {
			half->origin->half = change_origin(half->origin->half, 0, 0);
		}

		remove_loop(half->mate->left);

		delete(half->mate->next);
		delete(half->mate->previous);
		delete(half->next);
		delete(half->previous);
	}
	else {
		//Edge share one face
		if ((half->left->type == 1 && half->mate->left->type == 2) OR (half->left->type == 2 && half->mate->left->type == 1)) {
			cout << "Edge share one face" << endl;

			if(half->left->type == 1 && half->mate->left->type == 2){

				half->origin->half = change_origin(half->origin->half, half->left, 0);
				half->previous->origin->half = change_origin(half->previous->origin->half, half->left, 0);
				//External halfedge
				half->mate->previous->next = half->mate->next;
				half->mate->next->previous = half->mate->previous;
				//Mate
				half->next->mate->mate = half->previous->mate;
				half->previous->mate->mate = half->next->mate;

				if (half->origin->half->left->type == 2) {
					half->origin->half = change_origin(half->origin->half, 0, 0);
				}

				if (half->mate == _outer_loop->half) {//outer loop esta apontando para o half que sera deletado
					_outer_loop->half = half->mate->next;
				}



				delete(half->next);
				delete(half->previous);
			}
			else{

				cout << "half no outer" << endl;
				cout << "face:" << half->mate->left->ID << endl;

				half->origin->half = change_origin(half->origin->half, half->mate->left, 0);
				half->mate->previous->origin->half = change_origin(half->mate->previous->origin->half,half->mate->left, 0);

				//External halfedge
				half->previous->next = half->next;
				half->next->previous = half->previous;
				//Mate
				half->mate->next->mate->mate = half->mate->previous->mate;
				half->mate->previous->mate->mate = half->mate->next->mate;

				if (half->origin->half->left->type == 2) {
					half->origin->half = change_origin(half->origin->half, half->mate->left, 0);
				}

				if (half == _outer_loop->half) {//outer loop esta apontando para o half que sera deletado
					_outer_loop->half = half->next;
				}

				remove_loop(half->mate->left);
				delete(half->mate->next);
				delete(half->mate->previous);
			}
		}
		else {
			//Edge no share face
			half->origin->half = change_origin(half->origin->half, half->mate->left, 0);
			half->mate->previous->origin->half = change_origin(half->mate->previous->origin->half, half->mate->left, 0);

			//External halfedge
			half->previous->next = half->next;
			half->next->previous = half->previous;
			half->mate->previous->mate->mate = half->mate->next->mate;
			half->mate->next->mate->mate = half->mate->previous->mate;

			if (half->origin->half->left->type == 2) {
				half->origin->half = change_origin(half->origin->half, 0, 0);
			}
			remove_loop(half->mate->left);
			if (half == _outer_loop->half) {
				_outer_loop->half = half->next->next;
			}
		}
	}
	remove_vertex(vertice);
	remove_edge(half->edge);
	if(_outer_loop != half->left){
		remove_loop(half->left);
	}

	delete(half->mate);
	half = 0;
	delete(half);
	return id_prev_vertex;
}

int Mesh :: eliminate_face(HalfEdge *half) {


    return 0;
}

HalfEdge *Mesh :: change_origin(HalfEdge *half, Loop *f1, Loop *f2) {

	HalfEdge *half_travel, *update;

	if ((half->left != f1 && half->left != f2 AND half->left != _outer_loop) ) return half;
	update = 0;
	half_travel = half->mate->next;
	//cout << "vert:" << half_travel->origin->ID << endl;
	while (half_travel != half) {

		//cout << "half_travel:" << half_travel->left->ID << endl;

		if ((half_travel->left != f1 && half_travel->left != f2)) {
				return half_travel;
		}
		half_travel = half_travel->mate->next;
	}
	if(half->next != half->mate && half->left == _outer_loop)update = half->next;
	if(!update)update = half->next;
	return update;
}

HalfEdge *Mesh :: change_origin(HalfEdge *half, HalfEdge *halfFace) {

	HalfEdge *half_travel, *half_update;
	int face = 0;
	int share_one_face;
	half_update = 0;

	share_one_face = 0;
	//cout << "Id face half:" << halfFace->left->ID  << endl;
	//cout << "Id face half mate:" << halfFace->mate->left->ID  << endl;

	//cout << "Vertices do triangulo mate:" << halfFace->mate->origin->ID << " " << halfFace->mate->next->origin->ID << " " << halfFace->mate->previous->origin->ID << endl;

	//cout << "Type halfFace->mate->previous:" << halfFace->mate->previous->left->type << endl;

	//uma fusao
	//caso o unico half esteja apontado para outer
	if (halfFace->mate->left == _outer_loop) {
		cout << "compartilha uma face" << endl;
		half_update = halfFace->mate->next;
		share_one_face = 1;
	}

	if (!share_one_face) {
		cout << "while 1" << endl;
		half_travel = half->mate->next;
		while (!face && half_travel != half) {
			cout << "Id face" << half_travel->left->ID  << endl;
			cout << "travel:" << half_travel->origin->ID << endl;
			cout << "travel next:" << half_travel->next->origin->ID << endl;

			if (!half_travel) cout << "halfFace->left eh nulo" << endl;

			if (half_travel->left != halfFace->left && half_travel->left != halfFace->mate->left) {//fusao de duas faces

				cout << "Entra aqui" << endl;
				if (half_travel->left->type == 1) {
					cout << "Face" << endl;
					cout << "atribui:" << half_travel->origin->ID << endl;
					cout << "atribui next:" << half_travel->next->origin->ID << endl;
					face = 1;
				}
				else {
					half_update = half_travel;
				}
				//cout << "Nao face" << endl;
				cout << "atribui:" << half_travel->origin->ID << endl;
				cout << "atribui next:" << half_travel->next->origin->ID << endl;
				half_update = half_travel;

			}
			half_travel = half_travel->mate->next;
			cout << "Proximo" << endl;
		}

	}
	else {

		cout << "while 2 " << endl;
		half_travel = half->mate->next;
		while (!face && half_travel != half) {
			cout << "Id face" << half_travel->left->ID  << endl;
			cout << "travel:" << half_travel->origin->ID << endl;
			cout << "travel next:" << half_travel->next->origin->ID << endl;
			cout << "travel next next:" << half_travel->next->next->origin->ID << endl;
			if (half_travel->left != halfFace->left) {//fusao de duas faces
				if (half_travel->left->type == 1) {
					cout << "Face" << endl;
					cout << "Face type:" << half_travel->left->type << endl;
					cout << "atribui:" << half_travel->origin->ID << endl;
					cout << "atribui next:" << half_travel->next->origin->ID << endl;
					face = 1;
				}
				//cout << "Nao face" << endl;
				cout << "atribui:" << half_travel->origin->ID << endl;
				cout << "atribui next:" << half_travel->next->origin->ID << endl;
				half_update = half_travel;
			}
			half_travel = half_travel->mate->next;
		}
	}
	if (!half_update)cout << "Half_update nulo" << endl;
	cout << "sai while" << endl;
	return half_update;
}

VertexID Mesh::ReduceTriangleToPointL(const FaceID ID, const VertexID vID, void *ptr) {
	VertexID index;
	cout << "REMOVER\n";
	cout << "Id da face:" << ID << endl;
	Loop *f;

	typedef pair <int,Vertex*> Pair;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}


	 HalfEdge *half, *half_aux;
	 Vertex *vert_aux;
	 half = f->half;
	 cout << half->origin->ID << " face aponta para half." << endl;
	 cout << half->next->origin->ID << " nexthalf." << endl;


	 if (half->left->type == 1) {
		 cout << "Passando half" << endl;
		 vert_aux = JoinVertices( half,half->previous->origin->ID);
	 }
	 else {
		 cout << "Passando half-mate\n" << endl;
		 vert_aux = JoinVertices(half->mate,half->mate->previous->origin->ID);
	 }
	 cout <<"--------------------------------------------------------\n";
	 if (!vert_aux) cout << "vertex nulo\n";
	 else cout << "vertex nao nulo\n";

	 if (!vert_aux->half) cout << "half do vertex eh nulo\n";
	 else cout << "half do vertex nao eh nulo\n";
	 cout << "Join outro lado:" << vert_aux->ID << endl;
	 cout << "union prox:" << vert_aux->half->next->origin->ID << endl;
//	 cout << "Vertex Half:" << vert_aux->half << endl;
//	 cout << "Half:" << vert_aux->half->mate->mate << endl;
	// cout << "Face aponta para halfedge:" << vert_aux->half->left->half->origin->ID << endl;
	 //cout << "Face aponta para halfedge next:" << vert_aux->half->left->half->next->origin->ID << endl;
	 cout <<"--------------------------------------------------------\n";
//	 half = vert_aux->half;
//	 if (half->left->type == 1) {
//		 cout << "Passando half" << endl;
//		 vert_aux = JoinVertices(half,-1);
//	 }
//	 else {
//		 cout << "Passando half-mate\n" << endl;
//		 vert_aux = JoinVertices(half->mate,-1);
//	 }

	//  cout << "sai\n";
	 //cout << "vertex:" << vert_aux->half << endl;
	 //cout << "Face aponta para halfedge:" << vert_aux->half->left->half->origin->ID << endl;
	// cout << "Face aponta para halfedge next:" << vert_aux->half->left->half->next->origin->ID << endl;
	 cout <<"--------------------------------------------------------\n";
//	 cout << "vertex:" << vt2.half << endl;
//	 cout << "Face aponta para halfedge:" << vt2.half->left->half->origin->ID << endl;
//	 cout << "Face aponta para halfedge next:" << vt2.half->left->half->next->origin->ID << endl;
	 cout <<"--------------------------------------------------------\n";
//	 cout << "ID:" << vert_aux->half->origin->ID << endl;
//	 half_aux = vert_aux->half->next;
//	 while (half_aux != vert_aux->half) {
//		 cout << "ID:" << half_aux->origin->ID << endl;
//		 half_aux = half_aux->next;
//	 }

	 ptr = (Vertex *)vert_aux;

	 if (vert_aux) return vert_aux->ID;
	 	 else return -1;
}
void Mesh :: ChangeOrigin(HalfEdge **half) {

}

Vertex *Mesh::JoinVertices(HalfEdge *half, int close_ID) {

	Vertex *union_vertex;
	HalfEdge *half_aux;
	int flag;
	cout << "ID:" << (half)->origin->ID << endl;
	cout << "PROX:" << (half)->next->origin->ID << endl;
	//vertice - halfedge
	flag = 0;

	if (close_ID != -1) {
		half_aux = (half)->mate->next;
		cout << "!= -1\n";
		if (half_aux->next->origin->ID == close_ID) {
			(half)->origin->half = half_aux;
			if (half_aux->left->type == 1)flag = 1;
		}
		half_aux = (half_aux)->mate->next;
		while (!flag && half_aux != (half)) {
			if (half_aux->next->origin->ID == close_ID && half_aux->left->type == 1) {
				(half)->origin->half = half_aux;
				flag = 1;//prioridade para quem e face
			}
			else {
				if (half_aux->next->origin->ID == close_ID) {
					(half)->origin->half = half_aux;
				}
			}
			half_aux = half_aux->mate->next;
		}

		union_vertex = (half)->origin;
		(half)->previous->origin->half = (half)->origin->half->next;
		cout << "Vertice:" << union_vertex->ID << endl;
		cout << "Vertice aponta para:" << union_vertex->half->origin->ID << endl;
		cout << "Vertice aponta para para:" << union_vertex->half->next->origin->ID << endl;
	}

	cout << "ID:" << (half)->origin->ID << endl;
	cout << "PROX:" << (half)->next->origin->ID << endl;
	cout << "estamos aqui\n";
	cout << "PROX:" << (half)->next->origin->ID << endl;

	if ((half)->mate->left->type == 1 && (half)->left->type == 1) {
		cout << "Duas fusoes\n";

		cout << "Vertice retirado:" << (half)->next->origin->ID << endl;
		remove_vertex((half)->next->origin);

		//Atualizar as ORigens dos halfedges
		half_aux = (half)->next;
		half_aux->origin = (half)->origin;
		half_aux = half_aux->mate->next;
		while (half_aux != (half)->next) {
			half_aux->origin = (half)->origin;
			half_aux = half_aux->mate->next;
		}
//***********************************************************************************************
		//Atualizar os vertices que possuem ponteiros para as faces que serao destruidas
		//e que nao pertencem ao triangulo que sera removido
		//Primeiro vertice
		cout << "primeiro\n";
		flag = 0;
		half_aux = (half)->previous->mate->next;
		while (!flag && half_aux != (half)->previous) {
			if ((half_aux->left->ID != (half)->previous->left->ID) && half_aux->left->type == 1 ) {
				(half)->previous->origin->half = half_aux;
				flag = 1;//prioridade pra dentro da face
			}
			else {
				if ((half_aux->left->ID != (half)->previous->left->ID) ) {
					(half)->previous->origin->half = half_aux;
				}
			}
			half_aux = (half_aux)->mate->next;
		}
		//Segundo vertice
		cout << "segundo\n";
		flag = 0;
		half_aux = (half)->mate->previous->mate->next;
		while (!flag && half_aux != (half)->mate->previous) {
			if ((half_aux->left->ID != (half)->mate->previous->left->ID) && half_aux->left->type == 1 ) {
				(half)->mate->previous->origin->half = half_aux;
				flag = 1;//prioridade pra dentro da face
			}
			else {
				if ((half_aux->left->ID != (half)->mate->previous->left->ID) ) {
					(half)->mate->previous->origin->half = half_aux;
				}
			}
			half_aux = (half_aux)->mate->next;
		}
//***********************************************************************************************

		cout << "Fazer fusao\n";
		//Fazer fusao das arestas
		//Atualizar Mate
		//side 1
		(half)->mate->previous->mate->mate = (half)->mate->next->mate;
		(half)->mate->next->mate->mate = (half)->mate->previous->mate;

		//side 2
		(half)->next->mate->mate = (half)->previous->mate;
		(half)->previous->mate->mate = (half)->next->mate;

		cout << "Kill edge\n";
		//Kill edge
		(half)->mate->previous->mate->edge = (half)->mate->next->edge;
		(half)->next->mate->edge = (half)->previous->edge;
		remove_edge((half)->mate->next->edge);
		remove_edge((half)->next->edge);

		cout << "(*half)->origin->half:" << (half)->origin->half << endl;
		cout << "end4:" << (half) << endl;

		flag = 0;
		if (close_ID == -1) {
			half_aux = (half)->previous->mate;
			cout << "-1\n";
			if (half_aux->left->type == 1) {//nao pode ser a mesma face
				cout << "IF-1\n";
				(half)->origin->half = half_aux;

				cout << "half_aux:" << (half_aux)->origin->ID << endl;
				cout << "half_aux next:" << (half_aux)->next->origin->ID << endl;
				cout << "half_aux next next:" << (half_aux)->next->next->origin->ID << endl;
				cout << "----------------------------------\n";
				cout << "half:" << (half)->origin->ID << endl;
				cout << "half next:" << (half)->next->origin->ID << endl;
				cout << "half next next:" << (half)->next->next->origin->ID << endl;


				cout << "(*half)->mate->origin->ID:" << (half)->mate->origin->ID << endl;
				cout << "half_aux->next->origin->ID:" << half_aux->next->origin->ID << endl;
				flag = 1;
			}
			else {
					(half)->origin->half = half_aux;
					half_aux = (half_aux)->mate->next;
					while (!flag && half_aux != (half)->origin->half) {
						cout << "while\n";
						if (half_aux->left->type == 1 && (half_aux->left->ID != (half)->left->ID)&& (half)->previous->origin->ID != half_aux->origin->ID) {
							cout << "IF-3\n";
							(half)->origin->half = half_aux;
							flag = 1;//prioridade para quem e face
						}
						else {
							if ((half_aux->left->ID != (half)->left->ID)&& (half)->previous->origin->ID != half_aux->origin->ID) {
								(half)->origin->half = half_aux;
							}
						}
						half_aux = half_aux->mate->next;
					}
			}
			union_vertex = (half)->origin;
		}


		//Kill half edge
		delete((half)->mate->next);
		delete((half)->mate->previous);

		delete((half)->next);
		delete((half)->previous);

		//Kill face
		remove_loop((half)->mate->left);
		remove_loop((half)->left);

		//Aresta que pertence ao triangulo que sera removido
		remove_edge((half)->edge);

		delete((half)->mate);
		delete((half));
	}
	else {
		if ((half)->mate->left->type != 1 && (half)->left->type == 1) {

			//atualizar _outer_loop_
			if ((half)->mate == _outer_loop->half){
				_outer_loop->half = (half)->mate->next;
			}

			//vertice - halfedge
			remove_vertex((half)->next->origin);
			//Atualizar as ORigens dos halfedges
			half_aux = (half)->next;
			half_aux->origin = (half)->origin;
			half_aux = half_aux->mate->next;
			while (half_aux != (half)->next) {
				half_aux->origin = (half)->origin;
				half_aux = half_aux->mate->next;
			}

			//Atualizar os vertices que possuem ponteiros para as faces que serao destruidas
			//e que nao pertencem ao triangulo que sera removido
			//Primeiro vertice
			//***********************************************************************************************
			flag = 0;
			half_aux = (half)->previous->mate->next;
			while (!flag && half_aux != (half)->previous) {
				if ((half_aux->left->ID != (half)->previous->left->ID) && half_aux->left->type == 1 ) {
					(half)->previous->origin->half = half_aux;
					flag = 1;//prioridade pra dentro da face
				}
				else {
					if ((half_aux->left->ID != (half)->previous->left->ID) ) {
						(half)->previous->origin->half = half_aux;
					}
				}
				half_aux = (half_aux)->mate->next;
			}
			//***********************************************************************************************

			(half)->next->mate->edge = (half)->previous->edge;
			remove_edge((half)->next->edge);

			//Atualizar halfedge externos
			(half)->mate->previous->next = (half)->mate->next;
			(half)->mate->next->previous = (half)->mate->previous;

			//side 2
			//Atualizar mate
			(half)->next->mate->mate = (half)->previous->mate;
			(half)->previous->mate->mate = (half)->next->mate;

			flag = 0;
			if (close_ID == -1) {
				half_aux = (half)->previous->mate;
				if (half_aux->left->type == 1) {//nao pode ser a mesma face
					(half)->origin->half = half_aux;
					flag = 1;
				}
				else {
					(half)->origin->half = half_aux;
					half_aux = (half_aux)->mate->next;

					while (!flag && half_aux != ((half)->origin->half )) {
						if (half_aux->left->type == 1 && (half_aux->left->ID != (half)->left->ID)&& (half)->previous->origin->ID != half_aux->origin->ID) {
							(half)->origin->half = half_aux;
							flag = 1;//prioridade para quem e face
						}
						else {
							if ((half_aux->left->ID != (half)->left->ID)&& (half)->previous->origin->ID != half_aux->origin->ID) {
								(half)->origin->half = half_aux;
								//flag = 1;//flag utilizado aqui so para for�ar a saida, dpois tira
							}
						}
						half_aux = half_aux->mate->next;
					}
				}
				union_vertex = (half)->origin;
			}

			delete((half)->next);
			delete((half)->previous);
			remove_loop((half)->left);
			remove_edge((half)->edge);
			delete((half)->mate);
			delete((half));
		}
		else {
			//Nenhuma fusao
			remove_vertex(half->mate->origin);
			//Atualizar ORigens halfedges
				half_aux = half->next;
				while (half->mate != half_aux) {
					half_aux->origin = half->origin;
					half_aux = half_aux->mate->next;
				}

			//Atualizar ponteiros externos
			if (half->mate == half->previous) {
				half->next->previous = half->mate->previous;
				half->mate->previous->next = half->next;
				half_aux = half->next;//para ajudar no vertice - half
			}
			else {
				if (half->mate == half->next) {
					half->previous->next = half->mate->next;
					half->mate->next->previous = half->previous;
					half_aux = half->mate->next;//para ajudar no vertice - half
				}
				else {
					half->next->previous = half->previous;
					half->previous->next = half->next;

					half->mate->previous->next = half->mate->next;
					half->mate->next->previous = half->mate->previous;

					half_aux = half->next;
				}
			}

			//Atualizar vertice - half
			flag = 0;
			half->origin->half = half_aux;
			half_aux = half_aux->mate->next;
			while (!flag && half->origin->half != half_aux) {
				if (half_aux->left->type == 1) {
					half->origin->half = half_aux;
					flag = 1;
				}
				half_aux = half_aux->mate->next;
			}
			union_vertex = (half)->origin;
			//Verificar se outer loop nao vai ser deletado
			if (half->next == half->mate && half->previous == half->mate) {
				_outer_loop->half = 0;
				_outer_loop = 0;
			}
			else if (half == _outer_loop->half || half->mate == _outer_loop->half) {
				_outer_loop->half = half->next->next;
			}

			//kill edge
			remove_edge(half->edge);
			delete(half);
			delete(half->mate);
		}
	}
	return union_vertex;
//retorna o vertice q restou
}

VertexID Mesh::ReduceTriangleToPoint(const FaceID ID, const VertexID vID, void *ptr)
{
	VertexID index;

	Loop *f, *f1, *f2, *f3, *outer;
	Vertex *new_v, *vv;
	Edge *edge1, *edge2, *edge3;
	HalfEdge *e10, *e20, *e30, *e11, *e12, *e21, *e22, *e31, *e32;
	Vertex *va, *vb, *vc, *v1, *v2, *v3;
	HalfEdge *he, *mhe, *che;
	typedef pair <int,Vertex*> Pair;

	lIterator = loops.find(ID);
	if (lIterator != loops.end()) {
		f = lIterator->second;
		if (f->type != 1) {
			cout << ID << " is not a face." << endl;
			return -1;
		}
	} else {
		cout << "Face "<< ID << " does not exist." << endl;
		return -1;
	}

	cout << "face->halfId:" << f->half->origin->ID << endl;
	// Find the outer loop of the mesh ...
	outer = NULL;
	//outer = getOuterLoopOfMesh(f);
	if (!outer) outer = _outer_loop;

	// Get its bounding halfedges
	e10 = f->half;
	e20 = e10->next;
	e30 = e20->next;
	vb = e20->origin;
	vc = e30->origin;
	va = e10->origin;
	f1 = (e10->mate)->left;
	f2 = (e20->mate)->left;
	f3 = (e30->mate)->left;

	// Check the vertex degeneracies
	if (f1 && f1->type == 1 && f1 == f2 && f2 == f3) {
		if (e10->mate->next == e30->mate &&
				e20->mate->next == e10->mate &&
				e30->mate->next == e20->mate ) {
			if ((va->half == e30->mate && va->half->mate->next == e10) ||
					(va->half == e10 && va->half->mate->next == e30->mate)) {
				remove_vertex (va);
			} else {
				new_v = va;
			}
			if ((vb->half == e10->mate && vb->half->mate->next == e20) ||
					(vb->half == e20 && vb->half->mate->next == e10->mate)) {
				remove_vertex (vb);
			} else {
				new_v = vb;
			}
			if ((vc->half == e20->mate && vc->half->mate->next == e30) ||
					(vc->half == e30 && vc->half->mate->next == e20->mate)) {
				remove_vertex (vc);
			} else {
				new_v = vc;
			}
			remove_loop(f1);
			delete (e10->mate);
			delete (e20->mate);
			delete (e30->mate);
			remove_edge(e10->edge);
			remove_edge(e20->edge);
			remove_edge(e30->edge);
			delete(e10);
			delete(e20);
			delete(e30);
			remove_loop(f);

			cout << "There is a degenerated face." << endl;
			return -1;
		} else {
			cout << "There is a topological inconsistency." << endl;
			return -1;
		}
	}

	// Check vertex degeneracies (2 half-edges)
	if ((f1->type == 1 && f == f1 && va == vc) ||
			(f2->type == 1 && f == f2 && vb == va) ||
			(f3->type == 1 && f == f3 && vb == vc)) {
		cout << "There is a degeneracy in vertex AND edge adjacencies." << endl;
		if (va == vc) {
			va->half =  e30->mate->previous->mate;
			remove_vertex(vb);
			remove_edge(e10->edge);
			remove_loop(e30->mate->left);
			e30->mate->previous->origin->half = e30->mate->next->mate;
			remove_edge(e30->mate->next->mate->edge);
			e30->mate->next->mate->edge = e30->mate->previous->mate->edge;
			e30->mate->next->mate->edge->half = e30->mate->next->mate;
			e30->mate->previous->mate->mate = e30->mate->next->mate;
			e30->mate->next->mate->mate = e30->mate->previous->mate;
			remove_edge(e30->edge);
			delete(e30->mate->previous);
			delete(e30->mate->next);
			delete(e30->mate);
		} else if (vb == va) {
			vb->half =  e10->mate->previous->mate;
			remove_vertex(vc);
			remove_edge(e20->edge);
			remove_loop(e10->mate->left);
			e10->mate->previous->origin->half = e10->mate->next->mate;
			remove_edge(e10->mate->next->mate->edge);
			e10->mate->next->mate->edge = e10->mate->previous->mate->edge;
			e10->mate->next->mate->edge->half = e10->mate->next->mate;
			e10->mate->previous->mate->mate = e10->mate->next->mate;
			e10->mate->next->mate->mate = e10->mate->previous->mate;
			remove_edge(e10->edge);
			delete(e10->mate->previous);
			delete(e10->mate->next);
			delete(e10->mate);
		} else {
			vc->half =  e20->mate->previous->mate;
			remove_vertex(va);
			remove_edge(e30->edge);
			remove_loop(e20->mate->left);
			e20->mate->previous->origin->half = e20->mate->next->mate;
			remove_edge(e20->mate->next->mate->edge);
			e20->mate->next->mate->edge = e20->mate->previous->mate->edge;
			e20->mate->next->mate->edge->half = e20->mate->next->mate;
			e20->mate->previous->mate->mate = e20->mate->next->mate;
			e20->mate->next->mate->mate = e20->mate->previous->mate;
			remove_edge(e20->edge);
			delete(e20->mate->previous);
			delete(e20->mate->next);
			delete(e20->mate);
		}
		delete(e10);
		delete(e20);
		delete(e30);
		remove_loop(f);

		cout << "Face "<< ID << " is reduced to a vertex, but no change in the vertex information." << endl;

		return -1;
	}

	/// Handling vertex degeneracy (1 half-edge)
	if ((f1->type == 1 && va == vb) ||
			(f2->type == 1 && vb == vc) ||
			(f3->type == 1 && vc == va)) {
		cout << "There is a degeneracy in vertex AND edge adjacencies." << endl;
		if (va == vb) {
			if (! remove_inner_structures (e10, va)) {
				cout << "Face "<< ID << " is not removed, but its neighboring faces are." << endl;
				return -1;
			}
			va->half = e20->mate->next;
			vc->half = e20->mate;
			e20->mate->mate = e30->mate;
			e30->mate->mate = e20->mate;
			remove_edge(e10->edge);
		} else if (vb == vc) {
			if (! remove_inner_structures (e20, vb)) {
				cout << "Face "<< ID << " is not removed, but its neighboring faces are." << endl;
				return -1;
			}
			vb->half = e30->mate->next;
			va->half = e30->mate;
			e10->mate->mate = e30->mate;
			e30->mate->mate = e10->mate;
			remove_edge(e20->edge);
		} else {
			if (! remove_inner_structures (e30, vc)) {
				cout << "Face "<< ID << " is not removed, but its neighboring faces are." << endl;
				return -1;
			}
			vc->half = e10->mate->next;
			vb->half = e10->mate;
			e20->mate->mate = e10->mate;
			e10->mate->mate = e20->mate;
			remove_edge(e30->edge);
		}
		delete(e10);
		delete(e20);
		delete(e30);
		remove_loop(f);

		cout << "Face "<< ID << " is reduced to an edge." << endl;
		return -1;
	}

	// Check the vertex OR edge degeneracies
	if ((f1 && f1->type == 1 && f1 == f2) ||
			(f2 && f2->type == 1 && f2 == f3) ||
			(f3 && f3->type == 1 && f3 == f1)) {
		ReduceTtoP_caseIII(f, f1, f2, f3, e10, e20, e30, va, vb, vc, &new_v);
		if (new_v && (new_v)->half == NULL) {
			cout << "There is a degenerated vertex." << endl;
			//      return new_v->ID;
		} else if (!(new_v)) {
			cout << "Face "<< ID << " is removed." << endl;
		} else {
			cout << "Face "<< ID << " is reduced to an edge." << endl;
		}
		return -1;
	}


	// Update the new point entry
	index = replace_vertex (va, &vv, ptr, vID);

	cout << "index:" << index << endl;

	if (index == -1)
		return 0;
	else {
		new_v = vv;
		if (new_v != va) {
			//       replace_vertex_pointers (va, new_v);
			new_v->half = va->half;
			vertices.erase(va->ID);
			delete va;
			va = NULL;
		}
	}

	// Update the ORigin of halfedges

	//halfedge q tem ORigem em vb, passaram a ter ORigem em va (new_v)
	he = vb->half;
	he->origin = new_v;
	che = he->mate;
	if (che) che = che->next;
	while (che && che != he) {
		if (che->origin == vb) che->origin = new_v;
		che = che->mate;
		che = che->next;
	}

	//halfedge q tem ORigem em vc, passaram a ter ORigem em va (new_v)
	he = vc->half;
	he->origin = new_v;
	che = he->mate;
	if (che) che = che->next;
	while (che && che != he) {
		if (che->origin == vc) che->origin = new_v;
		che = che->mate;
		che = che->next;
	}

	// Remove the vertices
	remove_vertex(vb);
	remove_vertex(vc);

	// Update the half-edge's mate AND remove the adjacent edges AND faces
	edge1 = edge2 = edge3 = NULL;

	if (f1 == f2 && f2 == f3) {
		remove_loop(f1);
		// A new cycle should be created
		e10->mate->left = e20->mate->left = e30->mate->left = outer;
	}  else {
		//mata a aresta D-C
		che = e10->mate->next;
		if (f1->type == 1) {
			v1 = ((e10->mate)->previous)->origin;
			edge1 = ((e10->mate)->previous->mate)->edge;
			remove_edge((e10->mate->next)->edge);
			((e10->mate)->next->mate)->edge = edge1;
			edge1->half = ((e10->mate)->previous)->mate;
			he = (e10->mate)->next->mate;
			(edge1->half)->mate = he;
			he->mate = edge1->half;
			v1->half = (e10->mate)->next->mate;
			delete((e10->mate)->next);
			delete((e10->mate)->previous);
			remove_loop(f1);
		}
		if (f2->type == 1) {
			v2 = ((e20->mate)->previous)->origin;
			edge2 = ((e20->mate)->previous->mate)->edge;
			if (((e20->mate)->next->mate)->edge == edge1) {
				//mata F-C
				edge1->half->edge = edge1->half->mate->edge = edge2;
				remove_edge(edge1);
				edge1 = edge2;
			} else {
				//mata A-F
				remove_edge((e20->mate->next)->edge);
				((e20->mate)->next->mate)->edge = edge2;
			}
			edge2->half = ((e20->mate)->previous)->mate;
			((e20->mate)->previous->mate)->mate = ((e20->mate)->next)->mate;
			((e20->mate)->next->mate)->mate = ((e20->mate)->previous)->mate;
			v2->half = (e20->mate)->next->mate;
			delete((e20->mate)->next);
			delete((e20->mate)->previous);
			remove_loop(f2);
		}
		if (f3->type == 1) {
			v3 = ((e30->mate)->previous)->origin;
			edge3 = ((e30->mate)->previous->mate)->edge;
			if (((e30->mate)->next->mate)->edge == edge2) {
				//mata A-E
				edge2->half->edge = edge2->half->mate->edge = edge3;
				remove_edge (edge2);
				if (edge1 == edge2)
					edge1 = edge2 = edge3;
				else
					edge2 = edge3;
			} else {
				//mata E-B
				remove_edge((e30->mate->next)->edge);
				((e30->mate)->next->mate)->edge = edge3;
			}
			edge3->half = ((e30->mate)->previous)->mate;
			((e30->mate)->previous->mate)->mate = ((e30->mate)->next)->mate;
			((e30->mate)->next->mate)->mate = ((e30->mate)->previous)->mate;
			v3->half = (e30->mate)->next->mate;
			delete((e30->mate)->next);
			delete((e30->mate)->previous);
			remove_loop(f3);
		}
		if (f1 && f3 && f1->type == 1 && f3->type == 1 && v3 == v1) {
			// Check the adjacencies of e10->mate->next AND e30->mate->previous
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (edge1->half->origin == v1)
				mhe = edge1->half->mate;
			else
				mhe = edge1->half;
			if (he->mate == che && edge1 != edge3) {
				delete (he->mate);
				delete (mhe->mate);
				mhe->edge = edge3;
				he->mate = mhe;
				mhe->mate = he;
				if (edge1 != edge3) {
					remove_edge(edge1);
					edge3->half = he;
					edge1 = edge3;
				}
			}
		}
	}

	/// No edge-adjacent faces
	if (!edge1 && !edge2 && !edge3) {
		if ( e10->mate->previous == e20->mate &&
				e20->mate->previous == e30->mate &&
				e30->mate->previous == e10->mate ) {
			// Triangle is reduced to a point
			new_v->half = NULL;
			// Update the loop adjacencies
			outer->half = NULL;
		} else if (e10->mate->previous != e20->mate) {
			new_v->half = e10->mate->previous->mate;
			outer->half = e10->mate->previous;
			if (e10->mate->next != e30->mate) {
				e10->mate->next->previous = e10->mate->previous;
				e10->mate->previous->next = e10->mate->next;
				if (e30->mate->next != e20->mate) {
					e30->mate->next->previous = e30->mate->previous;
					e30->mate->previous->next = e30->mate->next;
					e20->mate->next->previous = e20->mate->previous;
					e20->mate->previous->next = e20->mate->next;
				}
			} else if ( e30->mate->next != e20->mate) {
				e30->mate->next->previous = e10->mate->previous;
				e10->mate->previous->next = e30->mate->next;
				e20->mate->next->previous = e20->mate->previous;
				e20->mate->previous->next = e20->mate->next;
			} else {
				e20->mate->next->previous = e10->mate->previous;
				e10->mate->previous->next = e20->mate->next;
			}
		} else if (e30->mate->previous != e10->mate) {
			new_v->half = e30->mate->previous->mate;
			outer->half = e30->mate->previous;
			if ( e30->mate->next != e20->mate) {
				e30->mate->next->previous = e30->mate->previous;
				e30->mate->previous->next = e30->mate->next;
				e10->mate->next->previous = e20->mate->previous;
				e20->mate->previous->next = e10->mate->next;
			} else {
				e10->mate->next->previous = e30->mate->previous;
				e30->mate->previous->next = e10->mate->next;
			}
		} else {
			new_v->half = e20->mate->previous->mate;
			outer->half = e20->mate->previous;
			e30->mate->next->previous = e20->mate->previous;
			e20->mate->previous->next = e30->mate->next;
		}

		/// One edge-adjacent face
	} else if (!edge1 && !edge2 && edge3) {
		// Update the loop adjacencies
		outer->half = e10->mate->next;
		// Update the vertex adjacencies
		new_v->half = e10->mate->next;
		if (e20->mate->next == e10->mate) {
			e20->mate->previous->next = e10->mate->next;
			e10->mate->next->previous = e20->mate->previous;
		} else {
			e20->mate->previous->next = e20->mate->next;
			e20->mate->next->previous = e20->mate->previous;
			e10->mate->previous->next = e10->mate->next;
			e10->mate->next->previous = e10->mate->previous;
		}
		if (edge3->half->origin == v3)
			he = edge3->half;
		else
			he = edge3->half->mate;
		if (he->left->type == 2 &&
				he->mate->left->type == 2) {
			if (he->previous == he->mate) {
				v3->half = NULL;
				outer = he->left;
			} else
				v3->half = he->previous->mate;
		}
		if (!v3->half) {
			if (he->next == he->mate)
				new_v->half = NULL;
			else {
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
			}
			// Update the loop adjacencies
			// It should consider the possilibity that there is more than one loop
			if (outer->half == he || outer->half == he->mate) {
				if (he->next == he->mate && he->previous == he->mate)
					outer->half = NULL;
				else if (he->next == he->mate)
					outer->half = he->previous;
				else
					outer->half = he->next;
			}
			// A dangling edge is removed
			delete (he->mate);
			delete (he);
			remove_edge (edge3);
			edge3 = NULL;
			// The vertex is removed
			remove_vertex(v3);
		}
	} else if (!edge3 && !edge1 && edge2) {
		// Update the loop adjacencies
		outer->half = e30->mate->next;
		// Update the vertex adjacencies
		new_v->half = e30->mate->next;
		if (e10->mate->next == e30->mate) {
			e10->mate->previous->next = e30->mate->next;
			e30->mate->next->previous = e10->mate->previous;
		} else {
			e10->mate->previous->next = e10->mate->next;
			e10->mate->next->previous = e10->mate->previous;
			e30->mate->previous->next = e30->mate->next;
			e30->mate->next->previous = e30->mate->previous;
		}
		if (edge2->half->origin == v2)
			he = edge2->half;
		else
			he = edge2->half->mate;
		if (he->left->type == 2 &&
				he->mate->left->type == 2) {
			if (he->previous == he->mate) {
				v2->half = NULL;
				outer = he->left;
			} else
				v2->half = he->previous->mate;
		}

		if (!v2->half) {
			if (he->next == he->mate)
				new_v->half = NULL;
			else {
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
			}
			// Update the loop adjacencies
			// It should consider the possilibity that there is more than one loop
			if (outer->half == he || outer->half == he->mate) {
				if (he->next == he->mate && he->previous == he->mate)
					outer->half = NULL;
				else if (he->next == he->mate)
					outer->half = he->previous;
				else
					outer->half = he->next;
			}
			// A dangling edge is removed
			delete (he->mate);
			delete (he);
			remove_edge (edge2);
			edge2 = NULL;
			remove_vertex(v2);
		}
	} else if (!edge2 && !edge3 && edge1) {
		// Update the loop adjacencies
		outer->half = e20->mate->next;
		// Update the vertex adjacencies
		new_v->half = e20->mate->next;
		if (e30->mate->next == e20->mate) {
			e30->mate->previous->next = e20->mate->next;
			e20->mate->next->previous = e30->mate->previous;
		} else {
			e30->mate->previous->next = e30->mate->next;
			e30->mate->next->previous = e30->mate->previous;
			e20->mate->previous->next = e20->mate->next;
			e20->mate->next->previous = e20->mate->previous;
		}
		if (edge1->half->origin == v1)
			he = edge1->half;
		else
			he = edge1->half->mate;
		if (he->left->type == 2 &&
				he->mate->left->type == 2) {
			if (he->previous == he->mate) {
				v1->half = NULL;
				outer = he->left;
			} else
				v1->half = he->previous->mate;
		}
		if (!v1->half) {
			if (he->next == he->mate)
				new_v->half = NULL;
			else {
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
			}
			// Update the loop adjacencies
			// It should consider the possilibity that there is more than one loop
			if (outer->half == he || outer->half == he->mate) {
				if (he->next == he->mate && he->previous == he->mate)
					outer->half = NULL;
				else if (he->next == he->mate)
					outer->half = he->previous;
				else
					outer->half = he->next;
			}
			// A dangling edge is removed
			delete (he->mate);
			delete (he);
			remove_edge (edge1);
			edge1 = NULL;
			remove_vertex(v1);
		}

		/// Two edge-adjacent faces
	} else if (!edge1 && edge2 && edge3) {
		// Update the loop adjacencies
		outer->half = e10->mate->next;
		// Update the vertex adjacencies
		new_v->half = e10->mate->next;
		e10->mate->next->previous = e10->mate->previous;
		e10->mate->previous->next = e10->mate->next;
		if (edge2 == edge3) {
			if (edge2->half->left == outer &&
					edge2->half->mate->left == outer) {
				if (edge2->half->origin == v2)
					he = edge2->half;
				else
					he = edge2->half->mate;
				if (he->previous == he->mate)
					v2->half = NULL;
				else
					v2->half = he->previous->mate;
				if (!v2->half) {
					// Update the edge adjacencies
					if (he->next == he->mate)
						new_v->half = NULL;
					else {
						new_v->half = he->next;
						he->mate->previous->next = he->next;
						he->next->previous = he->mate->previous;
					}
					// Update the loop adjacencies
					if (outer->half == he || outer->half == he->mate) {
						if (he->next == he->mate && he->previous == he->mate)
							outer->half = NULL;
						else if (he->next == he->mate)
							outer->half = he->previous;
						else
							outer->half = he->next;
					}
					// A dangling edge is removed
					delete (he->mate);
					delete (he);
					remove_edge (edge2);
					edge2 = edge3 = NULL;
					remove_vertex(v2);
				}
			}
		} else if (edge2->half->left == outer &&
				edge2->half->mate->left == outer &&
				edge3->half->left == outer &&
				edge3->half->mate->left == outer) {
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate)
				v2->half = NULL;
			else
				v2->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->mate->previous->next = he->next;
				he->next->previous = he->mate->previous;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = NULL;
				remove_vertex(v2);
			}
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (he->previous == he->mate)
				v3->half = NULL;
			else
				v3->half = he->previous->mate;
			if (!v3->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge3);
				edge3 = NULL;
				remove_vertex(v3);
			}
		} else if (edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate)
				v2->half = NULL;
			else
				v2->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = NULL;
				remove_vertex(v2);
			}
		} else if (edge3->half->left == outer &&
				edge3->half->mate->left == outer) {
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (he->previous == he->mate)
				v3->half = NULL;
			else
				v3->half = he->previous->mate;
			if (!v3->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge3);
				edge3 = NULL;
				remove_vertex(v3);
			}
		}
	} else if (edge1 && !edge2 && edge3) {
		// Update the loop adjacencies
		outer->half = e20->mate->next;
		// Update the vertex adjacencies
		new_v->half = e20->mate->next;
		e20->mate->next->previous = e20->mate->previous;
		e20->mate->previous->next = e20->mate->next;
		if (edge1 == edge3) {
			if (edge3->half->left == outer &&
					edge3->half->mate->left == outer) {
				if (edge3->half->origin == v3)
					he = edge3->half;
				else
					he = edge3->half->mate;
				if (he->previous == he->mate)
					v3->half = NULL;
				else
					v3->half = he->previous->mate;
				if (!v3->half) {
					// Update the edge adjacencies
					if (he->next == he->mate)
						new_v->half = NULL;
					else {
						new_v->half = he->next;
						he->mate->previous->next = he->next;
						he->next->previous = he->mate->previous;
					}
					// Update the loop adjacencies
					if (outer->half == he || outer->half == he->mate) {
						if (he->next == he->mate && he->previous == he->mate)
							outer->half = NULL;
						else if (he->next == he->mate)
							outer->half = he->previous;
						else
							outer->half = he->next;
					}
					// A dangling edge is removed
					delete (he->mate);
					delete (he);
					remove_edge (edge3);
					edge1 = edge3 = NULL;
					remove_vertex(v3);
				}
			}
		} else if (edge3->half->left == outer &&
				edge3->half->mate->left == outer &&
				edge1->half->left == outer &&
				edge1->half->mate->left == outer) {
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (he->previous == he->mate)
				v3->half = NULL;
			else
				v3->half = he->previous->mate;
			if (!v3->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->mate->previous->next = he->next;
				he->next->previous = he->mate->previous;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge3);
				edge3 = NULL;
				remove_vertex(v3);
			}
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = NULL;
				remove_vertex(v1);
			}
		} else if (edge3->half->left == outer &&
				edge3->half->mate->left == outer) {
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (he->previous == he->mate)
				v3->half = NULL;
			else
				v3->half = he->previous->mate;
			if (!v3->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge3);
				edge3 = NULL;
				remove_vertex(v3);
			}
		} else if (edge1->half->left == outer &&
				edge1->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = NULL;
				remove_vertex(v1);
			}
		}
	}  else if (edge1 && edge2 && !edge3) {
		// Update the loop adjacencies
		outer->half = e30->mate->next;
		// Update the vertex adjacencies
		new_v->half = e30->mate->next;
		e30->mate->next->previous = e30->mate->previous;
		e30->mate->previous->next = e30->mate->next;
		if (edge1 == edge2) {
			if (edge1->half->left == outer &&
					edge1->half->mate->left == outer) {
				if (edge1->half->origin == v1)
					he = edge1->half;
				else
					he = edge1->half->mate;
				if (he->previous == he->mate)
					v1->half = NULL;
				else
					v1->half = he->previous->mate;
				if (!v1->half) {
					// Update the edge adjacencies
					if (he->next == he->mate)
						new_v->half = NULL;
					else {
						new_v->half = he->next;
						he->mate->previous->next = he->next;
						he->next->previous = he->mate->previous;
					}
					// Update the loop adjacencies
					if (outer->half == he || outer->half == he->mate) {
						if (he->next == he->mate && he->previous == he->mate)
							outer->half = NULL;
						else if (he->next == he->mate)
							outer->half = he->previous;
						else
							outer->half = he->next;
					}
					// A dangling edge is removed
					delete (he->mate);
					delete (he);
					remove_edge (edge1);
					edge1 = edge2 = NULL;
					remove_vertex(v1);
				}
			}
		} else if (edge1->half->left == outer &&
				edge1->half->mate->left == outer &&
				edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->mate->previous->next = he->next;
				he->next->previous = he->mate->previous;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = NULL;
				remove_vertex(v1);
			}
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate)
				v2->half = NULL;
			else
				v2->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = NULL;
				remove_vertex(v2);
			}
		} else if (edge1->half->left == outer &&
				edge1->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = NULL;
				remove_vertex(v1);
			}
		} else if (edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = NULL;
				remove_vertex(v2);
			}
		}

		/// Three edge-adjacent faces
	} else if (edge1 && edge2 && edge3) {
		if (edge1->half->origin == new_v)
			he = edge1->half;
		else
			he = edge1->half->mate;
		new_v->half = he;

		// edge1 == edge2 && edge2 == edge3 is a spatial relation still not
		// handled
		if (edge1 == edge2 && edge2 != edge3 &&
				edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = edge2 = NULL;
				remove_vertex(v1);
			}
		} else if (edge2 == edge3 && edge1 != edge2 &&
				edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate)
				v2->half = NULL;
			else
				v2->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = edge3 = NULL;
				remove_vertex(v2);
			}
		} else if (edge1 == edge3 && edge1 != edge2 &&
				edge1->half->left == outer &&
				edge1->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate)
				v1->half = NULL;
			else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				new_v->half = he->next;
				he->next->previous = he->mate->previous;
				he->mate->previous->next = he->next;
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = edge3 = NULL;
				remove_vertex(v1);
			}
		}
		if ( edge1 && edge1->half->left == outer &&
				edge1->half->mate->left == outer) {
			if (edge1->half->origin == v1)
				he = edge1->half;
			else
				he = edge1->half->mate;
			if (he->previous == he->mate) {
				v1->half = NULL;
			} else
				v1->half = he->previous->mate;
			if (!v1->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge1);
				edge1 = NULL;
				remove_vertex(v1);
			}
		}
		if (edge2 && edge2->half->left == outer &&
				edge2->half->mate->left == outer) {
			if (edge2->half->origin == v2)
				he = edge2->half;
			else
				he = edge2->half->mate;
			if (he->previous == he->mate) {
				v2->half = NULL;
			} else
				v2->half = he->previous->mate;
			if (!v2->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge2);
				edge2 = NULL;
				remove_vertex(v2);
			}
		}
		if (edge3 && edge3->half->left == outer &&
				edge3->half->mate->left == outer) {
			if (edge3->half->origin == v3)
				he = edge3->half;
			else
				he = edge3->half->mate;
			if (he->previous == he->mate) {
				v3->half = NULL;
			} else
				v3->half = he->previous->mate;
			if (!v3->half) {
				// Update the vertex adjacencies
				if (he->next == he->mate)
					new_v->half = NULL;
				else {
					new_v->half = he->next;
					he->next->previous = he->mate->previous;
					he->mate->previous->next = he->next;
				}
				// Update the loop adjacencies
				if (outer->half == he || outer->half == he->mate) {
					if (he->next == he->mate && he->previous == he->mate)
						outer->half = NULL;
					else if (he->next == he->mate)
						outer->half = he->previous;
					else
						outer->half = he->next;
				}
				// A dangling edge is removed
				delete (he->mate);
				delete (he);
				remove_edge (edge3);
				edge3 = NULL;
				remove_vertex(v3);
			}
		}
	}

	// Check the dependencies of new_v
	if (new_v->half && new_v->half->left == outer &&
			new_v->half->previous == new_v->half->mate) {
		if (new_v->half->next != new_v->half->mate) {
			new_v->half->next->previous = new_v->half->mate->previous;
			new_v->half->mate->previous->next = new_v->half->next;
		}
		// Update the loop adjacencies
		// It should consider the possilibity that there is more than one loop
		if (outer->half == new_v->half ||
				outer->half == new_v->half->mate) {
			if (new_v->half->next == new_v->half->mate &&
					new_v->half->previous == new_v->half->mate)
				outer->half = NULL;
			else if (new_v->half->next == new_v->half->mate)
				outer->half = new_v->half->previous;
			else
				outer->half = new_v->half->next;
		}
		// A dangling edge is removed
		vv = new_v->half->mate->origin;
		if (vv->GeomPtr) delete (char *)(vv->GeomPtr);
		vv->GeomPtr = ptr;
		new_v->GeomPtr = NULL;
		if (new_v->half->previous == new_v->half->mate) {
			vv->half = NULL;
		} else {
			vv->half = new_v->half->mate->previous;
		}
		index = new_v->half->mate->origin->ID;
		delete (new_v->half->mate);
		delete (new_v->half);
		remove_edge (new_v->half->edge);
		remove_vertex(new_v);
		new_v = vv;
	}

	// Update the mesh topology
	// Remove the triangle f AND its adjacent vertices
	remove_edge(e10->edge);
	remove_edge(e20->edge);
	remove_edge(e30->edge);
	delete(e10->mate);
	delete(e20->mate);
	delete(e30->mate);
	delete(e10);
	delete(e20);
	delete(e30);
	remove_loop(f);

	return index;
} // ReduceTriangleToPoint

void Mesh::removeMesh() {
  HalfEdge *he;

  // Clear halfedges
  for (eIterator = edges.begin(); eIterator != edges.end(); eIterator++) {
    Edge *e = eIterator->second;
    if (e->GeomPtr) delete e->GeomPtr;
    HalfEdge *he = e->half;
    delete he->mate;
    delete he;
    delete e;
  }
  for (vIterator = vertices.begin(); vIterator != vertices.end(); vIterator++) {
    Vertex *e = vIterator->second;
    if (e->GeomPtr) delete e->GeomPtr;
    delete e;
  }
  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++) {
    Loop *e = lIterator->second;
    if (e->GeomPtr) delete e->GeomPtr;
    delete e;
  }

  vertices.clear();
  edges.clear();
  loops.clear();

  return;
}


// =======================================================================
// SET:
// =======================================================================
int Mesh::setVGeomPtr (const VertexID a, void * ptr)
{
  Vertex* v;

  vIterator = vertices.find(a);
  if (vIterator != vertices.end())
      v = vIterator->second;
  else {
      cout << "Vertex "<< a << " does not exist." << endl;
      return 0;
  }
  v->GeomPtr = ptr;

  return 1;
}

int Mesh::setFGeomPtr (const LoopID face, void * ptr)
{
  Loop* f;

  lIterator = loops.find(face);
  if (lIterator != loops.end())
      f = lIterator->second;
  else {
      cout << "Loop "<< face << " does not exist." << endl;
      return 0;
  }
  f->GeomPtr = ptr;

  return 1;
}

// =======================================================================
// QUERY:
// =======================================================================
bool Mesh::existFace (const FaceID f)
{
  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return false;
    }
    return true;
  } else
    return false;
}

void*  Mesh::getVGeomPtr (const VertexID a)
{
  Vertex* v;

  vIterator = vertices.find(a);
  if (vIterator != vertices.end())
      v = vIterator->second;
  else {
      cout << "Vertex "<< a << " does not exist." << endl;
      return(0);
  }
  return(v->GeomPtr);
}

void*  Mesh::getFGeomPtr (const LoopID f)
{
  Loop* face;

  lIterator = loops.find(f);
  if (lIterator != loops.end())
      face = lIterator->second;
  else {
      cout << "Loop "<< f << " does not exist." << endl;
      return(0);
  }
  return(face->GeomPtr);
}

EdgeID Mesh::getEdge (const VertexID a, const VertexID b)
{
  HalfEdge *he;

  he = getHalfEdge(a, b);

  if (!he) he = getHalfEdge(b,a);

  if (he) return((he->edge)->ID);

  else return 0;
}

FaceID Mesh::getFace (const VertexID a, const VertexID b)
{
  HalfEdge *he;

  he = getHalfEdge(a, b);

  if (he) return((he->left)->ID);

  else return 0;
}

signed char Mesh::isVOnBoundary (const VertexID v)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return -1;

  if (!he) return -1;

  che = he;
  if (che->left->type == 2) return(1);
  mhe = che->mate;
  che = mhe->next;
  while (che && che != he) {
    if (che->left->type == 2) return(1);
    mhe = che->mate;
    che = mhe->next;
  }
  return(0);
}

void Mesh::getVVertices (const VertexID v, vector<void *> *list)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return;

  if (!he) return;

  che = he;
  mhe = che->mate;
  (*list).push_back((mhe->origin)->GeomPtr);
  che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
    (*list).push_back((mhe->origin)->GeomPtr);
    che = mhe->next;
  }
  return;
}

void Mesh::getVVertices (const VertexID v, vector<VertexID> *list)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return;

  if (!he) return;

  che = he;
  mhe = che->mate;
  (*list).push_back((mhe->origin)->ID);
  che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
    (*list).push_back((mhe->origin)->ID);
    che = mhe->next;
  }
  return;
}

void Mesh::getVFaces (const VertexID v, vector<void *> *list)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return;

  if (!he) return;

  che = he;
  mhe = che->mate;
  if (mhe->left->type == 1)
    (*list).push_back((mhe->left)->GeomPtr);
  che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
    if (mhe->left->type == 1)
      (*list).push_back((mhe->left)->GeomPtr);
    che = mhe->next;
  }
  return;
}

void Mesh::getVFaces (const VertexID v, vector<FaceID> *list)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end()) 
    he = (vIterator->second)->half;
  else
    return;

  if (!he) return;
 
  che = he;
  mhe = che->mate; 
  if (mhe->left->type == 1)
    (*list).push_back((mhe->left)->ID);
  che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
    if (mhe->left->type == 1)
      (*list).push_back((mhe->left)->ID);
    che = mhe->next;   
  }     
  return;
}

void Mesh::getCVertices (const CycleID f, vector<VertexID> *list)
{
  HalfEdge *he, *che;

  // Cycles are unfilled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 2) {
      cout << f << "is not a cycle." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

  (*list).push_back((he->origin)->ID);
  che = he->next;
  while (che && che != he) {
    (*list).push_back((che->origin)->ID);
    che = che->next;
  }
  return;
}

void Mesh::getCVertices (const CycleID f, vector<void *> *list)
{
  HalfEdge *he, *che;

  // Cycles are unfilled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 2) {
      cout << f << "is not a cycle." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

  (*list).push_back((he->origin)->GeomPtr);
  che = he->next;
  while (che && che != he) {
    (*list).push_back((che->origin)->GeomPtr);
    che = che->next;
  }
  return;
}

signed char Mesh::isFOnBoundary (const FaceID f)
{
  HalfEdge *he, *che, *mhe;

  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return -1;
    }
    he = (lIterator->second)->half;
  } else
    return -1;

  if (!he) return -1;

  if (he->mate->left->type == 2) return(1);
  mhe = he->mate->next;
  while (mhe != he) {
    if (mhe->mate->left->type == 2) return (1);
    mhe = mhe->mate->next;
  }
  che = he->next;
  while (che && che != he) {
    if (che->mate->left->type == 2) return(1);
    mhe = che->mate->next;
    while (mhe != che) {
      if (mhe->mate->left->type == 2) return (1);
      mhe = mhe->mate->next;
    }
    che = che->next;
  }
  return(0);
}

void Mesh::getFVertices (const FaceID f, vector<VertexID> *list)
{
  HalfEdge *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

  (*list).push_back((he->origin)->ID);

  che = he->next;
  while (che && che != he) {
    (*list).push_back((che->origin)->ID);

    che = che->next;
  }

  return;
}

void Mesh::getFVertices (const FaceID f, vector<void *> *list)
{
  HalfEdge *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

   (*list).push_back((he->origin)->GeomPtr);
  che = he->next;
  while (che && che != he) {
    (*list).push_back((che->origin)->GeomPtr);
    che = che->next;
  }
  return;
}

void Mesh::getFOneRingVertices (const FaceID f, vector<VertexID> *list)
{
  HalfEdge *phe, *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return;
    }
    he = (lIterator->second)->half;
    phe = he->previous->mate;
    he = he->mate;
  } else
    return;

  if (!he) return;

  if (he->next != phe) {
    (*list).push_back((he->next->mate->origin)->ID);
    che = he->next->mate->next->mate;
  } else {
    che = he->next;
    phe = phe->mate->previous->mate;
  }
  while (che && che != he) {
    if (che->next == phe && che != phe->mate->next->mate ) {
      (*list).push_back((che->origin)->ID);
      che = che->next;
      phe = phe->mate->previous->mate;
    } else if (che->next == phe) {
      che = che->next;
      phe = phe->mate->previous->mate;
    } else if (che->next != phe && che != phe->mate->next->mate ) {
      (*list).push_back((che->origin)->ID);
      che = che->next->mate;
    } else if (che->next != phe) {
      che = che->next->mate;
    } else if (che == phe && che->next != phe->mate->previous->mate) {
      che = che->next->mate;
      phe = phe->mate->previous->mate;
    } else {
      che = che->next;
      phe = phe->mate->previous->mate;
    }
  }

  return;
}

void Mesh::getFAdjacentFacesPerEdge (const FaceID f, vector<FaceID> *list)
{
  HalfEdge *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << " is not a face." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

  if ((he->mate)->left->type == 1) (*list).push_back(((he->mate)->left)->ID);
  che = he->next;
  while (che && che != he) {
    if ((che->mate)->left->type == 1)
       (*list).push_back(((che->mate)->left)->ID);
    che = che->next;
  }
  return;
}

void Mesh::getFAdjacentFacesPerEdge (const FaceID f, vector<void *> *list)
{
  HalfEdge *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return;
    }
    he = (lIterator->second)->half;
  } else
    return;

  if (!he) return;

  (*list).push_back(((he->mate)->left)->GeomPtr);
  che = he->next;
  while (che && che != he) {
    (*list).push_back(((che->mate)->left)->GeomPtr);
    che = che->next;
  }
  return;
}

/// This function only works for triangular meshes
int Mesh::haveFDistinctAdjacentTrianglesPerEdge (const FaceID f)
{
  HalfEdge *he, *che;

  // Faces are filled loops
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) {
      cout << f << "is not a face." << endl;
      return -1;
    }
    he = (lIterator->second)->half;
  } else
    return -1;

  if (!he) return -1;

  if (he->left == he->mate->left)
    return 0;
  else if (he->mate->left != _outer_loop &&
      he->mate->left == he->next->mate->left &&
      he->mate->left == he->previous->mate->left &&
      he->next->mate->left == he->previous->mate->left)
    return 1;
  else if ((he->mate->left != _outer_loop &&
	   (he->mate->left == he->next->mate->left ||
	    he->mate->left == he->previous->mate->left)) ||
	   (he->next->mate->left != _outer_loop &&
	    he->next->mate->left == he->previous->mate->left))
    return 2;
  else
    return 3;
}

void Mesh::getAllVertices (vector<VertexID>* list)
{
  for (vIterator = vertices.begin(); vIterator != vertices.end(); vIterator++) {
    (*list).push_back((vIterator->second)->ID);
  }
  return;
}

void Mesh::getBorderVertices (vector<VertexID>* list)
{

	//Com a introducao do shell, podemos utilizar o
	//outer loop para percorrer a borda
	//obs a malha nao tem furo!
	HalfEdge *he, *first;

	if (_outer_loop &&_outer_loop->half &&_outer_loop->half) {
//		cout << "outer:" << _outer_loop->half->origin->ID << endl;
		(*list).push_back((_outer_loop->half->origin)->ID);
		he = _outer_loop->half->next;

		//cout << "BORDA\n";
		//cout << "Vertice:" << (_outer_loop->half->origin)->ID << endl;
		while (he != _outer_loop->half) {
			//	cout << "Vertice:" << (he->origin)->ID << endl;
			(*list).push_back((he->origin)->ID);
			he = he->next;
		}
	}

	/*
	int i;
	cout << "ENTRANDO BORDA\n";
	for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++) {
		i = 0;
		if ((lIterator->second)->type == 2) {
			cout << "Id faces:" << (lIterator->first) << endl;
			he = (lIterator->second)->half;
			cout << "origem he:" << he->origin->ID << endl;
			cout << "end he:" << he << endl;
			first = he;
			if (first) {
				(*list).push_back((he->origin)->ID);
				he = he->next;
				i++;
			}
			while (he && he != first) {
//				cout << "while\n";
				(*list).push_back((he->origin)->ID);
				he = he->next;
				i++;
			}
			// Insert the separator "-1"
			if (i>0) (*list).push_back(-1);
		}
	}*/
	return;
}

void Mesh::getBorderVertices (vector<void *> *list)
{
  HalfEdge *he, *first;
  int i;

  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++) {
    i = 0;
    if ((lIterator->second)->type == 2) {
      he = (lIterator->second)->half;
      first = he;
      if (first) {
	(*list).push_back((he->origin)->GeomPtr);
	he = he->next;
	i++;
      }
      while (he != first) {
	(*list).push_back((he->origin)->GeomPtr);
	he = he->next;
	i++;
      }
      // Insert the separator "NULL"
      if (i>0) (*list).push_back(NULL);
    }
  }

  return;
}

int Mesh::numCycles ()
{
  int x=0;

  // Cycles are unfilled loops
  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++){
    if ((lIterator->second)->type == 2) {
      x++;
    }
  }
  return x;
}

void Mesh::getAllCycles (vector<CycleID>* list)
{
  // Cycles are unfilled loops
  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++){
    if ((lIterator->second)->type == 2) {
      (*list).push_back((CycleID)(lIterator->second)->ID);
    }
  }
  return;
}

bool Mesh::isOuterLoop (const CycleID ID)
{
  // Cycles are unfilled loops
  lIterator = loops.find(ID);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type == 2 &&
	(CycleID)(lIterator->second)->ID == _outer_loop->ID) {
      return (1);
    }
  }
  return (0);
}

int Mesh::numFaces ()
{
  int x=0;

  // Cycles are unfilled loops
  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++){
    if ((lIterator->second)->type == 1) {
      x++;
    }
  }
  return x;
}

void Mesh::getAllFaces (vector<FaceID>* list)
{
  // Faces are filled loops

  for (lIterator = loops.begin(); lIterator != loops.end(); lIterator++){
    if ((lIterator->second)->type == 1) {
      (*list).push_back((FaceID)(lIterator->second)->ID);
    }
  }
  return;
}

// =======================================================================
// PRIVATE FUNCTIONS:
// =======================================================================
HalfEdge* Mesh::getHalfEdge (const VertexID a, const VertexID b)
{
	//cout << "Gethalfedge\n";
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(a);
  if ( vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return(0);

  if (!he) return(0);
// cout << "Id a:" << a << endl;
// cout << "Id b:" << b << endl;


  che = he;
  mhe = che->mate;

//  cout << "(che->origin)->ID:" << (che->origin)->ID << endl;
//   cout << "(mhe->origin)->ID:" << (mhe->origin)->ID << endl;


  if ((che->origin)->ID == a && (mhe->origin)->ID == b) return (che);
  else che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
//    cout << "(che->origin)->ID:" << (che->origin)->ID << endl;
//      cout << "(mhe->origin)->ID:" << (mhe->origin)->ID << endl;
    if ((che->origin)->ID == a && (mhe->origin)->ID == b) return (che);
    else {che = mhe->next;}
  }
  //cout << "retorna 0\n";
  return(0);
}

HalfEdge* Mesh::getOuterHalfEdge (const Vertex* v, HalfEdge *av, HalfEdge *vb, const Loop *outer)
{
  HalfEdge *he, *che, *cand, *cand1;


//  cout << "outer:" << _outer_loop->ID << endl;
//  cout << "av:" << av->left->ID << endl;
//  cout << "vb:" << vb->left->ID << endl;
//  cout << "vb type:" << vb->left->type << endl;

  //cout << "\nGetouterhalfedge\n";

  if (!v->half) return 0;

 /* cout << "proximo de ab ID:" << v->half->origin->ID << endl;
  cout << "proximo de ab:" << v->half->next << endl;
  cout << "mate de ab:" << v->half->mate << endl;*/
//  cout << "ab:" << v->half->origin->ID << endl;
//  cout << "proximo de ab:" << v->half->next->origin->ID << endl;
  che = v->half;
  he = che;
  int count = 0, count1=0;

  if (che->mate == av){
	  return(av);
  }
  else {
	   if (che == vb){
		   return(vb);
	   }
       else{
    	   if (che->left == outer) {
    	   // bouding halfedge lies on <outer>
    		   count1++;
    		   cand1 = che;
    	   } else {
    		   if (che->left->type == 2 && (che->previous)->left->type == 2) {
    	   // any outer edge
    		   count++;
    		   cand = che;
    	   }
         }
       }
  }
  //cout << "chegou no Repita\n";
  che = (che->previous)->mate;
  while (che && che != he) {
    if (che->mate == av) return(av);
    else if (che == vb) return(vb);
    else if (che->left == outer) {
      // bouding halfedge lies on <outer>
      count1++;
      cand1 = che;
    } else if (che->left->type == 2 && (che->previous)->left->type == 2) {
      // any outer edge
      count++;
      cand = che;
    }
    che = (che->previous)->mate;
  }

  if (count == 1) {//prioridade ao inner loop
	//  cout << "Cand\n";
	  return (cand);
  }
  else if (count1 == 1) {//prioridade ao outer loop
	  //cout << "Cand1\n";
	  return (cand1);
  }


  if (count == 0 && count1 == 0) {
    cout << v->ID << " is not a 2D-manifold vertex." << endl;
    return 0;
  } else {
    cout << "It is not allowed articulated vertex " << v->ID <<"." << endl;
    return 0;
  }

}
Loop * Mesh::getOuterL (const VertexID a, const VertexID b, const VertexID c) {

	/*This function return the cycle.
	 *Input: Identifier of the vertices a, b, c.
	 *Output: Cycle that contains at least one of the vertices: a, b, c
	 *if the cycle exist.   */

	HalfEdge *half_edge_a, *half_edge_b, *half_edge_c, *half_edge, *c_half_edge, *m_half_edge;

	half_edge_a = getHalfEdge(a,b);
	if (half_edge_a && half_edge_a->left->type == 2) {
		return (half_edge_a->left);
	}
	else {
		half_edge_b = getHalfEdge(b,c);
		if (half_edge_b && half_edge_b->left->type == 2) {
			return(half_edge_b->left);
		}
		else {
			half_edge_c = getHalfEdge(c,a);
			if (half_edge_c && half_edge_c->left->type == 2) {
				return(half_edge_c->left);
			}
		}
	}

	Loop *l = 0;

	vIterator = vertices.find(a);
	if (vIterator != vertices.end()) {
		half_edge = (vIterator->second)->half;
		if (half_edge) {
			if (half_edge->left->type == 2) l = half_edge->left;
			if (half_edge->mate->left->type == 2) l = half_edge->mate->left;

			c_half_edge = half_edge;
			m_half_edge = c_half_edge->mate;
			c_half_edge = half_edge->next;
			while (c_half_edge && c_half_edge != half_edge && !l) {

				if (c_half_edge->left->type == 2) l = c_half_edge->left;
				if (c_half_edge->mate->left->type == 2) l = c_half_edge->mate->left;

				c_half_edge = c_half_edge->next;
			}
		}
	}

	vIterator = vertices.find(b);
	if (vIterator != vertices.end()) {
		half_edge = (vIterator->second)->half;
		if (half_edge) {
			if (half_edge->left->type == 2) l = half_edge->left;
			if (half_edge->mate->left->type == 2) l = half_edge->mate->left;

			c_half_edge = half_edge;
			m_half_edge = c_half_edge->mate;
			c_half_edge = half_edge->next;
			while (c_half_edge && c_half_edge != half_edge && !l) {

				if (c_half_edge->left->type == 2) l = c_half_edge->left;
				if (c_half_edge->mate->left->type == 2) l = c_half_edge->mate->left;

				c_half_edge = c_half_edge->next;
			}
		}
	}

	vIterator = vertices.find(c);
	if (vIterator != vertices.end()) {
		half_edge = (vIterator->second)->half;
		if (half_edge) {
			if (half_edge->left->type == 2) l = half_edge->left;
			if (half_edge->mate->left->type == 2) l = half_edge->mate->left;

			c_half_edge = half_edge;
			m_half_edge = c_half_edge->mate;
			c_half_edge = half_edge->next;
			while (c_half_edge && c_half_edge != half_edge && !l) {

				if (c_half_edge->left->type == 2) l = c_half_edge->left;
				if (c_half_edge->mate->left->type == 2) l = c_half_edge->mate->left;

				c_half_edge = c_half_edge->next;
			}
		}
	}

	return l;
}

Loop * Mesh::getOuterLoop (const VertexID a, const VertexID b, const VertexID c)
{
	HalfEdge *he, *hea, *heb, *hec, *che, *mhe;
	vector<Loop *>list1, list2, list3;
	vector<Loop *>::iterator Iter1, Iter2, Iter3;

	// Get the common outer loop of the existing halfedges (retorna o loop ciclo comum aos 3 v�rtices)
	//se encontrar um loop externo e interno que seja comum a todos, retorna oq achar primeiro.
	//caso nao exista, volta algum loop que contenha pelo menos um dos v�rtices.

	hea = getHalfEdge(a,b);


	//verifica se existe um loop assim: a-b-c
	if (hea && hea->left->type == 2) {
		heb = getHalfEdge(b,c);
		if (heb && heb->left == hea->left) {
			hec = getHalfEdge(c,a);
			if (hec && hec->left == hea->left) {
				return(hea->left);
			}
		}
	} else {
		heb = getHalfEdge(b,c);
		if (heb && heb->left->type == 2) {
			hec = getHalfEdge(c,a);
			if (hec && hec->left == heb->left) {
				return(heb->left);
			}
			else return(heb->left);
		} else {
			hec = getHalfEdge(c,a);
			if (hec && hec->left->type == 2) {
				return(hec->left);
			}
		}
	}

	//Get the common outer loops associated to the existing vertices
	//pega os halfedges tipo ciclo
	vIterator = vertices.find(a);
	if (vIterator != vertices.end()) {
		he = (vIterator->second)->half;
		if (he) {
			che = he;
			mhe = che->mate;
			che = mhe->next;
			while (che && che != he) {
				mhe = che->mate;
				if (mhe->left->type == 2) list1.push_back(mhe->left);//verifica qual esta fora(half ou mate)
				if (mhe->mate->left->type == 2) list1.push_back(mhe->mate->left);
				//if (che->left->type == 2) list1.push_back(mhe->mate->left);
				che = mhe->next;
			}
		}
	}

	vIterator = vertices.find(b);
	if (vIterator != vertices.end()) {
		he = (vIterator->second)->half;
		if (he) {
			che = he;
			mhe = che->mate;
			che = mhe->next;
			while (che && che != he) {
				mhe = che->mate;
				if (mhe->left->type == 2) list2.push_back(mhe->left);
				if (mhe->mate->left->type == 2) list2.push_back(mhe->mate->left);
				che = mhe->next;
			}
		}
	}

	vIterator = vertices.find(c);
	if (vIterator != vertices.end()) {
		he = (vIterator->second)->half;
		if (he) {
			che = he;
			mhe = che->mate;
			che = mhe->next;
			while (che && che != he) {
				mhe = che->mate;
				if (mhe->left->type == 2) list3.push_back(mhe->left);
				if (mhe->mate->left->type == 2) list3.push_back(mhe->mate->left);
				che = mhe->next;
			}
		}
	}

	Loop *l;

	if (list1.size() > 0) {
		l = list1.front();
	} else if (list2.size() > 0) {
		l = list2.front();
	} else if (list3.size() > 0) {
		l = list3.front();
	} else l = 0;

	for ( Iter1 = list1.begin( ) ; Iter1 != list1.end( ) ; Iter1++ ) {
		for ( Iter2 = list2.begin( ) ; Iter2 != list2.end( ) ; Iter2++ ) {
			if (*Iter1 == *Iter2) {
				for ( Iter3 = list3.begin( ) ; Iter3 != list3.end( ) ; Iter3++ ) {
					if (*Iter1 == *Iter3) {
						l = *Iter1;
						break;
					}
				}
			}
		}
	}

	list1.clear();
	list2.clear();
	list3.clear();

	return (l);
}

void Mesh::getVVertices (const VertexID v, vector<Vertex*> *list)
{
  HalfEdge *he, *che, *mhe;

  vIterator = vertices.find(v);
  if (vIterator != vertices.end())
    he = (vIterator->second)->half;
  else
    return;

  if (!he) return;
 
  che = he;
  mhe = che->mate;

  (*list).push_back(mhe->origin);
  che = mhe->next;
  while (che && che != he) {
    mhe = che->mate;
    (*list).push_back(mhe->origin);
    che = mhe->next;
  }     
  return;
}

void Mesh::getFVertices (const FaceID f, vector<Vertex*> *list)
{
  HalfEdge *he, *che;

  // In this implementation the data type of loops is the same
  // as the data type of faces
  lIterator = loops.find(f);
  if (lIterator != loops.end()) {
    if ((lIterator->second)->type != 1) return;
    he = (lIterator->second)->half;
  }
  else
    return;

  if (!he) return;
 
  (*list).push_back(he->origin);
  che = he->next;
  while (che && che != he) {
    (*list).push_back(che->origin);
    che = che->next;
  }
  return;
}

void Mesh::MergeLoops(Loop* loop1, Loop* loop2)
{
  HalfEdge *he, *first;

  he = loop2->half;
  he->left = loop1;
  first = he;
  he = he->next;
  while (he != first) {
    if (he->left == loop2) {
      he->left = loop1;
    }
    he = he->next;
  }
}

void Mesh::SetLoop(Loop *loop, HalfEdge *e1, HalfEdge *e2)
{
  HalfEdge *he;

  he = e1;
  while (he != e2) {
    he->left = loop;
    he = he->next;
  }

  if (_outer_loop == loop) _outer_loop = loop;
  e1->left = e2->left = loop;
}
//MergeLoops((half_edge_ab->mate)->previous->left, (half_edge_ab->mate)->next->left, (half_edge_ab->mate)->next);
void Mesh::MergeLoops(Loop *loop1, Loop *loop2, HalfEdge *e)
{
  HalfEdge *he, *first;

  he = first = e;
  if (he->left == loop2) {
    he->left = loop1;
    if (loop2->half == he && he->next->left != loop2 && he->previous->left != loop2)
	loop2->half = NULL;
    else if (loop2->half == he && he->next->left == loop2)
      loop2->half = he->next;
    else if (loop2->half == he && he->previous->left == loop2)
      loop2->half = he->previous;
  }
  he = he->next;
  while (he != first) {
    if (he->left == loop2) {
      he->left = loop1;
      if (loop2->half == he && he->next->left != loop2 && he->previous->left != loop2)
	loop2->half = NULL;
      else if (loop2->half == he && he->next->left == loop2)
	loop2->half = he->next;
      else if (loop2->half == he && he->previous->left == loop2)
	loop2->half = he->previous;
    }
    he = he->next;
  }
}
//=====
//criada
void Mesh :: getV(int ID, Vertex *vt){
	 vIterator = vertices.find(ID);
	 *vt = *(vIterator)->second;
}

int Mesh :: is_external (deque<Store_loop *> last_loop, HalfEdge *prev_half, HalfEdge *next_half) {

	long int i;
	int in, out, index;

	//Behind
	i = 0;
	while (last_loop[i]->half != prev_half) i++;

	if (i+1 > last_loop.size()) index = 0;
	else index = i+1;

	if (last_loop[index]->half->left->type == 1) {//behind in
		in = 1;
	}
	else in = 0;

	//Next
	i = 0;
	while (last_loop[i]->half != next_half) i++;

	if (i-1 < 0) index = last_loop.size()-1;
	else index = i-1;

	if (last_loop[index]->half->left->type == 2) {//next out
		out = 1;
	}
	else out = 0;

	if (in && out) return 1; //external
	else return 0; //no external
}


void Mesh :: buid_edge_relations( HalfEdge *half_edge_ab, HalfEdge *half_edge_bc, HalfEdge *half_edge_ca, HalfEdge *half_edge1, HalfEdge *half_edge2, HalfEdge *half_edge3) {

	/*Build edge relations.
	 *Input: Halfegdes required: ab, bc, AND ca.
	 *The halfegdes for put the triangle in mesh: half_edge1, half_edge2, AND half_edge3
	 * Output: none*/

	//Make loop external
	//*********************************AB********************************
	if (half_edge_ab->next != half_edge_ab->mate) {
		if (half_edge_ab->next != half_edge_bc) {
			(half_edge_ab->next)->previous = half_edge_bc->mate;
			(half_edge_bc->mate)->next = half_edge_ab->next;
		}
		if (half_edge_ab->previous != half_edge_ca) {
			(half_edge_ab->previous)->next = half_edge_ca->mate;
			(half_edge_ca->mate)->previous = half_edge_ab->previous;
		}
	}
	else {
		if (half_edge_ca->next == half_edge_ca->mate) {
			(half_edge1->previous)->next = half_edge_ca->mate;
			(half_edge_ca->mate)->previous = half_edge1->previous;
			(half_edge_ab->mate)->next = half_edge1;
			half_edge1->previous = half_edge_ab->mate;
		}
	}
	//*********************************BC********************************
	if (half_edge_bc->next != half_edge_bc->mate) {
		if (half_edge_bc->next != half_edge_ca) {
			(half_edge_bc->next)->previous = half_edge_ca->mate;
			(half_edge_ca->mate)->next = half_edge_bc->next;
		}
		if (half_edge_bc->previous != half_edge_ab) {
			(half_edge_bc->previous)->next = half_edge_ab->mate;
			(half_edge_ab->mate)->previous = half_edge_bc->previous;
		}
	}
	else {
		if (half_edge_ab->next == half_edge_ab->mate) {
			(half_edge2->previous)->next = half_edge_ab->mate;
			(half_edge_ab->mate)->previous = half_edge2->previous;
			(half_edge_bc->mate)->next = half_edge2;
			half_edge2->previous = half_edge_bc->mate;
		}
	}
	//*********************************CB********************************
	if (half_edge_ca->next != half_edge_ca->mate) {
		if (half_edge_ca->next != half_edge_ab) {
			(half_edge_ca->next)->previous = half_edge_ab->mate;
			(half_edge_ab->mate)->next = half_edge_ca->next;
		}
		if (half_edge_ca->previous != half_edge_bc) {
			(half_edge_ca->previous)->next = half_edge_bc->mate;
			(half_edge_bc->mate)->previous = half_edge_ca->previous;
		}
	}
	else {
		if (half_edge_bc->next == half_edge_bc->mate) {
			(half_edge3->previous)->next = half_edge_bc->mate;
			(half_edge_bc->mate)->previous = half_edge3->previous;
			(half_edge_ca->mate)->next = half_edge3;
			half_edge3->previous = half_edge_ca->mate;
		}
	}

	//Make loop internal
	half_edge_ab->previous = half_edge_ca;
	half_edge_ca->next = half_edge_ab;
	half_edge_bc->previous = half_edge_ab;
	half_edge_ab->next = half_edge_bc;
	half_edge_ca->previous = half_edge_bc;
	half_edge_bc->next = half_edge_ca;

}


int Mesh :: buid_face_relations(HalfEdge *half_edge_ab, HalfEdge *half_edge_bc, HalfEdge *half_edge_ca, Loop *outer, Loop *face) {

	/*Build face-edge relations.
	 *Input: Halfegdes required: ab, bc, ca, outer_loop, face.
	 * Output: Face's id*/

	typedef pair <int,Loop*> Pair;
	Loop *cycle;

	// Update the border entry
	if (half_edge_ab->mate->previous == half_edge_bc->mate && half_edge_bc->mate->previous == half_edge_ca->mate &&
			half_edge_ca->mate->previous == half_edge_ab->mate) {
		if (half_edge_bc->mate->left == half_edge_ab->mate->left && half_edge_bc->mate->left == half_edge_ca->mate->left &&
				half_edge_bc->mate->left->type == 1) {
			// Kill a cycle
			outer->half = NULL;
			remove_loop(outer);
		} else if (outer->half) {
			// A new loop is created
			cycle = new Loop;
			cycle->ID = (long)cycle; cycle->half = half_edge_ab->mate; cycle->type = 2;
			cycle->GeomPtr = NULL;
			loops.insert(Pair(cycle->ID,cycle));
			_nCycles++;
			half_edge_ab->mate->left = half_edge_bc->mate->left = half_edge_ca->mate->left = cycle;
		} else {
			// It is the first loop. Only the pointer is updated.
			outer->half = half_edge_ab->mate;
		}
		return face->ID;
	}

	int make_loop = 0;
	int external;
	//Half edge AB
	if ((half_edge_ab->mate)->left->type == 2) { //not a face
		if ((half_edge_ab->mate)->previous != half_edge_bc->mate &&
				(half_edge_ab->mate)->next != half_edge_ca->mate) {
			//the edge is linking something, same OR no?
			if (((half_edge_ab->mate)->previous)->left == ((half_edge_ab->mate)->next)->left) {
				//Linking parts same
				if ((half_edge_bc->mate)->left->type == 2 || (half_edge_ca->mate)->left->type == 2) {
					//new loop
					//Pointer Face
					if (half_edge_bc->mate->left->type == 2) {
						half_edge_ab->mate->previous->left->half = half_edge_bc->mate;//change pointer of face before
					}
					else half_edge_ab->mate->previous->left->half = half_edge_ca->mate;//change pointer of face before

					make_loop++;
					cycle = new Loop;
					cycle->ID = (long)cycle; cycle->half = half_edge_ab->mate; cycle->type = 2;
					cycle->GeomPtr = NULL;
					loops.insert(Pair(cycle->ID,cycle));
					_nCycles++;
					SetLoop(cycle, (half_edge_ab->mate)->next, half_edge_ab->mate);
					(half_edge_ab->mate)->left = cycle;
				}
				else {
					// another halfedges belong a faces
					(half_edge_ab->mate)->left = (half_edge_ab->mate)->next->left;
					((half_edge_ab->mate)->next)->left->half = half_edge_ab->mate->next;
				}
			}
			else {
				//linking parts differents
				//Merge
				cycle = (half_edge_ab->mate)->next->left;
				MergeLoops((half_edge_ab->mate)->previous->left, (half_edge_ab->mate)->next->left, (half_edge_ab->mate)->next);
				//remove loop
				remove_loop(cycle);
				_nCycles--;
			}
		}
	}

	//HalfEdge BC
	if ((half_edge_bc->mate)->left->type == 2) { //not a face
		if ((half_edge_bc->mate)->previous != half_edge_ca->mate &&
				(half_edge_bc->mate)->next != half_edge_ab->mate) {
			//the edge is linking something, same OR no?
			if (((half_edge_bc->mate)->previous)->left == ((half_edge_bc->mate)->next)->left) {
				//Linking parts same
				if ((half_edge_ab->mate)->left->type == 2 || (half_edge_ca->mate)->left->type == 2) {
					if (half_edge_ca->mate->left->type == 2 && half_edge_ca->mate->next != half_edge_bc->mate && half_edge_ca->mate->previous != half_edge_ab->mate && half_edge_ca->mate->next->left == half_edge_ca->mate->previous->left) { // the halfegde is internal?
						// the halfegde is internal AND the halfedge ca is internal
						//the condition above, ask if the halfedge ca form a cycle
						//new loop
						half_edge_bc->mate->previous->left->half = half_edge_ca->mate;//change pointer of face before
						make_loop++;
						cycle = new Loop;
						cycle->ID = (long)cycle; cycle->half = half_edge_bc->mate; cycle->type = 2;
						cycle->GeomPtr = NULL;
						loops.insert(Pair(cycle->ID,cycle));
						_nCycles++;
						SetLoop(cycle, (half_edge_bc->mate)->next, half_edge_bc->mate);
						(half_edge_bc->mate)->left = cycle;
					}
				}
				else {
					// another halfedges belong a faces
					(half_edge_bc->mate)->left = (half_edge_bc->mate)->next->left;
					((half_edge_bc->mate)->next)->left->half = half_edge_bc->mate->next;
				}
			}
			else {
				//linking parts differents
				//Merge
				cycle = (half_edge_bc->mate)->next->left;
				MergeLoops((half_edge_bc->mate)->previous->left, (half_edge_bc->mate)->next->left, (half_edge_bc->mate)->next);
				//remove loop
				remove_loop(cycle);
				_nCycles--;
			}
		}
	}

	//HalfEdge CA
	if ((half_edge_ca->mate)->left->type == 2) { //not a face
		if ((half_edge_ca->mate)->previous != half_edge_ab->mate &&
				(half_edge_ca->mate)->next != half_edge_bc->mate) {
			//the edge is linking something, same OR no?
			if (((half_edge_ca->mate)->previous)->left == ((half_edge_ca->mate)->next)->left) {
				//Linking parts same
				if ((half_edge_ab->mate)->left->type == 2 || (half_edge_bc->mate)->left->type == 2) {
					// use the loop created
					(half_edge_ca->mate)->left = (half_edge_ca->mate)->next->left;
					((half_edge_ca->mate)->next)->left->half = half_edge_ca->mate;
				}
				else {
					// another halfedges belong a faces
					(half_edge_ca->mate)->left = (half_edge_ca->mate)->next->left;
					((half_edge_ca->mate)->next)->left->half = half_edge_ca->mate->next;
				}
			}
			else {
				//linking parts differents
				cycle = (half_edge_ca->mate)->next->left;
				MergeLoops((half_edge_ca->mate)->previous->left, (half_edge_ca->mate)->next->left, (half_edge_ca->mate)->next);
				//remove loop
				remove_loop(cycle);
				_nCycles--;
			}
		}
	}

	if (_outer_loop->half->left->type == 1) {
		HalfEdge *loop_outer;
		loop_outer = _outer_loop->half->next;
		int flag = 0;
		//return face->ID;
		while (!flag) {
			if (loop_outer->mate->left->type == 2) {
				_outer_loop->half = loop_outer->mate;
				flag = 1;
			}
			loop_outer = loop_outer->next;
		}
	}
	return face->ID;
}

void Mesh :: obtain_pointer(HalfEdge **half_edge1, HalfEdge **half_edge2, HalfEdge **half_edge3, HalfEdge *half_edge_ab, HalfEdge *half_edge_bc, HalfEdge *half_edge_ca, Loop *outer) {

	/*Input: Halfedges a, b, c AND outer loop.
	 * Search the pointers for put the new triangle in the mesh correctly
	 * Output: The pointers necessaries for to put the new triangle in the mesh*/

	if (half_edge_ab->previous == half_edge_ca){
		*half_edge1 = half_edge_ab;
		//cout << "==>prev he1\n";
	}
	else{

		if (!(half_edge_ab->origin)->half) {
			*half_edge1 = half_edge_ca->mate;
			//cout << "==>he1\n";
		}
		else {
			//cout << "==>getouter he1\n";
			*half_edge1 = getOuterHalfEdge (half_edge_ab->origin, half_edge_ca, half_edge_ab, outer);
			//cout << "fim getouterhalfedge\n";
		}
	}

	if (half_edge_bc->previous == half_edge_ab) {
		*half_edge2 = half_edge_bc;
	}
	else {
		if (!(half_edge_bc->origin)->half){
			*half_edge2 = half_edge_ab->mate;
			//cout << "==>he2\n";
		}
		else {
			*half_edge2 = getOuterHalfEdge (half_edge_bc->origin, half_edge_ab, half_edge_bc, outer);
			//cout << "fim getouterhalfedge\n";
		}
	}

	if (half_edge_ca->previous == half_edge_bc){
		*half_edge3 = half_edge_ca;
	}
	else {
		if (!half_edge_ca->origin->half) {
			*half_edge3 = half_edge_bc->mate;
			//cout << "==>he3\n";
		}
		else {
			*half_edge3 = getOuterHalfEdge (half_edge_ca->origin, half_edge_bc, half_edge_ca, outer);
			//cout << "fim getouterhalfedge\n";
		}
	}

}

void Mesh::getAdjFaces (const VertexID a, const VertexID b, 
     vector<FaceID> *list)
{
  HalfEdge *he;

  he = getHalfEdge(a, b);

  if (he && he->left->type == 1)
    (*list).push_back((he->left)->ID);

  if (he && he->mate->left->type == 1) 
    (*list).push_back((he->mate->left)->ID);

  return;
}
