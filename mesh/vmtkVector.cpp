/*
 * vmtkVector.cpp
 *
 *  Created on: 06/01/2011
 *      Author: ra100300
 */

#include "vmtkVector.h"
#include "vmtkDefs.h"

Vector3 Vector_NewVector(float x, float y, float z) {
	Vector3 vector;
	vector.x = x;
	vector.y = y;
	vector.z = z;
	return vector;
}

void Vector_normalize(Vector3 * vector) {
	float magnitude;

	magnitude = sqrt(
			(vector->x * vector->x) + (vector->y * vector->y)
					+ (vector->z * vector->z));

	if (magnitude > EPS) {
		vector->x = (float) (vector->x / magnitude);
		vector->y = (float) (vector->y / magnitude);
		vector->z = (float) (vector->z / magnitude);
	} else {
		vector->x = vector->y = vector->z = 0.0;
	}
}

Vector3 Vector_normalized(Vector3 vector) {
	Vector_normalize(&vector);
	return vector;
}

float Vector_magnitude(Vector3 vector) {
	return sqrt(
			(vector.x * vector.x) + (vector.y * vector.y)
					+ (vector.z * vector.z));
}

Vector3 Vector_add(Vector3 vector1, Vector3 vector2) {
	Vector3 result;
	result.x = vector1.x + vector2.x;
	result.y = vector1.y + vector2.y;
	result.z = vector1.z + vector2.z;
	return result;
}

Vector3 Vector_sub(Vector3 vector1, Vector3 vector2) {
	return Vector_NewVector((vector1.x - vector2.x), (vector1.y - vector2.y),
			(vector1.z - vector2.z));
}

float Vector_dot(Vector3 vector1, Vector3 vector2) {
	return ((vector1.x * vector2.x) + (vector1.y * vector2.y)
			+ (vector1.z * vector2.z));
}

Vector3 Vector_cross(Vector3 vector1, Vector3 vector2) {
	Vector3 result;

	result.x = ((vector1.y * vector2.z) - (vector1.z * vector2.y));
	result.y = ((vector1.z * vector2.x) - (vector1.x * vector2.z));
	result.z = ((vector1.x * vector2.y) - (vector1.y * vector2.x));
	return result;
}

void Vector_print(char *msg, Vector3 vec) {
	printf("%s %0.2f %0.2f %0.2f\n", msg, vec.x, vec.y, vec.z);
}

float Vector_distance2points(Vector3 vec1, Vector3 vec2) {
	float distance = (float) sqrt(
			SQR(vec1.x - vec2.x) + SQR(vec1.y - vec2.y )
					+ SQR(vec1.z - vec2.z ));
	return distance;
}

Vector3 Vector_scalarmult(float scalar, Vector3 vec) {
	Vector3 result;
	result.x = (float) (scalar * vec.x);
	result.y = (float) (scalar * vec.y);
	result.z = (float) (scalar * vec.z);
	return result;
}

Vector3 Vector_mult(Vector3 vec1, Vector3 vec2) {
	Vector3 result;
	result.x = vec1.x * vec2.x;
	result.y = vec1.y * vec2.y;
	result.z = vec1.z * vec2.z;
	return result;
}

bool Vector_equal(Vector3 vec1, Vector3 vec2) {
	return vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z;
}

float Vector_angle(Vector3 vec1, Vector3 vec2) {
	float cosine, den;
	den = Vector_magnitude(vec1) * Vector_magnitude(vec2);
	if (den == 0.0)
		cosine = 0.0;
	else
		cosine = Vector_dot(vec1, vec2) / den;
	return (acos(cosine)); //compute arc cosine
}
