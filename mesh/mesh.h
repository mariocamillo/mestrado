/*
 *  mesh.h
 *
 *  A half-edge mesh class
 *
 *  author: Wu Shin - Ting (25/05/09; 09/07/09)
 *          DCA - FEEC - Unicamp
 *
 */
#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <cmath>
#include <vector>
#include <deque>
#include <map>
#include <queue>
#include <string.h>
#include "vmtkMatrix.h"
#include <algorithm>
#include "vmtkDefs.h"
#include <set>
#include<float.h>
#include <assert.h>

#ifdef WIN32
using namespace std;
#else
using namespace __gnu_cxx;
#endif

using namespace std;

typedef long VertexID;
typedef long EdgeID;
typedef long LoopID;
typedef LoopID FaceID;
typedef long CycleID;


struct Vertex
{
  VertexID ID;
  void *GeomPtr;
  struct HalfEdge* half;
  Matrix16 q;
};

struct Edge
{
    EdgeID ID;
    void* GeomPtr;
    struct HalfEdge* half;
};

struct Loop
{
    LoopID ID;
    unsigned int type;    /* 1=face (filled loop); 2=cycle (unfilled loop) */
    void* GeomPtr;
    struct HalfEdge* half;
};

struct HalfEdge
{
    HalfEdge* next;
    HalfEdge* previous;
    HalfEdge* mate;

    Vertex* origin;
    Loop* left;
    Edge* edge;
};

struct Store_loop {   /* points */
  int id;
  HalfEdge *half;
};

typedef struct POINT_struct {   /* points */
  double x, y, z;
  double nx, ny, nz;
  unsigned short flag;    //=0, original; =1, modified
} Point3D;

struct eqint
{
  bool operator()(const int s1, const int s2) const
  {
    if (s1 == s2) return 1;
    else return 0;
  }
};

struct node{
	VertexID v0;
	VertexID v1;
	HalfEdge *h;
	double c;
};
// ======================================================================

class Mesh{

  // =====


  // HALFEDGES
  HalfEdge* addHalfEdge_(const VertexID a, const VertexID b);
  HalfEdge* addHalfEdge(const VertexID a, const VertexID b, Loop *outer);
  HalfEdge* getHalfEdge(const VertexID a, const VertexID b);
  HalfEdge* getOuterHalfEdge (const Vertex* v, HalfEdge *av, HalfEdge *vb, const Loop *outer);

  // =====
  // EDGES
  double computeSignedFaceArea(VertexID vA, VertexID vB, VertexID vC);
  void computePlane(VertexID vA, VertexID vB, VertexID vC, double &a, double &b, double &c, double &d);
  bool change_orientation(vector<FaceID> Face, HalfEdge* half);
  bool crossEdge(vector<FaceID> Face, HalfEdge* half);
  bool makeFlip(double ab, double bc, double ca);
  bool equal_sides(double ab, double bc, double ca);
  void flipEdge (HalfEdge *half);
  void buid_edge_relations( HalfEdge *, HalfEdge *, HalfEdge *, HalfEdge *, HalfEdge *, HalfEdge *);
  void obtain_pointer(HalfEdge **, HalfEdge **, HalfEdge **, HalfEdge *, HalfEdge *, HalfEdge *, Loop * );
  // =====
  // VERTICES
  void getFVertices (const FaceID f, vector<Vertex*> *list);
  void getVVertices (const VertexID v, vector<Vertex*> *list);

  // ==============
  // LOOPS
  int _nCycles;
  //Loop *_outer_loop;

  void MergeLoops(Loop *loop1, Loop *loop2);
  void MergeLoops(Loop *loop1, Loop *loop2, HalfEdge *e);
  void SetLoop(Loop *loop, HalfEdge *e1, HalfEdge *e2);


  // ==============
  // FACES
  int buid_face_relations(HalfEdge *, HalfEdge *, HalfEdge *, Loop *, Loop *);

public:
	LoopID face_paint;
	float xmin, xmax, ymin, ymax, zmin, zmax;
	bool mesh_exist;
  // ========================
  // CONSTRUCTOR & DESTRUCTOR
  Mesh();
  virtual ~Mesh();
  Loop *getOuterL (const VertexID a, const VertexID b, const VertexID c);//criada
  Loop *getOuterLoop(const VertexID a, const VertexID b, const VertexID c);
  Loop *_outer_loop;
	//criada
  void clear();
  void load(char *input_file);
	void getV(int ID, Vertex *vt);
	int is_external (deque<Store_loop *> last_loop, HalfEdge *prev_half, HalfEdge *next_half);
  // ========
  // VERTICES
  VertexID addVertex(const VertexID ID, void *ptr);
  int setVGeomPtr (const VertexID a, void * ptr);
  void* getVGeomPtr (const VertexID a);
  void getVVertices (const VertexID v, vector<void *> *list);
  void getVVertices (const VertexID v, vector<VertexID> *list);
  void getVFaces (const VertexID v, vector<FaceID> *list);
  void getVFaces (const VertexID v, vector<void *> *list);
  void getBorderVertices (vector<VertexID> *list);
  void getBorderVertices (vector<void *> *list);

  // =====
  // EDGES
  EdgeID addEdge(const EdgeID ID, VertexID a, VertexID b, void *ptr);
   
  // =========
  // FACES
  void adjust_half(HalfEdge *h);
  void half_out_from_vertice(VertexID a, vector<pair<HalfEdge *, HalfEdge *> >* list);
  void adjust_outer(vector<pair<HalfEdge *, HalfEdge *> > list, vector<pair<HalfEdge *, HalfEdge *> >* link);
  FaceID addTriangle_(LoopID ID, const VertexID a, const VertexID b, const VertexID c, void *ptr);
  bool triangle_inserted(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b, VertexID c);
  bool triangle_ok(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b);
  void outer_halfedge(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, VertexID a, VertexID b, vector<pair<HalfEdge **, HalfEdge *> > *c);
  bool avoid_loop(HalfEdge *h, HalfEdge *hab, HalfEdge *hbc);
  void inner_halfedge(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, vector<pair<HalfEdge **, HalfEdge *> > *c);
  void adjust_inner_half(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca);
  HalfEdge *articulate_vertex(HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca);
  void vertice_half(VertexID a, VertexID b, VertexID c, HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca);
  void loop_half(LoopID &ID,  HalfEdge *hab, HalfEdge *hbc, HalfEdge *hca, void *ptr);
  HalfEdge *outHalf(Vertex *v, Loop *f, int &flag);

  FaceID addTriangle(const LoopID ID, const VertexID a, const VertexID b, const VertexID c, void *ptr);
  int setFGeomPtr (const FaceID f, void * ptr);
  void* getFGeomPtr (const FaceID a);
  HalfEdge *SameFace(HalfEdge *h);
  HalfEdge *findHalf(HalfEdge *half);
  HalfEdge *findHalf(int i, int j);
  VertexID ReduceQEM(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) );
  VertexID ReduceQEM2(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) );

  void decimater_(vector<FaceID> f, int min_f, void (func)(void));
  void decimater(double err, int min_f, void (func)(void));
  void intialize(vector<FaceID> list);
  double evaluate(HalfEdge *hh);
  void update(vector<node *> *f, double err, pair <VertexID, VertexID> p);
  void computeQ(Vertex *v);
  void make_pairs(vector<FaceID> list, vector <node *> *f);
  void checkRateTopology (vector<FaceID> *list);
  bool check_triangle(FaceID id);

  /*openmesh*/
  void QEM(double max_err);
  void initializeQEM();
  void initializeHeap(double max_err, vector<node*> *heap);
  void Heap(double max_err, vector<node*> *heap);
  void updateHeap(double max_err, vector<node*> *heap, int vr,  Vertex *vv, Matrix16 mtx);
  void updateMatrix(VertexID vid);
  bool ok_collapse(HalfEdge *half);
  double collapseCost(VertexID v0, VertexID v1);
  double computeFaceArea(VertexID vA, VertexID vB, VertexID vC, double *n);
  void update_normal();
  //************
  void QEM_(double max_err, void (func)(void), vector<FaceID> fd);
  void initializeQEM_(vector<int> fd);
  //************
  //metrica da qualidade
void metric1area_side(vector<FaceID> *list, double t, double &ee);


  void decimate(double t, void (func)(void), int min);
  void updateCost(VertexID vid, Matrix16 *Qq,vector<pair<double,int> >*c, vector<pair<Point3D*, int> > *vb, vector<pair<VertexID, VertexID> > *lp, double t, map<int, int> hash);
  void computeQ(vector<VertexID> list, Matrix16 *Q, map<int, int> h);
  HalfEdge *findHalf(VertexID vid, HalfEdge *half);
  void validPairs(vector<VertexID> list, double tt, vector<pair<VertexID, VertexID> > *list_p);
  void computeCost(vector<pair<VertexID, VertexID> > list_p, vector< pair<double,int> > *c, vector<pair<Point3D *, int> > *v_b, Matrix16 *q);

  VertexID Reduce(const FaceID ID, const VertexID vID, void *ptr, void (func)(void) );
  VertexID Reduce3(const FaceID ID, const VertexID vID, void *ptr);
  Vertex *half_collapse (HalfEdge *half);
  int Point_Border (HalfEdge *half);
  int eliminate_edge(HalfEdge *half, HalfEdge **half_f);
  int eliminate_face(HalfEdge *half);
  HalfEdge *change_origin(HalfEdge *half, HalfEdge *halfFace);
  HalfEdge *change_origin(HalfEdge *half, Loop *f1, Loop *f2);
  VertexID ReduceTriangleToPointL(const FaceID ID, const VertexID vID, void *ptr);
  Vertex* JoinVertices(HalfEdge *half,int close_ID);
  void ChangeOrigin(HalfEdge **half);
  VertexID ReduceTriangleToPoint(const FaceID f, const VertexID vID, void *ptr);
  void getFAdjacentFacesPerEdge (const FaceID f, vector<FaceID> *list);
  void getFAdjacentFacesPerEdge (const FaceID f, vector<void *> *list);
  void getFOneRingVertices (const FaceID f, vector<VertexID> *list);
  void getFVertices (const FaceID f, vector<void *> *list);
  void getFVertices (const FaceID f, vector<VertexID> *list);

  // ===============
  // OTHER ACCESSORS
  // ========
  // VERTICES
  int numVertices() const { return vertices.size(); }
  void getAllVertices (vector<VertexID>* list);
  signed char isVOnBoundary (const VertexID v);

  // =====
  // EDGES
  int numEdges() const { return edges.size(); }
  EdgeID getEdge (const VertexID a, const VertexID b);

  // =========
  // CYCLES
  int numCycles();
  void getAllCycles (vector<CycleID>* list);
  void getCVertices (const CycleID f, vector<void *> *list);
  void getCVertices (const CycleID f, vector<VertexID> *list);
  bool isOuterLoop (const CycleID ID);
  bool removeDummyCycle (const CycleID ID);

  // =========
  // FACES
  int numFaces();
  void getAllFaces (vector<FaceID>* list);
  FaceID getFace (const VertexID a, const VertexID b);
  int haveFDistinctAdjacentTrianglesPerEdge (const FaceID f);
  bool existFace (const FaceID f);
  void RemoveDegeneratedTriangles ();
  signed char isFOnBoundary (const FaceID f);
  void getAdjFaces (const VertexID a, const VertexID b,
                    vector<FaceID>* list);

  //geometry
  double dist_point(VertexID idA, VertexID idB);

  // =========
  // Miscelaneous
  void removeMesh();
  map<int, Loop*> loops;
  map<int, Loop*>::iterator lIterator;

  //voltar para private
  map<int, Vertex*> vertices;
private:
  // ==============
  // REPRESENTATION
  map<int, Vertex*>::iterator vIterator;
  //  vector<HalfEdge*> halfedges;
  //  vector<HalfEdge*>::iterator hIterator;
  map<int, Edge*> edges;
  map<int, Edge*>::iterator eIterator;



  void remove_vertex (Vertex* v);
  void remove_edge (Edge *edge);
  void remove_loop (Loop *f);
  void ReduceTtoP_caseIII (Loop *f, Loop*f1, Loop *f2, Loop *f3, 
			   HalfEdge *e10, HalfEdge *e20, HalfEdge *e30, 
			   Vertex *va, Vertex *vb,
			   Vertex *vc, Vertex **new_v);
  unsigned remove_inner_structures (HalfEdge *he_ref, Vertex *v); 
  unsigned remove_inner_structures (HalfEdge *he1, HalfEdge *he2, 
				Vertex *v); 
  VertexID replace_vertex (Vertex *old_v, Vertex **new_v, void *ptr, 
			   const VertexID vID);

  /*
  map<int, Vertex*, hash<int>, eqint> vertices;
  map<int, Vertex*, hash<int>, eqint>::iterator vIterator;
  //  vector<HalfEdge*> halfedges;
  //  vector<HalfEdge*>::iterator hIterator;
  map<int, Edge*, hash<int>, eqint> edges;
  map<int, Edge*, hash<int>, eqint>::iterator eIterator;
  map<int, Loop*, hash<int>, eqint> loops;
  map<int, Loop*, hash<int>, eqint>::iterator lIterator;
  */
};

// ======================================================================
// ======================================================================

#endif




