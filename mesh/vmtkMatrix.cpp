/*
 Copyright (C) 2008 Alex Diener

 */

#include <cstdio>
#include <cmath>
#include "vmtkMatrix.h"
#include "vmtkDefs.h"

#define degreesToRadians(degrees) (((degrees) * M_PI) / 180.0f)
#define radiansToDegrees(radians) (((radians) * 180.0f) / M_PI)

void Matrix_loadIdentity(Matrix16 * matrix) {
	matrix->m[0] = 1.0;
	matrix->m[1] = 0.0;
	matrix->m[2] = 0.0;
	matrix->m[3] = 0.0;
	matrix->m[4] = 0.0;
	matrix->m[5] = 1.0;
	matrix->m[6] = 0.0;
	matrix->m[7] = 0.0;
	matrix->m[8] = 0.0;
	matrix->m[9] = 0.0;
	matrix->m[10] = 1.0;
	matrix->m[11] = 0.0;
	matrix->m[12] = 0.0;
	matrix->m[13] = 0.0;
	matrix->m[14] = 0.0;
	matrix->m[15] = 1.0;
}

Matrix16 Matrix_identity() {
	Matrix16 matrix;

	Matrix_loadIdentity(&matrix);
	return matrix;
}

Matrix16 Matrix_Copy(Matrix16 src) {
	Matrix16 dst;
	for (int var = 0; var < 16; var++) {
		dst.m[var] = src.m[var];
	}
	return dst;
}

Matrix16 Matrix_withValues(float m0, float m4, float m8, float m12, float m1,
		float m5, float m9, float m13, float m2, float m6, float m10, float m14,
		float m3, float m7, float m11, float m15) {
	Matrix16 matrix;

	matrix.m[0] = m0;
	matrix.m[1] = m1;
	matrix.m[2] = m2;
	matrix.m[3] = m3;
	matrix.m[4] = m4;
	matrix.m[5] = m5;
	matrix.m[6] = m6;
	matrix.m[7] = m7;
	matrix.m[8] = m8;
	matrix.m[9] = m9;
	matrix.m[10] = m10;
	matrix.m[11] = m11;
	matrix.m[12] = m12;
	matrix.m[13] = m13;
	matrix.m[14] = m14;
	matrix.m[15] = m15;
	return matrix;
}

Matrix16 Matrix_fromDirectionVectors(Vector3 right, Vector3 up, Vector3 front) {
	Matrix16 matrix;

	Matrix_loadIdentity(&matrix);
	matrix.m[0] = right.x;
	matrix.m[1] = right.y;
	matrix.m[2] = right.z;
	matrix.m[4] = up.x;
	matrix.m[5] = up.y;
	matrix.m[6] = up.z;
	matrix.m[8] = front.x;
	matrix.m[9] = front.y;
	matrix.m[10] = front.z;
	return matrix;
}

void Matrix_multiply(Matrix16 * matrix1, Matrix16 m2) {
	Matrix16 m1, result;

	m1 = *matrix1;

	result.m[0] = m1.m[0] * m2.m[0] + m1.m[4] * m2.m[1] + m1.m[8] * m2.m[2]
			+ m1.m[12] * m2.m[3];
	result.m[1] = m1.m[1] * m2.m[0] + m1.m[5] * m2.m[1] + m1.m[9] * m2.m[2]
			+ m1.m[13] * m2.m[3];
	result.m[2] = m1.m[2] * m2.m[0] + m1.m[6] * m2.m[1] + m1.m[10] * m2.m[2]
			+ m1.m[14] * m2.m[3];
	result.m[3] = m1.m[3] * m2.m[0] + m1.m[7] * m2.m[1] + m1.m[11] * m2.m[2]
			+ m1.m[15] * m2.m[3];
	result.m[4] = m1.m[0] * m2.m[4] + m1.m[4] * m2.m[5] + m1.m[8] * m2.m[6]
			+ m1.m[12] * m2.m[7];
	result.m[5] = m1.m[1] * m2.m[4] + m1.m[5] * m2.m[5] + m1.m[9] * m2.m[6]
			+ m1.m[13] * m2.m[7];
	result.m[6] = m1.m[2] * m2.m[4] + m1.m[6] * m2.m[5] + m1.m[10] * m2.m[6]
			+ m1.m[14] * m2.m[7];
	result.m[7] = m1.m[3] * m2.m[4] + m1.m[7] * m2.m[5] + m1.m[11] * m2.m[6]
			+ m1.m[15] * m2.m[7];
	result.m[8] = m1.m[0] * m2.m[8] + m1.m[4] * m2.m[9] + m1.m[8] * m2.m[10]
			+ m1.m[12] * m2.m[11];
	result.m[9] = m1.m[1] * m2.m[8] + m1.m[5] * m2.m[9] + m1.m[9] * m2.m[10]
			+ m1.m[13] * m2.m[11];
	result.m[10] = m1.m[2] * m2.m[8] + m1.m[6] * m2.m[9] + m1.m[10] * m2.m[10]
			+ m1.m[14] * m2.m[11];
	result.m[11] = m1.m[3] * m2.m[8] + m1.m[7] * m2.m[9] + m1.m[11] * m2.m[10]
			+ m1.m[15] * m2.m[11];
	result.m[12] = m1.m[0] * m2.m[12] + m1.m[4] * m2.m[13] + m1.m[8] * m2.m[14]
			+ m1.m[12] * m2.m[15];
	result.m[13] = m1.m[1] * m2.m[12] + m1.m[5] * m2.m[13] + m1.m[9] * m2.m[14]
			+ m1.m[13] * m2.m[15];
	result.m[14] = m1.m[2] * m2.m[12] + m1.m[6] * m2.m[13] + m1.m[10] * m2.m[14]
			+ m1.m[14] * m2.m[15];
	result.m[15] = m1.m[3] * m2.m[12] + m1.m[7] * m2.m[13] + m1.m[11] * m2.m[14]
			+ m1.m[15] * m2.m[15];

	*matrix1 = result;
}

Matrix16 Matrix_multiplied(Matrix16 matrix1, Matrix16 matrix2) {
	Matrix_multiply(&matrix1, matrix2);
	return matrix1;
}

void Matrix_translate(Matrix16 * matrix, float x, float y, float z) {
	Matrix16 translationMatrix;

	Matrix_loadIdentity(&translationMatrix);
	translationMatrix.m[12] = x;
	translationMatrix.m[13] = y;
	translationMatrix.m[14] = z;
	Matrix_multiply(matrix, translationMatrix);
}

Matrix16 Matrix_translated(Matrix16 matrix, float x, float y, float z) {
	Matrix_translate(&matrix, x, y, z);
	return matrix;
}

void Matrix_scale(Matrix16 * matrix, float x, float y, float z) {
	Matrix16 scalingMatrix;

	Matrix_loadIdentity(&scalingMatrix);
	scalingMatrix.m[0] = x;
	scalingMatrix.m[5] = y;
	scalingMatrix.m[10] = z;
	Matrix_multiply(matrix, scalingMatrix);
}

Matrix16 Matrix_scaled(Matrix16 matrix, float x, float y, float z) {
	Matrix_scale(&matrix, x, y, z);
	return matrix;
}

//when you need, please replace with <Trackball>

//void Matrix_rotate(Matrix16 * matrix, Vector3 axis, float angle) {
//	Matrix16 rotationMatrix;
//	Quaternion quaternion;
//
//	quaternion = Quaternion_fromAxisAngle(axis, angle);
//	rotationMatrix = Quaternion_toMatrix(quaternion);
//	Matrix_multiply(matrix, rotationMatrix);
//}
//
//Matrix16 Matrix_rotated(Matrix16 matrix, Vector3 axis, float angle) {
//	Matrix_rotate(&matrix, axis, angle);
//	return matrix;
//}

void Matrix_shearX(Matrix16 * matrix, float y, float z) {
	Matrix16 shearingMatrix;

	Matrix_loadIdentity(&shearingMatrix);
	shearingMatrix.m[1] = y;
	shearingMatrix.m[2] = z;
	Matrix_multiply(matrix, shearingMatrix);
}

Matrix16 Matrix_shearedX(Matrix16 matrix, float y, float z) {
	Matrix_shearX(&matrix, y, z);
	return matrix;
}

void Matrix_shearY(Matrix16 * matrix, float x, float z) {
	Matrix16 shearingMatrix;

	Matrix_loadIdentity(&shearingMatrix);
	shearingMatrix.m[4] = x;
	shearingMatrix.m[6] = z;
	Matrix_multiply(matrix, shearingMatrix);
}

Matrix16 Matrix_shearedY(Matrix16 matrix, float x, float z) {
	Matrix_shearY(&matrix, x, z);
	return matrix;
}

void Matrix_shearZ(Matrix16 * matrix, float x, float y) {
	Matrix16 shearingMatrix;

	Matrix_loadIdentity(&shearingMatrix);
	shearingMatrix.m[8] = x;
	shearingMatrix.m[9] = y;
	Matrix_multiply(matrix, shearingMatrix);
}

Matrix16 Matrix_shearedZ(Matrix16 matrix, float x, float y) {
	Matrix_shearZ(&matrix, x, y);
	return matrix;
}

void Matrix_applyPerspective(Matrix16 * matrix, float fovY, float aspect,
		float zNear, float zFar) {
	Matrix16 perspectiveMatrix;
	float sine, cotangent, deltaZ;

	fovY = (degreesToRadians(fovY) / 2.0f);
	deltaZ = (zFar - zNear);
	sine = sin(fovY);
	if (deltaZ == 0.0f || sine == 0.0f || aspect == 0.0f) {
		return;
	}
	cotangent = (cos(fovY) / sine);

	Matrix_loadIdentity(&perspectiveMatrix);
	perspectiveMatrix.m[0] = (cotangent / aspect);
	perspectiveMatrix.m[5] = cotangent;
	perspectiveMatrix.m[10] = (-(zFar + zNear) / deltaZ);
	perspectiveMatrix.m[11] = -1.0f;
	perspectiveMatrix.m[14] = ((-2.0f * zNear * zFar) / deltaZ);
	perspectiveMatrix.m[15] = 0.0f;
	Matrix_multiply(matrix, perspectiveMatrix);
}

Matrix16 Matrix_perspective(Matrix16 matrix, float fovY, float aspect,
		float zNear, float zFar) {
	Matrix_applyPerspective(&matrix, fovY, aspect, zNear, zFar);
	return matrix;
}

void Matrix_transpose(Matrix16 *matrix) {
	*matrix = Matrix_withValues(matrix->m[0], matrix->m[1], matrix->m[2],
			matrix->m[3], matrix->m[4], matrix->m[5], matrix->m[6],
			matrix->m[7], matrix->m[8], matrix->m[9], matrix->m[10],
			matrix->m[11], matrix->m[12], matrix->m[13], matrix->m[14],
			matrix->m[15]);
}

Matrix16 Matrix_transposed(Matrix16 matrix) {
	Matrix_transpose(&matrix);
	return matrix;
}

static float Matrix_subdeterminant(Matrix16 matrix, int excludeIndex) {
	int index4x4, index3x3;
	float matrix3x3[9];

	index3x3 = 0;
	for (index4x4 = 0; index4x4 < 16; index4x4++) {
		if (index4x4 / 4 == excludeIndex / 4
				|| index4x4 % 4 == excludeIndex % 4) {
			continue;
		}
		matrix3x3[index3x3++] = matrix.m[index4x4];
	}

	return matrix3x3[0]
			* (matrix3x3[4] * matrix3x3[8] - matrix3x3[5] * matrix3x3[7])
			- matrix3x3[3]
					* (matrix3x3[1] * matrix3x3[8] - matrix3x3[2] * matrix3x3[7])
			+ matrix3x3[6]
					* (matrix3x3[1] * matrix3x3[5] - matrix3x3[2] * matrix3x3[4]);
}

float Matrix_determinant(Matrix16 matrix) {
	float subdeterminant0, subdeterminant1, subdeterminant2, subdeterminant3;

	subdeterminant0 = Matrix_subdeterminant(matrix, 0);
	subdeterminant1 = Matrix_subdeterminant(matrix, 4);
	subdeterminant2 = Matrix_subdeterminant(matrix, 8);
	subdeterminant3 = Matrix_subdeterminant(matrix, 12);

	return matrix.m[0] * subdeterminant0 + matrix.m[4] * -subdeterminant1
			+ matrix.m[8] * subdeterminant2 + matrix.m[12] * -subdeterminant3;
}

float Matrix_determinant(float a[3][3]){

	return ((a[0][0]*a[1][1]*a[2][2]+a[0][1]*a[1][2]*a[2][0]+a[0][2]*a[2][1]*a[1][0])
		  -(a[0][2]*a[1][1]*a[2][0]+a[0][1]*a[1][0]*a[2][2]+a[0][0]*a[2][1]*a[1][2]));
}

Matrix16 Matrix_sum(Matrix16 matrixA, Matrix16 matrixB){
	Matrix16 matrix;

	for (int i = 0; i < 16; i++){
		matrix.m[i] = matrixA.m[i] + matrixB.m[i];
	}
	return matrix;
}

void Matrix_invert(Matrix16 *matrix) {
	float determinant;
	Matrix16 result;
	int index, indexTransposed;
	int sign;

	determinant = Matrix_determinant(*matrix);
	for (index = 0; index < 16; index++) {
		sign = 1 - (((index % 4) + (index / 4)) % 2) * 2;
		indexTransposed = (index % 4) * 4 + index / 4;
		result.m[indexTransposed] = Matrix_subdeterminant(*matrix, index) * sign
				/ determinant;
	}

	*matrix = result;
}

Matrix16 Matrix_inverted(Matrix16 matrix) {
	Matrix_invert(&matrix);
	return matrix;
}

Vector3 Matrix_multiplyVector(Matrix16 matrix, Vector3 vector) {
	Vector3 result; //w=0, in coordinate homogenius
	result.x = ((matrix.m[0] * vector.x) + (matrix.m[4] * vector.y)
			+ (matrix.m[8] * vector.z));
	result.y = ((matrix.m[1] * vector.x) + (matrix.m[5] * vector.y)
			+ (matrix.m[9] * vector.z));
	result.z = ((matrix.m[2] * vector.x) + (matrix.m[6] * vector.y)
			+ (matrix.m[10] * vector.z));
	return result;
}

Matrix16 Matrix_ArrayToMatrix(double array[16]) {
	Matrix16 result;
	int var;
	for (var = 0; var < 16; var++) {
		result.m[var] = (float) array[var];
	}
	return result;
}

void Matrix_print(char * msg, Matrix16 matrix) {
	printf("\n%s:\n", msg);
	for (int i = 0; i < 4; i++) { //column
		for (int j = 0; j < 4; j++) { //row
			printf(" %0.2f", matrix.m[i + 4 * j]);
		}
		printf("\n");
	}
}

