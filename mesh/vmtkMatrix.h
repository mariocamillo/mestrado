/*
 * vmtkMatrix.h
 *
 *  Created on: 07/04/2011
 *      Author: ra100300
 */

#ifndef VMTKMATRIX_H_
#define VMTKMATRIX_H_

#include "vmtkVector.h"

typedef struct _Matrix16 {
	float m[16];
} Matrix16;

void Matrix_loadIdentity(Matrix16 * matrix1);
Matrix16 Matrix_identity();

Matrix16 Matrix_Copy(Matrix16 src);

Matrix16 Matrix_withValues(float m0,  float m4,  float m8,  float m12,
                         float m1,  float m5,  float m9,  float m13,
                         float m2,  float m6,  float m10, float m14,
                         float m3,  float m7,  float m11, float m15);

Matrix16 Matrix_fromDirectionVectors( Vector3 right, Vector3 up, Vector3 front);

void Matrix_multiply(Matrix16 *matrix1, Matrix16 matrix2);
Matrix16 Matrix_multiplied(Matrix16 matrix1, Matrix16 matrix2);

void Matrix_translate(Matrix16 *matrix1, float x, float y, float z);
Matrix16 Matrix_translated(Matrix16 matrix1, float x, float y, float z);

void Matrix_scale(Matrix16 *matrix, float x, float y, float z);
Matrix16 Matrix_scaled(Matrix16 matrix, float x, float y, float z);

void Matrix_rotate(Matrix16 *matrix, Vector3 axis, float angle);
Matrix16 Matrix_rotated(Matrix16 matrix, Vector3 axis, float angle);

void Matrix_shearX(Matrix16 *matrix, float y, float z);
Matrix16 Matrix_shearedX(Matrix16 matrix, float y, float z);

void Matrix_shearY(Matrix16 *matrix, float x, float z);
Matrix16 Matrix_shearedY(Matrix16 matrix, float x, float z);

void Matrix_shearZ(Matrix16 *matrix, float x, float y);
Matrix16 Matrix_shearedZ(Matrix16 matrix, float x, float y);

void Matrix_applyPerspective(Matrix16 *matrix, float fovY, float aspect, float zNear, float zFar);
Matrix16 Matrix_perspective(Matrix16 matrix, float fovY, float aspect, float zNear, float zFar);

void Matrix_transpose(Matrix16 * matrix);
Matrix16 Matrix_transposed(Matrix16 matrix);

float Matrix_determinant(Matrix16 matrix);

float Matrix_determinant(float a[3][3]);

Matrix16 Matrix_sum(Matrix16 matrixA, Matrix16 matrixB);

void Matrix_invert(Matrix16 * matrix);
Matrix16 Matrix_inverted(Matrix16 matrix);

Vector3 Matrix_multiplyVector(Matrix16 matrix, Vector3 vector);

Matrix16 Matrix_ArrayToMatrix(double array[16]);

void Matrix_print(char * msg, Matrix16 matrix);

#endif /* VMTKMATRIX_H_ */
