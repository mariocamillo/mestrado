/** \defgroup vmtkPackage VMTK
 *
 * \brief
 *
 * \note 
 *
 * \author $Wu Shin-Ting$
 *
 * \version
 *
 * \date $Date: 2010/06/13 $
 *
 * $$
 *
 */

#ifndef _vmtkDefs_h
#define _vmtkDefs_h

//C/C++ Libraries
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <cmath>
#include <algorithm>
#include <locale>

#ifdef near
#undef near
#endif

#ifdef far
#undef far
#endif

using namespace std;

#define ROW 0
#define COLUMN 1
#define SLICE 2

//Applications definitions
#define EPS 1.e-20

#ifndef SQR
#define SQR(a) ((a) * (a))
#endif
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif

//type of data
typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned int UINT32;
typedef float FLOAT32;
typedef double FLOAT64;

//Projections parameters of Transfer Functions Frames
typedef struct _ProjParamsTransFunc {
	float left;
	float right;
	float bottom;
	float top;
} ProjParamsTransFunc;


typedef enum {
	DATRAW_UCHAR = 0,
	DATRAW_USHORT,
	DATRAW_UINT,
	DATRAW_FLOAT,
	DATRAW_DOUBLE,
	DATRAW_UNKNOWN
} DataType;

#ifndef M_PI
#define M_PI 3.14159265
#endif

#endif /* _vmtkDefs_h */
